
<div class="x_panel">
    <?php $form = yii\widgets\ActiveForm::begin([
                'id' => 'changefonctionform'
    ]);?>

     <?= $form->field($appartenir, 'fonction')->textInput(['maxlength' => 100]) ?>

    <div class="form-group">
        <?= \yii\bootstrap\Html::submitButton('Mettre à jour', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php yii\widgets\ActiveForm::end(); ?>
</div>
