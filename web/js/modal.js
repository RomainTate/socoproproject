$(function () {
    $('button[name=modalButton]').click(function () {

        $('#modal').modal('show')
                .find('#modalContent')
                .load($(this).attr('value'));
    });
    $('a[name=modalButton]').click(function () {

        $('#modal').modal('show')
                .find('#modalContent')
                .load($(this).attr('value'));
    });

    $('#modal').on('shown.bs.modal', function () {
        $(document).off('ajaxStart');
    });

    $('#modal').on('hidden.bs.modal', function () {
        $(document).on({ajaxStart: function () {
            $('#modalContent').append("<div class=\"loader\"> </div>");
        }});
        $('#modalContent').empty();
    });
});

$(document).on({
    ajaxStart: function () {
        $('#modalContent').append("<div class=\"loader\"> </div>");
    }
});