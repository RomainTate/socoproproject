<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */
$this->title = 'Administration : ' . Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
$user_id = Yii::$app->request->get('id');
?>

<div class="x_panel">
    <div class="x_title">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <ul id="w0" class="nav-tabs nav" style="margin-bottom: 15px">
        <li class="active"><a href="<?= \yii\helpers\Url::home()?>user/admin/index">Utilisateurs</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>rbac/role/index">Rôles</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>champsect/">Champs sectoriels</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexdoublon">Gestion doublons</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexnewsletters">Newsletters</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>suivi">Tous les Suivis</a></li>
        <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foires <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireprod">Prod</a></li>
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireconso">Conso</a></li>
            </ul>
        </li>
    </ul>

    <?php if (Yii::$app->session->hasFlash('success')): ?>
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h5><i class="icon fa fa-check"></i>Sauvegardé!</h5>
            <?= Yii::$app->session->getFlash('success') ?>
        </div>
    <?php endif; ?>

    <div class="admin-indexform">

        <?php
        $form = ActiveForm::begin(['action' => \yii\helpers\Url::home().'user/admin/index', 'method' => 'get']);
//        $users = ArrayHelper::map($dataProvider->getModels(), 'id', 'username');
        $users = ArrayHelper::map(\app\models\User::find()->where(['not', ['username' => ['romain', 'foire']]])->all(), 'id', 'username');
        //unset admin for the dropDownList to disable changes on admin user
        ?>

        <label>Selectionner un utilisateur</label> <?= Html::dropDownList('id', ($user_id) ? $user_id : null, $users, ($user_id) ?
                ['onchange' => 'this.form.submit()','class' => 'form-control js-multiple'] : ['prompt' => 'Selectionner un utilisateur', 'onchange' => 'this.form.submit()','class' => 'js-multiple form-control'])
        ?>

        <?php ActiveForm::end(); ?>

        <?php if (!is_null($user_id)) : ?>
            <div>
                <?php $role = app\modules\auth\models\AuthAssignment::findOne(['user_id' => $user_id]); ?>

                <?php ActiveForm::begin(['action' => 'updaterole?id=' . $user_id]) ?>
                <h3> <?php echo is_object($role) ? "Rôle : " . $role->item_name : "Rôle : non défini"; ?> </h3>
                <label>Changer le rôle</label>
                <?= Html::dropDownList('role', is_object($role) ? $role->item_name : null, $authRoles, is_object($role) ?['class' => 'form-control']:['class' => 'form-control', 'prompt' => 'Selectionner un rôle']) ?>

                <div class="form-group">
                    <div id="sectorsLink" style="display: none;">
                        <h3>Secteurs</h3>
                        <div>Tout Selectionner
                            <input id='selectall' type="checkbox">
                        </div>

                        <?php echo Html::checkboxList('gerer', $gerer, $secteurs,['label' => 'checkbox-inline'] ) ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <?= Html::submitButton('Modifier les acces de l\'utilisateur', ['class' => 'btn btn-primary', 'id' => 'submitBtn']) ?>
                    <?= Html::a('Supprimer l\'utilisateur', ['delete', 'id' => $user_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ]])?> 
                </div>
                <?php ActiveForm::end() ?>
 
            </div>       
            <div>
                <?php // echo $this->render('@app/views/gerer/_form', ['user_id' => $user_id]);  ?>
            </div>
        <?php endif; ?>
            <p style="text-align: right; margin-top: 10px">
                <?php if (!is_null($user_id)) : ?>
                <?= Html::a('Modifier l\'utilisateur', ['update', 'id' => $user_id], ['class' => 'btn btn-info']) ?>
                <?php endif;?>
                <?= Html::a('Ajouter un nouvel utilisateur', ['create'], ['class' => 'btn btn-success']) ?>
            </p>
    </div>
</div>
<?php $this->registerJsFile(\yii\helpers\Url::home().'js/adminIndex.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');
