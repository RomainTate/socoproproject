<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\auth\models\AuthItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Nom du rôle') ?>

    <?= $form->field($model, 'type')->hiddenInput(['value' => 1])->label(FALSE) ?>
    
        <fieldset style="margin: 10px 0">
                <?php // echo  Html::checkboxList('name', null, $authItems, ['class' => 'roleCapabilities']) ?>
        <div>
        Commentaires :
        <?= Html::checkboxList('children', null, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'comments'])->all(),'name', 'description'))?>
    </div>
    <div>
        Consommateurs :
        <?= Html::checkboxList('children', null, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'conso'])->all(),'name', 'description'))?>
    </div>
    <div>
        Contacts divers :
        <?= Html::checkboxList('children', null, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'divers'])->all(),'name', 'description'))?>
    </div>
    <div>
        Journalistes et médias :
        <?= Html::checkboxList('children', null, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'journaliste'])->all(),'name', 'description'))?>
    </div>
    <div>
        Producteurs :
        <?= Html::checkboxList('children', null, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'prod'])->all(),'name', 'description'))?>
    </div>
    <div>
        Entreprises-Associations :
        <?= Html::checkboxList('children', null, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'entreprise'])->all(),'name', 'description'))?>
    </div>
    </fieldset>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
