<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Media */

$this->title = $model->media_titre;
$this->params['breadcrumbs'][] = ['label' => 'Médias', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journal-view">
    <h1 class="pull-left">Profil de  <?= $model->media_titre ?></h1>
    <div class="pull-right">
        <?php
        if (Yii::$app->user->can('deleteJournaliste'))
            echo Html::a('Supprimer le média', ['delete', 'id' => $model->media_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Êtes-vous sûr de vouloir supprimer ce média?' . ($model->journalistes? ' Attention : des journalistes font partie de ce média, ils seront donc considérés sans média associé':''),
                    'method' => 'post',
                ],
            ])
            ?>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-lg-5 col-xs-12 profile_left">
            <div class="x_panel" >
                <div class="x_content">
                    <div class="img-profil">
                        <img src="../css/images/television.png" alt="producteur" />
                    </div>

                    <h3> <?= $model->media_titre ?> </h3>
                    <div class="mainInfos">


                        <p> <i class="fa fa-legal" aria-hidden="true"></i>
                            <?php if ($model->media_societe) : ?>
                                Société : <?= $model->media_societe ?> 
                            <?php else : ?>
                                Société inconnue
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/quickupdate', 'id' => $model->media_id, 'attribute' => 'media_societe']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p>
                        
                        <p> <i class="fa fa-newspaper-o" aria-hidden="true"></i>
                                <?= $model->media_titre ?> 
                            <?php if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/quickupdate', 'id' => $model->media_id, 'attribute' => 'media_titre']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p>
                        <p> <i class="fa fa-flag" aria-hidden="true"></i>
                                <?php if ($model->media_langue) : ?>
                                Langue : <?= $model->media_langue ?> 
                            <?php else : ?>
                                Langue inconnue
                                <?php endif;?>
                            <?php if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/quickupdate', 'id' => $model->media_id, 'attribute' => 'media_langue']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p>

                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->media_telephone) : ?> 
                                <?= $model->media_telephone ?>
                            <?php else : ?>
                                Numéro de téléphone inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/quickupdate', 'id' => $model->media_id, 'attribute' => 'media_telephone']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                            <?php if ($model->media_email) : ?>
                                <?= $model->media_email ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/quickupdate', 'id' => $model->media_id, 'attribute' => 'media_email']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>

                        <p> <i class="fa fa-globe" aria-hidden="true"></i> 
                            <?php if ($model->media_site_internet) : ?>
                                <?= $model->media_site_internet ?> 
                            <?php else : ?>
                                Site internet inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/quickupdate', 'id' => $model->media_id, 'attribute' => 'media_site_internet']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p>
                        <h3>Adresse</h3>
                                <?php if($model->adresse) : ?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?php if($model->adresse->adr_rue != null && $model->adresse->adr_rue != '' ):?>
                                            <?= $model->adresse->adr_rue ?>
                                        <?php else : ?>
                                            <?= 'Numéro et Rue inconnus'?>
                                        <?php endif; ?>
                                        <?php if (Yii::$app->user->can('createJournaliste')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id, 'relatedModelClass' => 'app\models\Media', 'relatedModelId' => $model->media_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                    </p>

                                    <?php if ($model->adresse->adr_code_postal != null && $model->adresse->adr_code_postal != '') :?>
                                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                            <?= 'Code Postal: '. $model->adresse->adr_code_postal?>
                                            <?php if (Yii::$app->user->can('createJournaliste')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id, 'relatedModelClass' => 'app\models\Media', 'relatedModelId' => $model->media_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                        </p>
                                    <?php endif?>

                                    <?php if ($model->adresse->adr_ville != null && $model->adresse->adr_ville != '') :?>
                                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                            <?= 'Localite: '. $model->adresse->adr_ville?>
                                            <?php if (Yii::$app->user->can('createJournaliste')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id, 'relatedModelClass' => 'app\models\Media', 'relatedModelId' => $model->media_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                        </p>
                                    <?php endif;?>

                                    <?php if ($model->adresse->adr_province != null && $model->adresse->adr_province != '') :?>
                                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                            <?= 'Province: '. $model->adresse->adr_province?>
                                            <?php if (Yii::$app->user->can('createJournaliste')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id, 'relatedModelClass' => 'app\models\Media', 'relatedModelId' => $model->media_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                        </p>
                                    <?php endif;?>
                                
                                        
                            <?php else :?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                    <?= 'Adresse inconnue';?>
                                    <?php if (Yii::$app->user->can('createJournaliste')){
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/addadresse','id' => $model->media_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                    }?>
                                </p>
                            <?php endif;?>
                                        
                            <?php if ($model->media_pays) :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Pays: '. $model->media_pays?>
                                        <?php if (Yii::$app->user->can('createJournaliste')) {
                                            if ($model->adresse) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id, 'relatedModelClass' => 'app\models\Media', 'relatedModelId' => $model->media_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            } else {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/addadresse','id' => $model->media_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }
                                        }?>
                                    </p>
                            <?php else : ?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        Pays inconnu
                                        <?php if (Yii::$app->user->can('createJournaliste')) {
                                            if ($model->adresse) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id, 'relatedModelClass' => 'app\models\Media', 'relatedModelId' => $model->media_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            } else {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/media/addadresse','id' => $model->media_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }
                                        }?>
                                    </p>
                            <?php endif;?>
                    </div>
                    <?php
                    if (Yii::$app->user->can('createJournaliste'))
                        echo '<div class="text-right">' .\yii\bootstrap\Html::button('Modifier les informations du journal', ['value' => \yii\helpers\Url::to(['/media/update', 'id' => $model->media_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]). '</div>'?>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-xs-12">
            <div class="x_panel secondInfos">
                <div class="x_content">
                    <h2> Autres informations </h2> 

                    <ul class="autres-infos">
                        <li>
                            Type de presse : <?php echo $model->media_type_presse == null ? 'inconnue' : $model->media_type_presse ?>
                        </li>
                        <li>
                            Type de support : <?php echo $model->media_type_support == null ? 'inconnue' : $model->media_type_support ?>
                        </li>
                        <li>
                            Périodicité : <?php echo $model->media_frequence == null ? 'inconnue' : $model->media_frequence ?>
                        </li>
                        <li>
                            Audience : <?php echo $model->media_audience == null ? 'inconnue' : $model->media_audience ?>
                        </li>
                    </ul>
                    
                    <?php
                    if (Yii::$app->user->can('createJournaliste'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/media/addmoreinfos', 'id' => $model->media_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                    <hr />
                    <h2>
                        Journalistes associés :
                    </h2>
                    <?php
                    if (empty($model->journalistes)) : echo 'Aucun Journaliste associé à ce média';
                    else :?>
                    <?= yii\bootstrap\Html::a('Voir tous les journalistes de '.$model->media_titre , yii\helpers\Url::to(['/journaliste' , 'JournalisteSearch[nommedia]' => $model->media_titre])) ?>
                    <table class="table table-bordered">
                        <th>
                            Nom et Prenom
                        </th>
                        <th>
                            Fonction
                        </th>
                        <th>
                            Email employé
                        </th>
                        <th>
                            GSM employé
                        </th>
                        <th>
                            Actions
                        </th>
                        <?php foreach ($model->journalistes as $journaliste) : ?>
                        <tr>
                            <td>
                                <a class="text-primary" href="<?php echo\yii\helpers\Url::home()?>journaliste/view?id=<?= $journaliste->cont_id ?>"> <?= $journaliste->contactCivilite . ' ' . $journaliste->contactNom . ' ' . $journaliste->contactPrenom  ?> </a>
                            </td>
                            <td>
                                <?= $journaliste->journaliste_fonction?>
                            </td>
                            <td>
                                <?= $journaliste->contact->cont_email_principal ?>
                            </td>
                            <td>
                                <?= $journaliste->contact->cont_gsm_principal ?>
                            </td>
                            <td>
                        <?= Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>',  \Yii::$app->urlManager->createUrl(['journaliste/view', 'id' => $journaliste->cont_id]));?>
                        <?php if (Yii::$app->user->can('createJournaliste'))?>
                                <?= Html::a('<span class="glyphicon glyphicon-pencil "></span>',  \Yii::$app->urlManager->createUrl(['journaliste/update', 'id' => $journaliste->cont_id]));?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                    
                    <?php endif;
                    ?>

                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php
                    echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
                        'entity' => (string) 'media-' . $model->media_id, // type and id
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home().'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>