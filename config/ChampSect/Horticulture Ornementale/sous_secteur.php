<?php

    return [
    'label' => 'Sous Secteur',
    'type' => 5,
    'value' =>[ 'Floriculture', 'Pépinière', 'Sapin de Noel', 'Jardinerie', 'Entrepreneur de jardin', 'Fleuriste']
    ];