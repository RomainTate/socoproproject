<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entreprise */
/* @var $form yii\widgets\ActiveForm */
if (!isset($model)) { 
    $model = new app\models\Entreprise();
    $action = '/entreprise/create';
} else {
    $action = '';
}
?>

<div class="entreprise-form">

    <?php $form = ActiveForm::begin([
        'action' => $action,
        'id' => 'entrepriseForm'
            ]); ?>
    
    <?php if (isset($contactId)) echo \yii\bootstrap\Html::hiddenInput('contactToAdd', $contactId);?>
    
    <?= $form->field($model, 'entr_societe')->textInput(['maxlength' => true]) ?>




    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
