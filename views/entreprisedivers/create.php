<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Entreprisedivers */

$this->title = 'Ajouter une Entreprise diverse';
$this->params['breadcrumbs'][] = ['label' => 'Entreprisedivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entreprisedivers-create">

    <h1><?= Html::encode($this->title) ?></h1>
        <?= $this->render('_form', [
            'model' => $model,
            'entrepriseModel' => $entrepriseModel
        ]) ?>
</div>
