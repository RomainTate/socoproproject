<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Consommateur */
if($model->needConfirm == 2 && Yii::$app->user->getUser()->getIsAdmin()) :
    $needConfirm = true;
else :
    $needConfirm = false;
endif;
$this->title = $model->contact->cont_nom . ' ' . $model->contact->cont_prenom;
if($needConfirm):
    $this->params['breadcrumbs'][] = ['label' => 'Admin : gestion foire', 'url' => ['user/admin/indexfoireconso']];
else:
    $this->params['breadcrumbs'][] = ['label' => 'Consommateurs', 'url' => ['index']];
endif;

$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consommateur-view">
    <h1 class="pull-left">Profil de <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?></h1>
    <div class="pull-right">
        <?php
        if (Yii::$app->user->can('deleteConso'))
                $dataconfirm = 'Êtes-vous sûr de vouloir supprimer ce ';
            if ($needConfirm) {
                $dataconfirm .= 'consommateur en attente';
                $pageToRedirect = 'foire';
            } else {
                $dataconfirm .= 'contact ?';
                $pageToRedirect = '';
            }
            echo Html::a('Supprimer le contact', ['delete', 'id' => $model->cont_id, 'pageToRedirect' => $pageToRedirect], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => $dataconfirm,
                    'method' => 'post',
                ],
            ])
            ?>
    </div> 
    <div class="clearfix"></div>
        <?php if($needConfirm) :?>
    <div class="x_panel" style="background-color: #fffad6">
        <h2 class="x_title"> Remarques : </h2>
        <?php
        $remarques = [];
        $model->validate();
        foreach((array)$model->getErrors() as $modelErrors) {
            foreach ($modelErrors as $modelError) {
                $remarques[] = $modelError;
            }
        }
        $model->contact->validate();
        foreach((array)$model->contact->getErrors() as $modelErrors) {
            foreach ($modelErrors as $modelError) {
                $remarques[] = $modelError;
            }
        }
        if(empty($remarques)) :?>
            Aucun problème détecté, ce Consommateur peut être validé
            <div class="pull-right">
                <?=Html::a('Valider', yii\helpers\Url::to(['/consommateur/accept', 'id' => $model->cont_id]), ['class' => 'btn btn-warning']) ?>
            </div>
        <?php else:?>
            <ul>
            <?php foreach ($remarques as $remarque) :?>
                <li> -<?=$remarque?></li>
            <?php endforeach;?>
            </ul>
        <?php endif; 
            if(!empty($otherConsos)) :?>
        <h2 class="x_title"> Attention : </h2>
            <h3>&#9888; <?= count($otherConsos)?> contact(s) existe(nt) déjà avec les mêmes informations :</h3>
            <ul class="list-group">
            <?php foreach ($otherConsos as $otherConso) :
                if($otherConso->contact->cont_nom && $otherConso->contact->cont_prenom) :
                    $conso = $otherConso->contact->cont_nom . ' '. $otherConso->contact->cont_prenom;
                endif;?>
                <li class="list-group-item"> <?= Html::a($conso, yii\helpers\Url::to(['/consommateur/view', 'id' => $otherConso->cont_id]),['target' => 'blank'])?> </li>
            <?php endforeach;?>
            </ul>
            <?php endif;?>
    </div>
    <?php endif ?>
    
    <?php if (Yii::$app->session->hasFlash('notice')): ?>
        <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?= Yii::$app->session->getFlash('notice') . '<br />' ?>
        </div>
    <?php endif;?>
    <div class="row">

        <div class="col-lg-6 col-xs-12 profile_left" >
            <div class="x_panel" >
                <div class="x_content">
                    <div class="text-right">
                        <p style="font-size: 11px">
                            <?php echo ($model->contactCreatedAt) ? 'Ajouté le ' . Yii::$app->formatter->asDatetime($model->contactCreatedAt) : 'date d\'ajout inconnue'; ?>
                        </p>
                    </div>
                    <div class="img-profil">
                        <img src="../css/images/basket.png" alt="producteur" />
                    </div>

                    <h3>
                        <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?>
                    </h3>

                    <div class="mainInfos">
                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->contactGSMPrincipal) : ?> 
                                <?= $model->contactGSMPrincipal ?>
                            <?php else : ?>
                                Numéro GSM inconnu
                            <?php endif; ?>
                            <?=
                            \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_gsm_principal']),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                            <?php if ($model->contactEmailPrincipal) : ?>
                            <?= Html::a(yii\helpers\BaseStringHelper::truncate($model->contactEmailPrincipal, 40, '...'), 'mailto:'.$model->contactEmailPrincipal) ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php endif; ?>
                            <?=
                            \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_email_principal']),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
<?php /*
                        <p> <i class="fa fa-calendar" aria-hidden="true"></i> 
                            <?php if ($model->contactAnneeNaissance) : ?>
                                <?= Yii::$app->formatter->asDate($model->contactAnneeNaissance) ?> 
                            <?php else : ?>
                                Année de naissance inconnue
                            <?php endif; ?>
                            <?=
                            \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_annee_naissance']),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
*/ ?>
                        <h3>Adresse</h3>
                        
                            <?php if ($model->contact->adresse) : ?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                    <?php if($model->contact->adresse->adr_rue != null && $model->contact->adresse->adr_rue != '' ):?>
                                        <?= $model->contact->adresse->adr_rue ?>
                                    <?php else : ?>
                                        <?= 'Numéro et Rue inconnus'?>
                                    <?php endif; ?>
                                            <?= \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);?>
                                </p>

                                <?php if ($model->contact->adresse->adr_code_postal != null && $model->contact->adresse->adr_code_postal != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Code Postal: '. $model->contact->adresse->adr_code_postal?>
                                            <?= \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);?>
                                    </p>
                                <?php endif?>
                                    
                                <?php if ($model->contact->adresse->adr_ville != null && $model->contact->adresse->adr_ville != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Localite: '. $model->contact->adresse->adr_ville?>
                                        <?= \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        ?>
                                    </p>
                                <?php endif;?>
                                    
                                <?php if ($model->contact->adresse->adr_province != null && $model->contact->adresse->adr_province != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Province: '. $model->contact->adresse->adr_province?>
                                        <?= \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        ?>
                                    </p>
                                <?php endif;?>
                            <?php else :?>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                            <?= 'Adresse inconnue';?>
                            <?= \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/addadresse','id' => $model->cont_id]),
                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
                            <?php endif;?>
                    </div>
                    <?=
                    '<div class="text-right">' .
                    \yii\bootstrap\Html::button('Modifier les informations du contact', ['value' => \yii\helpers\Url::to(['/consommateur/update', 'id' => $model->cont_id]),
                        'name' => 'modalButton', 'class' => "btn btn-info"]) .
                    '</div>'
                    ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12">
            <div class="x_panel secondInfos">

                <div class="x_content" >
                    <h2>Autres informations </h2> 

                    <ul class="autres-infos">
                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i> Lieu de prise de contact :
                            <?php
                            if ($model->consoLieuContactName) {
                                echo $model->consoLieuContactName;
                            } else {
                                echo "inconnu";
                            }
                            ?> 
                        </li>

                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            <?php if ($model->secteurs) : ?>
                                Centres d'intérêts :
                                <ul class="fa-ul">
                                    <?php
                                    foreach ($model->secteurs as $secteur) {
                                        echo "<li>" . $secteur->sect_nom . "</li>";
                                    }
                                    ?>
                                </ul>
                            <?php else : ?> 
                                aucun centre d'intérêt
                            <?php endif; ?> 
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            Société : <?= ($model->conso_societe)? $model->conso_societe : 'inconnue'?>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            Fonction : <?= ($model->conso_fonction)? $model->conso_fonction : 'inconnue'?>
                        </li>
                        
                    </ul>
                    <?=
                    '<div class="text-right">' .
                    \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/consommateur/addmoreinfos', 'contactId' => $model->cont_id]),
                        'name' => 'modalButton', 'class' => "btn btn-info"]). '</div>'
                    ?>
                    <hr />
                    <h2>Newsletters</h2>
                    <ul>
                        <li>
                            <?php if($model->contact->newsletters) :?>
                                <ul class="fa-ul">
                                    <?php foreach ($model->contact->newsletters as $newsletter) : ?>
                                        <li><i class='glyphicon glyphicon-chevron-right'></i> <?= $newsletter->newsletter_nom ?> </li>
                                    <?php                                endforeach;?>
                                </ul>
                            <?php else : ?>
                                aucune
                            <?php endif;?>
                        </li>
                    </ul>
                        <?php echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/contact/managenewsletters', 'id' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php
                    echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
                        'entity' => (string) 'consommateur-' . $model->cont_id, // type and id
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home().'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>