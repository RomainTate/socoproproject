<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RechAdminEncadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$search = Yii::$app->request->pathInfo == 'rechadminencad/search'? true : false;
$this->title = 'Rech Admin Encads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rech-admin-encad-index">

    <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createREA')) : ?>
                <?= Html::a('Ajouter un contact R.E.A.', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        <?php endif; ?>
    </div>
    
    <?php if($search) : ?>
        <div class="x_panel">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    <?php endif;?>
    
    <?php if (!$search || Yii::$app->request->get('RechadminencadSearch') != null) : ?>
    <?php $gridColumns = [
//            ['class' => 'yii\grid\SerialColumn'],
                [
                'attribute' => 'contact.cont_civilite',
                'label' => 'Civ',
                'format' => 'text',
                ],
                [
                    'attribute' => 'contactNom',
                    'format' => 'text',
                ],
                'contactPrenom',
                'contactGSMPrincipal',
                'contactEmailPrincipal',
                [
                    'attribute' => 'rae_type',
                    'filter' => array("1" => "Recherche", "2" => "Administratif", "3" => "Encadrement"),
                    'value' => function ($model) {
                        if ($model->rae_type === 1) {
                            return 'Recherche';
                        } elseif ($model->rae_type === 2) {
                            return 'Administratif';
                        } elseif ($model->rae_type === 3) {
                            return 'Encadrement';
                        } else {
                            return 'Non renseignée';
                        }
                    }
                ],
//            'cont_id',
                ['class' => 'kartik\grid\ActionColumn', 'hiddenFromExport' => true ,
                    'buttons' => [
                        'view' => function($url, $model) {
                            $url = \Yii::$app->urlManager->createUrl(['rechadminencad/view', 'id' => $model->cont_id]);
                            return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                        },
                        'update' => function($url, $model) {
                            if(!\Yii::$app->user->can('createREA')) {
                                return null;
                            }
                            $url = \Yii::$app->urlManager->createUrl(['rechadminencad/update', 'id' => $model->cont_id]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update')]);
                        },
                        'delete' => function($url, $model) {
                            if(!\Yii::$app->user->can('deleteREA')) {
                                return null;
                            }
                            $url = \Yii::$app->urlManager->createUrl(['rechadminencad/delete', 'id' => $model->cont_id]);
                            return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-confirm' => 'Etes vous sûr de vouloir supprimer ce contact?', 'data-method' => 'post']);
                        }
                    ],
                ],
            ] ?>
    <hr />
    <div class="x_panel">
            Exportation :
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'exportConfig' => [
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_EXCEL => false
            ],
            'noExportColumns' => [count($gridColumns) - 1]
        ]);?>
    </div>
    
    <div class="x_panel">
        <?=GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $search? null :$searchModel,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'columns' => $gridColumns
        ]);?>
    </div>
    <?php endif; ?>
</div>
