<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "evenement".
 *
 * @property integer $evenement_id
 * @property integer $evenement_type
 * @property string $evenement_nom
 * @property date   $evenement_date
 *
 * @property Contact[] $contacts
 */
class Evenement extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'evenement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['evenement_nom', 'evenement_date'], 'required'],
            [['evenement_date'], 'safe'],
            [['adr_id', 'evenement_type'], 'integer'],
            [['evenement_nom'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'evenement_id' => 'Evenement ID',
            'evenement_nom' => 'Nom Evenement',
            'evenement_type' => 'Type Evenement',
            'evenement_date' => 'Date',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['cont_id' => 'cont_id'])->viaTable('participer', ['evenement_id' => 'evenement_id']);
    }
}
