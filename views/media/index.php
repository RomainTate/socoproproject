<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MediaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Médias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="media-index">
    
    <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createJournaliste')) : ?>
            <?= Html::a('Ajouter un Média', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        <?php endif; ?>
    </div>
    
    <?php 
    $gridColumns = [
    //            ['class' => 'yii\grid\SerialColumn'],

    //            'media_id',
                [
                    'attribute' => 'media_societe',
                    'header' => 'Société <br/> Grp Presse',
                    'label' => 'Grp Presse'
                ],
                [
                    'attribute' => 'media_titre',
                    'label' => 'Média'
                ],
                [
                    'attribute' => 'media_telephone',
                    'label' => 'Téléphone'
                ],
                [
                    'attribute' => 'media_email',
                    'label' => 'E-mail'
                ],
                [
                    'attribute' => 'media_site_internet',
                    'label' => 'Site Web'
                ],
                [
                    'attribute' => 'media_type_presse',
                    'label' => 'Type Presse'
                ],
                [
                    'attribute' => 'media_type_support',
                    'label' => 'Type Support'
                ],
                [
                    'attribute' => 'media_frequence',
                    'label' => 'Périodicité'
                ],
                [
                    'attribute' => 'media_audience',
                    'label' => 'Audience Média'
                ],
                [
                    'attribute' => 'media_langue',
                    'label' => 'Langue Média'
                ],
                [
                    'attribute' => 'media_pays',
                    'label' => 'Pays'
                ],
                // 'adr_id',

                ['class' => 'kartik\grid\ActionColumn','template' => '{view}{update}{delete}','hiddenFromExport' => true ,
                    'buttons' => [
                        'delete' => function($url, $model) {
                            if (!\Yii::$app->user->can('deleteJournaliste')) {
                                return null;
                            }
                            $url = \Yii::$app->urlManager->createUrl(['media/delete', 'id' => $model->media_id]);
                            return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-confirm' => 'Êtes-vous sûr de vouloir supprimer ce média?' . ($model->journalistes? ' Attention : des journalistes font partie de ce média, ils seront donc considérés sans média associé':''), 'data-method' => 'post']);
                        }
                    ],
                ],
            ];?>
    <div class="x_panel">
    <div class="pull-right">
                <label>Exportation :</label>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'target' => ExportMenu::TARGET_POPUP,
//                    'stream' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
//                        'encoding' => 'utf-8'
                    ],
                    'noExportColumns' => [count($gridColumns) - 1]
                ]);
                ?>
    </div>
    </div>
    <div class="x_panel">
        <?=        GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'mediaGrid',
            'filterModel' => $searchModel,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'columns' => $gridColumns,
            'responsiveWrap' => false
        ]); ?>
    </div>
</div>
<?php
$responsiveCSS = \app\components\WhResponsive::writeResponsiveCss($gridColumns, 'mediaGrid');
$this->registerCss($responsiveCSS);
$this->registerJsFile(\yii\helpers\Url::home() . 'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);
