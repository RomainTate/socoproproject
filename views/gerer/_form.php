
<?php
use app\models\Secteur;
use app\models\Gerer;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Html;


$secteurs = ArrayHelper::map(Secteur::find()->all(), 'sect_id', 'sect_nom');
$gerer = ArrayHelper::map(Gerer::findAll(['user_id' => $user_id]),'sect_id' , 'sect_id' );


ActiveForm::begin(['action' => '/gerer/update?id=' . $user_id]);?>

<div>Tout Selectionner
    <input id='selectall' type="checkbox">
</div>

<?php echo Html::checkboxList('gerer', $gerer, $secteurs);

echo Html::submitButton();
ActiveForm::end() ;

        
        $this->registerJsFile(
    'js/selectAll.js',
    ['depends' => [\yii\web\JqueryAsset::className()]]
);
?>
