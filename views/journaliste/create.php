<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Journaliste */

$this->title = 'Ajouter Journaliste';
$this->params['breadcrumbs'][] = ['label' => 'Journalistes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journaliste-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <?= $this->render('_form', [
        'model' => $model,
        'contactModel' => $contactModel
    ]) ?>
    </div>
</div>
