<?php

namespace app\controllers;

use Yii;
use app\models\Contactdivers;
use app\models\ContactdiversSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContactdiversController implements the CRUD actions for Contactdivers model.
 */
class ContactdiversController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contactdivers models.
     * @return mixed
     */
    public function actionIndex()
    {
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        $searchModel = new ContactdiversSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch() {
        return $this->actionIndex();
    }

    /**
     * Displays a single Contactdivers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contactdivers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        $model = new Contactdivers();
        $contactModel = new \app\models\Contact();
        $post = Yii::$app->request->post();

        if ($contactModel->load($post) && $model->load($post)) {
//            $model->setSecteurs($post['Producteur']['secteurs']);
            if ($model->alreadyExists($contactModel->cont_nom, $contactModel->cont_prenom)) {
                
                if (!isset($post['forceadd'])) {
                    Yii::$app->session->setFlash('error', "Ce contact existe déjà !");
                    return $this->render('create', [
                                'model' => $model,
                                'contactModel' => $contactModel,
                    ]);
                }
            }
            $contactModel->cont_nom = strtoupper($contactModel->cont_nom);
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
                if ($model->save(false))
                    { // save(false) bypass validation (already did 2 lines ago)
                    if( isset($post['Contactdivers']['nomentr']) && $post['Contactdivers']['nomentr'] != '') {
                        if( isset($post['Contactdivers']['fct']) && $post['Contactdivers']['fct'] == '') {
                            $fonction = null;
                        } else {
                            $fonction = $post['Contactdivers']['fct'];
                        }
                        $societe = \app\models\Entreprise::find()->where(['entr_societe' => $post['Contactdivers']['nomentr']])->one();
                        if($societe) {
                            AppartenirController::addContactEntreprise($model->cont_id, $societe->entr_id, $fonction);
                        } else {
                            $societe_id = EntrepriseController::createEntreprise($post['Contactdivers']['nomentr']);
                            AppartenirController::addContactEntreprise($model->cont_id, $societe_id, $fonction);
                        }
                    }
                    $this->actionLinkSecteurs($model->cont_id,$post['Contactdivers']['secteurs']);
                    if ($post['membrecollege'] == '1') {
                        $membreCollege = new \app\models\MembreCollege();
                        $membreCollege->cont_id = $model->cont_id;
                        $membreCollege->save();
                    }
                    if (array_key_exists('membrefiliere', $post)) {
                        $this->actionLinkFilieres($model->cont_id,$post['membrefiliere']);
                    }
                    if(array_key_exists('newsletters', $post)) {
                        ContactController::manageNewsletters($model->cont_id, $post['newsletters']);
                    } 
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                } else {
                    throw new \yii\base\ErrorException; // should never been thrown but we never know
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'contactModel' => $contactModel,
            ]);
        }
    }

    /**
     * Updates an existing Contactdivers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    { 
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        $model = $this->findModel($id);
        $contactModel = $model->contact;
        $post = Yii::$app->request->post();
        
        if ($contactModel->load($post)) {
            $contactModel->cont_nom = strtoupper($contactModel->cont_nom);
            if ($contactModel->save()) { //validate $model BEFORE saving $contactModel
                if(isset($post['Contactdivers']['secteurs'])) {
                    $this->actionLinkSecteurs($contactModel->cont_id,$post['Contactdivers']['secteurs']);
                }
                if ($post['membrecollege'] == '1' && $model->contact->membrecollege == null) {
                    $membreCollege = new \app\models\MembreCollege();
                    $membreCollege->cont_id = $model->cont_id;
                    $membreCollege->save();
                } else if ($post['membrecollege'] == '0' && $model->contact->membrecollege != null) {
                    $membreCollege = \app\models\MembreCollege::find()->where(['cont_id' => $model->cont_id])->one();
                    $membreCollege->delete();
                }
                $filieres = isset($post['membrefiliere']) ? $post['membrefiliere'] : null;
                $this->actionLinkFilieres($model->cont_id, $filieres);
                if(array_key_exists('newsletters', $post)) {
                        $newsletters = $post['newsletters'];
                    } else {
                        $newsletters = null;
                    }
                    ContactController::manageNewsletters($model->cont_id, $newsletters);
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                } else {
                    throw new \yii\base\ErrorException; // should never been thrown but we never know
                }
        } 
        
        if (!Yii::$app->request->isAjax) {
            return $this->render('update', [
                        'model' => $model,
                        'contactModel' => $contactModel
            ]);
        }
        
        return $this->renderAjax('update', [
                    'model' => $model,
                    'contactModel' => $contactModel
        ]);
    }
    
    public function actionAddmoreinfos ($contactId) {
        $post = Yii::$app->request->post();
        $model = $this->findModel($contactId);
        if($model->load($post)) {

            if($model->save()&&($this->actionLinkSecteurs($model->cont_id,$post['Contactdivers']['secteurs']))) {
                if ($post['membrecollege'] == '1' && $model->contact->membrecollege == null) {
                    $membreCollege = new \app\models\MembreCollege();
                    $membreCollege->cont_id = $model->cont_id;
                    $membreCollege->save();
                } else if ($post['membrecollege'] == '0' && $model->contact->membrecollege != null) {
                    $membreCollege = \app\models\MembreCollege::find()->where(['cont_id' => $model->cont_id])->one();
                    $membreCollege->delete();
                }
                $filieres = isset($post['membrefiliere']) ? $post['membrefiliere'] : null;
                $this->actionLinkFilieres($model->cont_id, $filieres);
                return $this->redirect(['view', 'id' => $contactId]);
            }
        } else {
            return $this->renderAjax('_formInfos', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contactdivers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->request->referrer && (strpos(Yii::$app->request->referrer, 'index') !== false)) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Contactdivers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Contactdivers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contactdivers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function actionLinkSecteurs($id, $secteurs) {
        // get all auth_item_child where 'parent' = $role & delete them
        \app\models\Interesser::deleteAll(['cont_id' => $id]);

        if (is_array($secteurs)) {
            foreach ($secteurs as $secteur) {
                $interet = new \app\models\Interesser();
                $interet->cont_id = $id;
                $interet->sect_id = $secteur;
                if (!$interet->save()) {
                    return false;
                }
            }
        }
        return true;
    }

    public function actionLinkFilieres($id, $post) {
        
        \app\models\MembreCommissionFilliere::deleteAll(['cont_id' => $id]);
        
        if($post) {
            foreach ($post as $filiere) {
                    $membrefiliere = new \app\models\MembreCommissionFilliere();
                    $membrefiliere->cont_id = $id;
                    $membrefiliere->sect_id = $filiere;
                    $membrefiliere->save();
            }
        }
    }

}
