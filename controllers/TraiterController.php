<?php

namespace app\controllers;

class TraiterController extends \yii\web\Controller {

    public static function addSecteurEntreprise($sect_id, $entr_id) {
        $secteurEntreprise = new \app\models\Traiter();
        $secteurEntreprise->sect_id = $sect_id;
        $secteurEntreprise->entr_id = $entr_id;
        if ($secteurEntreprise->save()) {
            return true;
        }
        return false;
    }
    
    public static function deleteSecteurEntreprise($entr_id) {
//        ;
        if (\app\models\Traiter::deleteAll(['entr_id' => $entr_id])) {
            return true;
        }
        return false;
    }

}
