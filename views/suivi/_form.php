<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//$date = date("d/m/Y H:i");
/* @var $this yii\web\View */
/* @var $model app\models\Suivi */
/* @var $form ActiveForm */

?>
<div class="suivi-_form">

    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'suivi_date')->textInput()->widget(kartik\datetime\DateTimePicker::className(),[
	'pluginOptions' => [
		'language' => 'fr',
		'autoclose' => true,
//            'endDate'=> date('d/m/Y H:i'),
            'todayBtn' => true,
	]
    ]);?>
    
        <?= $form->field($model, 'suivi_type_contact')->dropDownList(['1' => 'Téléphonique', '2' => 'E-mail', '3' => 'Visite', '4' => 'Foire/Evènement']) ?>
        <?php //echo  $form->field($model, 'suivi_objet') ?>
        <?= 
        $form->field($model, 'suiviobjets_ids')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Objet::find()->all(), 'objet_id', 'objet_nom'),
//        $form->field($model, 'suiviobjets_ids')->dropDownList($model->suiviobjets_ids
                ['class' => 'js-multiple' , 'multiple'=>'multiple']) ?>
        <?= $form->field($model, 'suivi_suite')->checkbox() ?>
        <?= $form->field($model, 'suivi_commentaire')->textarea(['style' => 'resize: none']) ?>
    
        <div class="form-group">
            <?= Html::submitButton('Envoyer', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- suivi-_form -->
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');