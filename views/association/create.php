<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Association */

$this->title = 'Ajouter Association';
$this->params['breadcrumbs'][] = ['label' => 'Associations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="association-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'entrepriseModel' => $entrepriseModel
    ]) ?>

</div>
