<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
?>

<div class="x_panel">
    <div class="x_title">
        <h1>Mise à jour Newsletter</h1>
    </div>

    <div class="newsletter-form">
        <?php $form = ActiveForm::begin([
            'id' => 'newsletterUpdate'
        ]); ?>
        <?= $form->field($model, 'newsletter_nom')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
