<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Journaliste */

$this->title = $model->contact->cont_civilite . ' ' .$model->contact->cont_nom . ' ' . $model->contact->cont_prenom;
$this->params['breadcrumbs'][] = ['label' => 'Journalistes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journaliste-view">

    <h1 class="pull-left">Profil de <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?></h1>
    <div class="pull-right">
        <?php
        if (Yii::$app->user->can('deleteJournaliste'))
            echo Html::a('Supprimer le contact', ['delete', 'id' => $model->cont_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Êtes-vous sûr de vouloir supprimer ce contact?',
                    'method' => 'post',
                ],
            ])
            ?>
    </div>
    <div class="clearfix"></div>
    <div class="row">

        <div class="col-lg-6 col-xs-12 profile_left" >
            <div class="x_panel" >
                <div class="x_content">
                    <div class="text-right">
                        <p style="font-size: 11px">
                            <?php echo ($model->contactCreatedAt) ? 'Ajouté le ' . Yii::$app->formatter->asDatetime($model->contactCreatedAt) : 'date d\'ajout inconnue'; ?>
                        </p>
                    </div>
                    <div class="img-profil">
                        <img src="../css/images/news.png" alt="journaliste" />
                    </div>

                    <h3>
                        <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?>
                    </h3>
                    <div class="mainInfos">
                        <p> <i class="fa fa-lg fa-phone" aria-hidden="true"></i>
                            <?php if ($model->contactTelephonePrincipal) : ?> 
                                <?= $model->contactTelephonePrincipal ?>
                            <?php else : ?>
                                Numéro Téléphone inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_telephone_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 
                        
                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->contactGSMPrincipal) : ?> 
                                <?= $model->contactGSMPrincipal ?>
                            <?php else : ?>
                                Numéro GSM inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_gsm_principal']), 'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope" aria-hidden="true"></i> 
                            <?php if ($model->contactEmailPrincipal) : ?>
                                <?= Html::a(yii\helpers\BaseStringHelper::truncate($model->contactEmailPrincipal, 40, '...'), 'mailto:'.$model->contactEmailPrincipal) ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_email_principal']), 'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
                        
                        <p> <i class="fa fa-flag" aria-hidden="true"></i> 
                            <?php
                            if ($model->contactLangue) :
                                echo 'Langue : ' . $model->contactLangue;
                            else : ?>
                                Langue inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_langue']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
<?php /*
                        <p> <i class="fa fa-calendar" aria-hidden="true"></i> 
                            <?php if ($model->contactAnneeNaissance) : ?>
                                <?= $model->contactAnneeNaissance ?> 
                            <?php else : ?>
                                Année de naissance inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createJournaliste'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_annee_naissance']), 'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
*/?>
                         <h3>Adresse</h3>
                        
                            <?php if ($model->contact->adresse) : ?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                    <?php if($model->contact->adresse->adr_rue != null && $model->contact->adresse->adr_rue != '' ):?>
                                        <?= $model->contact->adresse->adr_rue ?>
                                    <?php else : ?>
                                        <?= 'Numéro et Rue inconnus'?>
                                    <?php endif; ?>
                                    <?php if (Yii::$app->user->can('createJournaliste')) {
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        }?>
                                </p>

                                <?php if ($model->contact->adresse->adr_code_postal != null && $model->contact->adresse->adr_code_postal != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Code Postal: '. $model->contact->adresse->adr_code_postal?>
                                        <?php if (Yii::$app->user->can('createJournaliste')) {
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        }?>
                                    </p>
                                <?php endif?>
                                    
                                <?php if ($model->contact->adresse->adr_ville != null && $model->contact->adresse->adr_ville != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Localite: '. $model->contact->adresse->adr_ville?>
                                        <?php if (Yii::$app->user->can('createJournaliste')) {
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        }?>
                                    </p>
                                <?php endif;?>
                                    
                                <?php if ($model->contact->adresse->adr_province != null && $model->contact->adresse->adr_province != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Province: '. $model->contact->adresse->adr_province?>
                                        <?php if (Yii::$app->user->can('createJournaliste')) {
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        }?>
                                    </p>
                                <?php endif;?>
                            <?php else :?>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                            <?= 'Adresse inconnue';?>
                            <?php if (Yii::$app->user->can('createJournaliste')){
                                    echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/addadresse','id' => $model->cont_id]),
                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            }?>
                        </p>
                            <?php endif;?>
                    </div>
                    <?php
                    if (Yii::$app->user->can('createJournaliste'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations du contact', ['value' => \yii\helpers\Url::to(['/journaliste/update', 'id' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                </div>
            </div>
        </div>

        <div class="col-lg-6 col-xs-12">
            <div class="x_panel secondInfos">
                <div class="x_content">
                    <h2>Autres informations</h2> 

                    <ul class="autres-infos">
                        <li>
                            Date du contact : 
                            <?php
                            if ($model->journaliste_date_contact === null) {
                                echo "inconnue";
                            } else {
                                echo Yii::$app->formatter->asDate($model->journaliste_date_contact);
                            }
                            ?> 
                        </li>
                        <li>
                            Fonction : 
                            <?php
                            if ($model->journaliste_fonction === null) {
                                echo "inconnue";
                            } else {
                                echo $model->journaliste_fonction;
                            }
                            ?> 
                        </li>
                        <li>
                            Rubrique : 
                            <?php
                            if ($model->journaliste_rubrique === null) {
                                echo "inconnue";
                            } else {
                                echo $model->journaliste_rubrique;
                            }
                            ?> 
                        </li>
<?php /*                        
                        <li>
                            Langue : 
                            <?php
                            if ($model->journaliste_langue === 'fr') {
                                echo "français";
                            } else if ($model->journaliste_langue === 'nl') {
                                echo "néerlandais";
                            } else if ($model->journaliste_langue === 'fr/nl') {
                                echo "français / néerlandais";
                            } else {
                                echo "inconnue";
                            }
                            ?> 
                        </li>
*/?>
                    </ul>
                    <?php
                    if (Yii::$app->user->can('createJournaliste'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/journaliste/addmoreinfos', 'contactId' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                    <hr />
                    <h2> Journal  </h2>
                    <?php if ($model->media == null) : ?>
                        <?php
                        if (Yii::$app->user->can('createJournaliste')) {
                            echo '<div class="text-right">'.
                             Html::button('Ajouter un journal au contact', ['value' => \yii\helpers\Url::to(['/journaliste/addjournal', 'contactId' => $model->cont_id]), 'class' => 'btn btn-success', 'name' => 'modalButton'])
                            .'</div>';
                        }
                        ?>
                        <h5> Aucun journal </h5> 
                            <?php else : ?>
                        <h4> <?= yii\bootstrap\Html::a($model->media->media_titre , yii\helpers\Url::to(['/media/view' , 'id' => $model->media->media_id])) ?>
                                </h4> 
                        <?= yii\bootstrap\Html::a('Voir tous les journalistes de '.$model->media->media_titre , yii\helpers\Url::to(['/journaliste' , 'JournalisteSearch[nommedia]' => $model->media->media_titre])) ?>
                                <?=
                                DetailView::widget([
                                    'model' => $model->media,
                                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
                                    'attributes' => [
                                        [
                                           'attribute' => 'media_societe',
                                            'label' => 'Société / Groupe de presse'
                                        ],
                                        'media_titre',
                                        'media_telephone',
                                        'media_site_internet',
                                        [
                                            'attribute' => 'media_email',
                                            'format' => 'html',
                                            'value' => function ($model) {
                                                if(!$model->media_email) return null;
                                                return \yii\helpers\Html::a(yii\helpers\BaseStringHelper::truncate($model->media_email, 40, '...'), 'mailto:'.$model->media_email) ;
                                            }
                                        ],
                                        'media_pays',
                                        'media_type_presse',
                                        'media_type_support',
                                        'media_frequence',
                                        'media_audience',
                                        [
                                            'label' => 'Adresse',
                                            'format' => 'raw',
                                            'value' => function ($model) {
                                                if (!$model->adresse) {
                                                    if (!Yii::$app->user->can('createJournaliste'))
                                                        return 'adresse inconnu';
                                                    return 'adresse inconnue' . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(\yii\helpers\Url::home().'media/addadresse?id=' . $model->media_id),
                                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                                }
                                                //else
                                                $adr = "<ul>" ;
                                                foreach($model->adresse->getAttributes(null, ['adr_id']) as $attr => $value) {
                                                    $adr .= "<li>". $model->adresse->getAttributeLabel($attr). ": "  ;
                                                    $adr .= $value ? $value : 'inconnu';
                                                    $adr .= "</li>";
                                                }
                                                $adr .= "</ul>";
                                                
                                                if (!Yii::$app->user->can('createJournaliste'))
                                                    return $adr;
                                                //else
                                                return $adr
                                                        . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(\yii\helpers\Url::home().'adresse/update?id=' . $model->adr_id),
                                                            'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }
                                        ],
                                    ],
                                ]);
                                if (Yii::$app->user->can('createJournaliste')) {
                                    echo '<div class="text-right">';
                                    echo \yii\bootstrap\Html::button('Modifier', ['value' => \yii\helpers\Url::to(['/media/update' , 'id' => $model->media->media_id, 'fullform' => true]), 'name' => 'modalButton', 'class' => "btn btn-info"]);
                                    echo \yii\bootstrap\Html::a("Enlever le média", \yii\helpers\Url::to(['/journaliste/deletemedia', 'id' => $model->cont_id]), ['class' => 'btn btn-danger', 'data-confirm' => 'Retirer ce contact de ce média ? Ceci ne supprimera pas le média de la base de données']);
                                    echo '</div>';
                                }
                                echo '<hr />';
                            endif;
                    ?>
                </div>

            </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php
                    echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
                        'entity' => (string) 'journaliste-' . $model->cont_id, // type and id
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div> 
</div>
<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home().'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>