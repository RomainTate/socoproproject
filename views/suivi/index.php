<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $suivis app\models\Suivi */

$this->title = 'Suivi du Producteur';
$name = ($model->contact->cont_nom && $model->contact->cont_prenom)? $model->contact->cont_nom . ' ' . $model->contact->cont_prenom :  $model->contact->getNature('label');
$this->params['breadcrumbs'][] = ['label' => 'Producteurs', 'url' => ['/producteur/index']];
$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['/producteur/view', 'id' => $model->cont_id]];
?>
<div class="x_panel">
    <div class="x_title">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--<a class="navbar-brand" href="#">Brand</a>-->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><?= Html::a('Profil', \yii\helpers\Url::to(['/producteur/view', 'id' => $model->cont_id])) ?></li>
                    <li><?= Html::a('Champs sectoriels', \yii\helpers\Url::to(['/producteur/details', 'id' => $model->cont_id])) ?></li>
                    <li><?= Html::a('Logs', \yii\helpers\Url::to(['/log', 'LogSearch[contID]' => $model->cont_id])) ?></li>
                    <li class="active"> <?= Html::a('Suivis', \yii\helpers\Url::to(['/suivi', 'id' => $model->cont_id])) ?> </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>

<div class="row">
    <?php if( !Yii::$app->user->isGuest &&  (Yii::$app->user->getUser()->getIsAdmin() || \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission' )) : ?>
    <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
        <div class="x_panel suivibox" style="height: 405px">
            <?php //            $form = \yii\widgets\ActiveForm::begin();?>
            <?php echo $this->render('_form', [
                'model' => new \app\models\Suivi(['suivi_date' => date("d/m/Y H:i")])
            ]) ?>
        </div>
    </div>
    <?php endif ?>
    <?php foreach ($suivis as $suivi) : ?>
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
            <?php if($newID && $suivi->suivi_id == $newID) :?>
            <div class="newSuivi" style="display: none">
            <?php endif;?>
            <div class="x_panel suivibox" style="height: 405px; font-size: 15px">
                        <div class="x_title text-right">
                            <?php echo ($suivi->suivi_date) ? 'le ' . Yii::$app->formatter->asDatetime($suivi->suivi_date) : 'date d\'ajout inconnue'; ?>
                            <?php echo ($suivi->user_id) ? 'par ' . \app\models\User::findIdentity($suivi->user_id)->username : 'inconnu'; ?>
                            <?php if ( Yii::$app->user->getId() == $suivi->user_id || Yii::$app->user->getUser()->getIsAdmin()) : ?>
                            <?= Html::a('x', yii\helpers\Url::to(['delete', 'id' => $suivi->suivi_id]),['class' => 'btn-danger', 'title' => 'Supprimer', 'data-confirm' => 'Etes vous sûr ?']) ?>
                            <?php endif; ?>
                        </div>
                        <div class="x_content text-left">
                            <?php if ( Yii::$app->user->getId() == $suivi->user_id || Yii::$app->user->getUser()->getIsAdmin()) : ?>
                            <p class="form-group">
                                <label style="font-weight: normal">
                                <?=                                     Html::checkbox(null, $suivi->suivi_suite, ['suiviId' => $suivi->suivi_id, 'class' => 'suiviSuiteCheckbox', 'homeUrl' => \yii\helpers\Url::home()])?>
                                   En attente de suite
                               </label>
                            </p>
                            <?php endif; ?>
<?php /*                            
                            <p>
                                en attente : 
                                <?php if($suivi->suivi_suite) :?>
                                OUI
                                    <?php if ( Yii::$app->user->getId() == $suivi->user_id || Yii::$app->user->getUser()->getIsAdmin()) : ?>
                                    <?= Html::a('(retirer)', yii\helpers\Url::to(['removesuite', 'id' => $suivi->suivi_id])) ?>
                                    <?php endif; ?>
                                <?php else : ?>
                                non
                                    <?php if ( Yii::$app->user->getId() == $suivi->user_id || Yii::$app->user->getUser()->getIsAdmin()) : ?>
                                    <?= Html::a('(ajouter)', yii\helpers\Url::to(['addsuite', 'id' => $suivi->suivi_id])) ?>
                                    <?php endif; ?>
                                <?php endif;?>
                            </p>
 */?>
                            <p>
                                Contact : <?php if ($suivi->suivi_type_contact == 1) echo 'Téléphonique';
                                if ($suivi->suivi_type_contact == 2) echo 'E-mail';
                                if ($suivi->suivi_type_contact == 3) echo 'Visite';
                                if ($suivi->suivi_type_contact == 4) echo 'Foire/Evènement';
                                ?>
                            </p>
                            <p>
                                Objet(s) : 
                                    <?php if(!$suivi->objets) :
                                        echo 'Sans objet';
                                    else :
                                        echo implode(', ', \yii\helpers\ArrayHelper::map($suivi->objets, 'objet_id', 'objet_nom'));
                                    endif;?>
                            </p>
                            <p style="overflow-wrap: break-word">
                                <?php if($suivi->suivi_commentaire) :
//                                    $com = yii\helpers\BaseStringHelper::truncate($suivi->suivi_commentaire,180,'<a href=# name = "modalButton" value="'. \yii\helpers\Url::to(['/suivi/showcom', 'id' => $suivi->suivi_id]).'"> ... lire la suite</a>');
                                    $com = yii\helpers\BaseStringHelper::truncate(htmlspecialchars($suivi->suivi_commentaire),180, Html::a(' ... lire la suite', null, [ 'name' => "modalButton", 'value'=>\yii\helpers\Url::to(['/suivi/showcom', 'id' => $suivi->suivi_id])]));
//                                    $com = yii\helpers\BaseStringHelper::truncate($suivi->suivi_commentaire,200, yii\helpers\Html::button('lire la suite', ['name' => 'modalButton', 'value' => \yii\helpers\Url::to(['/suivi/showcom', 'id' => $suivi->suivi_id])])) ;
                                else :
                                    $com = 'Aucun';
                                endif;?>
                                Commentaire : <?= $com?> 
                            </p>
                        </div>
            </div>
            <?php if($newID && $suivi->suivi_id == $newID) :?>
            </div>
            <?php endif;?>
        </div>
    <?php endforeach ?>
</div>

<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJs("$(window).on('load', function() { $('.newSuivi').delay(500).fadeIn(500);});", yii\web\View::POS_END);
$this->registerJsFile(\yii\helpers\Url::home() . 'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(\yii\helpers\Url::home() . 'js/indexsuivi.js', ['depends' => [yii\web\JqueryAsset::className()]]);
