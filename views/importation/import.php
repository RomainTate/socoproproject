<?php 
$i =1;?>
<div id="result">
    <table class="table table-bordered table-condensed table-responsive" id="table2excel" style="font-size: 12px" >
        <?php        foreach ($dataImport as $row) :?>
            <?php if ($row && ((array_key_exists('errors',$row)) || (array_key_exists('concordance', $row)))):?>
                <tr id="clm<?=$i?>">
                    <?php foreach ($row as $k => $v) :?>
                    <?php if(!is_numeric($k) ) break;?>
                        <?php if (!is_array($v)) : ?>
                    <td <?php if(isset($row['errors']) && isset($row['errors']['index']) && in_array($k, $row['errors']['index'])) :
                            echo "style= 'border : 2px solid red'"; 
                            elseif (isset($row['concordance']) && isset($row['concordance']['index']) && in_array($k, $row['concordance']['index'])) :
                            echo "style= 'border : 2px solid yellow'";     
                            endif;
                            ?>> <?= $v ?> </td>
                    
                        <?php endif;?>
                    <?php            endforeach;?>
                </tr>
                <?php  foreach ($row['messages'] as $message) :?>
                <tr>
                    <td colspan="13"> <?= $message ?> </td>
                </tr>
                <?php endforeach;?>
                <?php endif;?>
        <?php   $i++;     endforeach;?>
        
    </table>

</div>
<button>Exporter</button>
<?= $this->registerJsFile(\yii\helpers\Url::home().'js/jquery.table2excel.js', ['depends' => [yii\web\JqueryAsset::className()]]);
 $this->registerJsFile(\yii\helpers\Url::home().'js/exportTable.js', ['depends' => [yii\web\JqueryAsset::className()]]);