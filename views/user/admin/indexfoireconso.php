<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Secteur;
use kartik\grid\GridView;

$this->title = 'Administration : Consommateurs en attente de validation';
$this->params['breadcrumbs'][] = $this->title;?>

<div class="x_panel">
    <div class="x_title">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <ul id="w0" class="nav-tabs nav" style="margin-bottom: 15px">
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/index">Utilisateurs</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>rbac/role/index">Rôles</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>champsect/">Champs sectoriels</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexdoublon">Gestion doublons</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexnewsletters">Newsletters</a></li>
        <li class="dropdown active"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foires <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="indexfoireprod">Producteurs</a></li>
                <li class="active"><a href="indexfoireconso">Consommateurs</a></li>
            </ul>
        </li>
    </ul>

        <?php $gridColumns = [


            [
                'label' => 'Nom',
                'attribute' => 'contactNom', 
                'format' => 'raw'
            ],
            [
                'label' => 'Prenom',
                'attribute' => 'contactPrenom', 
                'format' => 'raw'
            ],
            [
                'label' => 'Boite',
                'attribute' => 'rue',
                'format' => 'raw'
            ],
            [
                'label' => 'Localité',
                'format' => 'raw',
                'attribute' => 'localite'
            ],
            [
                'label' => 'C.P.',
                'attribute' => 'codepostal', 
                'format' => 'raw'
            ],
            [
                'label' => 'Province',
                'attribute' => 'province', 
                'format' => 'raw'
            ],
            [
                'label' => 'Tel',
                'attribute' => 'contactTelephonePrincipal',
            ],
            [
                'label' => 'GSM',
                'attribute' => 'contactGSMPrincipal'
            ],

            [
                'label' => 'E-mail',
                'attribute' => 'contactEmailPrincipal', 
                'format' => 'email'
            ],
//            [
//                'label' => 'Secteurs',
//                'attribute' => 'secteursID',
//                'format' => 'html',
//                'filter' => ArrayHelper::map(Secteur::find()->asArray()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom'),
//                'value' => function($model) {
//                    $secteurNames = array();
//                    $secteursArray = ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
//                    asort($secteursArray);
//                    foreach ($secteursArray as $secteur) {
//                        $secteurNames[] = '<span style="display:block;">' . $secteur . '</span>';
//                    }
//                    return implode(", ", $secteurNames);
//                }
//            ],
        ];
        array_push($gridColumns, ['class' => 'kartik\grid\ActionColumn','template' => '{view}{details}{delete}', 'hiddenFromExport' => true,
            'buttons' => [
                'view' => function($url, $model) {
                    $url = \Yii::$app->urlManager->createUrl(['consommateur/view', 'id' => $model->cont_id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['title' => Yii::t('yii', 'View'), 'class' => 'action']);
                },
                'delete' => function($url, $model) {
                    if (!\Yii::$app->user->can('deleteConso')) {
                        return null;
                    }
                    $url = \Yii::$app->urlManager->createUrl(['consommateur/delete', 'id' => $model->cont_id]);
                    return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['class' => 'action', 'title' => Yii::t('yii', 'Delete'), 'data-confirm' =>  'Etes vous sûr de vouloir supprimer ce consommateur (en attente) ?', 'data-method' => 'post']);
                }
            ]
        ])
        ?>
    <div class="clear"></div>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'foireconsoGrid',
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
                'columns' => $gridColumns,
                'responsive' => false,
                'responsiveWrap' => false,
                'pager' => [
                    'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
                    'prevPageLabel' => 'Précédent',   // Set the label for the "previous" page button
                    'nextPageLabel' => 'Suivant',   // Set the label for the "next" page button
                    'firstPageLabel'=>'Début',   // Set the label for the "first" page button
                    'lastPageLabel'=>'Fin',    // Set the label for the "last" page button
                    'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
                    'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
                    'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
                    'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
                    'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
                ],      
            ]);
            ?>
</div>

<?php $this->registerJsFile(\yii\helpers\Url::home().'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);