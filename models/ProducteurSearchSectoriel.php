<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Producteur;
use yii\helpers\ArrayHelper;

/**
 * ProducteurSearch represents the model behind the search form about `app\models\Producteur`.
 */
class ProducteurSearchSectoriel extends Producteur
{
    public $contactNom;
    public $contactPrenom;
    public $secteursID;
    public $contactCivilite;
    public $contactTelephonePrincipal;
    public $contactGSMPrincipal;
    public $contactEmailPrincipal;
    public $contactAnneeNaissance;
    public $contactCreatedAt;
    public $associesnames;
    public $exploitations;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[ 'prod_sigec'], 'integer'],
            [['contactNom' , 'contactPrenom' , 'secteursID',  'prod_nom_exploitation'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(isset($params['sigec'])) {
            $query = Producteur::find();
        }else {
            $query = Producteur::find()->where(['prod_sigec' => 0]);
        }
        $query->orderBy('cont_id');
        
        $query->groupBy('producteur.cont_id');
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->setSort([
            'attributes' => [
                'contactNom' => [
                    'asc' => ['contact.cont_nom' => SORT_ASC],
                    'desc' => ['contact.cont_nom' => SORT_DESC],
                ],
                'contactPrenom' => [
                    'asc' => ['contact.cont_prenom' => SORT_ASC],
                    'desc' => ['contact.cont_prenom' => SORT_DESC],
                ],
                'contact.cont_civilite' => [
                    'asc' => ['contact.cont_civilite' => SORT_ASC],
                    'desc' => ['contact.cont_civilite' => SORT_DESC],
                ],
                'prod_sigec' => [
                    'asc' => ['prod_sigec' => SORT_ASC],
                    'desc' => ['prod_sigec' => SORT_DESC],
                ],

            ]
        ]);

        

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
//            $query->joinWith('contact'); //Eager loading
            return $dataProvider;
        }
        
        // grid filtering conditions
        
        
        $query->joinWith('contact');
        
        
        if (\app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission') {
            $secteurshandled = ArrayHelper::map(Gerer::find()->select('sect_id')->where(['user_id' => Yii::$app->user->getId()])->all(), 'sect_id', 'sect_id');
//            if (count($secteurshandled) == 1) {
                $condition = ['in', 'producteur.cont_id', ArrayHelper::map(Producteur::find()->select('producteur.cont_id')->innerJoin('travailler t', 'producteur.cont_id = t.cont_id')
                        ->innerJoin('secteur s', 't.sect_id = s.sect_id')->innerJoin('gerer g', 's.sect_id = g.sect_id')->where(['g.user_id' => \Yii::$app->user->getId()])->all(),'cont_id','cont_id')];
//            }

            $query->andFilterWhere($condition);
        }
        
        $query->joinWith('secteurs');
        
        if ($this->secteursID != null) {
            $query->andFilterWhere(['=', 'secteur.sect_id', $this->secteursID]);
        }
        
        foreach (\Yii::$app->request->get() as $key => $get) {
            if($key == 'ProducteurSearchSectoriel' || $key == 'sigec' || substr($key,-9) == '-operator' || $get == '' || $key == 'page' || $key == 'sort' || $key == 'membrefiliere') {
                if($key == 'membrefiliere') {
                    $query->joinWith('contact.secteursMembrecommission sc')->where(['sc.sect_id' => $this->secteursID]);
                }
                continue;
            }
            if (substr($key,0, 8) !== 'SPECIFIC') { // TRANSVERSAL FIELD
                if(strlen($key) > 12 && substr($key, -12) == '-includeNull' ) { 
                    if(array_key_exists(substr($key,0, -12), \Yii::$app->request->get()) && \Yii::$app->request->get(substr($key,0, -12)) != '') { // if not only null has been selected
                            continue;                                                                                                              // magic comes later
                        } else {                                                            // if only null has been selected
                            $key = substr($key,0, -12);
                            $query->andWhere(['travailler.'.$key => null]);
                            continue;
                        }
                }

                if(array_key_exists($key.'-includeNull', \Yii::$app->request->get())) { // if null value has been selected too 
                    switch (\app\controllers\ChampsectController::getType($key)) {
                        case 3 :
                            $query->andWhere(['or',['like','travailler.'.$key, ($get)],['travailler.'.$key => null]]);
                            break;
                        case 5 :
                            if (\Yii::$app->request->get($key . '-operator') == 'AND') {
                                $query->andWhere(['or', ['like', 'travailler.' . $key, \app\controllers\ChampsectController::getSQLFormatAnd($key, $get), false], ['travailler.' . $key => null]]);
                            } else {
                                $subQuery = array();
                                array_push($subQuery, 'or');
                                foreach ($get as $g) {
                                    array_push($subQuery, ['like', 'travailler.' . $key, \app\controllers\ChampsectController::getSQLFormatOr($key, $g), false]);
                                }
                                array_push($subQuery, ['travailler.' . $key => null]);
                                $query->andWhere($subQuery);
                            }
                            break;
                        case 4 :
                            $signe = \Yii::$app->request->get($key . '-operator');
                            if ($signe == '=') {
                                $query->andWhere(['or',['in', 'travailler.'.$key, ($get)],['travailler.'.$key => null]]);
                            } else {
                                $query->andWhere(['or',[$signe, 'travailler.'.$key, ($get)],['travailler.'.$key => null]]);
                            }
                        default :
                            $query->andWhere(['or',['in', 'travailler.'.$key, ($get)],['travailler.'.$key => null]]);
                            break;
                    }
                    
                } else { // if null has NOT been selected
                    switch (\app\controllers\ChampsectController::getType($key)) {
                    case 3 :
                        $query->andFilterWhere(['like', 'travailler.'.$key, ($get)]);
                        break;
                    case 5 : 
                        if(\Yii::$app->request->get($key.'-operator') == 'AND') {
                            
                            $query->andWhere(['like', 'travailler.'.$key, \app\controllers\ChampsectController::getSQLFormatAnd($key,$get), false]);
                        } else {
                            $subQuery = array();
                            array_push($subQuery, 'or');
                            foreach ($get as $g) {
                                array_push($subQuery, ['like','travailler.'.$key, \app\controllers\ChampsectController::getSQLFormatOr($key, $g), false]);
                            }
                            $query->andWhere($subQuery);
                        }
                        break;
                    case 4 :
                        $signe = \Yii::$app->request->get($key . '-operator');
                            if ($signe == '=') {
                                $query->andWhere(['in', 'travailler.'.$key, ($get)]);
                            } else {
                                $query->andWhere([$signe, 'travailler.'.$key, ($get)]);
                            }
                        break;
                    default :
                        $query->andFilterWhere(['in', 'travailler.'.$key, ($get)]);
                        break;
                    }
                }
            } else { // SPECIFIC FIELD
                
                $secteur = \app\models\Secteur::find()->where(['sect_id' => \Yii::$app->request->get('ProducteurSearchSectoriel')['secteursID']])->one()->sect_nom;
                if(strlen($key) > 12 && substr($key, -12) == '-includeNull' ) { 
                    if(array_key_exists(substr($key,0, -12), \Yii::$app->request->get()) && \Yii::$app->request->get(substr($key,0, -12)) != '') { // if not only null has been selected
                        continue;
                        } else {     // if only null has been selected
                            $query->andWhere(['in', 'producteur.cont_id', static::getContIDNull(substr(substr($key,0,-12), 8), $secteur)]);
                            continue;
                        }
                }
                $key = substr($key, 8);
                $type = \app\controllers\ChampsectController::getType($key, $secteur);
                $ChampCle = ChampSectorielCle::find()->where(['sect_id' => \Yii::$app->request->get('ProducteurSearchSectoriel')['secteursID'], 'champ_sectoriel_cle_field' => $key])->one();
                
                switch ($type) {
                    case 1:
                        $contID = ArrayHelper::map(ChampSectorielValeur::find()->where(['champ_sectoriel_cle_id' => $ChampCle->champ_sectoriel_cle_id, 'champ_sectoriel_valeur_val' => $get])->all(), 'cont_id', 'cont_id');
                        if (array_key_exists('SPECIFIC' . $key . '-includeNull', \Yii::$app->request->get())) { // if null value has been selected too 
                            $query->andWhere(['or', ['in', 'producteur.cont_id', $contID], ['in', 'producteur.cont_id', static::getContIDNull($key, $secteur)]]);
                        } else {
                            $query->andWhere(['in', 'producteur.cont_id', $contID]);
                        }
                        break;
                    case 2: 
                        $contID = ArrayHelper::map(ChampSectorielValeur::find()->where(['champ_sectoriel_cle_id' => $ChampCle->champ_sectoriel_cle_id, 'champ_sectoriel_valeur_val' => $get])->all(), 'cont_id', 'cont_id');
                        if (array_key_exists('SPECIFIC' . $key . '-includeNull', \Yii::$app->request->get())) { // if null value has been selected too 
                            $query->andWhere(['or', ['in', 'producteur.cont_id', $contID], ['in', 'producteur.cont_id', static::getContIDNull($key, $secteur)]]);
                        } else {
                            $query->andWhere(['in', 'producteur.cont_id', $contID]);
                        }
                        break;
                    case 3:
                        $contID = ArrayHelper::map(ChampSectorielValeur::find()->where(['champ_sectoriel_cle_id' => $ChampCle->champ_sectoriel_cle_id])->andWhere(['like', 'champ_sectoriel_valeur_val',$get])->all(), 'cont_id', 'cont_id');
                        if (array_key_exists('SPECIFIC' . $key . '-includeNull', \Yii::$app->request->get())) { // if null value has been selected too 
                            $query->andWhere(['or', ['in', 'producteur.cont_id', $contID], ['in', 'producteur.cont_id', static::getContIDNull($key, $secteur)]]);
                        } else {
                            $query->andWhere(['in', 'producteur.cont_id', $contID]);
                        }
                        break;
                    case 4:
                        $signe = \Yii::$app->request->get('SPECIFIC'.$key . '-operator');
                        if ($signe == '=') {
                            $contID = ArrayHelper::map(ChampSectorielValeur::find()->select('cont_id')->where(['champ_sectoriel_cle_id' => $ChampCle->champ_sectoriel_cle_id, 'champ_sectoriel_valeur_val' => $get])->all(), 'cont_id', 'cont_id');
                        } else {
                            $contID = ArrayHelper::map(ChampSectorielValeur::find()->select('cont_id')->where(['champ_sectoriel_cle_id' => $ChampCle->champ_sectoriel_cle_id])->andWhere([$signe, 'champ_sectoriel_valeur_val' , $get])->all(), 'cont_id', 'cont_id');
                        }
                        if (array_key_exists('SPECIFIC' . $key . '-includeNull', \Yii::$app->request->get())) { // if null value has been selected too 
                            $query->andWhere(['or', ['in', 'producteur.cont_id', $contID], ['in', 'producteur.cont_id', static::getContIDNull($key, $secteur)]]);
                        } else {
                            $query->andWhere(['in', 'producteur.cont_id', $contID]);
                        }
                        break;
                    case 5:
                        if(\Yii::$app->request->get('SPECIFIC'.$key.'-operator') == 'AND') {
                            $contID = ArrayHelper::map(ChampSectorielValeur::find()->where(['AND',['champ_sectoriel_cle_id' => $ChampCle->champ_sectoriel_cle_id],['like','champ_sectoriel_valeur_val' , \app\controllers\ChampsectController::getSQLFormatAnd($key,$get,$secteur), false]])->all(), 'cont_id', 'cont_id');
                        } else {
                            $subQuery = array();
                            array_push($subQuery, 'or');
                            foreach ($get as $g) {
                                array_push($subQuery, ['like','champ_sectoriel_valeur_val',  \app\controllers\ChampsectController::getSQLFormatOr($key, $g, $secteur), false]);
                            }
                            $contID = ArrayHelper::map(ChampSectorielValeur::find()->andwhere(['AND',['champ_sectoriel_cle_id' => $ChampCle->champ_sectoriel_cle_id], $subQuery])
                                    ->all(), 'cont_id', 'cont_id');
                        }
                        if (array_key_exists('SPECIFIC' . $key . '-includeNull', \Yii::$app->request->get())) { // if null value has been selected too 
                            $query->andWhere(['or', ['in', 'producteur.cont_id', $contID], ['in', 'producteur.cont_id', static::getContIDNull($key, $secteur)]]);
                        } else {
                            $query->andWhere(['in', 'producteur.cont_id', $contID]);
                        }
                        break;
                    
                }
            }
        }
        return $dataProvider;
    }
    
    public static function getContIDNull($key, $secteur) {
        $subQuery = ChampSectorielValeur::find()->select('cont_id')
                ->where(['champ_sectoriel_cle_id' =>
                    ChampSectorielCle::find()
                        ->where(['sect_id' => \Yii::$app->request->get('ProducteurSearchSectoriel')['secteursID'], 'champ_sectoriel_cle_field' => $key])
                        ->one()->champ_sectoriel_cle_id])
                ->all();
        
        $contIDNull = ArrayHelper::map(Producteur::find()->joinWith('champSectorielValeurs')->joinWith('secteurs')
                                ->where(['secteur.sect_nom' => $secteur])
                                ->andWhere(['not in', 'producteur.cont_id', ArrayHelper::map($subQuery, 'cont_id', 'cont_id')])
                                ->all(), 'cont_id', 'cont_id');
        return $contIDNull;
    }

}
