<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Associe */

$this->title = 'Mise à jour de '. $contactModel->cont_civilite  .' '. $contactModel->cont_nom .' '. $contactModel->cont_prenom;
$this->params['breadcrumbs'][] = ['label' => 'Associes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cont_id, 'url' => ['view', 'id' => $model->cont_id]];
$this->params['breadcrumbs'][] = 'Mise à jour';
?>
<div class="associe-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contactModel' => $contactModel
    ]) ?>

</div>
