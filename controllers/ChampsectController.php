<?php

namespace app\controllers;

class ChampsectController extends \yii\web\Controller{
    
    private static $path = '/config/ChampSect/';
    
    public function actionIndex() {
        if (!\Yii::$app->user->getUser()->getIsAdmin())
            throw new \yii\web\NotFoundHttpException('La page demandée n\'existe pas.');
        return $this->render('index');
    }
    
    public function actionCreate($sectID) {
        $post = \Yii::$app->request->post();
        $champSect = new \app\models\ChampSectorielCle();
        if ($champSect->load($post)) {
            $champSect->champ_sectoriel_cle_field = $this->CleanLabel($champSect->labelField);
            //check if allreadyExist
            if ($this->allreadyExist($champSect, $sectID)) {
                \Yii::$app->session->setFlash('error', "Erreur, Ce champ existe déjà !");
                        return $this->render('create',[
                            'sectID' => $sectID,
                            'champSect' => $champSect
                        ]);
            }
            //check if empty dynamic fields (options for lists)
            if(isset($post['DynamElemField']) && ($champSect->type == '2' || $champSect->type == '5')) {
                foreach ($post['DynamElemField'] as $dynamField) {
                    if ($dynamField == null || $dynamField == '') {
                        \Yii::$app->session->setFlash('error', "Erreur concernant les éléments de la liste !");
                        return $this->render('create',[
                            'sectID' => $sectID,
                            'champSect' => $champSect
                        ]);
                    }
                }
            }
            
            if ($sectID == 'all') { // champ transversal
                $fileName = '..' . static::$path . $champSect->champ_sectoriel_cle_field . '.php';
                switch ($champSect->type) {
                    case 1: $type = 'TINYINT(1)';
                        break;
                    case 2: $type = 'TINYINT(4)';
                        break;
                    case 3: $type = 'varchar(35)';
                        break;
                    case 4: $type = 'int';
                        break;
                    case 5: $type = 'varchar(20)';
                        break;
                }
                $command = \Yii::$app->db->createCommand();
                $command->addColumn('travailler', $champSect->champ_sectoriel_cle_field, $type);
                $command->execute();
            } else { // champ spécifique
                $secteurName = \app\models\Secteur::findOne($sectID)->sect_nom;
                $fileName = '..' . static::$path . $secteurName . '/' . $champSect->champ_sectoriel_cle_field . '.php';
                $champSect->sect_id = $sectID;
                $champSect->save();
            }
//                    create php file
            if ($champSect->type == '2' || $champSect->type == '5') {
                $values = implode('","', $post['DynamElemField']);
                $fileContent = "<?php

                                return [
                                'label' => \"" . $champSect->labelField . "\",
                                'type' => " . $champSect->type . ",
                                'value' =>[\"" . $values . "\"]
                                ];"
                ;
            } else {
                $fileContent = "<?php

                                return [
                                 'label' => \"" . $champSect->labelField . "\",
                                'type' => " . $champSect->type . "
                                ];"
                ;
            }
            file_put_contents($fileName, $fileContent);
            return $this->redirect(['index', 'secteurID' => $sectID]);
        }
        return $this->render('create',[
            'sectID' => $sectID,
            'champSect' => $champSect
                ]);
    }
    
    public function actionUpdate($attr, $secteurName = null) {
        if ($secteurName == null) {
            $fileName = '..' . static::$path . $attr . '.php';
        } else {
            $fileName = '..' . static::$path .'/'.$secteurName.'/'. $attr . '.php';
        }
        $file = require ($fileName);
        
        $post = \Yii::$app->request->post();
        if ($post) {
            $values = implode('","', $file['value']);
            $values .= '","'. $post['newElem'] ;
                $fileContent = "<?php

                                return [
                                'label' => \"" . $file['label'] . "\",
                                'type' => " . $file['type'] . ",
                                'value' =>[\"" . $values . "\"]
                                ];"
                ;
            file_put_contents($fileName, $fileContent);
            return $this->refresh();
        }
        
        return $this->render('update',[
            'file' => $file,
            ]);
    }
      
    public function actionDelete($attr, $sect_id = null) {
        if(!$sect_id) {
        $command = \Yii::$app->db->createCommand();
        $command->dropColumn('travailler', $attr);
        $command->execute();
        
        $fileName = '..' . static::$path . $attr . '.php';
        unlink($fileName);
        
        return $this->redirect(['index', 'secteurID' => 'all']);
        } 
        \app\models\ChampSectorielCle::find()->where(['sect_id' => $sect_id , 'champ_sectoriel_cle_field' => $attr])->one()->delete();
        $secteurName = \app\models\Secteur::find()->select('sect_nom')->where(['sect_id' => $sect_id])->one()->sect_nom;
        $fileName = '..' . static::$path .'/'.$secteurName .'/'.$attr . '.php';
        unlink($fileName);
        return $this->redirect(['index', 'secteurID' => $sect_id]);
    }

    public static function allreadyExist($champsect, $sectID) {
        if ($sectID == 'all') {
            if(in_array($champsect->champ_sectoriel_cle_field, \app\models\Travailler::getTableSchema()->columnNames)) {
                return true;
            }
        } else {
            if (\app\models\ChampSectorielCle::find()->where(['champ_sectoriel_cle_field' => $champsect->champ_sectoriel_cle_field, 'sect_id' => $sectID])->one() != null) {
                return true;
            }
        }
        return false;
    }

    //function called to interpret the values stored in db
    public static function getValue($champ, $val, $secteur = null) {
        if ($secteur != null) {
            $path = static::$path . $secteur . '/';
        } else {
            $path = static::$path;
        }
        if (file_exists(\Yii::$app->basePath . $path. $champ . '.php') == false) {
        } else {
            $value = array();
            $file = require (\Yii::$app->basePath . $path . $champ . '.php');
            
            $value['k'] = $file['label'];
            $value['t'] = $file['type'];
            
            if ($val === null) {
                $value['v'] = "inconnu";
            } else {
                if ($file['type'] == 1) {
                    if ($val == 0) {
                        $value['v'] = 'non';
                    } else {
                        $value['v'] = 'oui';
                    }
                } elseif ($file['type'] == 2) {
                    try{
                    $value['v'] = $file['value'][$val];
                    } catch (\yii\base\ErrorException $e) {
                        throw new \yii\base\ErrorException($e->getMessage());
                    }
                } elseif ($file['type'] == 5) {
                    if((int)$val == 0) {
                        $value['v'] = 'aucun';
                    } else {
                       $value['v'] = '';
                        $i = 0;
                        foreach (str_split($val) as $char) {
                            if($char == '1') {
                               $value['v'] .=  $file['value'][$i] . '; ';
                            }
                            $i++;
                        }
                        $value['v'] = substr($value['v'], 0, -2); // just to delete the last ';'
                    }
                } else {
                    
                    $value['v'] = $val;
                }
            }
            return $value;
        }
    }
        //function called to have the corect label of the field
    public static function getLabel($champ, $secteur = null) {
        if ($secteur != null) {
            $path = static::$path . $secteur . '/';
        } else {
            $path = static::$path;
        }
        if (file_exists(\Yii::$app->basePath . $path . $champ . '.php') == false) {
        } else {
            $file = require (\Yii::$app->basePath . $path . $champ . '.php');
            
            return $file['label'];
        }
    }

        //return all possibles values
    public static function getOptions($champ, $secteur = null ) {
        if ($secteur != null) {
            $path = static::$path . $secteur . '/';
        } else {
            $path = static::$path;
        }
        if (file_exists(\Yii::$app->basePath . $path . $champ . '.php') == false) {
        } else {
            
            $file = require (\Yii::$app->basePath . $path . $champ . '.php');
            return $file['value'];
        }
    }
        //function called by search forms
    public static function getFields() {
        $files = glob(\Yii::$app->basePath . ChampsectController::$path.'*.php', GLOB_BRACE);
        $fields = array();
        $order = [5,12,7,2,8,10,11,13,1,9,6,4,3];
        foreach($order as $index) {
            $key = explode('/',rtrim($files[$index-1],'/'));
            $key = substr(end($key),0,-4); 
            $fields[$key] = require($files[$index-1]);
        }
        return $fields;
    }
    
    public static function getSpecificsFields($secteur) {
        $path = static::$path . $secteur . '/';
        $files = glob(\Yii::$app->basePath . $path.'*.php', GLOB_BRACE);
        $fields = array();
        foreach($files as $file) {
            $key = explode('/',rtrim($file,'/'));
            $key = substr(end($key),0,-4); 
            $fields[$key] = require($file);
        }
        return $fields;
    }


    //function called to set checked checkboxes on edit
    public static function getSelectedValues($v) {
        $selectedVals = array();
        $i = 0;
        foreach (str_split($v) as $char) {
            if($char == '1') {
                array_push($selectedVals, $i);
            }
            $i++;
        }
        return $selectedVals;
    }
    
    public static function getType($k, $secteur = null) {
        if ($secteur != null) {
            $path = static::$path . $secteur . '/';
        } else {
            $path = static::$path;
        }
        $file = require (\Yii::$app->basePath . $path . $k . '.php');
        return $file['type'];
    }
    
    //transforms array of values to string for DB
    public static function getSQLFormatAnd($key, $values, $secteur = null) {
        $sqlFormat = array();
        $sqlFormat = array_fill(0, count(ChampsectController::getOptions($key, $secteur)), '_');
        foreach($values as $value) {
            $sqlFormat[$value] = 1;
        }
        return (implode('',$sqlFormat));
    }
    
    public static function getSQLFormatOr($key, $value, $secteur = null) {
        $sqlFormat = array();
        $sqlFormat = array_fill(0, count(ChampsectController::getOptions($key, $secteur)), '_');
            $sqlFormat[$value] = 1;
        return (implode('',$sqlFormat));
    }
    
    private function CleanLabel($label) {
         $cleaner = ['Á' => 'a', 'Â' => 'a', 'Ä' => 'a', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ä' => 'a', '@' => 'a',
		'È' => 'e', 'É' => 'e', 'Ê' => 'e', 'Ë' => 'e', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', '€' => 'e',
		'Ì' => 'i', 'Í' => 'i', 'Î' => 'i', 'Ï' => 'i', 'ì' => 'i', 'í' => 'i', 'î' => 'i', 'ï' => 'i',
		'Ò' => 'o', 'Ó' => 'o', 'Ô' => 'o', 'Ö' => 'o', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'ö' => 'o',
		'Ù' => 'u', 'Ú' => 'u', 'Û' => 'u', 'Ü' => 'u', 'ù' => 'u', 'ú' => 'u', 'û' => 'u', 'ü' => 'u', 'µ' => 'u',
		'Œ' => 'oe', 'œ' => 'oe',' ' => '_',
		'$' => 's'];

	$label = strtr($label, $cleaner);
	$label = preg_replace('#[^A-Za-z0-9]+#', '_', $label);
	$label = trim($label, '_');
	$label = strtolower($label);

	return $label;
    }
}
