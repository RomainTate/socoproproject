<?php

namespace app\controllers;

class GererController extends \yii\web\Controller
{

    public function actionUpdate($id) {
        $posts = \Yii::$app->request->post('gerer');
        \app\models\Gerer::deleteAll(['user_id' => $id]);

        if ($posts) {
            foreach ($posts as $post) {
                $gerer = new \app\models\Gerer();
                $gerer->user_id = $id;
                $gerer->sect_id = $post;
                $gerer->save();
            }
        }
        \Yii::$app->session->setFlash('success', "L'assignation à été mise à jour !");
        return $this->redirect(['/user/admin/index', 'id' => $id]);
    }

}
