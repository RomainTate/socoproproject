<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "secteur".
 *
 * @property integer $sect_id
 * @property string $sect_nom
 *
 * @property Gerer[] $gerers
 * @property User[] $users
 * @property Travailler[] $travaillers
 * @property Contact[] $conts
 */
class Secteur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'secteur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sect_nom'], 'required'],
            [['sect_nom'], 'string', 'max' => 20],
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sect_id' => 'Sect ID',
            'sect_nom' => 'Sect Nom',
            'sect' => 'Secteur'
        ];
    }
    
    public static function getLabelAttribute($attribute) {
        $sect = New Secteur();
        return $sect->getAttributeLabel($attribute);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGerers()
    {
        return $this->hasMany(Gerer::className(), ['sect_id' => 'sect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id'])->viaTable('gerer', ['sect_id' => 'sect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravaillers()
    {
        return $this->hasMany(Travailler::className(), ['sect_id' => 'sect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['cont_id' => 'cont_id'])->viaTable('travailler', ['sect_id' => 'sect_id']);
    }
    public function getProducteurs()
    {
        return $this->hasMany(Producteur::className(), ['cont_id' => 'cont_id'])->viaTable('travailler', ['sect_id' => 'sect_id']);
    }
    public function getExploitations()
    {
        return $this->hasMany(Exploitation::className(), ['entr_id' => 'entr_id'])->viaTable('traiter', ['sect_id' => 'sect_id']);
    }
    
    public static function getSecteursNames($secteursID) {
        $secteursNames = [];
        foreach ($secteursID as $secteurID) {
            array_push($secteursNames, Secteur::find()->where(['sect_id' => $secteurID])->one()->sect_nom);
        }
        return $secteursNames;
    }
    
    public function getChampSectorielCles()
    {
        return $this->hasMany(ChampSectorielCle::className(), ['sect_id' => 'sect_id'])->orderBy(['priority' => SORT_ASC]);
    }
    
//    public function getDetails() {
//        return $this->hasOne(Travailler::className(), ['sect_id' => 'sect_id']);
//    }
}
