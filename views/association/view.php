<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Association */

$this->title = $model->entreprise->entr_nom;
$this->params['breadcrumbs'][] = ['label' => 'Associations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="association-view">
    <h1 class="pull-left">Profil de l'association <?= $model->entreprise->entr_nom ?></h1>
    <div class="pull-right">
        <?php
        if (Yii::$app->user->can('deleteAssociation'))
            echo Html::a('Supprimer l\'association', ['delete', 'id' => $model->entr_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Êtes-vous sûr de vouloir supprimer cette association?',
                    'method' => 'post',
                ],
            ])
            ?>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
            <div class="x_panel" >
                <div class="x_content">
                    <div class="img-profil">
                        <img src="../css/images/teamwork.png" alt="producteur" />
                    </div>

                    <h3> <?= $model->entrepriseNom ?> </h3>
                    <div class="mainInfos">

                        <p> <i class="fa fa-legal" aria-hidden="true"></i>
                            <?php if ($model->entrepriseFormeJuridique) : ?>
                                Forme juridique : <?= $model->entrepriseFormeJuridique ?> 
                            <?php else : ?>
                                Forme Juridique inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createAssociation'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/entreprise/quickupdate', 'id' => $model->entr_id, 'attribute' => 'entr_forme_juridique']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p>

                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->entrepriseTelephonePrincipal) : ?> 
                                <?= $model->entrepriseTelephonePrincipal ?>
                            <?php else : ?>
                                Numéro GSM inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createAssociation'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/entreprise/quickupdate', 'id' => $model->entr_id, 'attribute' => 'entr_telephone_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                            <?php if ($model->entrepriseEmailPrincipal) : ?>
                                <?= $model->entrepriseEmailPrincipal ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createAssociation'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/entreprise/quickupdate', 'id' => $model->entr_id, 'attribute' => 'entr_email_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>

                        <p> <i class="fa fa-globe" aria-hidden="true"></i> 
                            <?php if ($model->entrepriseSiteInternet) : ?>
                                <?= $model->entrepriseSiteInternet ?> 
                            <?php else : ?>
                                Site internet inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createAssociation'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/entreprise/quickupdate', 'id' => $model->entr_id, 'attribute' => 'entr_site_internet']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p>

                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> <?php if ($model->entreprise->adresse) : ?> 
                                <?=
                                $model->entreprise->adresse->adr_numero . ' ' . $model->entreprise->adresse->adr_rue . ' ' .
                                $model->entreprise->adresse->adr_code_postal . ' ' . $model->entreprise->adresse->adr_ville
                                ?> 
                                <?php
                                if (Yii::$app->user->can('createAssociation'))
                                    echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(\yii\helpers\Url::home().'/adresse/update?id=' . $model->entreprise->adr_id),
                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                                ?>

                            <?php else : ?>
                                Adresse inconnue 
                                <?php
                                if (Yii::$app->user->can('createAssociation'))
                                    echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(\yii\helpers\Url::home().'/adresse/create?contactId&entr_id=' . $model->entreprise->entr_id),
                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                                ?>
                            <?php endif; ?></p>
                    </div>
                    <?php
                    if (Yii::$app->user->can('createAssociation'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations de l\'association', ['value' => \yii\helpers\Url::to(['/association/update', 'id' => $model->entr_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="x_panel secondInfos">
                <div class="x_content">
                    <h2>Autres informations </h2> 

                    <ul class="autres-infos">
                        <li>
                            Type d'association : <?php echo $model->association_type == null ? 'inconnue' : $model->association_type ?>
                        </li>
                    </ul>
                    <?php
                    if (Yii::$app->user->can('createAssociation'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/association/addmoreinfos', 'entr_id' => $model->entr_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>';
                    ?>
                    <hr />
                    <h2>
                        Contacts associés :
                    </h2>
                    <?php
                    if (!$model->entreprise->contacts) : echo 'Aucun contact associé à cette exploitation';
                    else :
                        echo '<ul>';
                        foreach ($model->entreprise->contacts as $contact) : echo '<li>';
                            if ($contact->getNature() != 'inconnu')
                                echo "<a href='/" . $contact->getNature('url') . "/view?id= $contact->cont_id '> "
                                ?>
                            <?= $contact->cont_civilite . ' ' . $contact->cont_nom . ' ' . $contact->cont_prenom . ' (' . $contact->getNature() . ') </br>' ?>
                            <?php if ($contact->getNature() != 'inconnu') echo '</a>';echo '</li>'; ?>


                            <?php
                        endforeach;
                        echo '</ul>';
                    endif;
                    ?>

                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php
                    echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
                        'entity' => (string) 'association-' . $model->entr_id, // type and id
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home().'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>