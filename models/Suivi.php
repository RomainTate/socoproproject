<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suivi".
 *
 * @property integer $suivi_id
 * @property integer $cont_id
 * @property integer $user_id
 * @property integer $suivi_type_contact
 * @property string $suivi_commentaire
 * @property bool $suivi_suite
 * @property string $suivi_date
 * @property string $suivi_date_update
 *
 * @property Contact $contact
 * @property User $user
 * @property Objets $pbjet[] 
 */
class Suivi extends \yii\db\ActiveRecord
{
    public $suiviobjets_ids;
//    public $suiviobjet_id;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suivi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id', 'suivi_date'], 'required'],
            [['suivi_suite'], 'boolean'],
            [['cont_id', 'user_id', 'suivi_type_contact'], 'integer'],
            [['suivi_commentaire'], 'string'],
            [['suivi_date', 'suiviobjets_ids'], 'safe'],
//            [['suivi_objet'], 'string', 'max' => 75],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'suivi_id' => 'Suivi ID',
            'cont_id' => 'Cont ID',
            'user_id' => 'User ID',
            'suivi_type_contact' => 'Type Contact',
//            'suivi_objet' => 'Objet',
            'suiviobjets_ids' => 'Objet(s)',
            'suivi_commentaire' => 'Commentaire',
            'suivi_date' => 'Date Suivi',
            'suivi_suite' => 'En attente de suite',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
    public function getObjets()
    {
         return $this->hasMany(Objet::className(), ['objet_id' => 'objet_id'])->viaTable('suivi_has_objet', ['suivi_id' => 'suivi_id']);
    }
    
}
