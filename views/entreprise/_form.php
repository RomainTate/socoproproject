<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Entreprise */
/* @var $form yii\widgets\ActiveForm */
if (!isset($model)) { 
    $model = new app\models\Entreprise();
    $action = \yii\helpers\Url::home().'entreprise/create';
} else {
    $action = '';
}
?>

<div class="entreprise-form">
    <div class="x_panel">
        <?php
        $form = ActiveForm::begin([
                    'enableAjaxValidation' => true,
                    'action' => $action,
                    'id' => 'entrepriseForm'
        ]);
        ?>


        <?php if (Yii::$app->request->isAjax) echo yii\bootstrap\Html::hiddenInput('ajax', 1); ?>

        <?= $form->field($model, 'entr_societe')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'entr_telephone')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'entr_email')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'entr_domaine_activite')->textInput(['maxlength' => true]) ?>

        <?php if (isset($contactId)) :?>
            <?= \yii\bootstrap\Html::hiddenInput('contactToAdd', $contactId);?>
            <div class="form-group">
                <?= Html::label('Fonction du contact', 'fonctionContact', ['class' => 'control-label'])?>
                <?= Html::textInput('fonctionContact', null, ['id' => 'fonctionContact','class' => 'form-control', 'maxlength' => 100])?>
            </div>
        <?php endif;?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
