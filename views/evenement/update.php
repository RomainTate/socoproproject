<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Evenement */

$this->title = 'Mettre à jour un évènement';
$this->params['breadcrumbs'][] = ['label' => 'Evenements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->evenement_nom, 'url' => ['view', 'id' => $model->evenement_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="evenement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model
    ]) ?>

</div>
