<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Associe */

$this->title = 'Ajout d\'un associé';
$this->params['breadcrumbs'][] = ['label' => 'Associes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="associe-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contactModel' => $contactModel
    ]) ?>

</div>
