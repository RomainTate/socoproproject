<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Contactdivers */

$this->title = 'Ajouter Contact Divers';
$this->params['breadcrumbs'][] = ['label' => 'Contactdivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactdivers-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <?= $this->render('_form', [
        'model' => $model,
        'contactModel' => $contactModel
    ]) ?>
    </div>
</div>
