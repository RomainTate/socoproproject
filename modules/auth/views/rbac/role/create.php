<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\auth\models\AuthItem */
$this->title = 'Creation Nouveau rôle';
$this->params['breadcrumbs'][] = ['label' => 'gérer les Rôles', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">

    <div class="x_panel">
        <div class="x_title">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>

        <?=
        $this->render('_form', [
            'model' => $model,
        ])
        ?>
    </div>
</div>
