<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Journal */

$this->title = 'Mise à jour du journal ' . $entrepriseModel->entr_nom;
$this->params['breadcrumbs'][] = ['label' => 'Journals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $entrepriseModel->entr_nom, 'url' => ['view', 'id' => $model->entr_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="journal-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render('_form', [
        'model' => $model,
        'entrepriseModel' => $entrepriseModel
    ])
    ?>

</div>
