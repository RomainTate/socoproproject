<?php

namespace app\controllers;

use Yii;
use app\models\Entreprise;
use app\models\EntrepriseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EntrepriseController implements the CRUD actions for Entreprise model.
 */
class EntrepriseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Entreprise models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EntrepriseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entreprise model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entreprise model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        $model = new Entreprise();
        $post = Yii::$app->request->post();

        
        if ($model->load($post) && $model->save()) {
            if(isset($post['contactToAdd'])) { //if user adds Entreprise from contact view
            if(AppartenirController::addContactEntreprise($post['contactToAdd'], $model->entr_id)) {
                $natureContact = \app\models\Contact::findOne($post['contactToAdd'])->getNature();
                return $this->redirect([$natureContact.'/view', 'id' => $post['contactToAdd']]);
            }
        }
            return $this->redirect(['view', 'id' => $model->entr_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Entreprise model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->entr_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
    public function actionQuickupdate($id, $attribute) {
        $model = $this->findModel($id);
        $type = $model->getTableSchema()->getColumn($attribute)->type;
        $post = Yii::$app->request->post();
        if ($post) {
            if ($type =='date') {
                $model->$attribute = date("Y-m-d", strtotime($post['Entreprise'][$attribute]));
            } else {
                $model->$attribute = $post['Entreprise'][$attribute];
            }
            $model->save();
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        } else {
            return $this->renderAjax('quickform/updateform', [
                        'model' => $model,
                        'attribute' => $attribute,
                        'type' => $type
            ]);
        }
    }

    /**
     * Deletes an existing Entreprise model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Entreprise model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entreprise the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entreprise::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
