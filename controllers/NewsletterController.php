<?php

namespace app\controllers;

class NewsletterController extends \yii\web\Controller
{
        public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'delete'],
                        'roles' => ['Administrateur'],
                    ],
                ],
            ]
        ];


        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionCreate() {
        $model = new \app\models\Newsletter();
        $post = \Yii::$app->request->post();
        
        if ($model->load($post)) {
            $model->save();
            return $this->redirect(['/user/admin/indexnewsletters']);
        }
         
        return $this->render('/user/admin/newsletter/create', [
            'model' => $model
        ]);
    }

    public function actionDelete($id) {
        $model = $this->findModel($id);
        $model->delete();
        return $this->redirect(['/user/admin/indexnewsletters']);
    }

    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $post = \Yii::$app->request->post();
        
        if ($model->load($post)) {
            $model->save();
            return $this->redirect(['/user/admin/indexnewsletters']);
        }
        
        return $this->renderAjax('/user/admin/newsletter/update', [
            'model' => $model
        ]);
    }
    
    protected function findModel($id) {
        if (($model = \app\models\Newsletter::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
