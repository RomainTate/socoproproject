<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Producteur */
/* @var $contactModel app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
if (Yii::$app->session->hasFlash('notice')): ?>
    <div class="alert alert-danger alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h5><i class="icon fa fa-times"></i> Attention !</h5>
    <?= Yii::$app->session->getFlash('notice') . '<br />' ?>
    </div>
<?php endif;

if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-warning alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h5><i class="icon fa fa-times"></i> Attention !</h5>
    <?= Yii::$app->session->getFlash('error') . '<br />' ?>
    <?php
        if (\Yii::$app->user->getUser()->getIsAdmin() && !empty($existingContacts)) :
            foreach ($existingContacts as $existingContact) : ?>
                <?= Html::a($existingContact->contact->cont_civilite . ' ' . $existingContact->contact->cont_nom . ' ' . $existingContact->contact->cont_prenom,
                     yii\helpers\Url::to(['/producteur/view', 'id'=>$existingContact->cont_id]), ['target' => 'blank']) . '<br />'?>
            <?php endforeach;
        endif;?>
    </div>
<?php endif; ?>
<div class="producteur-form">

    <?php $form = ActiveForm::begin([
        'id' => 'producteurForm',
        'enableAjaxValidation' => ($model->isNewRecord)
//        'fieldConfig' => [
//                    'template' => " <div class=\"form-group row\">{label}\n<div class=\"col-10\">{input}</div> <div class=\"form-control-focus\"> </div>\n{error}\n</div>",
//                    'horizontalCssClasses' => [
//                        'label' => 'col-md-2 control-label',
//                        'offset' => 'col-sm-offset-4',
//                        'wrapper' => 'col-sm-10',
//                        'error' => 'has-error',
//                        'hint' => 'help-block',
//                    ],
//        ],
    ]); ?>
    <?php if($model->isNewRecord) : ?>
            <?= $form->field($model, 'secteurs')
        ->dropDownList(ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Secteurs*') 
            ?>
    <?php endif ?>
    <?= $form->field($contactModel, 'cont_civilite')->textInput(['maxlength' => true])->dropDownList(['Mr' => 'Mr' , 'Mme' => 'Mme']) ?>
    
    <?= $form->field($contactModel, 'cont_nom')->textInput(['maxlength' => true])->label('Nom') ?>
    <?= $form->field($contactModel, 'cont_prenom')->textInput(['maxlength' => true])->label('Prénom') ?>
    
    <?= $form->field($contactModel, 'cont_langue')->checkboxList(['FR' => 'FR', 'NL' => 'NL', 'DE' => 'DE', 'EN' => 'EN'],['item'=>function ($index, $label, $name, $checked, $value) use ($contactModel){
        if(strpos($contactModel->cont_langue, $value) !== false) {
            $checked = 'checked';
        } else {
            $checked = '';
        }
        return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
    }]); ?>

    <?= $form->field($contactModel, 'cont_telephone_principal')->textInput(['maxlength' => true, 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    <?= $form->field($contactModel, 'cont_gsm_principal')->textInput(['maxlength' => true, 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    <?= $form->field($contactModel, 'cont_email_principal')->textInput(['maxlength' => true]) ?>    
    
    <?php if($model->isNewRecord) :?>
        <?= $form->field($autreInfos, 'site_internet')->textInput(['maxlength' => true]);?>
        <?= $form->field($autreInfos, 'page_facebook')->textInput(['maxlength' => true]);?>
        <?php if ((!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username != 'foire') : ?>
            <div class="form-group customradio">
                <?= Html::label('Membre du college', null, ['class' => 'control-label']); ?>
                <?= Html::radioList('membrecollege', '0', ['0' => 'non', '1' => 'oui']) ?>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'prod_statut_pro')->dropDownList(["1" => "Actif", "0" => "Retraité"],['prompt' => '']) ?>
        <?= $form->field($model, 'prod_numero_producteur')->textInput(['maxlength' => true])->label('Numéro(s) Producteur (séparés par un point virgule)') ?>
        <?= $form->field($model, 'prod_numero_BCE')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'prod_nom_exploitation')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'prod_statut_juridique')->textInput()->dropDownList(["1"=>"SA","2"=>"SPRL","3"=>"SPRLS","4"=>"SCRL","5"=>"SNC","6"=>"ASBL"],['prompt' => '']) ?>
        <?= $form->field($model, 'prod_date_debut_activite')->textInput()->widget(\yii\jui\DatePicker::className(),[
//            'model' => $model,
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
            'options' => [
            'class' => 'form-control',]
        ])
        ?> 
    <?= $this->render('../newsletter/_form', array('model'=>$contactModel)) ?>
    <h3 class="x_title">Adresse</h3>
    <?= $this->render('../adresse/_partial', array('adrModel'=>$adrModel, 'form' => $form)) ?>
    <div class="form-group">
        <?php if ((!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') : ?>
        <?= Html::label('Commentaire', 'prodComment', ['class' => 'control-label']) ?>
        <?= Html::textarea('comment', null, ['id' => 'prodComment', 'class' => 'form-control']) ?>
        <?php endif; ?>
    </div>
    <?php    endif;?>
    <div class="form-group">
        <?php if(\Yii::$app->user->getUser()->getIsAdmin() && Yii::$app->session->hasFlash('error')) echo Html::submitButton($model->isNewRecord ? 'Créer quand même' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value' => 'forceAdd', 'name'=>'forceadd']);
        else echo Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    
    

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');?>