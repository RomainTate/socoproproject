<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Adresse */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="adresse-form">

    <?php $form = ActiveForm::begin([
        'id' => 'adresseForm'
    ]); ?>

    <?= $form->field($model, 'adr_rue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adr_code_postal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adr_ville')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'adr_province')->dropDownList(['ANVERS'=>'ANVERS', 'BRABANT WALLON'=>'BRABANT WALLON', 'BRABANT FLAMAND'=>'BRABANT FLAMAND',
        'BRUXELLES'=>'BRUXELLES', 'FLANDRE OCCIDENTALE'=>'FLANDRE OCCIDENTALE', 'FLANDRE ORIENTALE'=>'FLANDRE ORIENTALE', 'HAINAUT'=>'HAINAUT',
        'LIMBOURG'=>'LIMBOURG', 'LIÈGE'=>'LIÈGE', 'LUXEMBOURG'=>'LUXEMBOURG', 'NAMUR'=>'NAMUR'], ['prompt' => 'inconnue']) ?>
    
    <?php if ($relatedModel && get_class($relatedModel) ==  "app\models\Media") :?>
        <?= $form->field($relatedModel, 'media_pays')->textInput(['maxlength' => true]) ?>
    <?php endif ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer l\'adresse' : 'Mettre à jour l\'adresse', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
