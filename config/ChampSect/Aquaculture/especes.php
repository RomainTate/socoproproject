<?php

    return [
    'label' => 'Espèces',
    'type' => 5,
    'value' =>[ 'Truite arc-en-ciel', 'Truite fario', 'Saumons de fontaine', 'Ombres', 'Autres salmonidés', 'Esturgeons', 'Poissons d\'etangs', 'Vairons', 'Ides mélanottes', 'Goujons', 'Kois', 'Poissons rouges', 'Lottes', 'Autres']
    ];