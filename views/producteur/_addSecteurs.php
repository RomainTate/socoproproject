<?php 
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$selectedSecteurs = yii\helpers\ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
if(empty($selectedSecteurs)) {
    $items = yii\helpers\ArrayHelper::map(app\models\Secteur::find()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom');
} else {
    $items = yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->where('sect_id not in('. implode(',', yii\helpers\ArrayHelper::map($model->secteurs, 'sect_id', 'sect_id')).')')->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom');
}

?>
<div class="addsecteurs-form">

    <?php ActiveForm::begin([
        'id' => 'prodInfosForm'
    ]); ?>
    
    <?php if (empty($selectedSecteurs)) : ?>
        Ce producteur ne fait partie d'aucun secteur.
    <?php else : ?>
        Ce producteur fait partie des secteurs suivants :
        <ul class="fa-ul">
            <?php         foreach ($selectedSecteurs as $sect) :?>
            <li> <i class="glyphicon glyphicon-chevron-right"></i> <?=$sect?> </li>
            <?php         endforeach;?>
        </ul>
    <?php endif?>

    <div class="form-group">
        <?= Html::label('Ajouter un ou des secteurs') ?>
        <?= Html::dropDownList('newSecteurs', NULL, $items, ['class' => 'js-multiple' , 'multiple'=>'multiple'])?>
    </div>
    
    <div class="form-group">
        <?=            Html::submitButton('Modifier', ['class' => 'btn btn-info']) ?>
    </div>
    
    <?php ActiveForm::end()?>
</div>

<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');?>