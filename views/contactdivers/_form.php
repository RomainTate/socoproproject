<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

//$suggestFonction = yii\helpers\ArrayHelper::map(\app\models\Contactdivers::find()->select('divers_fonction')
//        ->where(['not' , ['divers_fonction' => null]])->all() , 'divers_fonction', 'divers_fonction');
//$suggestFonction = array_values($suggestFonction);  
$entrDivers = yii\helpers\ArrayHelper::map(\app\models\Entreprise::find()->select(['entr_societe', 'entr_id'])->all() , 'entr_id', 'entr_societe');
//$suggestLieu = yii\helpers\ArrayHelper::map(\app\models\Lieuprisecontact::find()->select('lieu')
//        ->where(['not' , ['lieu' => null]])->all() , 'lieu', 'lieu');
//$suggestLieu = array_values($suggestLieu);  
/* @var $this yii\web\View */
/* @var $model app\models\Contactdivers */
/* @var $form yii\widgets\ActiveForm */
    if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert <?php echo Yii::$app->user->getUser()->getIsAdmin()? " alert-warning ": " alert-danger ";?> alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h5><i class="icon fa fa-times"></i><?php echo Yii::$app->user->getUser()->getIsAdmin()? " Attention !" : " Erreur !"; ?></h5>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

<div class="contactdivers-form">
    <?php if(!Yii::$app->request->isAjax) :?>
    <div class="col-sm-6">
    <?php            endif;?>
        
   <?php $form = ActiveForm::begin([
        'id' => 'contactdiversForm'
    ]); ?>

    <?= $form->field($contactModel, 'cont_civilite')->textInput(['maxlength' => true])->dropDownList(['Mr' => 'Mr' , 'Mme' => 'Mme']) ?>

    <?= $form->field($contactModel, 'cont_nom')->textInput(['maxlength' => true]) ?>
    <?= $form->field($contactModel, 'cont_prenom')->textInput(['maxlength' => true]) ?>
 
    <?= $form->field($contactModel, 'cont_telephone_principal')->textInput(['maxlength' => true, 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    <?= $form->field($contactModel, 'cont_gsm_principal')->textInput(['maxlength' => true, 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    
    <?= $form->field($contactModel, 'cont_email_principal')->textInput(['maxlength' => true]) ?>
        
    <?php if(!Yii::$app->request->isAjax) :?>
        <div class="form-group customradio">
            <?= Html::label('Membre du college',null, ['class' => 'control-label']);?>
            <?= Html::radioList('membrecollege', '0', ['0' => 'non', '1' => 'oui'] )?>
        </div>
        <div class="form-group">
            <?= Html::label('Membre commission filière',null, ['class' => 'control-label']);?>
            <?= Html::dropDownList('membrefiliere', null, ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple']);?>
        </div>
        <?= $form->field($model, 'secteurs')
            ->dropDownList(ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Intérêts') 
        ?>

        <?php // echo $form->field($model, 'divers_fonction')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
//            'clientOptions' => [
//                'source' => $suggestFonction,
//                'appendTo'=>'#modalContent'
//            ],
//            'options' => [
//                'class' => 'form-control'
//            ],

//        ]); 
        ?>  
            <?php if ($model->isNewRecord) : ?>
                <?php if (Yii::$app->user->can('CreateEntreprise')) : ?>
                    <?=
                    $form->field($model, 'nomentr')->widget(\yii\jui\AutoComplete::classname(), [
                        'clientOptions' => [
                            'source' => array_values($entrDivers)
                        ],
                        'options' => [
                            'class' => 'form-control'
                        ],
                    ]);
                    ?>
                <?php else : ?>
                    <?=
                        $form->field($model, 'nomentr')
                        ->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Entreprise::find()->all(), 'entr_societe', 'entr_societe'), [ 'prompt' => 'Sélectionnez ...'])
                    ?>
                <?php endif; ?>
                <?= $form->field($model, 'fct')->textInput(['maxlength' => 100])->label('Fonction au sein de la société') ?>
    <?php endif; ?>
        
        <?= $form->field($model, 'divers_AS_AG')->dropDownList(['1' => 'AS' , '2' => 'AG' , '3' => 'AS / AG' ], ['prompt' => 'non']);?>
        <?= $this->render('../newsletter/_form', array('model'=>$contactModel)) ?>
    <?php endif;?>
        
    <?php // echo  $form->field($model, 'contDiversLieuContactName')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
//        'clientOptions' => [
//            'source' => $suggestLieu,
//        ],
//        'options' => [
//            'class' => 'form-control'
//        ],

//    ]); ?>


    <div class="form-group">
        <?php if (Yii::$app->session->hasFlash('error')) echo Html::submitButton($model->isNewRecord ? 'Créer quand même' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value' => 'forceAdd', 'name'=>'forceadd']);
              else echo Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end(); ?>
        
    <?php if(!Yii::$app->request->isAjax) :?>
    </div>
    <?php            endif;?>
    
</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');
$this->registerJS("$(function (){
    $('#contactdivers-fct').attr('disabled', 'disabled');
       
   });
   
$('#contactdivers-nomentr').on('input',function(){
 if($(this).val() == null || $(this).val() == '') {
    $('#contactdivers-fct').attr('disabled', 'disabled');
 } else {
    $('#contactdivers-fct').removeAttr('disabled');
    }
});
$('#contactdivers-nomentr').change('input',function(){
 if($(this).val() == null || $(this).val() == '') {
    $('#contactdivers-fct').attr('disabled', 'disabled');
 } else {
    $('#contactdivers-fct').removeAttr('disabled');
    }
});



   
");