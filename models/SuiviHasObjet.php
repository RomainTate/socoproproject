<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "suivi_has_objet".
 *
 * @property integer $suivi_id
 * @property integer $objet_id
 *
 * @property Objet $objet
 * @property Suivi $suivi
 */
class SuiviHasObjet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'suivi_has_objet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['suivi_id', 'objet_id'], 'required'],
            [['suivi_id', 'objet_id'], 'integer'],
            [['objet_id'], 'exist', 'skipOnError' => true, 'targetClass' => Objet::className(), 'targetAttribute' => ['objet_id' => 'objet_id']],
            [['suivi_id'], 'exist', 'skipOnError' => true, 'targetClass' => Suivi::className(), 'targetAttribute' => ['suivi_id' => 'suivi_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'suivi_id' => 'Suivi ID',
            'objet_id' => 'Objet ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getObjet()
    {
        return $this->hasOne(Objet::className(), ['objet_id' => 'objet_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuivi()
    {
        return $this->hasOne(Suivi::className(), ['suivi_id' => 'suivi_id']);
    }
}
