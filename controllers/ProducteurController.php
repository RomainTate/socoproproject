<?php

namespace app\controllers;

use Yii;
use app\models\Producteur;
use app\models\ProducteurSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ProducteurController implements the CRUD actions for Producteur model.
 */
class ProducteurController extends Controller {

    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search', 'view', 'searchsectoriel', 'details' ],
                        'roles' => ['indexProducteur'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'addmoreinfos', 'quickupdate', 'addexploitation',
                            'addsecteurs', 'updatedetails', 'updatespecificdetails', 'deletedetails', 'deletespecificdetails',
                            'removemembrefiliere', 'addmembrefiliere', 'addothersecteurs', 'removesecteur', 'doublon'],
                        'roles' => ['createProducteur'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteProducteur'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['accept', 'searchactivity'],
                        'roles' => ['Administrateur'],
                    ],
                ],
            ]
        ];


        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'removesecteur' => ['POST'],
                ],
            ],
        ];
    }

    public function actionDetails($id) {
        $model = $this->findModel($id);
        if (!\Yii::$app->user->canSee($model->cont_id))
            throw new NotFoundHttpException('La page demandée n\'existe pas.');

        return $this->render('viewDetails', [
                    'model' => $model,
        ]);
    }

    private function getAttributesToShow() {
        $model = new Producteur();
        $toShow = \Yii::$app->request->get('show');
        $attributes = array();
        foreach ($model->getAttributes(null, ['cont_id', 'prod_sigec', 'prod_nom_exploitation', 'prod_date_debut_activite', 'needConfirm']) as $key => $value) {
            $attribute = array();
            $attribute['attribute'] = $key;
            $attribute['label'] = $model->getAttributeLabel($key);
            $attributes[] = $attribute;
        }
        return $attributes;
    }

    /**
     * Lists all Producteur models.
     * @return mixed
     */
    public function actionIndex($sigec = 0) {
        $searchModel = new ProducteurSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['needConfirm' => 0]);
        $attributes = $this->getAttributesToShow();

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'attributes' => $attributes
        ]);
    }

    public function actionSearch() {
        return $this->actionIndex();
    }
    public function actionSearchactivity() {
        $searchModel = new \app\models\ProducteurSearchActivity();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['needConfirm' => 0]);
        
        return $this->render('_searchActivity', [
            'model' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionSearchsectoriel() {
        $searchModel = new \app\models\ProducteurSearchSectoriel();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['needConfirm' => 0]);

        return $this->render('_searchSectoriel', [
                    'model' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id) {
        $model = $this->findModel($id);
        if ( !\Yii::$app->user->canSee($model->cont_id) || ($model->needConfirm && !\Yii::$app->user->getUser()->getIsAdmin()) || ( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire'))
            throw new NotFoundHttpException('La page demandée n\'existe pas.');
        if ($model->needConfirm != 0) {
            $otherProds = $this->getDoublons($model);
        } else {
            $otherProds = null;
        }
        return $this->render('view', [
                    'model' => $model,
                    'otherProds' => $otherProds
        ]);
    }

    /**
     * Creates a new Producteur model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionOldCreate() {
        $model = new Producteur();
        $model->setScenario('form');
        $contactModel = new \app\models\Contact(['scenario' => 'typeProducteur']);
        $autreInfos = new \app\models\AutreInfos();
        $post = Yii::$app->request->post();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        
        if ($contactModel->load($post) && $model->load($post) && $autreInfos->load($post)) {
            if($contactModel->cont_langue != '') {
                $contactModel->cont_langue = ContactController::setLangue($contactModel->cont_langue);
            }
            if ($post['Producteur']['prod_nom_exploitation'] == '' && $post['Contact']['cont_prenom'] == '' && $post['Contact']['cont_nom'] == '') {
                Yii::$app->session->setFlash('notice', "veuillez renseigner au moins une information entre le nom, le prénom et l'exploitation.");
                return $this->render('create', [
                            'model' => $model,
                            'contactModel' => $contactModel,
                            'autreInfos' => $autreInfos
                ]);
            }
            if ($model->prod_date_debut_activite != '') {
                $model->prod_date_debut_activite = date("Y-m-d", strtotime($model->prod_date_debut_activite));
            }
            $model->setSecteurs($post['Producteur']['secteurs']);
            
            
            if ($post['Producteur']['prod_nom_exploitation'] != '') {
                $checkExploit = \app\models\Contact::find()->joinWith('producteur')->where(['prod_nom_exploitation' => $post['Producteur']['prod_nom_exploitation']])->all();
            } else {
                $checkExploit = null;
            }
            if ($contactModel->alreadyExists() || $checkExploit != null) {
                $existingContacts = \app\models\Contact::find()->where((['cont_prenom' => $contactModel->cont_prenom, 'cont_nom' => $contactModel->cont_nom]))->all();

                if ($checkExploit != null) {
                    if (\Yii::$app->user->getUser()->getIsAdmin()) {
                        if (!isset($post['forceadd'])) {
                            Yii::$app->session->setFlash('error', "Ce contact existe déjà !");
                            return $this->render('create', [
                                        'model' => $model,
                                        'contactModel' => $contactModel,
                                        'existingContacts' => $checkExploit,
                                        'autreInfos' => $autreInfos
                            ]);
                        }
                    }
                    if (!isset($post['forceadd'])) {
                        if (count($checkExploit) > 1) {
                            Yii::$app->session->setFlash('error', "Plusieurs Producteurs avec cet exploitation existent déjà !");
                            return $this->render('create', [
                                        'model' => $model,
                                        'contactModel' => $contactModel,
                                        'autreInfos' => $autreInfos
                            ]);
                        } else {
                            $secteursExistingProd = [];
                            foreach ($checkExploit[0]->secteurs as $secteurExistingProd) {
                                array_push($secteursExistingProd, $secteurExistingProd->sect_id);
                            }
                            $secteursToAdd = [];
                            foreach ($model->secteurs as $secteur) {
                                if (!in_array($secteur, $secteursExistingProd)) {
                                    array_push($secteursToAdd, $secteur);
                                }
                            }
                            if (empty($secteursToAdd)) {
                                Yii::$app->session->setFlash('error', "Un producteur avec ce nom et prénom existe déjà");
                                return $this->render('create', [
                                            'model' => $model,
                                            'contactModel' => $contactModel,
                                            'autreInfos' => $autreInfos
                                ]);
                            } else {
                                Yii::$app->session->setFlash('error', "Ce Producteur existe déjà !");
                                return $this->render('_formAddSecteur', [
                                            'model' => $model,
                                            'contactModel' => $contactModel,
                                            'existingProd' => $checkExploit[0],
                                            'secteursToAdd' => $secteursToAdd
                                ]);
                            }
                        }
                    }
                } else if ($existingContacts != null) {

                    if (\Yii::$app->user->getUser()->getIsAdmin()) {
                        if (!isset($post['forceadd'])) {
                            Yii::$app->session->setFlash('error', "Ce contact existe déjà !");
                            return $this->render('create', [
                                        'model' => $model,
                                        'contactModel' => $contactModel,
                                        'existingContacts' => $existingContacts,
                                        'autreInfos' => $autreInfos
                            ]);
                        }
                    }

                    if (!isset($post['forceadd'])) {
                        $existingProds = [];
                        foreach ($existingContacts as $existingContact) {
                            if ($existingContact->getNature() == 'producteur') {
                                array_push($existingProds, $existingContact);
                            }
                        }
                        if (count($existingProds) > 1) {
                            Yii::$app->session->setFlash('error', "Plusieurs Producteurs avec ce nom et prénom existent déjà !");
                            return $this->render('create', [
                                        'model' => $model,
                                        'contactModel' => $contactModel,
                                        'autreInfos' => $autreInfos
                            ]);
                        } else {
                            $secteursExistingProd = [];
                            foreach ($existingProds[0]->secteurs as $secteurExistingProd) {
                                array_push($secteursExistingProd, $secteurExistingProd->sect_id);
                            }
                            $secteursToAdd = [];
                            foreach ($model->secteurs as $secteur) {
                                if (!in_array($secteur, $secteursExistingProd)) {
                                    array_push($secteursToAdd, $secteur);
                                }
                            }
                            if (empty($secteursToAdd)) {
                                Yii::$app->session->setFlash('error', "Un producteur avec ce nom et prénom existe déjà");
                                return $this->render('create', [
                                            'model' => $model,
                                            'contactModel' => $contactModel,
                                            'autreInfos' => $autreInfos
                                ]);
                            } else {
                                Yii::$app->session->setFlash('error', "Ce Producteur existe déjà !");
                                return $this->render('_formAddSecteur', [
                                            'model' => $model,
                                            'contactModel' => $contactModel,
                                            'existingProd' => $existingProds[0],
                                            'secteursToAdd' => $secteursToAdd
                                ]);
                            }
                        }
                    }
                }
            }
            
            
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
                if ($model->save(false) && ($this->actionLinkSecteurs($model->cont_id, $post['Producteur']['secteurs']))) { // save(false) bypass validation (already did 2 lines ago)
                    //TODO : if no autreInfos : don't create record
                    $autreInfos->cont_id = $model->cont_id;
                    $autreInfos->save();
                    if(array_key_exists('newsletters', $post)) {
                        ContactController::manageNewsletters($model->cont_id, $post['newsletters']);
                    } 
                    if ($post['membrecollege'] == '1') {
                        $membreCollege = new \app\models\MembreCollege();
                        $membreCollege->cont_id = $model->cont_id;
                        $membreCollege->save();
                    }
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                } else {
                    throw new \yii\base\ErrorException; // should never been thrown but we never know
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'contactModel' => $contactModel,
                        'autreInfos' => $autreInfos
            ]);
        }
    }
    
   public function actionCreate() {
        $foire = Yii::$app->user->getUser()->username == 'foire'; // true or false
        $model = new Producteur();
        $contactModel = new \app\models\Contact();
        $adrModel = new \app\models\Adresse();
        $adrModel->setScenario('formContact');
        
        if($foire) {
          $model->needConfirm = 2;
          $contactModel->setScenario('foire');
        } else {
            $model->setScenario('form');
            $contactModel->setScenario('typeProducteur');
        }

        $autreInfos = new \app\models\AutreInfos();
        $post = Yii::$app->request->post();
        
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        
        if ($contactModel->load($post) && $model->load($post) && $autreInfos->load($post)&& $adrModel->load($post)) {
            if($contactModel->cont_langue != '') {
                $contactModel->cont_langue = ContactController::setLangue($contactModel->cont_langue);
            }
            
            if(!$foire) {
                if ($post['Producteur']['prod_nom_exploitation'] == '' && $post['Contact']['cont_prenom'] == '' && $post['Contact']['cont_nom'] == '') {
                    Yii::$app->session->setFlash('notice', "veuillez renseigner au moins une information entre le nom, le prénom et l'exploitation.");
                    return $this->render('create', [
                                'model' => $model,
                                'contactModel' => $contactModel,
                                'autreInfos' => $autreInfos,
                                'adrModel' => $adrModel
                    ]);
                }
            }
            
            if ($model->prod_date_debut_activite != '') {
                $model->prod_date_debut_activite = date("Y-m-d", strtotime($model->prod_date_debut_activite));
            }
            if($post['Producteur']['secteurs'] != '') {
                $model->setSecteurs($post['Producteur']['secteurs']);
            }
            
            if(!$foire) {
                $existingContacts= [];
                $exists = $this->checkExists($post, $existingContacts);
            
                if($exists) {
                    if (\Yii::$app->user->getUser()->getIsAdmin()) {
                            if (!isset($post['forceadd'])) {
                                Yii::$app->session->setFlash('error', $exists);
                                return $this->render('create', [
                                            'model' => $model,
                                            'contactModel' => $contactModel,
                                            'existingContacts' => $existingContacts,
                                            'autreInfos' => $autreInfos,
                                            'adrModel' => $adrModel
                                ]);
                            }
                        }
                    $model->needConfirm = 1;    
                }
            }
            
            if ($model->validate() 
                    && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
                if ($model->save(false) && ($this->actionLinkSecteurs($model->cont_id, $post['Producteur']['secteurs']))) { // save(false) bypass validation (already did 2 lines ago)
                    //TODO : if no autreInfos : don't create record
                    $autreInfos->cont_id = $model->cont_id;
                    $autreInfos->save();
                    if(array_key_exists('newsletters', $post)) {
                        ContactController::manageNewsletters($model->cont_id, $post['newsletters']);
                    }
                    if($adrModel->adr_rue != '' || $adrModel->adr_code_postal != '' || $adrModel->adr_ville != '' || $adrModel->adr_province != '') {
                        $adrModel->save();
                        $contactModel->adr_id = $adrModel->adr_id;
                        $contactModel->save();
                    }
                    if (!$foire && $post['membrecollege'] == '1') {
                        $membreCollege = new \app\models\MembreCollege();
                        $membreCollege->cont_id = $model->cont_id;
                        $membreCollege->save();
                    }
                    if($foire) {
                        if($post['comment'] != '') {
                            $comment = new \rmrevin\yii\module\Comments\models\Comment();
                            $comment->entity = 'producteur-'.$model->cont_id;
                            $comment->text = $post['comment'];
                            $comment->created_by = \dektrium\user\models\User::find()->where(['username' => 'foire'])->one()->id;
                            $comment->updated_by = \dektrium\user\models\User::find()->where(['username' => 'foire'])->one()->id;
                            $comment->save();
                        }
                        Yii::$app->session->setFlash('success', 'Merci de vous être enregistré <br /> Redirection vers la page d\'accueil en cours <br /> <a href="'.\yii\helpers\Url::home().'"> Cliquez ici si vous n\'êtes pas redirigé </a>');
                        return $this->redirect(\yii\helpers\Url::home());
                    }
                    if(!$model->needConfirm) {
                        return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                    } else {
                        Yii::$app->session->setFlash('error', $exists);
                        return $this->redirect(['doublon']);
                    }
                } else {
                    throw new \yii\base\ErrorException; // should never been thrown but we never know
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'contactModel' => $contactModel,
                        'autreInfos' => $autreInfos,
                        'adrModel' => $adrModel
            ]);
        }
    }
    
    public function actionDoublon() {
        return $this->render('needConfirm');
    }

    public function actionAddmoreinfos($contactId) {
        $post = Yii::$app->request->post();
        $model = Producteur::findOne($contactId);
        $autreInfos = ($model->autreinfos) ? $model->autreinfos : new \app\models\AutreInfos();
        
        if ($model->load($post) && $autreInfos->load($post)) {
            if ($model->prod_date_debut_activite != '') {
                $model->prod_date_debut_activite = date("Y-m-d", strtotime($model->prod_date_debut_activite));
            }
            if ($model->save(false)) {
                $autreInfos->cont_id = $model->cont_id;
                $autreInfos->save();
                if ($post['membrecollege'] == '1' && $model->contact->membrecollege == null) {
                    $membreCollege = new \app\models\MembreCollege();
                    $membreCollege->cont_id = $model->cont_id;
                    $membreCollege->save();
                } else if ($post['membrecollege'] == '0' && $model->contact->membrecollege != null) {
                    $membreCollege = \app\models\MembreCollege::find()->where(['cont_id' => $model->cont_id])->one();
                    $membreCollege->delete();
                }
                if ($post['Producteur']['activites'] != '') {
                    \app\models\Avoir::deleteAll(['cont_id' => $model->cont_id]);
                    foreach ($post['Producteur']['activites'] as $activite) {
                        $prodHasActivite = new \app\models\Avoir();
                        $prodHasActivite->cont_id = $model->cont_id;
                        $prodHasActivite->activite_id = $activite;
                        $prodHasActivite->save();
                    }
                } else {
                    if($model->activites) {
                        \app\models\Avoir::deleteAll(['cont_id' => $model->cont_id]);
                    }
                }
                return $this->redirect(['view', 'id' => $contactId]);
            }
        } else {
            return $this->renderAjax('_formInfos', [
                        'model' => $model,
                        'autreInfos' => $autreInfos,
            ]);
        }
    }

    /**
     * Updates an existing Producteur model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) { 
        $model = $this->findModel($id);
        if (!\Yii::$app->user->canSee($model->cont_id))
            throw new NotFoundHttpException('La page demandée n\'existe pas.');
        $contactModel = $model->contact;
        $contactModel->setScenario('typeProducteur');
        $post = Yii::$app->request->post();

        if ($post) {
            $contactModel->load($post);
            $model->load($post);
            if($contactModel->cont_langue != '') {
                $contactModel->cont_langue = ContactController::setLangue($contactModel->cont_langue);
            }
            
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                return $this->redirect(['view', 'id' => $contactModel->cont_id]);
            } else {
                throw new \yii\base\ErrorException;
            }
        }

        if (!Yii::$app->request->isAjax) {
            return $this->render('update', [
                        'model' => $model,
                        'contactModel' => $contactModel
            ]);
        }

        return $this->renderAjax('update', [
                    'model' => $model,
                    'contactModel' => $contactModel
        ]);
    }

    /**
     * Deletes an existing Producteur model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $pageToRedirect = '') {
        if ($this->findModel($id)->delete()) {
            \Yii::$app->session->setFlash('success', "La suppression à été effectuée !");
        } else {
            \Yii::$app->session->setFlash('danger', "Erreur lors de la suppression !");
        }
        
        if($pageToRedirect == 'doublon') {
            return $this->redirect(\yii\helpers\Url::to(['/user/admin/indexdoublon']));
        }
        if($pageToRedirect == 'foire') {
            return $this->redirect(\yii\helpers\Url::to(['/user/admin/indexfoireprod']));
        }
        if (Yii::$app->request->referrer && (strpos(Yii::$app->request->referrer, 'index') !== false)) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Producteur model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Producteur the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Producteur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    //link Prods to Secteurs (only called when newRecord)
    private function actionLinkSecteurs($id, $secteurs) {
        if (is_array($secteurs)) {
            foreach ($secteurs as $secteur) {
                $travail = new \app\models\Travailler();
                $travail->cont_id = $id;
                $travail->sect_id = $secteur;
                if (!$travail->save()) {
                    return false;
                }
            }
        }
        return true;
    }

    //link Prods to Secteurs (only called when NOT newRecord)
    public function actionAddsecteurs() {
        $post = Yii::$app->request->post();

        foreach ($post['secteurs'] as $secteur) {
            $travail = new \app\models\Travailler();
            $travail->cont_id = $post['cont_id'];
            $travail->sect_id = $secteur;
            if (!$travail->save()) {
                return false;
            }
        }
        return $this->redirect(['view', 'id' => $post['cont_id']]);
    }

    public function actionUpdatedetails($cont_id, $sect_id) {
        $post = Yii::$app->request->post();
        $tTable = \app\models\Travailler::find()->where(['cont_id' => $cont_id, 'sect_id' => $sect_id])->one();

        if (isset($post['typeChamp']) && $post['typeChamp'] == 5) { //set default values
            $valToDB = array();
            $valToDB = array_fill(0, count(ChampsectController::getOptions($post['champ'])), '0');
            $tTable->$post['champ'] = (implode('', $valToDB)); // met 0000000 dans la DB
        }

        foreach ($post as $k => $v) {
            if ($k == '_csrf' || $k == 'typeChamp' || $k == 'champ')
                continue;
            if (isset($post['typeChamp']) && $post['typeChamp'] == 4 && is_numeric($v) && (int) $v > 2147483647) {
                Yii::$app->session->setFlash('error', "L'information renseignée n'est pas corecte.");
                $this->redirect(['details', 'id' => $cont_id, 'sect_id' => $sect_id]);
            }

            if (isset($post['typeChamp']) && $post['typeChamp'] == 5) {
                $valToDB = array(); // re initialize array to fill corectly this time
                for ($i = 0; $i < count(ChampsectController::getOptions($k)); $i++) {
                    if (in_array((string) $i, $v)) { //if the checkbox with index $i has been selected (meaning : if $i is in the array $v (post value))
                        array_push($valToDB, '1');
                    } else {
                        array_push($valToDB, '0');
                    }
                }
                $tTable->$k = (implode('', $valToDB));
            } else { //type 1 2 & 3
                $tTable->$k = $v;
            }
        }

        try {
            $tTable->save();
        } catch (\Exception $e) {
            Yii::$app->session->setFlash('error', "L'information renseignée n'est pas corecte.");
        }
        $this->redirect(['details', 'id' => $cont_id, 'sect_id' => $sect_id]);
    }

    public function actionDeletedetails($cont_id, $sect_id, $attr) {
        $tTable = \app\models\Travailler::find()->where(['cont_id' => $cont_id, 'sect_id' => $sect_id])->one();
        $tTable->$attr = null;
        $tTable->save();

        $this->redirect(['details', 'id' => $cont_id, 'sect_id' => $sect_id]);
    }

    public function actionUpdatespecificdetails($cont_id, $sect_id) {
        $post = Yii::$app->request->post();
        $secteur = \app\models\Secteur::find()->select('sect_nom')->where(['sect_id' => $sect_id])->one()->sect_nom;
        if ($post) {
            if (isset($post['typeChamp']) && $post['typeChamp'] == 5) { //set default values
                $valToDB = array();
                $valToDB = array_fill(0, count(ChampsectController::getOptions($post['champ'], $secteur)), '0');
                $champsVal = \app\models\ChampSectorielValeur::find()->joinWith('champSectorielCle')->where(['champ_sectoriel_cle_field' => $post['champ'], 'cont_id' => $cont_id, 'sect_id' => $sect_id])->one();
                if ($champsVal == null) {
                    $champsVal = new \app\models\ChampSectorielValeur();
                    $champsVal->cont_id = $cont_id;
                    $champsVal->champ_sectoriel_cle_id = \app\models\ChampSectorielCle::find()->select('champ_sectoriel_cle_id')->where(['champ_sectoriel_cle_field' => $post['champ'], 'sect_id' => $sect_id])->one()->champ_sectoriel_cle_id;
                }
                $champsVal->champ_sectoriel_valeur_val = (implode('', $valToDB));
            }

            foreach ($post as $k => $v) {

                if ($k == '_csrf' || $k == 'typeChamp' || $k == 'champ')
                    continue;
                if (isset($post['typeChamp']) && $post['typeChamp'] == 4 && is_numeric($v) && (int) $v > 2147483647) {
                    Yii::$app->session->setFlash('error', "L'information renseignée n'est pas corecte.");
                    $this->redirect(['details', 'id' => $cont_id, 'sect_id' => $sect_id]);
                }

                if (!isset($champsVal)) {
                    $champsVal = \app\models\ChampSectorielValeur::find()->joinWith('champSectorielCle')->where(['champ_sectoriel_cle_field' => $k, 'cont_id' => $cont_id, 'sect_id' => $sect_id])->one();
                    if ($champsVal == null) {
                        $champsVal = new \app\models\ChampSectorielValeur();
                        $champsVal->cont_id = $cont_id;
                        $champsVal->champ_sectoriel_cle_id = \app\models\ChampSectorielCle::find()->select('champ_sectoriel_cle_id')->where(['champ_sectoriel_cle_field' => $k, 'sect_id' => $sect_id])->one()->champ_sectoriel_cle_id;
                    }
                }
                if (isset($post['typeChamp']) && $post['typeChamp'] == 5) {
                    $valToDB = array(); // re initialize array
                    for ($i = 0; $i < count(ChampsectController::getOptions($k, $secteur)); $i++) {
                        if (in_array((string) $i, $v)) {
                            array_push($valToDB, '1');
                        } else {
                            array_push($valToDB, '0');
                        }
                    }
                    $champsVal->champ_sectoriel_valeur_val = (implode('', $valToDB));
                } else {
                    $champsVal->champ_sectoriel_valeur_val = $v;
                }
            }
            $champsVal->save();
        }
        $this->redirect(['details', 'id' => $cont_id, 'sect_id' => $sect_id]);
    }

    public function actionDeletespecificdetails($cont_id, $sect_id, $champ_cle_id) {
        \app\models\ChampSectorielValeur::find()->where(['cont_id' => $cont_id, 'champ_sectoriel_cle_id' => $champ_cle_id])->one()->delete();
        $this->redirect(['details', 'id' => $cont_id, 'sect_id' => $sect_id]);
    }
    
    public function actionAddmembrefiliere($id, $sect_id) {
        $newMembreCommission = new \app\models\MembreCommissionFilliere();
        $newMembreCommission->cont_id = $id;
        $newMembreCommission->sect_id = $sect_id;
        $newMembreCommission->save();
        $this->redirect(['details', 'id' => $id, 'sect_id' => $sect_id]);
    }
    public function actionRemovemembrefiliere($id, $sect_id) {
        \app\models\MembreCommissionFilliere::find()->where(['cont_id' => $id, 'sect_id' => $sect_id])->one()->delete();
        $this->redirect(['details', 'id' => $id, 'sect_id' => $sect_id]);
    }
    
    public function actionAddothersecteurs($id) {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        
        if ($post) {
            if(array_key_exists('newSecteurs', $post) && !empty($post['newSecteurs'])) {
                foreach ($post['newSecteurs'] as $secteur) {
                    $travail = new \app\models\Travailler();
                    $travail->cont_id = $id;
                    $travail->sect_id = $secteur;
                    $travail->save();
                }
            }
            return $this->redirect(['view', 'id' => $id]);
        }

        return $this->renderAjax('_addSecteurs', [
                    'model' => $model,
        ]);
    }
    public function actionRemovesecteur($id, $sectID) {
        $model = $this->findModel($id);
        \app\models\Travailler::find()->where(['cont_id' => $id, 'sect_id' => $sectID])->one()->delete();
        if (empty($model->secteurs)) {
            $model->delete();
        }
        return $this->redirect(['view', 'id' => $id]);
    }

    public function checkExists($post, &$existingContacts) {
        $error = '';
        if ($post['Producteur']['prod_nom_exploitation'] != '') {
            $checkExploits = Producteur::find()->where(['prod_nom_exploitation' => $post['Producteur']['prod_nom_exploitation']])->all();
            if ($checkExploits) {
                foreach ($checkExploits as $checkExploit) {
                    $existingContacts[] = $checkExploit;
                }
                $error .= 'Un producteur avec cet exploitation existe déjà <br />';
            }
        }
        
        if($post['Contact']['cont_nom'] != '' && $post['Contact']['cont_prenom'] != '') {
            $checkProds = Producteur::find()->joinWith('contact')->where(['cont_nom' => $post['Contact']['cont_nom'], 'cont_prenom' => $post['Contact']['cont_prenom']])->all();
            if ($checkProds) {
                foreach ($checkProds as $checkProd) {
                    $existingContacts[] = $checkProd;
                }
                $error .= 'Un producteur avec ce nom et ce prénom existe déjà <br />';
            }
        }
        
        return $error;
    }
    
    public function getDoublons($model) {
        $otherProds = [];
        if ($model->prod_nom_exploitation != '') {
            foreach (Producteur::find()->where(['prod_nom_exploitation' => $model->prod_nom_exploitation, 'needConfirm' => 0])
                    ->andWhere(['not', ['producteur.cont_id' => $model->cont_id]])->all() as $v) {
                $otherProds[] = $v;
            }
        }
        if($model->contact->cont_nom && $model->contact->cont_prenom) {
            foreach (Producteur::find()->joinWith('contact')->where(['cont_nom' => $model->contact->cont_nom, 'cont_prenom' => $model->contact->cont_prenom, 'needConfirm' => 0])
                    ->andWhere(['not', ['producteur.cont_id' => $model->cont_id]])->all() as $v) {
                $otherProds[] = $v;
            }
        }
        return $otherProds;
    }
    
    public function actionAccept($id) {
        $model = $this->findModel($id);
        $model->needConfirm = 0;
        $model->save();
        Yii::$app->session->setFlash('notice', "Ce contact à bien été validé <br /> <a href='../user/admin/indexfoireprod'> Cliquez ici pour revenir à la liste des Producteurs en attente de validation </a>");
        return $this->redirect(['view', 'id' => $model->cont_id]);
    }

}
