<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\jui\DatePicker;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $model app\models\ProducteurSearch */
/* @var $form yii\widgets\ActiveForm */
$this->title = 'Recherche par activité';
$this->params['breadcrumbs'][] = $this->title;
$search = false;
$get = Yii::$app->request->get('ProducteurSearchActivity');

if(!empty($get)) {
        $search = true;
}

if(Yii::$app->request->post('export_type')) {
    $exporting = true;
} else {
    $exporting = false;
}
?>

<?php if (!$search) : ?>
<div class="producteur-search-activity">
    <div class="x_panel">
        <h1 class="x_title">
            <?=$this->title?>
        </h1>
    <?php $form = ActiveForm::begin([
        'action' => 'searchactivity',
        'method' => 'get',
    ]); ?>
    <h2 class="x_title">Options globales</h2>

    <?= Html::checkbox('membres', Yii::$app->request->get('membres') ==1 ? true: false,['id' => 'membresCheck'])?>
    <?= Html::label('Membres du collège seulement', 'membresCheck')?>
    
    <?= $form->field($model, 'codepostal')
        ->dropDownList(ArrayHelper::map(\app\models\Adresse::find()->where(['not', ['adr_code_postal' => null]])->orderBy('adr_code_postal')->all(),'adr_code_postal' , 'adr_code_postal')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Code postal')
    ?>
    
    <?= $form->field($model, 'secteursID')
        ->dropDownList(ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Secteurs Producteurs')
    ?>

    <?= $form->field($model, 'secteursOperator')->radioList([1 => 'Tous' , 0 => 'Au choix'], ['itemOptions' => ['disabled' => 'disabled']])->label('Secteurs sélectionnés');?>
    <?= Html::checkbox('sigec', Yii::$app->request->get('sigec') ==1 ? true: false,['id' => 'sigecCheck']) ?>
    <?= Html::label('Inclure les producteurs confidentiels', 'sigecCheck')?>

    <hr />
    <h2 class="x_title">Intéractions</h2>
    <label>Ayant eu au minimum <?= Html::textInput('minInteraction',null,  ['type' => 'number', 'id' => 'minInteraction', 'min' => '0']) ?> intéractions générales</label><br />
        <label>  entre le </label>
            <?= DatePicker::widget([
            'name' => 'interactionMinDate',
            'id' => 'interactionMinDate',
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control']
        ]) ?> 
   <label> et le </label>
            <?= DatePicker::widget([
            'name' => 'interactionMaxDate',
            'id' => 'interactionMaxDate',
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control']
        ]) ?> 
    
    <hr />
    <h2 class="x_title">Suivis</h2>
    <label>Ayant eu au minimum <?= Html::textInput('minSuivi',null,  ['type' => 'number', 'id' => 'minSuivi', 'min' => '0']) ?> intéractions </label><br />
    <label>Ayant eu au minimum <?= Html::textInput('minSuiviTEL',null,  ['type' => 'number', 'id' => 'minSuiviTEL', 'min' => '0']) ?> intéractions de type Téléphoniques</label><br />
    <label>Ayant eu au minimum <?= Html::textInput('minSuiviEMAIL',null,  ['type' => 'number', 'id' => 'minSuiviEMAIL', 'min' => '0']) ?> intéractions de type E-Mail</label><br />
    <label>Ayant eu au minimum <?= Html::textInput('minSuiviVISITE',null,  ['type' => 'number', 'id' => 'minSuiviVISITE', 'min' => '0']) ?> intéractions de type Visite</label><br />
    <label>Ayant eu au minimum <?= Html::textInput('minSuiviFOIREEVENT',null,  ['type' => 'number', 'id' => 'minSuiviFOIREEVENT', 'min' => '0']) ?> intéractions de type Foire/Evènements</label><br />
    <label>  entre le </label>
            <?= DatePicker::widget([
            'name' => 'suiviMinDate',
            'id' => 'suiviMinDate',
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control']
        ]) ?> 
   <label> et le </label>
            <?= DatePicker::widget([
            'name' => 'suiviMaxDate',
            'id' => 'suiviMaxDate',
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control']
        ]) ?> 
    
    <hr />
    <h2 class="x_title">&Eacute;vènements</h2>
    
    <div class="form-group">
        <label>Ayant participé au minimum à <?= Html::textInput('minEvent',null,  ['type' => 'number', 'id' => 'minEvent', 'min' => '0']) ?> &Eacute;vènements </label><br />
        <label>Ayant participé au minimum à <?= Html::textInput('minEventAS',null,  ['type' => 'number', 'id' => 'minEventAS', 'min' => '0']) ?> Assemblées sectorielles </label><br />
        <label>Ayant participé au minimum à <?= Html::textInput('minEventAG',null,  ['type' => 'number', 'id' => 'minEventAG', 'min' => '0']) ?> Assemblées générales </label><br />
        <label>Ayant participé au minimum à <?= Html::textInput('minEventCOLLEGE',null,  ['type' => 'number', 'id' => 'minEventCOLLEGE', 'min' => '0']) ?> Réunions du collège</label><br />
        <label>Ayant participé au minimum à <?= Html::textInput('minEventVOYAGE',null,  ['type' => 'number', 'id' => 'minEventVOYAGE', 'min' => '0']) ?> Voyages-colloques</label><br />
        <label>Ayant participé au minimum à <?= Html::textInput('minEventAUTRE',null,  ['type' => 'number', 'id' => 'minEventAUTRE', 'min' => '0']) ?> &Eacute;vènements Autre </label><br />
       <label> entre le </label>
        <?= DatePicker::widget([
            'name' => 'eventMinDate',
            'id' => 'eventMinDate',
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control']
        ]) ?> 
       <label> et le </label>
        <?= DatePicker::widget([
            'name' => 'eventMaxDate',
            'id' => 'eventMaxDate',
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control']
        ]) ?> 
        <br />
        <label>Ayant participé à <?= Html::dropDownList('eventSpecificOperator',null, ['or' => 'Au moins un des', 'and' => 'Tous les'])?> &Eacute;vènements suivants : </label>
                <?=    Html::dropDownList('eventsSpecific', null, ArrayHelper::map(app\models\Evenement::find()->select(['evenement_id', 'evenement_nom'])->all(), 'evenement_id', 'evenement_nom') ,['class' => 'js-multiple' , 'multiple'=>'multiple']) ?>
        <br />
    </div>
    
    <div class="form-group">
        <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>

</div>
<?php else :?>
<div class="producteur-search-activity">
    <div class="x_panel">
        <h1 class="x_title">
            <?=$this->title?>
        </h1>

<?php 
$gridColumns = [
    'prod_nom_exploitation',
    'contactNom',
    'contactPrenom',
    'codepostal',
    'contactGSMPrincipal',
    'contactEmailPrincipal',
    [
        'label' => 'Secteurs',
        'attribute' => 'secteursID',
        'format' => 'html',
        'filter' => ArrayHelper::map(\app\models\Secteur::find()->asArray()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom'),
        'value' => function($model) {
            $secteurNames = array();
            $secteursArray = ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
            asort($secteursArray);
            foreach ($secteursArray as $secteur) {
                $secteurNames[] = '<span style="display:block;">' . $secteur . '</span>';
            }
            return implode(", ", $secteurNames);
        }
    ],
    [
        'attribute' => 'allSuivi',
        'label' => 'Suivi Total',
        'header' => $exporting ? null: '<a id="allSuiviSort"> Suivis <br />Total </a>'
    ],
    [
        'attribute' => 'allEvent',
        'label' => 'Evènements Total',
        'header' => $exporting ? null: '<a id="allEventSort"> Evènements <br />Total </a>'
    ],
    [
        'attribute' => 'nbInteraction',
        'label' => 'Intéractions Total',
        'header' => $exporting ? null:  '<a id="nbInteractionSort">Intéractions <br />Total </a>'
    ],
];
    ?>
<?php 
$select = $dataProvider->query->select;
//var_dump($select);
$getvars = Yii::$app->request->get();
//var_dump($getvars);
if(array_key_exists('interactionQuery', $select) && ( (isset($getvars['interactionMinDate']) && $getvars['interactionMinDate']) || (isset($getvars['interactionMaxDate']) && $getvars['interactionMaxDate']) ) ) {
    $label = 'Intéractions <br />';
    if (isset($getvars['interactionMinDate']) && $getvars['interactionMinDate']) {
     $label .= 'du '.$getvars['interactionMinDate'].'<br />';
    }
    if (isset($getvars['interactionMaxDate']) && $getvars['interactionMaxDate']) {
     $label .= ' jusqu\'au '.$getvars['interactionMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'interactionQuery', 'label' => str_replace('<br />', '', $label)  ] :
            ['header' => '<a id="interactionQuerySort">'.$label.'</a>', 'attribute' => 'interactionQuery', 'label' => str_replace('<br />', '', $label)  ] ;
        
}

if(array_key_exists('suiviQuery', $select) && ( (isset($getvars['suiviMinDate']) && $getvars['suiviMinDate']) || (isset($getvars['suiviMaxDate']) && $getvars['suiviMaxDate']) ) ) {
    $label = 'Suivis <br />';
    if (isset($getvars['suiviMinDate']) && $getvars['suiviMinDate']) {
     $label .= 'du '.$getvars['suiviMinDate'].'<br />';
    }
    if (isset($getvars['suiviMaxDate']) && $getvars['suiviMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['suiviMaxDate'];
    }
    $gridColumns[] = $exporting ?
            ['attribute' => 'suiviQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="suiviQuerySort">'.$label.'</a>', 'attribute' => 'suiviQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('suiviTelQuery', $select)) {
    $label = 'Suivis<br />Téléphoniques <br />';
    if (isset($getvars['suiviMinDate']) && $getvars['suiviMinDate']) {
     $label .= 'du '.$getvars['suiviMinDate'].'<br />';
    }
    if (isset($getvars['suiviMaxDate']) && $getvars['suiviMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['suiviMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'suiviTelQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="suiviTelQuerySort">'.$label.'</a>', 'attribute' => 'suiviTelQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('suiviEmailQuery', $select)) {
    $label = 'Suivis<br />type E-mail <br />';
    if (isset($getvars['suiviMinDate']) && $getvars['suiviMinDate']) {
     $label .= 'du '.$getvars['suiviMinDate'].'<br />';
    }
    if (isset($getvars['suiviMaxDate']) && $getvars['suiviMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['suiviMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'suiviEmailQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="suiviEmailQuerySort">'.$label.'</a>', 'attribute' => 'suiviEmailQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('suiviVisiteQuery', $select)) {
    $label = 'Suivis<br />type Visite <br />';
    if (isset($getvars['suiviMinDate']) && $getvars['suiviMinDate']) {
     $label .= 'du '.$getvars['suiviMinDate'].'<br />';
    }
    if (isset($getvars['suiviMaxDate']) && $getvars['suiviMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['suiviMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'suiviVisiteQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="suiviVisiteQuerySort">'.$label.'</a>', 'attribute' => 'suiviVisiteQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('suiviFoireEventQuery', $select)) {
    $label = 'Suivis<br />Foire/Evènement <br />';
    if (isset($getvars['suiviMinDate']) && $getvars['suiviMinDate']) {
     $label .= 'du '.$getvars['suiviMinDate'].'<br />';
    }
    if (isset($getvars['suiviMaxDate']) && $getvars['suiviMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['suiviMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'suiviFoireEventQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="suiviFoireEventQuerySort">'.$label.'</a>', 'attribute' => 'suiviFoireEventQuery', 'label' => str_replace('<br />', '', $label) ];
}

if(array_key_exists('eventQuery', $select) && ( (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) || (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) ) ) {
    $label = 'Evènements <br />';
    if (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) {
     $label .= 'du '.$getvars['eventMinDate'].'<br />';
    }
    if (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['eventMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'eventQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="eventQuerySort">'.$label.'</a>', 'attribute' => 'eventQuery', 'label' => str_replace('<br />', '', $label) ];
}

if(array_key_exists('eventASQuery', $select) && ( (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) || (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) ) ) {
    $label = 'Evènements AS<br />';
    if (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) {
     $label .= 'du '.$getvars['eventMinDate'].'<br />';
    }
    if (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['eventMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'eventASQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="eventASQuerySort">'.$label.'</a>', 'attribute' => 'eventASQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('eventAGQuery', $select) && ( (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) || (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) ) ) {
    $label = 'Evènements AG<br />';
    if (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) {
     $label .= 'du '.$getvars['eventMinDate'].'<br />';
    }
    if (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['eventMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'eventAGQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="eventAGQuerySort">'.$label.'</a>', 'attribute' => 'eventAGQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('eventCollegeQuery', $select) && ( (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) || (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) ) ) {
    $label = 'Evènements College<br />';
    if (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) {
     $label .= 'du '.$getvars['eventMinDate'].'<br />';
    }
    if (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['eventMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'eventCollegeQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="eventCollegeQuerySort">'.$label.'</a>', 'attribute' => 'eventCollegeQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('eventVoyageQuery', $select) && ( (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) || (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) ) ) {
    $label = 'Evènements Voyage<br />';
    if (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) {
     $label .= 'du '.$getvars['eventMinDate'].'<br />';
    }
    if (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['eventMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'eventVoyageQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="eventVoyageQuerySort">'.$label.'</a>', 'attribute' => 'eventVoyageQuery', 'label' => str_replace('<br />', '', $label) ];
}
if(array_key_exists('eventAutreQuery', $select) && ( (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) || (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) ) ) {
    $label = 'Evènements Autre<br />';
    if (isset($getvars['eventMinDate']) && $getvars['eventMinDate']) {
     $label .= 'du '.$getvars['eventMinDate'].'<br />';
    }
    if (isset($getvars['eventMaxDate']) && $getvars['eventMaxDate']) {
     $label .= 'jusqu\'au '.$getvars['eventMaxDate'];
    }
    $gridColumns[] = $exporting ? 
            ['attribute' => 'eventAutreQuery', 'label' => str_replace('<br />', '', $label) ]:
            ['header' => '<a id="eventAutreQuerySort">'.$label.'</a>', 'attribute' => 'eventAutreQuery', 'label' => str_replace('<br />', '', $label) ];
}
?>
        <div class="pull-right">
                <label>Exportation :</label>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'target' => ExportMenu::TARGET_POPUP,
//                    'stream' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
//                        'encoding' => 'utf-8'
                    ],
//                    'noExportColumns' => [count($allGridColumns) - 1]
                ]);
                ?>
            </div>
        <?=
        kartik\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'columns' => $gridColumns,
            'responsive' => true,
            'responsiveWrap' => false,
            'rowOptions' => function ($model) {
                if ($model->prod_sigec == '1') {
                    return ['class' => 'sigecGrid'];
                }
            },
        ]);
        ?>
    </div>
</div>

<?php endif ;?>
<?php
$this->registerJs(
    "$('.js-multiple').select2();",
    yii\web\View::POS_READY,
    'select2-prod'
); 
$this->registerJsFile(\yii\helpers\Url::home().'js/prodSearchActivity.js', ['depends' => [yii\web\JqueryAsset::className()]]);