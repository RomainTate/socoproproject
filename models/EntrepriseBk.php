<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entreprise".
 *
 * @property integer $entr_id
 * @property string $entr_nom
 * @property string $entr_forme_juridique 
 * @property string $entr_telephone_principal 
 * @property string $entr_email_principal 
 * @property string $entr_site_internet 
 * @property integer $adr_id
 *
 * @property Appartenir[] $appartenirs
 * @property Contact[] $contacts
 * @property Adresse $adresse
 * 
 * @property Association $association
 * @property Exploitation $exploitation
 * @property Journal $journal
 * @property Traiter[] $traiters
 * @property Secteur[] $sects
 */
class Entreprise extends \yii\db\ActiveRecord
{
    public $nature ;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entreprise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entr_nom'], 'required'],
            [['adr_id'], 'integer'],
            [['entr_nom', 'entr_forme_juridique', 'entr_telephone_principal'], 'string', 'max' => 25],
            [['entr_email_principal'], 'string', 'max' => 40],
            [['entr_site_internet'], 'string', 'max' => 30],
            [['adr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Adresse::className(), 'targetAttribute' => ['adr_id' => 'adr_id']],
            [['nature'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entr_id' => 'Entr ID',
            'entr_nom' => 'Nom',
            'entr_forme_juridique' => 'Forme Juridique',
            'entr_telephone_principal' => 'Telephone',
            'entr_email_principal' => 'Email',
            'entr_site_internet' => 'Site Internet',
            'adr_id' => 'Adr ID',
        ];
    }
       
    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        return parent::beforeSave($insert);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppartenirs()
    {
        return $this->hasMany(Appartenir::className(), ['entr_id' => 'entr_id']);
    }
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['cont_id' => 'cont_id'])->viaTable('appartenir', ['entr_id' => 'entr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdresse()
    {
        return $this->hasOne(Adresse::className(), ['adr_id' => 'adr_id']);
    }
    
    /* héritage */
//    public function getAssociation()
//		   {
//        return $this->hasOne(Association::className(), ['entr_id' => 'entr_id']);
//    }
//
//    public function getExploitation() 
//    {
//        return $this->hasOne(Exploitation::className(), ['entr_id' => 'entr_id']);
//    }
//
//    public function getJournal() {
//        return $this->hasOne(Journal::className(), ['entr_id' => 'entr_id']);
//    }
//    public function getDivers() {
//        return $this->hasOne(Entreprisedivers::className(), ['entr_id' => 'entr_id']);
//    }



    /**
     * @return \yii\db\ActiveQuery 
     */
    
//    public function getTraiters() {
//        return $this->hasMany(Traiter::className(), ['entr_id' => 'entr_id']);
//    }
//    public function getSecteurs() {
//        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable('traiter', ['entr_id' => 'entr_id']);
//    }
    
//    public function getNature() {
//        if ($this->journal) return 'journal';
//        if ($this->exploitation) return 'exploitation';
//        return 'entreprise';
//    }

}
