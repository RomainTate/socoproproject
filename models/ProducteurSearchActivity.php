<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Producteur;
use yii\helpers\ArrayHelper;

/**
 * ProducteurSearch represents the model behind the search form about `app\models\Producteur`.
 */
class ProducteurSearchActivity extends Producteur
{
    public $contactNom;
    public $contactPrenom;
    public $secteursID;
    public $contactCivilite;
    public $contactTelephonePrincipal;
    public $contactGSMPrincipal;
    public $contactEmailPrincipal;
    public $contactAnneeNaissance;
    public $contactCreatedAt;
    public $associesnames;
    public $exploitations;
    public $rue;
    public $localite;
    public $province;
    public $langue;
    public $codepostal;
    /*** search fields **/
    public $contactTelephonePrincipalNotNull = 0;
    public $contactGSMPrincipalNotNull = 0;
    public $contactEmailPrincipalNotNull = 0;
    public $secteursOperator = 0;
    public $prod_activite_principale = '';
    public $prod_collab_echange = '';
    public $prod_porte_parole = '';
    public $prod_accepte_stagiaire = '';
    public $prod_qualite_diff = '';
    /* --- */
//    public $allSuivi;
    public $eventsSpecific;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'prod_activite_principale', 'prod_qualite_diff', 'prod_collab_echange',
                'prod_porte_parole', 'prod_accepte_stagiaire',*/ 'contactGSMPrincipalNotNull','contactTelephonePrincipalNotNull','secteursOperator',
                'contactEmailPrincipalNotNull', 'cont_id', 'prod_sigec', 'prod_numero_BCE', 'prod_statut_pro', 'nbInteraction', 'allSuivi'], 'integer'],
            [['localite', 'province', 'rue', 'prod_numero_producteur'], 'string'],
            [['contactNom' , 'contactPrenom' , 'secteursID', 'contactTelephonePrincipal', 'contactGSMPrincipal', 'contactEmailPrincipal', 'secteursOperator', 
                'contactAnneeNaissance', 'contactTelephonePrincipalNotNull', 'contactGSMPrincipalNotNull',
                /*'exploitations'*/ 'prod_statut_juridique', 'prod_date_debut_activite', 'prod_nom_exploitation', 'localite', 'prod_statut_pro', 'langue', 'codepostal', 'eventsSpecific',
                'nbInteraction', 'allSuivi'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (empty($params)) {
            return new ActiveDataProvider(['query' => Producteur::find()]);
        }
//        var_dump($params);
        if(isset($params['sigec'])) {
            $query = Producteur::find();
        }else {
            $query = Producteur::find()->where(['prod_sigec' => 0]);
        }
        
        $selectFields = ['producteur.cont_id', 'prod_nom_exploitation', 'cp' => 'ca.adr_code_postal', 'producteur.prod_sigec'];

        /* GENERALS INTERACTIONS */
        if($params['minInteraction']) {
            $interactionQuery = "((SELECT COUNT(*) FROM suivi where suivi.cont_id = producteur.cont_id";
            if(isset($params['interactionMinDate']) && $params['interactionMinDate']) {
                $interactionQuery .= " AND suivi_date >= '" . date("Y-m-d", strtotime($params['interactionMinDate']))."'";
            }
            if(isset($params['interactionMaxDate']) && $params['interactionMaxDate']) {
                $interactionQuery .= " AND suivi_date <= '" . date("Y-m-d", strtotime($params['interactionMaxDate']))." 23:59:59'";
            }
            $interactionQuery .= ") + (SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id ";
            if(isset($params['interactionMinDate']) && $params['interactionMinDate']) {
                $interactionQuery .= " AND evenement_date > '" . date("Y-m-d", strtotime($params['interactionMinDate']))."'";
            }
            if(isset($params['interactionMaxDate']) && $params['interactionMaxDate']) {
                $interactionQuery .= " AND evenement_date < '" . date("Y-m-d", strtotime($params['interactionMaxDate']))."'";
            }
            $interactionQuery .= "))";
            $selectFields['interactionQuery'] = $interactionQuery;
        }
        
        /* SUIVIS */
        if($params['minSuivi']) {
            $suiviQuery = "(SELECT COUNT(*) FROM suivi where suivi.cont_id = producteur.cont_id";
            if(isset($params['suiviMinDate']) && $params['suiviMinDate']) {
                $suiviQuery .= " AND suivi_date >= '" . date("Y-m-d", strtotime($params['suiviMinDate']))."'";
            }
            if(isset($params['suiviMaxDate']) && $params['suiviMaxDate']) {
                $suiviQuery .= " AND suivi_date <= '" . date("Y-m-d", strtotime($params['suiviMaxDate']))." 23:59:59'";
            }
            $suiviQuery .= ")";
            $selectFields['suiviQuery'] = $suiviQuery;
        }
        if($params['minSuiviTEL']) {
            $suiviTelQuery = "(SELECT COUNT(*) FROM suivi where suivi.cont_id = producteur.cont_id AND suivi_type_contact = 1" ;
            if(isset($params['suiviMinDate']) && $params['suiviMinDate']) {
                $suiviTelQuery .= " AND suivi_date >= '" . date("Y-m-d", strtotime($params['suiviMinDate']))."'";
            }
            if(isset($params['suiviMaxDate']) && $params['suiviMaxDate']) {
                $suiviTelQuery .= " AND suivi_date <= '" . date("Y-m-d", strtotime($params['suiviMaxDate']))." 23:59:59'";
            }
            $suiviTelQuery .= ")";
            $selectFields['suiviTelQuery'] = $suiviTelQuery;
        }
        if($params['minSuiviEMAIL']) {
            $suiviEmailQuery = "(SELECT COUNT(*) FROM suivi where suivi.cont_id = producteur.cont_id AND suivi_type_contact = 2" ;
            if(isset($params['suiviMinDate']) && $params['suiviMinDate']) {
                $suiviEmailQuery .= " AND suivi_date >= '" . date("Y-m-d", strtotime($params['suiviMinDate']))."'";
            }
            if(isset($params['suiviMaxDate']) && $params['suiviMaxDate']) {
                $suiviEmailQuery .= " AND suivi_date <= '" . date("Y-m-d", strtotime($params['suiviMaxDate']))." 23:59:59'";
            }
            $suiviEmailQuery .= ")";
            $selectFields['suiviEmailQuery'] = $suiviEmailQuery;
        }
        if($params['minSuiviVISITE']) {
            $suiviVisiteQuery = "(SELECT COUNT(*) FROM suivi where suivi.cont_id = producteur.cont_id AND suivi_type_contact = 3" ;
            if(isset($params['suiviMinDate']) && $params['suiviMinDate']) {
                $suiviVisiteQuery .= " AND suivi_date >= '" . date("Y-m-d", strtotime($params['suiviMinDate']))."'";
            }
            if(isset($params['suiviMaxDate']) && $params['suiviMaxDate']) {
                $suiviVisiteQuery .= " AND suivi_date <= '" . date("Y-m-d", strtotime($params['suiviMaxDate']))." 23:59:59'";
            }
            $suiviVisiteQuery .= ")";
            $selectFields['suiviVisiteQuery'] = $suiviVisiteQuery;
        }
        if($params['minSuiviFOIREEVENT']) {
            $suiviFoireEventQuery = "(SELECT COUNT(*) FROM suivi where suivi.cont_id = producteur.cont_id AND suivi_type_contact = 4" ;
            if(isset($params['suiviMinDate']) && $params['suiviMinDate']) {
                $suiviFoireEventQuery .= " AND suivi_date >= '" . date("Y-m-d", strtotime($params['suiviMinDate']))."'";
            }
            if(isset($params['suiviMaxDate']) && $params['suiviMaxDate']) {
                $suiviFoireEventQuery .= " AND suivi_date <= '" . date("Y-m-d", strtotime($params['suiviMaxDate']))." 23:59:59'";
            }
            $suiviFoireEventQuery .= ")";
            $selectFields['suiviFoireEventQuery'] = $suiviFoireEventQuery;
        }
        
        
        /* EVENTS*/
        if($params['minEvent']) {
            $eventQuery = "(SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id ";
            if(isset($params['eventMinDate']) && $params['eventMinDate']) {
                $eventQuery .= " AND evenement_date >= '" . date("Y-m-d", strtotime($params['eventMinDate']))."'";
            }
            if(isset($params['eventMaxDate']) && $params['eventMaxDate']) {
                $eventQuery .= " AND evenement_date <= '" . date("Y-m-d", strtotime($params['eventMaxDate']))."'";
            }
            $eventQuery .= ")";
            $selectFields['eventQuery'] = $eventQuery;
        }
        if($params['minEventAS']) {
            $eventASQuery = "(SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id AND evenement_type = 1";
            if(isset($params['eventMinDate']) && $params['eventMinDate']) {
                $eventASQuery .= " AND evenement_date >= '" . date("Y-m-d", strtotime($params['eventMinDate']))."'";
            }
            if(isset($params['eventMaxDate']) && $params['eventMaxDate']) {
                $eventASQuery .= " AND evenement_date <= '" . date("Y-m-d", strtotime($params['eventMaxDate']))."'";
            }
            $eventASQuery .= ")";
            $selectFields['eventASQuery'] = $eventASQuery;
        }
        if($params['minEventAG']) {
            $eventAGQuery = "(SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id AND evenement_type = 2";
            if(isset($params['eventMinDate']) && $params['eventMinDate']) {
                $eventAGQuery .= " AND evenement_date >= '" . date("Y-m-d", strtotime($params['eventMinDate']))."'";
            }
            if(isset($params['eventMaxDate']) && $params['eventMaxDate']) {
                $eventAGQuery .= " AND evenement_date <= '" . date("Y-m-d", strtotime($params['eventMaxDate']))."'";
            }
            $eventAGQuery .= ")";
            $selectFields['eventAGQuery'] = $eventAGQuery;
        }
        if($params['minEventCOLLEGE']) {
            $eventCollegeQuery = "(SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id AND evenement_type = 3";
            if(isset($params['eventMinDate']) && $params['eventMinDate']) {
                $eventCollegeQuery .= " AND evenement_date >= '" . date("Y-m-d", strtotime($params['eventMinDate']))."'";
            }
            if(isset($params['eventMaxDate']) && $params['eventMaxDate']) {
                $eventCollegeQuery .= " AND evenement_date <= '" . date("Y-m-d", strtotime($params['eventMaxDate']))."'";
            }
            $eventCollegeQuery .= ")";
            $selectFields['eventCollegeQuery'] = $eventCollegeQuery;
        }
        if($params['minEventVOYAGE']) {
            $eventVoyageQuery = "(SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id AND evenement_type = 4";
            if(isset($params['eventMinDate']) && $params['eventMinDate']) {
                $eventVoyageQuery .= " AND evenement_date >= '" . date("Y-m-d", strtotime($params['eventMinDate']))."'";
            }
            if(isset($params['eventMaxDate']) && $params['eventMaxDate']) {
                $eventVoyageQuery .= " AND evenement_date <= '" . date("Y-m-d", strtotime($params['eventMaxDate']))."'";
            }
            $eventVoyageQuery .= ")";
            $selectFields['eventVoyageQuery'] = $eventVoyageQuery;
        }
        if($params['minEventAUTRE']) {
            $eventAutreQuery = "(SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id AND evenement_type = 5";
            if(isset($params['eventMinDate']) && $params['eventMinDate']) {
                $eventAutreQuery .= " AND evenement_date >= '" . date("Y-m-d", strtotime($params['eventMinDate']))."'";
            }
            if(isset($params['eventMaxDate']) && $params['eventMaxDate']) {
                $eventAutreQuery .= " AND evenement_date <= '" . date("Y-m-d", strtotime($params['eventMaxDate']))."'";
            }
            $eventAutreQuery .= ")";
            $selectFields['eventAutreQuery'] = $eventAutreQuery;
        }
        
        $allSuivi = "(SELECT COUNT(*) FROM suivi where suivi.cont_id = producteur.cont_id)";
        $selectFields['allSuivi'] = $allSuivi;
        $allEvent = "(SELECT COUNT(*) FROM participer INNER JOIN evenement ON participer.evenement_id = evenement.evenement_id where participer.cont_id = producteur.cont_id)";
        $selectFields['allEvent'] = $allEvent;
        $nbInteraction = '('.$allSuivi.'+'.$allEvent.')';
        $selectFields['nbInteraction'] = $nbInteraction;
        
        $query->select($selectFields);
        
        if (\app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission') {
            $condition = ['in', 'producteur.cont_id', ArrayHelper::map(Producteur::find()->select('producteur.cont_id')->innerJoin('travailler t', 'producteur.cont_id = t.cont_id')
                    ->innerJoin('secteur s', 't.sect_id = s.sect_id')->innerJoin('gerer g', 's.sect_id = g.sect_id')->where(['g.user_id' => \Yii::$app->user->getId()])->all(),'cont_id','cont_id')];
            $query->andFilterWhere($condition);
        }
        
        $query->joinWith(['contact c', 'contact.adresse ca', 'secteurs', 'associes', 'associes.contact ac', 'contact.membrecollege']);
        
        $query->groupBy('producteur.cont_id');
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->where('0=1');
            return $dataProvider;
        }

        $dataProvider->setSort([
            'attributes' => [
                'contactNom' => [
                    'asc' => ['c.cont_nom' => SORT_ASC],
                    'desc' => ['c.cont_nom' => SORT_DESC],
                ],
                'contactPrenom' => [
                    'asc' => ['c.cont_prenom' => SORT_ASC],
                    'desc' => ['c.cont_prenom' => SORT_DESC],
                ],
                'codepostal' => [
                    'asc' => ['adr_code_postal' => SORT_ASC],
                    'desc' => ['adr_code_postal' => SORT_DESC],
                ],
                'prod_sigec' => [
                    'asc' => ['prod_sigec' => SORT_ASC],
                    'desc' => ['prod_sigec' => SORT_DESC],
                ],
                'prod_nom_exploitation' => [
                    'asc' => ['prod_nom_exploitation' => SORT_ASC],
                    'desc' => ['prod_nom_exploitation' => SORT_DESC]
                ],
                'allSuivi' => [
                    'asc' => ['allSuivi' => SORT_ASC],
                    'desc' => ['allSuivi' => SORT_DESC]
                ],
                'allEvent' => [
                    'asc' => ['allEvent' => SORT_ASC],
                    'desc' => ['allEvent' => SORT_DESC]
                ],
                'nbInteraction' => [
                    'asc' => ['nbInteraction' => SORT_ASC],
                    'desc' => ['nbInteraction' => SORT_DESC]
                ],
                'suiviQuery' => [
                    'asc' => ['suiviQuery' => SORT_ASC],
                    'desc' => ['suiviQuery' => SORT_DESC]
                ],
                'suiviTelQuery' => [
                    'asc' => ['suiviTelQuery' => SORT_ASC],
                    'desc' => ['suiviTelQuery' => SORT_DESC]
                ],
                'suiviEmailQuery' => [
                    'asc' => ['suiviEmailQuery' => SORT_ASC],
                    'desc' => ['suiviEmailQuery' => SORT_DESC]
                ],
                'suiviVisiteQuery' => [
                    'asc' => ['suiviVisiteQuery' => SORT_ASC],
                    'desc' => ['suiviVisiteQuery' => SORT_DESC]
                ],
                'suiviFoireEventQuery' => [
                    'asc' => ['suiviFoireEventQuery' => SORT_ASC],
                    'desc' => ['suiviFoireEventQuery' => SORT_DESC]
                ],
                'eventQuery' => [
                    'asc' => ['eventQuery' => SORT_ASC],
                    'desc' => ['eventQuery' => SORT_DESC]
                ],
                'eventASQuery' => [
                    'asc' => ['eventASQuery' => SORT_ASC],
                    'desc' => ['eventASQuery' => SORT_DESC]
                ],
                'eventAGQuery' => [
                    'asc' => ['eventAGQuery' => SORT_ASC],
                    'desc' => ['eventAGQuery' => SORT_DESC]
                ],
                'eventCollegeQuery' => [
                    'asc' => ['eventCollegeQuery' => SORT_ASC],
                    'desc' => ['eventCollegeQuery' => SORT_DESC]
                ],
                'eventVoyageQuery' => [
                    'asc' => ['eventVoyageQuery' => SORT_ASC],
                    'desc' => ['eventVoyageQuery' => SORT_DESC]
                ],
                'eventAutreQuery' => [
                    'asc' => ['eventAutreQuery' => SORT_ASC],
                    'desc' => ['eventAutreQuery' => SORT_DESC]
                ],
                'interactionQuery' => [
                    'asc' => ['interactionQuery' => SORT_ASC],
                    'desc' => ['interactionQuery' => SORT_DESC]
                ],

            ]
        ]);

        if(isset($params['ProducteurSearch']['prod_sigec'])) {
            $query->andFilterWhere(['prod_sigec' => $params['ProducteurSearch']['prod_sigec']]);
        }
        
        
        // grid filtering conditions
        
            
        if ($this->secteursID != null) {
            if (is_array($this->secteursID)) {
                if ($this->secteursOperator == 1) {
                    foreach ($this->secteursID as $secteur) {
                        $subQuery = Contact::find()->select('contact.cont_id')->joinWith('secteurs')->where(['secteur.sect_id' => $secteur]);
                        $query->andWhere(['producteur.cont_id' => $subQuery]);
                    }
                } else {
                    $query->andFilterWhere(['in', 'secteur.sect_id', $this->secteursID]);
                }
            } else {
                $subQuery = Contact::find()->select('contact.cont_id')->joinWith('secteurs')->where(['secteur.sect_id' => $this->secteursID]);
                $query->andWhere(['producteur.cont_id' => $subQuery]);
            }
        }
           

//        if((isset($params['ProducteurSearch']['langue'])) && is_array($params['ProducteurSearch']['langue'])) {
//            $query->andFilterWhere(['or like', 'c.cont_langue', $params['ProducteurSearch']['langue'] ]);
//        }
        if((isset($params['ProducteurSearchActivity']['codepostal'])) 
                ) {
            $query->andFilterWhere(['or like', 'adr_code_postal', $params['ProducteurSearchActivity']['codepostal'] ]);
        }
        
        if(isset($params['membres']) && $params['membres'] == '1') {
            $query->join('INNER JOIN', 'membre_college', 'c.cont_id = membre_college.cont_id');
        }
       
        if(isset($params['minInteraction']) && $params['minInteraction']) {
            $query->andHaving(['>=', 'interactionQuery', $params['minInteraction']]);
        }
        
        if(isset($params['minSuivi']) && $params['minSuivi']) {
            $query->andHaving(['>=', 'suiviQuery', $params['minSuivi']]);
        }
        if(isset($params['minSuiviTEL']) && $params['minSuiviTEL']) {
            $query->andHaving(['>=', 'suiviTelQuery', $params['minSuiviTEL']]);
        }
        if(isset($params['minSuiviEMAIL']) && $params['minSuiviEMAIL']) {
            $query->andHaving(['>=', 'suiviEmailQuery', $params['minSuiviEMAIL']]);
        }
        if(isset($params['minSuiviVISITE']) && $params['minSuiviVISITE']) {
            $query->andHaving(['>=', 'suiviVisiteQuery', $params['minSuiviVISITE']]);
        }
        if(isset($params['minSuiviFOIREEVENT']) && $params['minSuiviFOIREEVENT']) {
            $query->andHaving(['>=', 'suiviFoireEventQuery', $params['minSuiviFOIREEVENT']]);
        }
        
        if(isset($params['minEvent']) && $params['minEvent']) {
            $query->andHaving(['>=', 'eventQuery', $params['minEvent']]);
        }
        if(isset($params['minEventAS']) && $params['minEventAS']) {
            $query->andHaving(['>=', 'eventASQuery', $params['minEventAS']]);
        }
        if(isset($params['minEventAG']) && $params['minEventAG']) {
            $query->andHaving(['>=', 'eventAGQuery', $params['minEventAG']]);
        }
        if(isset($params['minEventCOLLEGE']) && $params['minEventCOLLEGE']) {
            $query->andHaving(['>=', 'eventCollegeQuery', $params['minEventCOLLEGE']]);
        }
        if(isset($params['minEventVOYAGE']) && $params['minEventVOYAGE']) {
            $query->andHaving(['>=', 'eventVoyageQuery', $params['minEventVOYAGE']]);
        }
        if(isset($params['minEventAUTRE']) && $params['minEventAUTRE']) {
            $query->andHaving(['>=', 'eventAutreQuery', $params['minEventAUTRE']]);
        }
        
        if(isset($params['eventsSpecific']) && $params['eventsSpecific']) {
            $query->joinWith('contact.evenements');
            if ($params['eventSpecificOperator'] == 'and') {
                    foreach ($params['eventsSpecific'] as $event) {
                        $subQuery = Contact::find()->select('contact.cont_id')->joinWith('evenements')->where(['evenement.evenement_id' => $event]);
                        $query->andWhere(['producteur.cont_id' => $subQuery]);
                    }
                } else {
                    $query->andFilterWhere(['in', 'evenement.evenement_id', $params['eventsSpecific']]);
                }
        }

        return $dataProvider;
    }
    

}
