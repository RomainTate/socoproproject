<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entreprise".
 *
 * @property integer $entr_id
 * @property string $entr_societe
 * @property string $entr_telephone
 * @property string $entr_email
 * @property string $entr_domaine_activite
 * @property integer $adr_id
 *
 * @property Appartenir[] $appartenirs
 * @property Contact[] $contacts
 * @property Adresse $adresse
 */
class Entreprise extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'entreprise';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entr_societe'], 'required'],
            [['adr_id'], 'integer'],
            [['entr_societe'], 'unique'],
            [['entr_societe', 'entr_domaine_activite'], 'string', 'max' => 100],
            [['entr_email'], 'string', 'max' => 75],
            [['entr_email'], 'email', 'message' => 'L\'adresse e-mail n\'est pas valide'],
            [['entr_telephone'], 'string', 'max' => 12],
            [['adr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Adresse::className(), 'targetAttribute' => ['adr_id' => 'adr_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entr_id' => 'Entr ID',
            'entr_societe' => 'Societe',
            'entr_telephone' => 'Telephone',
            'entr_email' => 'Email',
            'entr_domaine_activite' => 'Domaine d\'Activite',
            'adr_id' => 'Adr ID',
        ];
    }
    
    public function beforeDelete() {
        if (!empty($this->contacts)) {
            Appartenir::deleteAll(['entr_id' => $this->entr_id]);
        }
        return parent::beforeDelete();
    }
    
    public function afterDelete() {
        if($this->adresse) {
            Adresse::find()->where(['adr_id' => $this->adr_id])->one()->delete();
        }
        return parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppartenirs()
    {
        return $this->hasMany(Appartenir::className(), ['entr_id' => 'entr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['cont_id' => 'cont_id'])->viaTable('appartenir', ['entr_id' => 'entr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdresse()
    {
        return $this->hasOne(Adresse::className(), ['adr_id' => 'adr_id']);
    }
}
