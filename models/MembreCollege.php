<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "membre_college".
 *
 * @property string $membre_date_debut
 * @property integer $cont_id
 *
 * @property Contact $contact
 */
class MembreCollege extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membre_college';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['membre_date_debut'], 'safe'],
            [['cont_id'], 'required'],
            [['cont_id'], 'integer'],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'membre_date_debut' => 'Membre Date Debut',
//            'cont_id' => 'Cont ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
}
