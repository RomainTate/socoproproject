<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Media */

$this->title = 'Mettre à jour Media: ' . $model->media_titre;
$this->params['breadcrumbs'][] = ['label' => 'Media', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->media_titre, 'url' => ['view', 'id' => $model->media_id]];
$this->params['breadcrumbs'][] = 'Mise à jour';
?>
<div class="media-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'fullform' => $fullform
    ]) ?>

</div>
