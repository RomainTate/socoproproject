<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Journaliste;

/**
 * JournalisteSearch represents the model behind the search form about `app\models\Journaliste`.
 */
class JournalisteSearch extends Journaliste
{
    public $contactNom;
    public $contactPrenom;
    public $contactCivilite;
    public $contactTelephonePrincipal;
    public $contactGSMPrincipal;
    public $contactEmailPrincipal;
    public $contactAnneeNaissance;
    public $contactCreatedAt;
    public $contactLangue;    
    public $societemedia;
    public $nommedia;
    public $nomsmedia;
    public $ruemedia;
    public $localitemedia;
    public $sitemedia;
    public $paysmedia;
    public $audiencemedia;
    public $typesupportmedia;
    public $periodicitemedia;
    public $langue;
        /*** ! **/
    public $contactGSMPrincipalNotNull = 0;
    public $contactEmailPrincipalNotNull = 0;
//    public $journaliste_langue = '';
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['journaliste_fonction', /*'journaliste_langue',*/ 'contactNom' ,
                'contactPrenom' , 'contactGSMPrincipal' , 'contactEmailPrincipal','contactAnneeNaissance','contactCreatedAt',
                'nommedia','nomsmedia', 'contactTelephonePrincipal','societemedia', 'typesupportmedia', 'periodicitemedia', 'langue'], 'safe'],
//            [['nommedia'], 'string'],
            [['contactGSMPrincipalNotNull', 'contactEmailPrincipalNotNull' ], 'integer'],
            [['cont_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Journaliste::find();
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        
        $dataProvider->setSort([
            'attributes' => [
                'contactNom' => [
                    'asc' => ['contact.cont_nom' => SORT_ASC],
                    'desc' => ['contact.cont_nom' => SORT_DESC],
                ],
                'contactPrenom' => [
                    'asc' => ['contact.cont_prenom' => SORT_ASC],
                    'desc' => ['contact.cont_prenom' => SORT_DESC],
                ],
                'contact.cont_civilite' => [
                    'asc' => ['contact.cont_civilite' => SORT_ASC],
                    'desc' => ['contact.cont_civilite' => SORT_DESC],
                ],
                'journ_type' => [
                    'asc' => ['journ_type' => SORT_ASC],
                    'desc' => ['journ_type' => SORT_DESC],
                ]
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith('contact'); //Eager loading
            return $dataProvider;
        }

        // grid filtering conditions

        $query->andFilterWhere(['like', 'journaliste_fonction', $this->journaliste_fonction]);
//            ->andFilterWhere(['like', 'journaliste_langue', $this->journaliste_langue]);

        // filter by contact nom
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_nom LIKE "%' . $this->contactNom . '%"');
            }]);
            
        // filter by contact prenom
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_prenom LIKE "%' . $this->contactPrenom . '%"');
            }]);
            
        if (isset($params['JournalisteSearch']['langue']) && $params['JournalisteSearch']['langue'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_langue LIKE "%' . $this->langue . '%"');
                }]);
            }
        if (isset($params['JournalisteSearch']['contactTelephonePrincipal']) && $params['JournalisteSearch']['contactTelephonePrincipal'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_telephone_principal LIKE "%' . $this->contactTelephonePrincipal . '%"');
                }]);
            }
        if (isset($params['JournalisteSearch']['contactGSMPrincipal']) && $params['JournalisteSearch']['contactGSMPrincipal'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_gsm_principal LIKE "%' . $this->contactGSMPrincipal . '%"');
                }]);
            }
            
        if (isset($params['JournalisteSearch']['contactEmailPrincipal']) && $params['JournalisteSearch']['contactEmailPrincipal'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_email_principal LIKE "%' . $this->contactEmailPrincipal . '%"');
                }]);
        }
        
        if (isset($params['JournalisteSearch']['societemedia']) && $params['JournalisteSearch']['societemedia'] != '') {
            $query->joinWith('media');
                $query->andFilterWhere(['like','media_societe', $this->societemedia]);
        }
        
        if (isset($params['JournalisteSearch']['nommedia']) && $params['JournalisteSearch']['nommedia'] != '') {
            $query->joinWith('media');
                $query->andFilterWhere(['like','media_titre', $this->nommedia]);
        }
        
        if (isset($params['JournalisteSearch']['periodicitemedia']) && $params['JournalisteSearch']['periodicitemedia'] != '') {
            $query->joinWith('media');
                $query->andFilterWhere(['like','media_frequence', $this->periodicitemedia]);
        }
        
        if (isset($params['JournalisteSearch']['nomsmedia']) && is_array($params['JournalisteSearch']['nomsmedia'])) {
            $query->joinWith('media');
                $query->andFilterWhere(['or like','media_titre', $params['JournalisteSearch']['nomsmedia']]);
        }
        
        if (isset($params['JournalisteSearch']['typesupportmedia']) && $params['JournalisteSearch']['typesupportmedia'] != '') {
            $query->joinWith('media');
                $query->andFilterWhere(['like','media_type_support', $params['JournalisteSearch']['typesupportmedia']]);
        }
        
        
        if (isset($params['JournalisteSearch']['contactGSMPrincipalNotNull']) && $params['JournalisteSearch']['contactGSMPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_gsm_principal' => null]]);
            }]);
        }
        
        if (isset($params['JournalisteSearch']['contactEmailPrincipalNotNull']) &&  $params['JournalisteSearch']['contactEmailPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_email_principal' => null]]);
            }]);
        }

        return $dataProvider;
    }
}
