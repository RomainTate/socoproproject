<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EntrepriseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entreprises';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entreprise-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Entreprise', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'entr_id',
            'entr_nom',
            'entr_forme_juridique',
            'entr_telephone_principal',
            'entr_email_principal:email',
            'nature',
            // 'entr_site_internet',
            // 'adr_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
