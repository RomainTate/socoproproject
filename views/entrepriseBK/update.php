<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entreprise */

$this->title = 'Update Entreprise: ' . $model->entr_id;
$this->params['breadcrumbs'][] = ['label' => 'Entreprises', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->entr_id, 'url' => ['view', 'id' => $model->entr_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entreprise-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
