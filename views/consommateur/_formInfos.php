<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Consommateur */
/* @var $form yii\widgets\ActiveForm */
$suggest = yii\helpers\ArrayHelper::map(\app\models\Lieuprisecontact::find()->select('lieu')->all() , 'lieu', 'lieu');
$suggest = array_values($suggest);

$fctsuggest = \app\models\Consommateur::find()->select(['conso_fonction'])->where(['not' , ['conso_fonction' => null]])->all();
$societesuggest = \app\models\Consommateur::find()->select(['conso_societe'])->where(['not' , ['conso_societe' => null]])->all();
?>

<div class="conso-infos-form">

    <?php $form = ActiveForm::begin([
        'id' => 'consommateurInfosForm'
    ]); ?>

    
    <?= $form->field($model, 'consoLieuContactName')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => $suggest,
            'appendTo'=>'#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]); ?>
    
    <?= $form->field($model, 'secteurs')
        ->dropDownList(yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Intérêts') 
    ?>
    
    <?= $form->field($model, 'conso_societe')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => array_values(yii\helpers\ArrayHelper::map($societesuggest, 'conso_societe', 'conso_societe')),
            'appendTo'=>'#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]);?>
        
    <?= $form->field($model, 'conso_fonction')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => array_values(yii\helpers\ArrayHelper::map($fctsuggest, 'conso_fonction', 'conso_fonction')),
            'appendTo'=>'#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]);?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');?>