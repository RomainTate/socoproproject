<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Producteur */
$name = ($model->contact->cont_nom && $model->contact->cont_prenom)? $model->contact->cont_nom . ' ' . $model->contact->cont_prenom : (($model->prod_nom_exploitation )? $model->prod_nom_exploitation : 'Producteur');
$this->title = 'Details ' . $name;
$this->params['breadcrumbs'][] = ['label' => 'Producteurs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['view', 'id' => $model->cont_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="x_panel">
    <div class="x_title">
        <h1>Informations sectorielles</h1> 
    </div>
    <?php if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h5><i class="icon fa fa-times"></i> Attention !</h5>
        <?= Yii::$app->session->getFlash('error') . '<br />' ?>
        </div>

    <?php endif; ?>
    
    <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a class="navbar-brand" href="#">Brand</a>-->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><?= Html::a('Profil', \yii\helpers\Url::to(['/producteur/view', 'id' => $model->cont_id])) ?></li>
                        <li class="active"><a href="#">Champs sectoriels <span class="sr-only">(current)</span></a></li>
                        <li> <?= Html::a('Logs', \yii\helpers\Url::to(['/log', 'LogSearch[contID]' => $model->cont_id])) ?> </li>
                        <?php if( !Yii::$app->user->isGuest &&  (Yii::$app->user->getUser()->getIsAdmin() || \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission' )) : ?>
                        <li> <?= Html::a('Suivis', \yii\helpers\Url::to(['/suivi', 'id' => $model->cont_id])) ?> </li>
                        <?php endif ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
</div>
<div class="x_panel">
    <ul class="nav nav-tabs" id="myTab" role="tablist"> 
        <?php
        $i = 0;
        foreach ($model->secteurs as $secteur) :
            if ($secteur->sect_nom == 'Agriculture Bio')
                continue;
            ?>
            <li class="nav-item <?php if(Yii::$app->request->get('sect_id') != null) {
                    if(Yii::$app->request->get('sect_id') == $secteur->sect_id) {
                        echo 'active';
                    }
                } else if ($i == 0) { echo 'active';} ?>">
                <a class="nav-link <?php if(Yii::$app->request->get('sect_id') != null) {
                    if(Yii::$app->request->get('sect_id') == $secteur->sect_id) {
                        echo 'active';
                    }
                } else if ($i == 0) { echo 'active';} ?>" data-toggle="tab" href="#<?= $secteur->sect_id ?>" role="tab" aria-controls="<?= $secteur->sect_id ?>"><?= $secteur->sect_nom ?></a>
            </li>
            <?php
            $i++;
        endforeach;
        ?>
    </ul>


    <div class="tab-content">
        <?php
        $i = 0;
        foreach ($model->secteurs as $secteur) :
            $membrefiliere = in_array($secteur->sect_id, ArrayHelper::map($model->contact->secteursMembrecommission, 'sect_id', 'sect_id'));
            if ($secteur->sect_nom == 'Agriculture Bio')
                continue;
            ?>
            <div class="tab-pane 
            <?php if(Yii::$app->request->get('sect_id') != null) {
                    if(Yii::$app->request->get('sect_id') == $secteur->sect_id) {
                        echo 'active';
                    }
                } else if ($i == 0) { echo 'active';} ?>"
             id="<?= $secteur->sect_id ?>" role="tabpanel">
                <div class="x_title">
                    <h3>
                    <?= "Membre de la commission filière {$secteur->sect_nom} :"?>
                    <?= $membrefiliere? 'oui' : 'non';?>
                    <?php if (Yii::$app->user->can('createProducteur')) :
                        echo $membrefiliere?
                        yii\bootstrap\Html::a('',['producteur/removemembrefiliere', 'id' => $model->cont_id, 'sect_id' => $secteur->sect_id], ['class' => "fa fa-times btn-edit", 'style' => 'color : red'] ):
                        yii\bootstrap\Html::a('',['producteur/addmembrefiliere', 'id' => $model->cont_id, 'sect_id' => $secteur->sect_id], ['class' => "fa fa-plus btn-edit", 'style' => 'color : green'] );
                    endif;?>
                    </h3>
                    
                </div>
                <?php 
                        $details = \app\models\Travailler::find()->where(['sect_id' => $secteur->sect_id , 'cont_id' => $model->cont_id])->one();
                        echo '<table class="table table-bordered"> <tr> <th> Champs Sectoriels </th> <th> Valeur </th> </tr>';
                        foreach ($details->getAttributes(NULL, ['cont_id', 'sect_id']) as $k => $v) {
                            $champs = \app\controllers\ChampsectController::getValue($k,$v);
                            echo '<tr>';
                            echo '<td>' .$champs['k'] . '</td>';
                            echo '<td> <div name="divShow">' .$champs['v'] . ' ';
                            if (Yii::$app->user->can('createProducteur')) :
                                if ($champs['v'] != 'inconnu') {
                                    echo yii\bootstrap\Html::a('',\Yii::$app->urlManager->createUrl(['producteur/deletedetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id, 'attr' => $k]), ['class' => "fa fa-times btn btn-edit" , 'style' => 'color : red', 'name' => 'deleteButton']);
                                }
                                echo yii\bootstrap\Html::button('', ['class' => "fa fa-pencil btn-edit" , 'name' => 'editButton']);
                                echo '</div>';
                                echo '<div name="editForm" style="display: none">';
                                switch ($champs['t']) {
                                    case 1 :
                                        \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatedetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                        echo yii\helpers\Html::radioList($k,$v, ['1' => 'oui', '0' => 'non']);
                                        echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                        echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                        \yii\widgets\ActiveForm::end();
                                        break;
                                    case 2 :
                                        \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatedetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                        echo yii\bootstrap\Html::dropDownList($k,$v , \app\controllers\ChampsectController::getOptions($k));
                                        echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                        echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                        \yii\widgets\ActiveForm::end();
                                        break;
                                    case 3 :
                                        \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatedetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                        echo yii\bootstrap\Html::textInput($k,$v, ['maxlength' => '35']);
                                        echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                        echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                        \yii\widgets\ActiveForm::end();
                                        break;
                                    case 4 :
                                        \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatedetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                        echo yii\bootstrap\Html::Input('number',$k,$v, ['max' => 2147483647]);
                                        echo yii\bootstrap\Html::hiddenInput('typeChamp', '4');
                                        echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                        echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                        \yii\widgets\ActiveForm::end();
                                        break;
                                    case 5:
                                        \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatedetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                        echo yii\helpers\Html::checkboxList($k, \app\controllers\ChampsectController::getSelectedValues($v), \app\controllers\ChampsectController::getOptions($k));
                                        echo yii\bootstrap\Html::hiddenInput('champ', $k);
                                        echo yii\bootstrap\Html::hiddenInput('typeChamp', '5');
                                        echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                        echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                        \yii\widgets\ActiveForm::end();
                                        break;
                                }
                            endif;
                            echo '</div>';
                            echo '</td>' ;
                            echo '</tr>';

                        }
                        echo '</table>';
                if ($secteur->champSectorielCles) {
                    echo '<table class="table table-bordered"> <tr> <th> Champs Sectoriel Spécifique</th> <th> Valeur </th> </tr>';
                    foreach ($secteur->champSectorielCles as $champSect) {
                        $k = $champSect->champ_sectoriel_cle_field;
                        if(app\models\ChampSectorielValeur::find()->where(['cont_id' => $model->cont_id, 'champ_sectoriel_cle_id' => $champSect->champ_sectoriel_cle_id])->one()) {
                            $v = app\models\ChampSectorielValeur::find()->where(['cont_id' => $model->cont_id, 'champ_sectoriel_cle_id' => $champSect->champ_sectoriel_cle_id])->one()->champ_sectoriel_valeur_val;
                        } else {
                            $v = null;
                        }
                        $champs = \app\controllers\ChampsectController::getValue($k,
                                $v,
                                $secteur->sect_nom);
                        echo '<tr>';
                        echo '<td>' .$champs['k'] . '</td>';
                        echo '<td> <div name="divShow">' .$champs['v'] . ' ';
                        if (Yii::$app->user->can('createProducteur')) :
                            if ($champs['v'] != 'inconnu') {
                                echo yii\bootstrap\Html::a('',\Yii::$app->urlManager->createUrl(['producteur/deletespecificdetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id, 'champ_cle_id' => $champSect->champ_sectoriel_cle_id]), ['class' => "fa fa-times btn btn-edit" , 'style' => 'color : red', 'name' => 'deleteButton']);
                            }
                            echo yii\bootstrap\Html::button('', ['class' => "fa fa-pencil btn-edit" , 'name' => 'editButton']);
                            echo '</div>';
                            echo '<div name="editForm" style="display: none">';
                            switch ($champs['t']) {
                                case 1 :
                                    \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatespecificdetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                    echo yii\helpers\Html::radioList($k,$v, ['1' => 'oui', '0' => 'non']);
                                    echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                    echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                    \yii\widgets\ActiveForm::end();
                                    break;
                                case 2 :
                                    \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatespecificdetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                    echo yii\bootstrap\Html::dropDownList($k,$v,\app\controllers\ChampsectController::getOptions($champSect->champ_sectoriel_cle_field, $secteur->sect_nom));
                                    echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                    echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                    \yii\widgets\ActiveForm::end();
                                    break;
                                case 3 :
                                    \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatespecificdetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                    echo yii\bootstrap\Html::textInput($k,$v, ['maxlength' => '35']);
                                    echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                    echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                    \yii\widgets\ActiveForm::end();
                                    break;
                                case 4 :
                                    \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatespecificdetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                    echo yii\bootstrap\Html::Input('number',$k,$v, ['max' => 2147483647]);
                                    echo yii\bootstrap\Html::hiddenInput('typeChamp', '4');
                                    echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                    echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                    \yii\widgets\ActiveForm::end();
                                    break;
                                case 5:
                                    \yii\widgets\ActiveForm::begin(['id'=>$k, 'action' => ['/producteur/updatespecificdetails', 'cont_id' => $model->cont_id, 'sect_id' => $secteur->sect_id]]);
                                    echo yii\helpers\Html::checkboxList($k, \app\controllers\ChampsectController::getSelectedValues($v), \app\controllers\ChampsectController::getOptions($champSect->champ_sectoriel_cle_field, $secteur->sect_nom));
                                    echo yii\bootstrap\Html::hiddenInput('champ', $k);
                                    echo yii\bootstrap\Html::hiddenInput('typeChamp', '5');
                                    echo Html::submitButton('',['class' => 'fa fa-check' , 'style' => 'color : green']);
                                    echo Html::button('', ['name' => 'resetEdit', 'class' => 'fa fa-times' , 'style' => 'color : red']);
                                    \yii\widgets\ActiveForm::end();
                                    break;
                            }
                        endif;
                        echo '</div>';
                        echo '</td>' ;
                        echo '</tr>';

                    }
                    echo '</table>';
                } else {
                    echo 'aucun champ sectoriel spécifique pour ce secteur';
                }
        ?>
            </div>
            <?php
            $i++;
        endforeach;
        ?>
    </div>
    <div class="x_content">
        <?php echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
            'entity' => (string) 'prod-details' . $model->cont_id, // type and id
        ]); ?>
    </div> 
</div>

    <?php $this->registerJsFile(\yii\helpers\Url::home().'js/tableditable.js', ['depends' => [yii\web\JqueryAsset::className()]]);
    ?>
