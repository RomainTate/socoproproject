<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "log".
 *
 * @property integer $log_id
 * @property integer $action 
 * @property string $log_attr_modifies
 * @property integer $id
 * @property integer $cont_id
 * @property integer $created_at
 *
 * @property Contact $contact
 * @property User $user
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_attr_modifies'], 'string'],
            [['id'], 'required'],
            [['id', 'cont_id', 'created_at'], 'integer'],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                ],
                'value' => function() { return microtime(true); }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'log_id' => 'Log ID',
            'log_attr_modifies' => 'Log Attr Modifies',
            'id' => 'ID',
            'cont_id' => 'Cont ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
    public function getContID()
    {
        if(!$this->contact) return null;
        return $this->contact->cont_id;
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id']);
    }
    
    public function getUserID()
    {
        if(!$this->user) return null;
        return $this->user->id;
    }
    
        public function getUserName()
    {
        if (!$this->user) return 'inconnu';
        return $this->user->username;
    }
}
