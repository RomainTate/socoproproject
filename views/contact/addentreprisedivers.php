<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$entrDiversAlreadyIn = yii\helpers\ArrayHelper::map(app\models\Entreprise::find()->select('entreprise.entr_id')->joinwith('contacts',true, 'inner join')->where(['contact.cont_id' => $contactId])->all(), 'entr_id','entr_id');
$entrDivers = yii\helpers\ArrayHelper::map(\app\models\Entreprise::find()->select(['entr_societe', 'entr_id'])->where(['not in','entr_id',$entrDiversAlreadyIn])->all() , 'entr_id', 'entr_societe');
?>

<?php if (!empty($entrDivers)) :?>
<button class="btn btn-info" id="addexistingentreprise" name="formchoice"> Rechercher une entreprise </button> 
<button class="btn btn-success" id="addnewentreprise" name="formchoice"> Ajouter une entreprise </button> 


<div id="existingEntreprise" style="display: none">

    <h3> Rechercher une entreprise déjà existante </h3>
    <?php ActiveForm::begin() ?>

        <div class="form-group">
            <?= Html::label('Nom', 'entrepriseNomForm', ['class' => 'control-label'])?>
            <?= Html::dropDownList('entrepriseId',null, $entrDivers , ['id' => 'entrepriseNomForm','class' => 'form-control'])?>
        </div>
        <div class="form-group">
            <?= Html::label('Fonction du contact', 'fonctionContact', ['class' => 'control-label'])?>
            <?= Html::textInput('fonctionContact', null, ['id' => 'fonctionContact','class' => 'form-control', 'maxlength' => 100])?>
        </div>
        <div class="form-group">
            <?= Html::submitButton('Ajouter cette entreprise', ['class' =>  'btn btn-form btn-success' ]) ?>
        </div>
        <?php ActiveForm::end();?> 
</div>
<?php endif;?>
<div id="newEntreprise" <?php if (!empty($entrDivers)) :?> style="display: none" <?php endif?>>
<h3> Créer une entreprise </h3>
    <?= $this->render('/entreprise/_form', [
        'contactId' => $contactId
    ]) ?>
</div>

<?php // $this->registerJsFile(\yii\helpers\Url::home().'js/addentreprise.js', ['depends' => [yii\web\JqueryAsset::className()]]); 
$this->registerJS("   $('button[name=formchoice]').on('click' ,function () {
       if(this.id === 'addexistingentreprise') {
           $('#existingEntreprise').show();
           $('#newEntreprise').hide();
       } else if (this.id === 'addnewentreprise') {
           $('#existingEntreprise').hide();
           $('#newEntreprise').show();
       }
       
   });");?>