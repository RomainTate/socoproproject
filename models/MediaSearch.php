<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Media;

/**
 * MediaSearch represents the model behind the search form about `app\models\Media`.
 */
class MediaSearch extends Media
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['media_id', 'adr_id'], 'integer'],
            [['media_societe', 'media_titre', 'media_telephone', 'media_email', 'media_site_internet',
                'media_type_presse', 'media_type_support', 'media_frequence', 'media_audience', 'media_pays', 'media_langue'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Media::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'media_id' => $this->media_id,
            'adr_id' => $this->adr_id,
        ]);

        $query->andFilterWhere(['like', 'media_societe', $this->media_societe])
            ->andFilterWhere(['like', 'media_titre', $this->media_titre])
            ->andFilterWhere(['like', 'media_telephone', $this->media_telephone])
            ->andFilterWhere(['like', 'media_email', $this->media_email])
            ->andFilterWhere(['like', 'media_site_internet', $this->media_site_internet])
            ->andFilterWhere(['like', 'media_type_presse', $this->media_type_presse])
            ->andFilterWhere(['like', 'media_type_support', $this->media_type_support])
            ->andFilterWhere(['like', 'media_frequence', $this->media_frequence])
            ->andFilterWhere(['like', 'media_audience', $this->media_audience])
            ->andFilterWhere(['like', 'media_pays', $this->media_langue])
            ->andFilterWhere(['like', 'media_pays', $this->media_pays]);

        return $dataProvider;
    }
}
