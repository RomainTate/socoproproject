<?php

namespace app\components;


class Logger {
    public function log($contID, $jsonlogs, $action)
    {
        $log = new \app\models\Log();
        $log->log_attr_modifies = $jsonlogs;
        $log->id = \Yii::$app->getUser()->id;
        $log->cont_id = $contID;
        $log->action = $action;
        $log->save();
    }
}
