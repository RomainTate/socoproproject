<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Producteur */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producteur-form">

    <?php $form = ActiveForm::begin([
        'id' => 'prodInfosForm'
    ]); ?>

    <div class="form-group customradio">
        <?= Html::label('Membre du college',null, ['class' => 'control-label']);?>
        <?= Html::radioList('membrecollege', ($model->contact->membrecollege)?'1': '0', ['0' => 'non', '1' => 'oui'] )?>
    </div>
        <?php // echo $form->field($model, 'secteurs')
//            ->dropDownList(yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Secteurs*')?>
        <?= $form->field($autreInfos, 'site_internet')->textInput(['maxlength' => true]);?>
        <?= $form->field($autreInfos, 'page_facebook')->textInput(['maxlength' => true]);?>
        <?= $form->field($model, 'prod_statut_pro')->dropDownList(["1" => "Actif", "0" => "Retraité"],['prompt' => '']) ?>
        <?= $form->field($model, 'prod_numero_producteur')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'prod_numero_BCE')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'prod_nom_exploitation')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'prod_statut_juridique')->textInput()->dropDownList(["1"=>"SA","2"=>"SPRL","3"=>"SPRLS","4"=>"SCRL","5"=>"SNC","6"=>"ASBL"],['prompt' => '']) ?>
        <?= $form->field($model, 'prod_date_debut_activite')->label('Date début d\'activité (jj-mm-aaaa)')->textInput()->widget(\yii\jui\DatePicker::className(),[
//            'model' => $model,
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        ])
        ?> 
        <?= $form->field($model, 'activites')->dropDownList(yii\helpers\ArrayHelper::map(\app\models\Activite::find()->all(),'activite_id' , 'activite_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple'])?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');?>