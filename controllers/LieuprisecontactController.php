<?php

namespace app\controllers;

class LieuprisecontactController extends \yii\web\Controller
{
    public static function actionCreate($name) // return Lieu.id
    {
        $model = new \app\models\Lieuprisecontact();
        
        if ($model->findOne(['lieu' => $name])) { //if lieu already exists in DB
            return $model->findOne(['lieu' => $name])->lieu_prise_contact_id;
        
            
        } else { //lieu don't exist
            $model->lieu = $name;
            $model->save();
            return $model->lieu_prise_contact_id;
        }
    }
}
