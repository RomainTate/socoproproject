<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RechAdminEncad */

$this->title = 'Ajouter Contact Rech Admin Encad';
$this->params['breadcrumbs'][] = ['label' => 'Rech Admin Encads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rech-admin-encad-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'contactModel' => $contactModel,
    ]) ?>

</div>
