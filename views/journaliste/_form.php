<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Journaliste */
/* @var $form yii\widgets\ActiveForm */
$suggest = ArrayHelper::map(\app\models\Journaliste::find()->select('journaliste_fonction')
        ->where(['not' , ['journaliste_fonction' => null]])->all() , 'journaliste_fonction', 'journaliste_fonction');
$suggest = array_values($suggest);  
 if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h5><i class="icon fa fa-times"></i> Erreur!</h5>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>

<div class="journaliste-form">
    <?php if(!Yii::$app->request->isAjax) :?>
    <div class="col-sm-6">
    <?php            endif;?>
        
    <?php $form = ActiveForm::begin([
        'id' => 'journalisteForm'
    ]); ?>
    
    <?= $form->field($contactModel, 'cont_civilite')->textInput(['maxlength' => true])->dropDownList(['Mr' => 'Mr' , 'Mme' => 'Mme']) ?>
    
    <?= $form->field($contactModel, 'cont_nom')->textInput(['maxlength' => true]) ?>
    <?= $form->field($contactModel, 'cont_prenom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($contactModel, 'cont_telephone_principal')->textInput(['maxlength' => true , 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    <?= $form->field($contactModel, 'cont_gsm_principal')->textInput(['maxlength' => true, 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    <?= $form->field($contactModel, 'cont_email_principal')->textInput(['maxlength' => true]) ?>
  
    <?= $form->field($contactModel, 'cont_langue')->checkboxList(['FR' => 'FR', 'NL' => 'NL', 'DE' => 'DE', 'EN' => 'EN'],['item'=>function ($index, $label, $name, $checked, $value) use ($contactModel){
    if(strpos($contactModel->cont_langue, $value) !== false) {
        $checked = 'checked';
    } else {
        $checked = '';
    }
    return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";}]); 
    ?>
 <?php if ($model->isNewRecord) :?>             
        <?= $form->field($model, 'journaliste_date_contact')->textInput()->widget(\yii\jui\DatePicker::className(),[
                'dateFormat' => 'dd-MM-yyyy',
                'options' => [
                'class' => 'form-control',]
            ])
            ?> 

        <?= $form->field($model, 'journaliste_fonction')->widget(\yii\jui\AutoComplete::classname(), [
            'clientOptions' => [
                'source' => $suggest,
                'appendTo'=>'#modalContent'
            ],
            'options' => [
                'class' => 'form-control'
            ],
        ]);?>
        <?= $form->field($model, 'journaliste_rubrique')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'nommedia')->widget(\yii\jui\AutoComplete::classname(), [
            'clientOptions' => [
                'source' => array_values(ArrayHelper::map(\app\models\Media::find()->select('media_titre')->all(), 'media_titre', 'media_titre')),
                'appendTo'=>'#modalContent'
            ],
            'options' => [
                'class' => 'form-control'
            ],
        ]);?>
<?php endif;?>
    
<?php /*    
    <?= $form->field($model, 'journaliste_langue')->dropDownList(["fr" => "Français", "nl" => "Néerlandais", "fr/nl" => "Français/Néerlandais"], ['prompt' => '']); ?>

*/?>

    <div class="form-group">
        <?php if (Yii::$app->session->hasFlash('error')) echo Html::submitButton($model->isNewRecord ? 'Créer quand même' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value' => 'forceAdd', 'name'=>'forceadd']);
              else echo Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if(!Yii::$app->request->isAjax) :?>
    </div>
    <?php            endif;?>
</div>
