<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Journal */
/* @var $form yii\widgets\ActiveForm */
if (!isset($model)) {
    $model = new app\models\Journal();
    $entrepriseModel = new app\models\Entreprise();
    $action = '/journal/create';
} else {
    $action = '';
}
?>

<div class="journal-form">
    <div class="x_panel">
        <?php
        $form = ActiveForm::begin([
                    'action' => $action,
                    'id' => 'journalForm'
        ]);
        ?>

        <?php if (isset($contactId)) echo \yii\bootstrap\Html::hiddenInput('contactToAdd', $contactId); ?>
        <?php if (Yii::$app->request->isAjax) echo yii\bootstrap\Html::hiddenInput('ajax', 1); ?>

        <?= $form->field($entrepriseModel, 'entr_nom')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_forme_juridique')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_telephone_principal')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_email_principal')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_site_internet')->textInput(['maxlength' => true]) ?>


            <?= $form->field($model, 'journal_frequence')->textInput(['maxlength' => true]) ?>

        <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

<?php ActiveForm::end(); ?>
    </div>
</div>
