<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\JournalisteSearch */
/* @var $form yii\widgets\ActiveForm */
$suggestFunction = yii\helpers\ArrayHelper::map(\app\models\Journaliste::find()->select('journaliste_fonction')
        ->where(['not' , ['journaliste_fonction' => null]])->all() , 'journaliste_fonction', 'journaliste_fonction');
$suggestFunction = array_values($suggestFunction);  
//$suggestJournal = yii\helpers\ArrayHelper::map(\app\models\Entreprise::find()->select('entr_nom')->joinWith('journal', false, 'inner join')
//        ->all() , 'entr_nom', 'entr_nom');
//$suggestJournal = array_values($suggestJournal);  
?>

<div class="journaliste-search">

     <?php $form = ActiveForm::begin([
        'action' => 'search',
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'contactGSMPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Numéro GSM');?>
    <?= $form->field($model, 'contactEmailPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Adresse e-mail');?>
    <?= $form->field($model, 'journaliste_fonction')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => $suggestFunction,
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]);?>
    
    <?= $form->field($model, 'nomsmedia')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Media::find()->all(),'media_titre' , 'media_titre')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Médias')?>
<?php /*    
    <?= $form->field($model, 'journaliste_langue')->radioList(["fr"=>"français","nl"=>"Néerlandais", "fr/nl"=>"FR ET NL", NULL => "Tous"])->label('Langue');?>
    <?= $form->field($model, 'journal')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => $suggestJournal,
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]);?>
*/?>
    <?php // foreach ($model->getAttributes(null, ['cont_id']) as $key => $value) {
//        echo $form->field($model, $key);
//    }?>
    <div class="form-group">
        <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs(
    "$('.js-multiple').select2();",
    yii\web\View::POS_READY,
    'select2-prod'
); 