<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

if ($model->isNewRecord) {
    $selectionNewsLetters = null;
    if(Yii::$app->request->post('newsletters')) {
        $selectionNewsLetters = Yii::$app->request->post('newsletters');
    }
} else {
    $selectionNewsLetters = ArrayHelper::map($model->newsletters, 'newsletter_id', 'newsletter_id');
}
?>

<?= Html::label('Newsletters', 'ContactNewsletters') ?>
<?= Html::checkboxList('newsletters',$selectionNewsLetters ,
        ArrayHelper::map(app\models\Newsletter::find()->all(), 'newsletter_id', 'newsletter_nom'), ['id' => 'ContactNewsletters']) ?>
