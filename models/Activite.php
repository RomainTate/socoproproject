<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activite".
 *
 * @property integer $activite_id
 * @property string $activite_nom
 *
 * @property Avoir[] $avoirs
 * @property Producteur[] $producteurs
 */
class Activite extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'activite';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['activite_nom'], 'required'],
            [['activite_nom'], 'string', 'max' => 25],
            [['activite_nom'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'activite_id' => 'Activite ID',
            'activite_nom' => 'Activite Nom',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAvoirs()
    {
        return $this->hasMany(Avoir::className(), ['activite_id' => 'activite_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducteurs()
    {
        return $this->hasMany(Producteur::className(), ['cont_id' => 'cont_id'])->viaTable('avoir', ['activite_id' => 'activite_id']);
    }
}
