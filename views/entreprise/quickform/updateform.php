<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>

<div class="x_panel">

    <?php $form = ActiveForm::begin(); ?>

    <?php if ($type == 'string' || $type == 'char') : ?>
        <?= $form->field($model, $attribute)->textInput(['maxlength' => true]) ?>
    <?php elseif ($type == 'date') : ?>
        <?= $form->field($model, $attribute)->widget(\yii\jui\DatePicker::className(),[
//            'model' => $model,
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        ])
        ?>
    
    <?php endif; ?>
    
    <div class="form-group">
        <?= Html::submitButton('Mettre à jour', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>