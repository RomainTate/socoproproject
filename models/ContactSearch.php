<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contact;

/**
 * ContactSearch represents the model behind the search form about `app\models\Contact`.
 */
class ContactSearch extends Contact
{
    public $_membrecollege;
    public $membrefiliere;
    public $_secteurs;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id'], 'integer'],
            [['cont_email_principal', 'cont_telephone_principal', 'cont_gsm_principal'], 'string'],
            [['cont_prenom', 'cont_nom', 'cont_civilite', 'cont_annee_naissance', 'cont_created_at', 'nature', '_membrecollege', 'membrefiliere', '_secteurs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        //add virtual field 'nature' to check the nature of Contact
        $query = Contact::find()->select([
                'contact.cont_id', 'contact.cont_nom', 'contact.cont_prenom', 'contact.cont_civilite', 'contact.cont_telephone_principal', 'contact.cont_gsm_principal', 'contact.cont_email_principal',
                'nature' => "(CASE 
                WHEN producteur.cont_id IS NOT NULL THEN 'Producteur' 
                WHEN journaliste.cont_id IS NOT NULL THEN 'Journaliste' 
                WHEN consommateur.cont_id IS NOT NULL THEN 'Consommateur' 
                WHEN divers.cont_id IS NOT NULL THEN 'Contact divers' 
                ELSE 'inconnu !' END)"])
                ->leftJoin('producteur', 'producteur.cont_id = contact.cont_id')
                ->leftJoin('journaliste', 'journaliste.cont_id = contact.cont_id')
                ->leftJoin('consommateur', 'consommateur.cont_id = contact.cont_id')
                ->leftJoin('divers', 'divers.cont_id = contact.cont_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $query->joinWith('membrecollege');
        $query->joinWith('secteursMembrecommission sm');
        
        $query->groupBy('cont_id');
        
        $dataProvider->setSort([
            'attributes' => [
                'nature' => [
                    'asc' => ['nature' => SORT_ASC],
                    'desc' => ['nature' => SORT_DESC],
                    'label' => 'Type contact'
                ],
                'cont_nom' => [
                    'asc' => ['cont_nom' => SORT_ASC],
                    'desc' => ['cont_nom' => SORT_DESC],
                ],
                'cont_prenom' => [
                    'asc' => ['cont_prenom' => SORT_ASC],
                    'desc' => ['cont_prenom' => SORT_DESC],
                ],
                'cont_civilite' => [
                    'asc' => ['cont_civilite' => SORT_ASC],
                    'desc' => ['cont_civilite' => SORT_DESC],
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
//            'cont_annee_naissance' => $this->cont_annee_naissance,
            'cont_created_at' => $this->cont_created_at,
            
        ]);
        
        if ($this->_membrecollege != '') {
            if($this->_membrecollege == 1) {
                $query->andWhere(['not' , ['membre_college.cont_id' => null]]);
            } else {
                $query->andWhere(['membre_college.cont_id' => null]);
            }
        }
        
        if ($this->membrefiliere != null) {
            $query->andFilterWhere(['=', 'sm.sect_id', $this->membrefiliere]);
        }
        

        $query->andFilterWhere(['like', 'cont_prenom', $this->cont_prenom])
            ->andFilterWhere(['like', 'cont_nom', $this->cont_nom])
            ->andFilterWhere(['like', 'cont_civilite', $this->cont_civilite])
            ->andFilterWhere(['like', 'cont_email_principal', $this->cont_email_principal])
            ->andFilterWhere(['like', 'cont_telephone_principal', $this->cont_telephone_principal])
            ->andFilterWhere(['like', 'cont_gsm_principal', $this->cont_gsm_principal]);

         if ($this->_secteurs != null) {
                $subQuery = Contact::find()->select('contact.cont_id')->joinWith('secteurs')->where(['secteur.sect_id' => $this->_secteurs]);
                $query->andWhere(['producteur.cont_id' => $subQuery]);
        }
        
        if($this->nature) {
            $query->having(['like', 'nature', $this->nature]);
        }
        
        return $dataProvider;
    }
}
