<?php
/**
 * WhResponsive class
 * Extends WhGridView to provide responsive Tables
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @adapted by Romain Tate
 * @copyright Copyright &copy; Antonio Ramirez 2013-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package yiiwheels.widgets.grid.behaviors
 */
namespace app\components;
class WhResponsive 
{
	/**
	 * Writes responsiveCSS
	 */
	public static function writeResponsiveCss($columns, $gridId)
	{
		$cnt = 1;
		$labels = '';
		foreach ($columns as $column) {
                    if(is_array($column)) {
                        if( (array_key_exists('visible', $column) && $column['visible'] == false) || (array_key_exists('hiddenFromExport', $column) && $column['hiddenFromExport'] == true)) {
                            continue;
                        }
                        $name = $column['label'] ;
                    } else {
                        $name = $column ;
                    }
                    $labels .= "#$gridId td:nth-of-type($cnt):before { content: '{$name}'; }\n";
                    $cnt++;
                }

        $css = <<<EOD
@media
	only screen and (max-width: 768px),
	(min-device-width: 768px) and (max-device-width: 1200px)  {

		/* Force table to not be like tables anymore */
		#{$gridId} table,#{$gridId} thead,#{$gridId} tbody,#{$gridId} th,#{$gridId} td,#{$gridId} tr {
			display: block;
		}

		/* Hide table headers (but not display: none;, for accessibility) */
		#{$gridId} thead tr {
			position: absolute;
			top: -9999px;
			left: -9999px;
		}
                #{$gridId} thead #$gridId-filters {
                        position: inherit;
			top: inherit;
			left: inherit;
                }
		#{$gridId} tr { border: 1px solid #ccc; }

		#{$gridId} td {
			/* Behave  like a "row" */
			border: none;
			border-bottom: 1px solid #eee;
			position: relative;
			padding-left: 35%;
                        min-height : 35px;
                        white-space : inherit;
		}

		#{$gridId} td:before {
			/* Now like a table header */
			position: absolute;
			/* Top/left values mimic padding */
			top: 6px;
			left: 6px;
			width: 45%;
			padding-right: 10px;
		}
		.grid-view .button-column {
			text-align: left;
			width:auto;
		}
		/*
		Label the data
		*/
		{$labels}
	}
EOD;
          return $css;      
	}
        
}