<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $specific ? 'Logs du producteur' :'Logs généraux';
if($specific) {
    if($model) {
    $name = ($model->cont_nom && $model->cont_prenom)? $model->cont_nom . ' ' . $model->cont_prenom :  $model->getNature('label');
    $this->params['breadcrumbs'][] = ['label' => 'Producteurs', 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['/producteur/view', 'id' => $model->cont_id]];
    }
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-index">

    <div class="x_panel">
        <div class="x_title">
    <h1><?= Html::encode($this->title) ?></h1>
        </div>
    <?php if($specific && $model) :?>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a class="navbar-brand" href="#">Brand</a>-->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li><?= Html::a('Profil', \yii\helpers\Url::to(['/producteur/view', 'id' => $model->cont_id])) ?></li>
                        <li><?= Html::a('Champs sectoriels', \yii\helpers\Url::to(['/producteur/details', 'id' => $model->cont_id])) ?></li>
                        <li class="active"><a href="#">Logs <span class="sr-only">(current)</span></a></li>
                        <?php if( !Yii::$app->user->isGuest &&  (Yii::$app->user->getUser()->getIsAdmin() || \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission' )) : ?>
                        <li><?= Html::a('Suivis', \yii\helpers\Url::to(['/suivi', 'id' => $model->cont_id])) ?></li>
                        <?php endif ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    <?php endif;?>
    
        </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="x_panel">
    <?= GridView::widget([
        'id' => 'logGrid',
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
//        'pjax'=>true,
        'columns' => [

            //'created_at',
            [
                'label' => 'Date',
                'attribute' => 'date',
                'format' => 'html',
                'filter' => \yii\jui\DatePicker::widget([
                    'model'=>$searchModel,
                    'attribute'=>'createdAt',
                    'language' => 'fr',
                    'dateFormat' => 'dd-MM-yyyy',
                    'options' => ['class' => 'form-control', 'placeholder' => 'A partir du ...']
                ]),
                'value' => function ($model) {
                    return 'le ' . Yii::$app->formatter->asDatetime($model->created_at);
                },
                
            ],   
            //user (log->id)
            [
                'label' => 'utilisateur',
                'attribute' => 'userID',
                'filter' => ArrayHelper::map(app\models\User::find()->asArray()->orderBy('username')->all(), 'id', 'username'),
                'value' => 'userName'
            ],
                    
                    
            //action
            [
                'label' => 'Action',
                'attribute' => 'action',
                'filter' => ['Ajout', 'Modification', 'Suppression', 'Nouveau Contact', 'Ancien Contact'],
                'value' => function ($model) {
                    switch ($model->action) {
                        case 0: return 'a ajouté';
                            break;
                        case 1: return 'a modifié';
                            break;
                        case 2: return 'a supprimé';
                            break;
                        case 3: return 'a créer';
                            break;
                        case 4: return 'a supprimé de la base de données';
                            break;
                    }
                }
                
            ],
            
            //'log_attr_modifies',
            [
                'label' => 'infos',
                'attribute' => 'infos',
                'format' => 'html',
                'filter' => ['GSM', 'E-mail', 'Statut professionel', 'Secteur', 'Nouveau Contact'],
                'value' => function ($model) {
                if($model->action == 3) {
                    return 'Un <b> Nouveau Producteur Dans la base de données</b>';
                }
                
                    $attr_modifies = json_decode($model->log_attr_modifies,true);
                    if($model->action == 4) {
                        $log = '';
                        foreach ($attr_modifies as $key => $value) {
                            if(!is_array($value)) {
                            $log .= \app\models\Producteur::getLabelAttribute($key) .' -> ' .$value;
                            $log .= '<br />';
                            } else {
                                $log .= 'Secteurs : <br />';
                                foreach ($value as $v) {
                                    $log .= '- '. $v . '<br />';
                                }
                            }
                            
                        }
                        return $log;
                    }
                    
                    $keys = array_keys($attr_modifies);
                    switch(substr($keys[0],3,4)) {
                        case 'cont' : $entity = app\models\Contact::class;
                            break;
                        case 'prod' : $entity = \app\models\Producteur::class;
                            break;
                        case 'sect' : $entity = app\models\Secteur::class;
                            break;
                    }
//                    if(!isset($entity)) die(substr($keys[0],3,4));
                    if(isset($entity)) {
                        if(count($attr_modifies) == 1) {
                            $log = ' l\'attribut <b>'. $entity::getLabelAttribute(substr($keys[0], 3)). '</b> '. $attr_modifies[$keys[0]]; // ok
                        } else {
                            $log = ' l\'attribut <b>'. $entity::getLabelAttribute(substr($keys[0], 3)). '</b> '. $attr_modifies[$keys[0]] .' par '. $attr_modifies[$keys[1]] ; // ok
                        }
                    }
                    return $log ;
                }
            ],
                    
            //'contact',
            [
                'label' => 'contact',
                'attribute' => 'natureContact',
                'filter' => ['Producteurs', 'Journalistes', 'Consommateurs', 'Contacts divers'],
                'value' => function ($model) {
                if(!$model->contact) {
                    return 'Contact inconnu ou supprimé';
                }
                    $natureContact = $model->contact->getNature();
                    if ($natureContact == 'associe') {
                        if ($model->contact->cont_nom && $model->contact->cont_prenom) {
                            $nom = $model->contact->cont_nom .' '.$model->contact->cont_prenom ;
                        } else {
                            if($model->contact->cont_nom) {
                                $nom =  $model->contact->cont_nom . ' (prénom inconnus)';
                            } else if($model->contact->cont_prenom) {
                                $nom = '(nom inconnu) '. $model->contact->cont_prenom;
                            } else {
                                $nom = '(nom et prénom inconnus)';
                            }
                        }
                        $s = "de l'associé ". $nom  ." du producteur ";
                        if($model->contact->associe->producteur->contact->cont_nom && $model->contact->associe->producteur->contact->cont_prenom) {
                            $s.= $model->contact->associe->producteur->contact->cont_nom .' '. $model->contact->associe->producteur->contact->cont_prenom;
                        } else {
                            if($model->contact->associe->producteur->contact->producteur->prod_nom_exploitation) {
                                $s.= 'ayant pour exploitation '. $model->contact->associe->producteur->contact->producteur->prod_nom_exploitation;
                            } else {
                                if($model->contact->associe->producteur->contact->cont_nom) {
                                    $s.= $model->contact->associe->producteur->contact->cont_nom. ' (prénom inconnu)';
                                } else if ($model->contact->associe->producteur->contact->cont_prenom) {
                                    $s.= '(nom inconnu) '. $model->contact->associe->producteur->contact->cont_prenom;
                                } else {
                                    $s.= ' (nom et prénom inconnus) ';
                                }
                            }
                        }
                    }
                    else 
                    {
                        $s = 'du ';
                        $s .=  $natureContact. ' ';
                        if($model->contact->cont_nom && $model->contact->cont_prenom) {
                            $s.= $model->contact->cont_nom .' '. $model->contact->cont_prenom;
                        } else {
                            if($natureContact == 'producteur' && $model->contact->producteur->prod_nom_exploitation) {
                                $s.= 'ayant pour exploitation '. $model->contact->producteur->prod_nom_exploitation;
                            } else {
                                if($model->contact->cont_nom) {
                                    $s.= $model->contact->cont_nom. ' (prénom inconnu)';
                                } else if ($model->contact->cont_prenom) {
                                    $s.= '(nom inconnu) '. $model->contact->cont_prenom;
                                } else {
                                    $s.= '(nom et prénom inconnus)';
                                }
                            }
                        }
                    }
                    return $s;
                }
            ],
                    
            //id Contact
            [
                'label' => 'id Contact',
                'attribute' => 'contID',
                'hidden' => true
            ],
            
            //voir contact
            [
                'class' => 'kartik\grid\ActionColumn',
                'template' => '{view}',
                'header' => 'voir le contact',
                'visible' => !$specific,
                'hiddenFromExport' => true ,
                'buttons' => [
                        'view' => function($url, $model) {
                if(!$model->contact) {
                    return;
                }
                            $nature = $model->contact->getNature('url');
                            if($nature == 'associe') {
                                $nature = 'producteur';
                                $contID = $model->contact->associe->producteur->cont_id;
                            } else {
                                $contID = $model->cont_id;
                            }
                            $url = \Yii::$app->urlManager->createUrl([$nature.'/view', 'id' => $contID]);
                            return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['class' => 'action','title' => Yii::t('yii', 'View')]);
                        }
                ]
            ],
                        
                                
            //supprimer log
            [
                'class' => 'kartik\grid\ActionColumn',
                'visible' => Yii::$app->user->getUser()->getIsAdmin(),
                'template' => '{delete}',
                'hiddenFromExport' => true,
                'buttons' => [
                    'delete' => function($url, $model) {
                        $url = \Yii::$app->urlManager->createUrl(['log/delete', 'id' => $model->log_id]);
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['class' => 'action', 'title' => Yii::t('yii', 'Delete'), 'data-confirm' =>  'Etes vous sûr de vouloir supprimer ce log?', 'data-method' => 'post']);
                    }
               ]
            ]
            

        ],
    ]); ?>
    </div>
</div>
<?php
//$js = "$(document).on('ready pjax:success', function() { jQuery('#logsearch-createdat').LogSearch[createdAt](); });";
//$js = "$(document).on('pjax:complete', function() {
//    $('#logsearch-createdat').datepicker(". yii\helpers\Json::htmlEncode(['language' => 'fr','dateFormat' => 'dd-MM-yyyy']).");
//});";
//$this->registerJs($js, \yii\web\View::POS_END);