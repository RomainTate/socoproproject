<?php
$this->title = 'Gérer le champ sectoriel : ' . $file['label'];
$this->params['breadcrumbs'][] = ['label' => 'Administration : champs sectoriels', 'url' => ['index', 'secteurID' => 'all']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="x_panel">
    <div class="x_title">
        <h1> <?= yii\helpers\Html::encode($this->title) ?>  </h1>
    </div>

    <div>
        <h4>
            Options existantes :
        </h4>
        <ul class="list-group">
            <?php foreach ($file['value'] as $item) : ?>
                <li class="list-group-item">
                    &#8226; <?= $item ?>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <h4>
        Ajouter un élément
    </h4>

    <?php \yii\widgets\ActiveForm::begin(['method' => 'post']); ?>
    <p>
        <?= yii\helpers\Html::textInput('newElem', null, ['class' => 'form-control']); ?>
    </p>
    <?= yii\helpers\Html::submitButton('Ajouter', ['class' => 'btn btn-success']); ?>
    <?php \yii\widgets\ActiveForm::end(); ?>
</div>