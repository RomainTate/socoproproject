<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
$associationsAlreadyIn = yii\helpers\ArrayHelper::map(app\models\Association::find()->select('association.entr_id')->joinWith('entreprise')->joinwith('contacts',true, 'inner join')->where(['contact.cont_id' => $contactId])->all(), 'entr_id','entr_id');
$associations = yii\helpers\ArrayHelper::map(\app\models\Association::find()->joinWith('entreprise')->joinWith('entreprise.contacts')->select(['entreprise.entr_nom', 'association.entr_id'])->where(['not in','association.entr_id',$associationsAlreadyIn])->all() , 'entr_id', 'entreprise.entr_nom');

if (!empty($associations)) :?>
    <h3> Rechercher une association déjà existante </h3>
        <?php ActiveForm::begin() ;

        echo Html::dropDownList('associationId', null, $associations, ['class' => 'form-control']) ?>
        <div class="form-group">
            <?= Html::submitButton('Ajouter cette association', ['class' => 'btn btn-form btn-success']) ?>
        </div>
        <?php ActiveForm::end();
else :
    echo "<h3> Aucune association n'a été trouvée </h3> <hr />";
endif;
if (Yii::$app->user->can('createAssociation')) : ?>
    <h3> Créer une association </h3>
        <?= $this->render('/association/_form', [
            'contactId' => $contactId])?>
<?php endif; ?>