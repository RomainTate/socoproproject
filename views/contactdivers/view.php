<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Contactdivers */

$this->title = $model->contact->cont_nom . ' ' . $model->contact->cont_prenom;
$this->params['breadcrumbs'][] = ['label' => 'Contact Divers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consommateur-view">
    <h1 class="pull-left">Profil de <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?></h1>
    <div class="pull-right">
        <?php
        if (Yii::$app->user->can('deleteContactDivers'))
            echo Html::a('Supprimer le contact', ['delete', 'id' => $model->cont_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Êtes-vous sûr de vouloir supprimer ce contact?',
                    'method' => 'post',
                ],
            ])
            ?>
    </div> 
    <div class="clearfix"></div>
    <div class="row">

        <div class="col-lg-6 col-xs-12 profile_left" >
            <div class="x_panel" >
                <div class="x_content">
                    <div class="text-right">
                        <p style="font-size: 11px">
                            <?php echo ($model->contactCreatedAt) ? 'Ajouté le ' . Yii::$app->formatter->asDatetime($model->contactCreatedAt) : 'date d\'ajout inconnue'; ?>
                        </p>
                    </div>
                    <div class="img-profil">
                        <img src="../css/images/user.png" alt="producteur" />
                    </div>

                    <h3>
                        <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?>
                    </h3>

                    <div class="mainInfos">
                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->contactTelephonePrincipal) : ?> 
                                <?= $model->contactTelephonePrincipal ?>
                            <?php else : ?>
                                Numéro Téléphone inconnu
                            <?php
                            endif;
                            if (Yii::$app->user->can('createContactDivers'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_telephone_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 
                        
                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->contactGSMPrincipal) : ?> 
                                <?= $model->contactGSMPrincipal ?>
                            <?php else : ?>
                                Numéro GSM inconnu
                            <?php
                            endif;
                            if (Yii::$app->user->can('createContactDivers'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_gsm_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                            <?php if ($model->contactEmailPrincipal) : ?>
                            <?= Html::a(yii\helpers\BaseStringHelper::truncate($model->contactEmailPrincipal, 40, '...'), 'mailto:'.$model->contactEmailPrincipal) ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php
                            endif;
                            if (Yii::$app->user->can('createContactDivers'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_email_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
<?php /*
                        <p> <i class="fa fa-calendar" aria-hidden="true"></i> 
                            <?php if ($model->contactAnneeNaissance) : ?>
                                <?= Yii::$app->formatter->asDate($model->contactAnneeNaissance) ?> 
                            <?php else : ?>
                                Année de naissance inconnue
                            <?php
                            endif;
                            if (Yii::$app->user->can('createContactDivers'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_annee_naissance']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>
*/?>
                        <h3>Adresse</h3>
                        
                            <?php if ($model->contact->adresse) : ?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                    <?php if($model->contact->adresse->adr_rue != null && $model->contact->adresse->adr_rue != '' ):?>
                                        <?php if (strlen($model->contact->adresse->adr_rue) < 40) :?>
                                                <?= $model->contact->adresse->adr_rue ?>
                                            <?php else : ?>
                                                <?php
                                                $explodedAdr = explode(' ', $model->contact->adresse->adr_rue);
                                                $okRue = [];
                                                $indexOkRue = 0;
                                                for($i=0; $i < count($explodedAdr); $i++) { //ok
                                                    $okRue[$indexOkRue] = $explodedAdr[$i];
                                                    while( isset($explodedAdr[$i+1]) && ((strlen($okRue[$indexOkRue]) + strlen($explodedAdr[$i+1])) < 40) ) {
                                                        $okRue[$indexOkRue] .= ' ' . $explodedAdr[$i+1];
                                                        $i++;
                                                    }
                                                    $indexOkRue++;
                                                }
                                                for($j=0; $j < count($okRue); $j++) {
                                                    echo $okRue[$j];
                                                    if(isset ($okRue[$j+1])) {
                                                        echo '</p>';
                                                        echo '<p><i class="fa fa-map-marker" aria-hidden="true"></i> ';
                                                    }
                                                }
                                                ?>
                                            <?php endif ?>
                                        <?php else : ?>
                                            <?= 'Numéro et Rue inconnus'?>
                                        <?php endif; ?>
                                        <?php if (Yii::$app->user->can('createContactDivers')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                    </p>

                                <?php if ($model->contact->adresse->adr_code_postal != null && $model->contact->adresse->adr_code_postal != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Code Postal: '. $model->contact->adresse->adr_code_postal?>
                                        <?php if (Yii::$app->user->can('createContactDivers')) {
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        }?>
                                    </p>
                                <?php endif?>
                                    
                                <?php if ($model->contact->adresse->adr_ville != null && $model->contact->adresse->adr_ville != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Localite: '. $model->contact->adresse->adr_ville?>
                                        <?php if (Yii::$app->user->can('createContactDivers')) {
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        }?>
                                    </p>
                                <?php endif;?>
                                    
                                <?php if ($model->contact->adresse->adr_province != null && $model->contact->adresse->adr_province != '') :?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?= 'Province: '. $model->contact->adresse->adr_province?>
                                        <?php if (Yii::$app->user->can('createContactDivers')) {
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->contact->adr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                        }?>
                                    </p>
                                <?php endif;?>
                            <?php else :?>
                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                            <?= 'Adresse inconnue';?>
                            <?php if (Yii::$app->user->can('createContactDivers')){
                                    echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/addadresse','id' => $model->cont_id]),
                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            }?>
                        </p>
                            <?php endif;?>
                    </div>

                    <?php
                    if (Yii::$app->user->can('createContactDivers'))
                        echo '<div class="text-right">' .
                        \yii\bootstrap\Html::button('Modifier les informations du contact', ['value' => \yii\helpers\Url::to(['/contactdivers/update', 'id' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>';
                    ?>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xs-12">
            <div class="x_panel secondInfos">

                <div class="x_content" >
                    <h2>Autres informations </h2> 

                    <ul class="autres-infos">

                        <li>
                            <h3>
                                Membre du collège : <?php echo ($model->contact->membrecollege) ? 'oui':'non';?>
                            </h3>
                        </li>
                        <li>
                            <h4>
                                <i class="glyphicon glyphicon-chevron-right"></i>
                                Membre commission filière :</h4>
                                <?php if (!$model->contact->membrecommission) : ?>
                                    <h4>Non</h4>
                                <?php else : ?> 
                                <ul class="fa-ul">
                                        <?php
                                        foreach ($model->contact->membrecommission as $filiere) {
                                            echo "<li><h4>" . app\models\Secteur::find()->where(['sect_id' => $filiere->sect_id])->select('sect_nom')->one()->sect_nom . "</h4></li>";
                                        }
                                        ?>
                                    </ul>
                                <?php endif; ?> 
                            </h4>
                        </li>
                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            <?php if ($model->secteurs) : ?>
                                Centres d'intérêts :
                                <ul class="fa-ul">
                                    <?php
                                    foreach ($model->secteurs as $secteur) {
                                        echo "<li>" . $secteur->sect_nom . "</li>";
                                    }
                                    ?>
                                </ul>
                            <?php else : ?> 
                                Aucun centre d'intérêt
                            <?php endif; ?> 

                        </li>
                        <?php /*
                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            Fonction : 
                            <?php if ($model->divers_fonction) : ?>
                                <?= $model->divers_fonction ?>
                            <?php else : ?> 
                                inconnue
                            <?php endif; ?> 
                        </li>
                        */?>
                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            Invitation AS/AG :
                            <?php
                            switch ($model->divers_AS_AG) :
                                case 1: echo 'AS';
                                    break;
                                case 2: echo 'AG';
                                    break;
                                case 3: echo 'AS / AG';
                                    break;
                                default : echo 'non';
                                    break;
                            endswitch;
                            ?>
                        </li>
                    </ul>

                    <?php if (Yii::$app->user->can('createContactDivers'))
                                echo '<div class="text-right">' .
                    \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/contactdivers/addmoreinfos', 'contactId' => $model->cont_id]),
                        'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>';
                    ?>
                    <hr />
                    <h2>Newsletters</h2>
                    <ul>
                        <li>
                            <?php if($model->contact->newsletters) :?>
                                <ul class="fa-ul">
                                    <?php foreach ($model->contact->newsletters as $newsletter) : ?>
                                        <li><i class='glyphicon glyphicon-chevron-right'></i> <?= $newsletter->newsletter_nom ?> </li>
                                    <?php                                endforeach;?>
                                </ul>
                            <?php else : ?>
                                aucune
                            <?php endif;?>
                        </li>
                    </ul>
                        <?php echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/contact/managenewsletters', 'id' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                    <hr />
                    <h2>Entreprises-Associations </h2>
                            <?php if (Yii::$app->user->can('createContactDivers')): ?>
                                <div class="text-right">
                                    <?= Html::button('Ajouter une entreprise', ['value' => \yii\helpers\Url::to(['/contact/addentreprise', 'contactId' => $model->cont_id]), 'class' => 'btn btn-success', 'name' => 'modalButton']) ?> 
                                </div>
                                <?php
                            endif;
                            if (!$model->contact->entreprises) {
                                echo 'Aucune Entreprise';
                            } else {
                                    foreach ($model->contact->entreprises as $entreprise) :?>
                                        <h4> <?= $entreprise->entr_societe ?></h4> 
                                        <?=$this->render('../entreprise/preview', array('model'=>$model, 'entreprise'=>$entreprise));
                                        if ((Yii::$app->user->can('createEntreprise')) || Yii::$app->user->can('createContactDivers')) {
                                            echo '<div class="text-right">';
                                            if (Yii::$app->user->can('createEntreprise'))
                                                echo \yii\bootstrap\Html::button('Modifier', ['value' => \yii\helpers\Url::to(\yii\helpers\Url::home().'entreprise/update?id=' . $entreprise->entr_id), 'name' => 'modalButton', 'class' => "btn btn-info"]);
                                            if (Yii::$app->user->can('createContactDivers'))
                                                echo \yii\bootstrap\Html::a("Enlever l'entreprise", \yii\helpers\Url::to(['/contact/deleteentreprise', 'contactId' => $model->cont_id, 'entr_id' => $entreprise->entr_id]), ['class' => 'btn btn-danger', 'data-confirm' => 'Retirer ce contact de cette entreprise ? Ceci ne supprimera pas l\'entreprise de la base de données']);
                                            echo '</div>';
                                        }
                                        echo '<hr />';
                                    endforeach;
                                }
                            ?>
                    </div>
                </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php
                    echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
                        'entity' => (string) 'contactDivers-' . $model->cont_id, // type and id
                    ]);
                    ?>
                </div>
            </div>
        </div> 
    </div>
</div>

<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home().'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>