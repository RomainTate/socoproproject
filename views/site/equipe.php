<?php
/* @var $this yii\web\View */
/* @var $users[] yii\model\user */
use yii\helpers\Html;

$this->title = 'l\'Equipe';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php endif; ?>

<?php if (Yii::$app->session->hasFlash('danger')): ?>
    <div class="alert alert-danger alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?= Yii::$app->session->getFlash('danger') ?>
    </div>
<?php endif; ?>

<div class="site-about">
    <div class="x_panel">
        <h1 class="x_title"><?= Html::encode($this->title) ?></h1>
        <div class="x_content">
            <table id="usersTable" class="table table-bordered table-hover">
                <thead>
                    <tr>
                        <th>Accès</th>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>E-mail</th>
                        <th>GSM</th>
                        <th>Téléphone</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($users as $user) : ?>
                        <tr>
                            <td><?= $user['Active'] == 'Active' ? 'Oui' : 'Non' ?></td>
                            <td><?= htmlspecialchars($user['Nom']) ?></td>
                            <td><?= htmlspecialchars($user['Prenom']) ?></td>
                            <td><?= htmlspecialchars($user['email']) ?></td>
                            <td><?= htmlspecialchars($user['GSM']) ?></td>
                            <td><?= htmlspecialchars($user['Telephone']) ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    <?php if( !Yii::$app->user->isGuest &&  Yii::$app->user->getUser()->getIsAdmin()) : ?>
    <div class="x_panel">
        <h2 class="x_title">Ajouter un membre de l'équipe</h2>
        <?= $this->render('addInactiveUserForm', [
            'model' => new \app\models\InactiveUser()
        ]) ?>
    </div>
    <div class="x_panel">
        <h2 class='x_title'>Supprimer un membre de l'équipe</h2>
        <?php \yii\widgets\ActiveForm::begin([
        'action' => ['/site/deleteinactiveuserform']
        ]); ?>
        <div class="form-group">
        <?= Html::dropDownList('idInactiveUser', NULL, yii\helpers\ArrayHelper::map(\app\models\InactiveUser::find()->all(), 'id', 'fullName')) ?>
        </div>
        <?= Html::submitButton('Supprimer', ['class' => 'btn btn-danger']) ?>
        <?php \yii\widgets\ActiveForm::end() ?>
    </div>
    <?php endif ?>
</div>
<?php
$this->registerJsFile(\yii\helpers\Url::home() . 'js/jquery.dataTables.min.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(\yii\helpers\Url::home() . 'js/dataT.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerCssFile(\yii\helpers\Url::home() . 'css/jquery.dataTables.min.css');
