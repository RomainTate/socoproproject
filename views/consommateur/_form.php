<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Consommateur */
/* @var $contactModel app\models\Contact */
/* @var $form yii\widgets\ActiveForm */

$suggest = yii\helpers\ArrayHelper::map(\app\models\Lieuprisecontact::find()->select('lieu')
        ->where(['not' , ['lieu' => null]])->all() , 'lieu', 'lieu');
$suggest = array_values($suggest);  
$fctsuggest = \app\models\Consommateur::find()->select(['conso_fonction'])->where(['not' , ['conso_fonction' => null]])->all();
$societesuggest = \app\models\Consommateur::find()->select(['conso_societe'])->where(['not' , ['conso_societe' => null]])->all();

    if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert <?php echo Yii::$app->user->getUser()->getIsAdmin()? " alert-warning ": " alert-danger ";?> alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h5><i class="icon fa fa-times"></i><?php echo Yii::$app->user->getUser()->getIsAdmin()? " Attention !" : " Erreur !"; ?></h5>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif;
?>

<div class="consommateur-form">
    <?php if(!Yii::$app->request->isAjax) :?>
    <div class="col-sm-6">
    <?php            endif;?>
        
    <?php $form = ActiveForm::begin([
        'id' => 'consommateurForm'
    ]); ?>

    <?= $form->field($contactModel, 'cont_civilite')->textInput(['maxlength' => true])->dropDownList(['Mr' => 'Mr' , 'Mme' => 'Mme']) ?>

    <?= $form->field($contactModel, 'cont_nom')->textInput(['maxlength' => true]) ?>
    <?= $form->field($contactModel, 'cont_prenom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($contactModel, 'cont_telephone_principal')->textInput(['maxlength' => true, 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    <?= $form->field($contactModel, 'cont_gsm_principal')->textInput(['maxlength' => true, 'onkeydown' => "if(event.keyCode==32) return false;"]) ?>
    <?= $form->field($contactModel, 'cont_email_principal')->textInput(['maxlength' => true]) ?>
    <?= $this->render('../newsletter/_form', array('model'=>$contactModel)) ?>
    
    <?= $form->field($model, 'secteurs')
        ->dropDownList(yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Intérêt') 
    ?>

    <?= $form->field($model, 'consoLieuContactName')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => $suggest,
            'appendTo'=>'#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]); ?>
        
    <?= $form->field($model, 'conso_societe')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => array_values(yii\helpers\ArrayHelper::map($societesuggest, 'conso_societe', 'conso_societe')),
            'appendTo'=>'#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]);?>
        
    <?= $form->field($model, 'conso_fonction')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => array_values(yii\helpers\ArrayHelper::map($fctsuggest, 'conso_fonction', 'conso_fonction')),
            'appendTo'=>'#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]);?>
        
    <?php if ($model->isNewRecord) : ?>
        <h3 class="x_title">Adresse</h3>
        <?= $this->render('../adresse/_partial', array('adrModel' => $adrModel, 'form' => $form)) ?>
        <div class="form-group">
            <?php if ((!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') : ?>
            <?= Html::label('Commentaire', 'consoComment', ['class' => 'control-label']) ?>
            <?= Html::textarea('comment', null, ['id' => 'consoComment', 'class' => 'form-control']) ?>
            <?php endif; ?>
        </div>
    <?php endif; ?>


    <div class="form-group">
        <?php if (Yii::$app->session->hasFlash('error') && Yii::$app->user->getUser()->getIsAdmin()) echo Html::submitButton($model->isNewRecord ? 'Créer quand même' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value' => 'forceAdd', 'name'=>'forceadd']);
              else echo Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end(); ?>
    <?php if(!Yii::$app->request->isAjax) :?>
    </div>
    <?php            endif;?>
</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');
//$this->registerJS("$(function (){
//    $('#consommateur-fct').attr('disabled', 'disabled');
//       
//   });
//   
//$('#consommateur-nomentr').on('input',function(){
// if($(this).val() == '') {
//    $('#consommateur-fct').attr('disabled', 'disabled');
// } else {
//    $('#consommateur-fct').removeAttr('disabled');
//    }
//});
//$('#consommateur-nomentr').change('input',function(){
// if($(this).val() == null) {
//    $('#consommateur-fct').attr('disabled', 'disabled');
// } else {
//    $('#consommateur-fct').removeAttr('disabled');
//    }
//});
//
//
//
//   
//");
?>
