<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Evenement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="evenement-form">
    <div class="x_panel">
    <?php $form = ActiveForm::begin(); ?>

        <h2>
            &Eacute;vènement
        </h2>
    <?= $form->field($model, 'evenement_nom')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'evenement_date')->widget(\yii\jui\DatePicker::className(),[
            'model' => $model,
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        'options' => ['class' => 'form-control']
        ])
        ?>
    <?= $form->field($model, 'evenement_type')->dropDownList(['1' => 'Assemblée sectorielle', '2' => 'Assemblée générale', '3' => 'Réunion Collège', '4' => 'Voyage / Colloque', '5' => 'Autre' ])  ?>   
 
           <?= Html::label('Secteurs') ?>
            <?=        Html::dropDownList('evenement_secteurs', null, yii\helpers\ArrayHelper::map(app\models\Secteur::find()->orderBy('sect_nom')->all(), 'sect_id','sect_nom'), ['class' => 'form-control js-multiple' , 'multiple'=>'multiple']) ?>
        <hr />
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');