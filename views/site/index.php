<?php
$this->title = 'CollegeDB';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bienvenue!</h1>
        <p class="lead">Bienvenue sur la base de données du collège.</p>
    </div>

    <div class="body-content">
        <?php if( !Yii::$app->user->isGuest &&  (Yii::$app->user->getUser()->getIsAdmin() || \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission' )) : ?>
        <div class="row">
            <h2> Suivis</h2>
            <?php $suivis = app\models\Suivi::find()->where(['user_id' => Yii::$app->user->id, 'suivi_suite' => 1])->all();?>
            <?php if(empty($suivis)) : ?>
            Vous n'avez pas de suivi en attente
            <?php else : ?>
            Vous avez <?= count($suivis) ?> suivis en attente : 
            <ul>
            <?php foreach ($suivis as $suivi) : ?>
                <li>
                <?= 'le '. Yii::$app->formatter->asDatetime($suivi->suivi_date). ' avec ' . \yii\helpers\Html::a($suivi->contact->producteur->getName(), yii\helpers\Url::to(['producteur/view', 'id' => $suivi->cont_id]))?>
                <?php if($suivi->suivi_commentaire) echo yii\helpers\Html::a(' (Voir le commentaire)', null, [ 'name' => "modalButton", 'value'=>\yii\helpers\Url::to(['/suivi/showcom', 'id' => $suivi->suivi_id]) ]);?>
                </li>
            <?php endforeach ?>
            </ul>
            <?php endif;?>
        </div>
        <hr />
        <?php endif; ?>
        <div class="row">
            <?php if (Yii::$app->user->can('indexProducteur')) :?>
                <div class="col-lg-4">
                    <div class="x_panel">
                        <img class="pull-left" src="css/images/farmer.png" alt="producteur" />
                        <h2>Producteur</h2>
                            <ul>
                                <li>
                                    <a href="producteur">&rarr; Voir les producteurs</a>
                                </li>
                                <?php if (Yii::$app->user->can('createProducteur')) :?>
                                    <li>
                                        <a href="producteur/create">&rarr; Ajouter un producteur</a>
                                    </li>
                                <?php endif ?>
                                <li>
                                    <a href="producteur/search">&rarr; Effectuer une recherche</a>
                                </li>
                            </ul>

                    </div>
                </div>
            <?php endif ?>
            <?php if (Yii::$app->user->can('indexJournaliste')) :?>
                <div class="col-lg-4">
                    <div class="x_panel">
                        <img class="pull-left" src="css/images/news.png" alt="journaliste" style="width: 128px;" />
                        <h2>Journaliste</h2>
                            <ul>
                                <li>
                                    <a href="journaliste">&rarr; Voir les journalistes</a>
                                </li>
                                <?php if (Yii::$app->user->can('createJournaliste')) :?>
                                    <li>
                                        <a href="journaliste/create">&rarr; Ajouter un journaliste</a>
                                    </li>
                                <?php endif ?>
                                <li>
                                    <a href="journaliste/search">&rarr; Effectuer une recherche</a>
                                </li>
                            </ul>

                    </div>
                </div>
            <?php endif ?>
<!--            <div class="col-lg-4">
                <div class="x_panel">
                    <img class="pull-left" src="css/images/manager.png" alt="R.E.A." />
                    <h2>R.E.A.</h2>

                        <ul>
                            <li>
                                <a href="rechadminencad">&rarr; Voir les R.E.A.</a>
                            </li>
                            <li>
                                <a href="rechadminencad/create">&rarr; Ajouter un R.E.A.</a>
                            </li>
                            <li>
                                <a href="rechadminencad/search">&rarr; Effectuer une recherche</a>
                            </li>
                        </ul>
               
                </div>
            </div>-->
            <?php if (Yii::$app->user->can('indexConso')) :?>
                <div class="col-lg-4">
                    <div class="x_panel">
                        <img class="pull-left" src="css/images/basket.png" alt="consommateur" />
                        <h2>Consommateur</h2>
                            <ul>
                                <li>
                                    <a href="consommateur">&rarr; Voir les consommateurs</a>
                                </li>
                                <?php if (Yii::$app->user->can('createConso')) :?>
                                    <li>
                                        <a href="consommateur/create">&rarr; Ajouter un consommateur</a>
                                    </li>
                                <?php endif ?>
                                <li>
                                    <a href="consommateur/search">&rarr; Effectuer une recherche</a>
                                </li>
                            </ul>
                    </div>
                </div>
            <?php endif ?>
            <div class="col-lg-4">
                <div class="x_panel">
                    <img class="pull-left" src="css/images/user.png" alt="contact divers" />
                    <h2>Contacts Divers</h2>
                        <ul>
                            <li>
                                <a href="contactdivers">&rarr; Voir les contacts divers</a>
                            </li>
                            <?php if(Yii::$app->user->can('createContactDivers')) : ?>
                                <li>
                                    <a href="contactdivers/create">&rarr; Ajouter un contact divers</a>
                                </li>
                            <?php endif ?>
                            <li>
                                <a href="contactdivers/search">&rarr; Effectuer une recherche</a>
                            </li>
                        </ul>
                </div>
            </div>
<!--            <div class="col-lg-4">
                <div class="x_panel">
                    <img class="pull-left" src="css/images/tractor.png" alt="exploitation" style="width: 128px" />
                    <h2>Exploitation</h2>

                        <ul>
                            <li>
                                <a href="exploitation">&rarr; Voir les exploitations</a>
                            </li>
                            <li>
                                <a href="exploitation/create">&rarr; Ajouter une exploitation</a>
                            </li>
                            <li>
                                <a href="exploitation/search">&rarr; Effectuer une recherche</a>
                            </li>
                        </ul>
               
                </div>
            </div>-->
            <?php if (Yii::$app->user->can('indexJournaliste')) : ?>
                <div class="col-lg-4">
                    <div class="x_panel">
                        <img class="pull-left" src="css/images/television.png" alt="media" style="width: 128px;"/>
                        <h2>Médias</h2>
                            <ul>
                                <li>
                                    <a href="media">&rarr; Voir les médias</a>
                                </li>
                                <?php if (Yii::$app->user->can('createJournaliste')) : ?>
                                    <li>
                                        <a href="media/create">&rarr; Ajouter un média</a>
                                    </li>
                                <?php endif ?>
    <!--                            <li>
                                    <a href="journal/search">&rarr; Effectuer une recherche</a>
                                </li>-->
                            </ul>
                    </div>
                </div>
            <?php endif ?>
            <?php if (Yii::$app->user->can('indexEntreprise')) : ?>
                <div class="col-lg-4">
                    <div class="x_panel">
                        <img class="pull-left" src="css/images/teamwork.png" alt="entreprise" />
                        <h2>Entreprises</h2>
                            <ul>
                                <li>
                                    <a href="entreprise">&rarr; Voir les entreprises</a>
                                </li>
                                <?php if (Yii::$app->user->can('createEntreprise')) : ?>
                                    <li>
                                        <a href="entreprise/create">&rarr; Ajouter une entreprise</a>
                                    </li>
                                <?php endif ?>
    <!--                            <li>
                                    <a href="association/search">&rarr; Effectuer une recherche</a>
                                </li>-->
                            </ul>
                    </div>
                </div>
            <?php endif ?>
<!--            <div class="col-lg-4">
                <div class="x_panel">
                    <img class="pull-left" src="css/images/flats.png" alt="entreprise diverse" />
                    <h2>Entreprises Diverses</h2>

                        <ul>
                            <li>
                                <a href="entreprisedivers">&rarr; Voir les entreprises diverses</a>
                            </li>
                            <li>
                                <a href="entreprisedivers/create">&rarr; Ajouter une entreprise diverse</a>
                            </li>
                            <li>
                                <a href="entreprisedivers/search">&rarr; Effectuer une recherche</a>
                            </li>
                        </ul>
               
                </div>
            </div>-->
        </div>
    </div>
</div>
<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home() . 'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);