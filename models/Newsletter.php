<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "newsletter".
 *
 * @property integer $newsletter_id
 * @property string $newsletter_nom
 * @property Contact[] $contacts
 */
class Newsletter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'newsletter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['newsletter_nom'], 'required'],
            [['newsletter_nom'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newsletter_id' => 'Newsletter ID',
            'newsletter_nom' => 'Newsletter Nom',
        ];
    }
    
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['cont_id' => 'cont_id'])->viaTable('recevoir', ['newsletter_id' => 'newsletter_id']);
    }
}
