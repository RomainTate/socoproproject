<?php

namespace app\controllers;

use Yii;
use app\models\Media;
use app\models\MediaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MediaController implements the CRUD actions for Media model.
 */
class MediaController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search', 'view'],
                        'roles' => ['indexJournaliste'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'addmoreinfos' , 'addjournal', 'deletemedia', 'quickupdate', 'addadresse'],
                        'roles' => ['createJournaliste'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteJournaliste'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Media models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MediaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Media model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Media model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Media();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            if (isset($post['contactToAdd'])) { //if user adds Media from contact view
                $journaliste = \app\models\Journaliste::find()->where(['cont_id' => $post['contactToAdd']])->one();
                $journaliste->media_id = $model->media_id;
                $journaliste->save();
                return $this->redirect(['journaliste/view', 'id' => $post['contactToAdd']]);
            }
            return $this->redirect(['view', 'id' => $model->media_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Media model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $fullform = false)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            if (isset($post['contactToAdd'])) { //if user adds MEdia from contact view
                $journaliste = \app\models\Journaliste::find()->where(['cont_id' => $post['contactToAdd']])->one();
                $journaliste->media_id = $model->media_id;
                return $this->redirect(['journaliste/view', 'id' => $post['contactToAdd']]);
            }
            if (isset($post['ajax']) && $post['ajax'] == 1) {
                return $this->redirect(Yii::$app->request->referrer);
            }
            return $this->redirect(['view', 'id' => $model->media_id]);
        } else {
            if(!Yii::$app->request->isAjax) {
                return $this->render('update', [
                    'model' => $model,
                    'fullform' => $fullform
                ]);
            } else {
               return $this->renderAjax('update', [
                    'model' => $model,
                   'fullform' => $fullform
                ]); 
            }
        }
    }
    
    public function actionQuickupdate($id, $attribute) {
        $model = $this->findModel($id);
        $type = $model->getTableSchema()->getColumn($attribute)->type;
        $post = Yii::$app->request->post();
        if ($post) {
            if ($type =='date') {
                $model->$attribute = date("Y-m-d", strtotime($post['Media'][$attribute]));
            } else {
                $model->$attribute = $post['Media'][$attribute];
            }
            $model->save();
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        } else {
            return $this->renderAjax('quickform/updateform', [
                        'model' => $model,
                        'attribute' => $attribute,
                        'type' => $type
            ]);
        }
    }
    
    public function actionAddmoreinfos ($id) {
        $post = Yii::$app->request->post();
        $model = $this->findModel($id);
        if($model->load($post)) {
            if($model->save()) {
                return $this->redirect(['view', 'id' => $id]);
            }
        } else {
            return $this->renderAjax('_formInfos', [
                        'model' => $model,
            ]);
        }
    }    
     public function actionSearch()
       {
//           $searchModel = new MediaSearch();
//           $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

           return $this->render('_search');
       }

    /**
     * Deletes an existing Media model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->request->referrer && (strpos(Yii::$app->request->referrer, 'index') !== false)) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Media model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Media the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Media::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAddadresse($id) {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($post) {
            $model->load($post);
            $model->adr_id = AdresseController::CreateAdresse($post);
            $model->save();
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        }
        return $this->renderAjax('/adresse/create', [
            'model' => new \app\models\Adresse(['scenario' => 'media']),
            'relatedModel' => $model
            ]);
    }
}
