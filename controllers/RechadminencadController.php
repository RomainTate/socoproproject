<?php

namespace app\controllers;

use Yii;
use app\models\Rechadminencad;
use app\models\RechadminencadSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RechAdminEncadController implements the CRUD actions for RechAdminEncad model.
 */
class RechadminencadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RechAdminEncad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RechAdminEncadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch() {
        return $this->actionIndex();
    }

    /**
     * Displays a single RechAdminEncad model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RechAdminEncad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RechAdminEncad();
        $contactModel = new \app\models\Contact();
        $post = Yii::$app->request->post();
        
        if ($contactModel->load($post) && $model->load($post)) {
            if ($contactModel->alreadyExists()) {
                if (!isset($post['forceadd'])) {
                    Yii::$app->session->setFlash('error', "Ce contact existe déjà !");
                    return $this->render('create', [
                                'model' => $model,
                                'contactModel' => $contactModel,
                    ]);
                }
            }
            $contactModel->cont_created_at = date("Y-m-d H:i:s");

            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
                if ($model->save(false)){ // save(false) bypass validation (already did 2 lines ago)
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'contactModel' => $contactModel,
            ]);
        }
    }

    /**
     * Updates an existing RechAdminEncad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $contactModel = $model->contact;
        $post = Yii::$app->request->post();

        if ($contactModel->load($post) && $model->load($post)) {
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                if ($model->save(false)) { // save(false) bypass validation (already did 2 lines ago)
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        }
        
        if (!Yii::$app->request->isAjax) {
            return $this->render('update', [
                        'model' => $model,
                        'contactModel' => $contactModel
            ]);
        }
        return $this->renderAjax('update', [
                    'model' => $model,
                    'contactModel' => $contactModel
        ]);
    }
    
    public function actionAddmoreinfos ($contactId) {
        $post = Yii::$app->request->post();
        $model = $this->findModel($contactId);
        if($model->load($post)) {
            if($model->save()&&($this->actionLinkSecteurs($model->cont_id,$post['Rechadminencad']['secteurs']))) {
                return $this->redirect(['view', 'id' => $contactId]);
            }
        } else {
            return $this->renderAjax('_formInfos', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RechAdminEncad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RechAdminEncad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RechAdminEncad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RechAdminEncad::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function actionLinkSecteurs($id, $secteurs) {
        // get all auth_item_child where 'parent' = $role & delete them
        \app\models\Travailler::deleteAll(['cont_id' => $id]);

        if (is_array($secteurs)) {
            foreach ($secteurs as $secteur) {
                $travail = new \app\models\Travailler();
                $travail->cont_id = $id;
                $travail->sect_id = $secteur;
                if (!$travail->save()) {
                    return false;
                }
            }
        }
        return true;
    }    
}
