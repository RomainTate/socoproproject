<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\ProducteurSearch */
/* @var $form yii\widgets\ActiveForm */
$get = Yii::$app->request->get('ProducteurSearch');
?>

<div class="producteur-search">

    <?php $form = ActiveForm::begin([
        'action' => 'search',
        'method' => 'get',
    ]); ?>
    <?= $form->field($model, 'contactTelephonePrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Numéro de téléphone (principal + associés)');?>
    <?= $form->field($model, 'contactGSMPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Numéro GSM (principal + associés)');?>
    <?= $form->field($model, 'contactEmailPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Adresse e-mail (principal + associés)');?>

    <?= $form->field($model, 'langue')->checkboxList(['FR' => 'FR', 'NL' => 'NL', 'DE' => 'DE'], ['separator'=>'<br/>'])    ?>
    <?= \yii\bootstrap\Html::checkbox('membres', Yii::$app->request->get('membres') ==1 ? true: false,['id' => 'membresCheck'])?>
    <?= yii\bootstrap\Html::label('Membres du collège', 'membresCheck')?>
    
    <?= $form->field($model, 'codepostal')
        ->dropDownList(ArrayHelper::map(\app\models\Adresse::find()->where(['not', ['adr_code_postal' => null]])->orderBy('adr_code_postal')->all(),'adr_code_postal' , 'adr_code_postal')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Code postal')
    ?>
    <?= $form->field($model, 'secteursID')
        ->dropDownList(ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Secteurs')
    ?>

    <?= $form->field($model, 'secteursOperator')->radioList([1 => 'Tous' , 0 => 'Au choix'], ['itemOptions' => ['disabled' => 'disabled']])->label('Secteurs sélectionnés');?>
<?php /*
        <?= $form->field($model, 'prod_activite_principale')->radioList(["1"=>"Principale","0"=>"Complémentaire", NULL => "Tous"])->label('Activité principale');?>
        <?= $form->field($model, 'prod_collab_echange')->radioList(["1"=>"Oui","0"=>"Non", NULL => "Tous"]);?>
        <?= $form->field($model, 'prod_porte_parole')->radioList(["1"=>"Oui","0"=>"Non", NULL => "Tous"]);?>
        <?= $form->field($model, 'prod_accepte_stagiaire')->radioList(["1"=>"Oui","0"=>"Non", NULL => "Tous"]);?>
        <?= $form->field($model, 'prod_qualite_diff')->radioList(["1"=>"Oui","0"=>"Non", NULL => "Tous"]);?>
        <?= yii\bootstrap\Html::checkbox('hasExploit', Yii::$app->request->get('hasExploit') ==1 ? true: false,['id' => 'hasExploitCheck']) ?>
        <?= yii\bootstrap\Html::label('Inclure seulement les producteurs faisant partie d\'une Exploitation', 'hasExploitCheck')?>
        <br />
        <?= yii\bootstrap\Html::checkbox('hasAssoc', Yii::$app->request->get('hasAssoc') ==1 ? true: false,['id' => 'hasAssocCheck']) ?>
        <?= yii\bootstrap\Html::label('Inclure seulement les producteurs faisant partie d\'une Association', 'hasAssocCheck')?>
        <br />
 */?>
    <?= yii\bootstrap\Html::checkbox('sigec', Yii::$app->request->get('sigec') ==1 ? true: false,['id' => 'sigecCheck']) ?>
    <?= yii\bootstrap\Html::label('Inclure les producteurs confidentiels', 'sigecCheck')?>

    <div class="form-group">
        <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php
$this->registerJs(
    "$('.js-multiple').select2();",
    yii\web\View::POS_READY,
    'select2-prod'
); 
$this->registerJsFile(\yii\helpers\Url::home().'js/prodSearch.js', ['depends' => [yii\web\JqueryAsset::className()]]);