<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


$suggest = ArrayHelper::map(\app\models\Journaliste::find()->select('journaliste_fonction')
        ->where(['not' , ['journaliste_fonction' => null]])->all() , 'journaliste_fonction', 'journaliste_fonction');
$suggest = array_values($suggest);  
?>

<div class="journaliste-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'journaliste_date_contact')->textInput()->widget(\yii\jui\DatePicker::className(),[
            'dateFormat' => 'dd-MM-yyyy',
            'options' => [
            'class' => 'form-control',]
        ])
        ?> 
    <?= $form->field($model, 'journaliste_fonction')->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => $suggest,
            'appendTo'=>'#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ],
    ]);?>

    <?= $form->field($model, 'journaliste_rubrique')->textInput(['maxlength' => true]) ?>
<?php /*    
    <?= $form->field($model, 'journaliste_langue')->dropDownList(["fr" => "Français", "nl" => "Néerlandais", "fr/nl" => "Français/Néerlandais"], ['prompt' => '']); ?>
*/?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>