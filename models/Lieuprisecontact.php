<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lieu_prise_contact".
 *
 * @property integer $lieu_prise_contact_id
 * @property string $lieu
 *
 * @property Consommateur[] $consommateurs
 */
class Lieuprisecontact extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lieu_prise_contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lieu'], 'required'],
            [['lieu'], 'string', 'max' => 100],
            [['lieu'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lieu_prise_contact_id' => 'Lieu Prise Contact ID',
            'lieu' => 'Lieu',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsommateurs()
    {
        return $this->hasMany(Consommateur::className(), ['conso_lieu_contact' => 'lieu_prise_contact_id']);
    }

}
