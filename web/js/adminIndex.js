$('#selectall').change(function () {
    if ($(this).prop('checked')) {
        $("input[type='checkbox']").prop('checked', true);
    } else {
        $("input[type='checkbox']").prop('checked', false);
    }
});


$('select[name=role]').change(function () {
    if ($('select[name=role]').val() == 'Chargé de mission') {
       $("#sectorsLink").show();
   } else {
       $("#sectorsLink").hide();
   }
   if ($('select[name=role]').val() === "") {
       $("#submitBtn").prop('disabled', true);
   } else {
       $("#submitBtn").prop('disabled', false);
   }
});


$(function (){
   $('select[name=role]').change();
});