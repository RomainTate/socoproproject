<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Media */
/* @var $form yii\widgets\ActiveForm */
if (!isset($model)) {
    $model = new app\models\Media();
    $action = \yii\helpers\Url::home().'media/create';
} else {
    $action = '';
}
?>

<div class="media-form">
        <div class="x_panel">
            <?php
            $form = ActiveForm::begin([
                        'action' => $action,
                        'id' => 'mediaForm'
            ]);
            ?>

            <?= $form->field($model, 'media_type_presse')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'media_type_support')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'media_frequence')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'media_audience')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'media_langue')->textInput(['maxlength' => true]) ?>
            <?= $form->field($model, 'media_pays')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
</div>
