<?php
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

$medias = yii\helpers\ArrayHelper::map(\app\models\Media::find()->select(['media_titre', 'media_id'])->all() , 'media_id', 'media_titre');
?>

<?php if (!empty($medias)) :?>
<button class="btn btn-info" id="addexistingentreprise" name="formchoice"> Rechercher un media déjà existant </button> 
<button class="btn btn-success" id="addnewentreprise" name="formchoice"> Créer un média </button> 

<div id="existingEntreprise" style="display: none">
<h3> Rechercher un media déjà existant </h3>
    <?php ActiveForm::begin() ?>

    <?= Html::dropDownList('entrepriseId',null, $medias , ['class' => 'form-control'])?>
<div class="form-group">
    <?= Html::submitButton('Ajouter ce media', ['class' =>  'btn btn-form btn-success' ]) ?>
</div>
    <?php ActiveForm::end() ?>
</div>
<?php endif;?>
<div id="newEntreprise" <?php if (!empty($medias)) :?> style="display: none" <?php endif?>>
<h3> Créer un média </h3>
    <?= $this->render('/media/_form', [
        'contactId' => $contactId
    ]) ?>
</div>

<?php // $this->registerJsFile(\yii\helpers\Url::home().'js/addentreprise.js', ['depends' => [yii\web\JqueryAsset::className()]]); 
$this->registerJS("   $('button[name=formchoice]').on('click' ,function () {
       if(this.id === 'addexistingentreprise') {
           $('#existingEntreprise').show();
           $('#newEntreprise').hide();
       } else if (this.id === 'addnewentreprise') {
           $('#existingEntreprise').hide();
           $('#newEntreprise').show();
       }
       
   });");?>