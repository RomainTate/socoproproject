<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contact */

$this->title = 'Mise à jour des newsletters';
?>
<div class="producteur-update">

    <?php ActiveForm::begin([
        'id' => 'Newsletterform'
    ]);?>
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model
    ])?>

    <?= Html::submitButton('Mettre à jour', ['class' => 'btn btn-info']) ?>
    <?php ActiveForm::end()?>
</div>