<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Association;

/**
 * AssociationSearch represents the model behind the search form about `app\models\Association`.
 */
class AssociationSearch extends Association
{
    public $entrepriseNom;
    public $entrepriseFormeJuridique;
    public $entrepriseTelephonePrincipal;
    public $entrepriseEmailPrincipal;
    public $entrepriseSiteInternet;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['association_type', 'entrepriseNom', 'entrepriseFormeJuridique', 'entrepriseTelephonePrincipal',
               'entrepriseEmailPrincipal', 'entrepriseSiteInternet' ], 'safe'],
            [['entr_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Association::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'entr_id' => $this->entr_id,
        ]);

        $query->andFilterWhere(['like', 'association_type', $this->association_type]);

        return $dataProvider;
    }
}
