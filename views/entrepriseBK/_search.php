<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EntrepriseSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="entreprise-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'entr_id') ?>

    <?= $form->field($model, 'entr_nom') ?>

    <?= $form->field($model, 'entr_forme_juridique') ?>

    <?= $form->field($model, 'entr_telephone_principal') ?>

    <?= $form->field($model, 'entr_email_principal') ?>

    <?php // echo $form->field($model, 'entr_site_internet') ?>

    <?php // echo $form->field($model, 'adr_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
