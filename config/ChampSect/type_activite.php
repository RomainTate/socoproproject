<?php

    return [
    'label' => 'Type d\'activité',
    'type' => 2,
    'value' =>[ 'Principal', 'Complémentaire', 'Hobbyiste']
    ];