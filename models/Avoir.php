<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "avoir".
 *
 * @property integer $cont_id
 * @property integer $activite_id
 *
 * @property Activite $activite
 * @property Contact $cont
 */
class Avoir extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'avoir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id', 'activite_id'], 'required'],
            [['cont_id', 'activite_id'], 'integer'],
            [['activite_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activite::className(), 'targetAttribute' => ['activite_id' => 'activite_id']],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'activite_id' => 'Activite ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivite()
    {
        return $this->hasOne(Activite::className(), ['activite_id' => 'activite_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCont()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
}
