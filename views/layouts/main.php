
<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
/**
 * @var string $content
 * @var \yii\web\View $this
 */
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage(); ?>
<?php if((!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') : ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="robots" content="noindex, nofollow" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
        <?php $this->head() ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="nav-sm">
        <?php $this->beginBody(); ?>
        <div class="container body">
            <div class="main_container">
                <?php if (!Yii::$app->user->isGuest) : ?>
                    <!-- top navigation -->
                    <div class="top_nav">
                        <div class="nav_menu">
                            <nav class="" role="navigation">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="">
                                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <?= Yii::$app->user->getUser()->username ?>
                                            <span class=" fa fa-angle-down"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                                            <li>
                                                <?=
                                                Html::a('<i class="fa fa-sign-out pull-right"></i> Se déconnecter', ['/user/logout'], ['data-method' => 'post']);
                                                ?>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <!-- /top navigation -->
                <?php endif; ?>
                <!-- page content -->
                <div class="right_col" role="main">
                    <?php if (isset($this->params['h1'])): ?>
                        <div class="page-title">
                            <div class="title_left">
                                <h1><?= $this->params['h1'] ?></h1>
                            </div>
                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                    <?= $content ?>
                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        CollegeDB
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>
        <!-- /footer content -->
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>

<?php else : ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta name="robots" content="noindex, nofollow" />
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet"> 
        <?php $this->head() ?>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="nav-md">
        <?php $this->beginBody(); ?>
        <div class="container body">

            <div class="main_container">
                <?php if (!Yii::$app->user->isGuest) : ?>
                    <div class="col-md-3 left_col">
                        <div class="left_col scroll-view">

                            <div class="navbar nav_title" style="border: 0;">
                                <a href="<?php echo \yii\helpers\Url::home();?>" class="site_title"><i class="fa fa-address-book-o"></i> <span>CollegeDB</span></a>
                            </div>
                            <div class="clearfix"></div>

                            <!-- menu prile quick info -->
                            <div class="profile clearfix">
                                <div class="profile_pic">
                                    <img src="<?php echo \yii\helpers\Url::home();?>css/images/logo.png" alt="..." class="img-circle profile_img">
                                </div>
                                <div class="profile_info">
                                    <span>Bienvenue,</span>
                                    <h2><?= Yii::$app->user->getUser()->username ?></h2>
                                </div>
                            </div>
                            <!-- /menu prile quick info -->

                            <br />

                            <!-- sidebar menu -->
                            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                                <div class="menu_section">
                                    <h3>Contacts</h3>
                                    <?php
                                    $items = [];
                                    
                                    if (Yii::$app->user->can('indexProducteur')) {
                                        $itemsProd = [];
                                        if (Yii::$app->user->can('createProducteur')) {
                                            array_push($itemsProd, ["label" => "Ajouter un producteur", "url" => ["/producteur/create"]]);
                                        }
                                        array_push($itemsProd, ["label" => "Voir tous les producteurs", "url" => ["/producteur"]]);
                                        array_push($itemsProd, ['label' => 'Effectuer une recherche', 'url' => ['/producteur/search']]);
                                        array_push($itemsProd, ['label' => 'Effectuer une recherche sectorielle', 'url' => ['/producteur/searchsectoriel']]);
                                        if(Yii::$app->user->getUser()->getIsAdmin()) {
                                            array_push($itemsProd, ['label' => 'Effectuer une recherche par activité', 'url' => ['/producteur/searchactivity']]);
                                        }
                                        array_push($items, [
                                            "label" => "Producteurs",
                                            "icon" => "user",
                                            "url" => "#",
                                            "items" => $itemsProd
                                        ]);
                                    }

                                    if (Yii::$app->user->can('indexJournaliste')) {
                                        $itemsJourn = [];
                                        if(Yii::$app->user->can('createJournaliste')) {
                                            array_push($itemsJourn, ["label" => "Ajouter un journaliste", "url" => ["/journaliste/create"]]);
                                        }
                                        array_push($itemsJourn, ["label" => "Voir tous les journalistes", "url" => ["/journaliste"]]);
                                        array_push($itemsJourn, ['label' => 'Effectuer une recherche', 'url' => ['/journaliste/search']]);
                                        array_push($items, [
                                            "label" => "Journalistes",
                                            "icon" => "user-secret",
                                            "url" => "#",
                                            "items" => $itemsJourn
                                        ]);
                                    }
                                    
//                                    if (Yii::$app->user->can('indexREA')) {
//                                        $itemsREA = [];
//                                        if(Yii::$app->user->can('createREA')) {
//                                            array_push($itemsREA, ["label" => "Ajouter un contact R.E.A.", "url" => ["/rechadminencad/create"]]);
//                                        }
//                                        array_push($itemsREA, ["label" => "Voir tous les contacts R.E.A.", "url" => ["/rechadminencad"]]);
//                                        array_push($itemsREA, ['label' => 'Effectuer une recherche', 'url' => ['/rechadminencad/search']]);
//                                        array_push($items, [
//                                            "label" => "Rech./ Admin./ Encad.",
//                                            "icon" => "university",
//                                            "url" => "#",
//                                            "items" => $itemsREA
//                                        ]);
//                                    }
                                    
                                    if (Yii::$app->user->can('indexConso')) {
                                        $itemsConso = [];
                                        if(Yii::$app->user->can('createConso')) {
                                            array_push($itemsConso, ["label" => "Ajouter un consommateur", "url" => ["/consommateur/create"]]);
                                        }
                                        array_push($itemsConso, ["label" => "Voir tous les consommateurs", "url" => ["/consommateur"]]);
                                        array_push($itemsConso, ['label' => 'Effectuer une recherche', 'url' => ['/consommateur/search']]);
                                        array_push($items, [
                                            "label" => "Consommateurs",
                                            "icon" => "user-circle",
                                            "url" => "#",
                                            "items" => $itemsConso
                                        ]);
                                    }
                                    
                                    $itemsContactDivers = [];
                                    if(Yii::$app->user->can('createContactDivers')) {
                                        array_push($itemsContactDivers, ['label' => 'Ajouter un contacts divers', 'url' => ['/contactdivers/create']]);
                                    }
                                    array_push($itemsContactDivers, ['label' => 'Voir les contacts divers', 'url' => ['/contactdivers']]);
                                    array_push($itemsContactDivers, ['label' => 'Effectuer une recherche', 'url' => ['/contactdivers/search']]);
                                    array_push($items, [
                                        "label" => "Contacts",
                                        "icon" => "address-book",
                                        "url" => "#",
                                        "items" => $itemsContactDivers
                                    ]);
                                    
    //                                    if (Yii::$app->user->can('indexProducteur')) {
                                        $itemsMembres = [];
    //                                        if (Yii::$app->user->can('createContucteur')) {
    //                                            array_push($itemsCont, ["label" => "Ajouter un producteur", "url" => ["/producteur/create"]]);
    //                                        }
                                        array_push($itemsMembres, ["label" => "Voir tous les membres du collège", "url" => ["/contact/indexmembres"]]);
                                        array_push($itemsMembres, ["label" => "Voir tous les membres des commissions fillières", "url" => ["/contact/indexmembrescommission"]]);
                                        array_push($items, [
                                            "label" => "Membres",
                                            "icon" => "user-plus",
                                            "url" => "#",
                                            "items" => $itemsMembres
                                        ]);
//                                    }
                                    echo \yiister\gentelella\widgets\Menu::widget(
                                            [
                                                "items" => $items
//                                            ,
                                            ])?>
                                </div>
                                <div class="menu_section">
                                    <h3>Groupes</h3>
                                    <?php 
                                    $items = [];
                                    if (Yii::$app->user->can('indexEntreprise')) {
                                        $itemsEntre = [];
                                        if (Yii::$app->user->can('createEntreprise')) {
                                            array_push($itemsEntre, ["label" => "Créer une entreprise", "url" => ["/entreprise/create"]]);
                                        }
                                        array_push($itemsEntre, ["label" => "Voir les entreprises", "url" => ["/entreprise"]]);
                                        array_push($items, [
                                            "label" => "Entreprises-Assoc.",
                                            "icon" => "home",
                                            "url" => "#",
                                            "items" => $itemsEntre
                                        ]);
                                    }

                                    if (Yii::$app->user->can('indexJournaliste')) {
                                        $itemsMedia = [];
                                        if(Yii::$app->user->can('createJournaliste')) {
                                            array_push($itemsMedia, ["label" => "Ajouter un média", "url" => ["/media/create"]]);
                                        }
                                        array_push($itemsMedia, ["label" => "Voir tous les médias", "url" => ["/media"]]);
                                        array_push($items, [
                                            "label" => "Médias",
                                            "icon" => "newspaper-o",
                                            "url" => "#",
                                            "items" => $itemsMedia
                                        ]);
                                    }
                                    $items[] = ['label' => "l'équipe", 'url' => ["/site/equipe"], "icon" => 'users'];
                                    echo \yiister\gentelella\widgets\Menu::widget(
                                            [
                                                "items" => $items
//                                                [
////                                                    [
////                                                        "label" => "Entreprises",
////                                                        "icon" => "building",
////                                                        "url" => "#",
////                                                        "items" => [
//////                                                        ["label" => "Ajouter un journaliste", "url" => ["/journaliste/create"]],
//////                                                            ["label" => "Voir toutes les entreprises", "url" => ["/entreprise"]],
////                                                            ["label" => "Voir les entreprises diverses", "url" => ["/entreprisedivers"]],
//////                                                        ['label' => 'Effectuer une recherche', 'url' => ['/journaliste/search']],
////                                                        ],
////                                                    ],
////                                                    [
////                                                        "label" => "Exploitations",
////                                                        "icon" => "home",
////                                                        "url" => "#",
////                                                        "items" => [
////                                                            ["label" => "Voir les exploitations", "url" => ["/exploitation"]],
////                                                        ],
////                                                    ],
//                                                    [
//                                                        "label" => "Médias",
//                                                        "icon" => "newspaper-o",
//                                                        "url" => "#",
//                                                        "items" => [
//                                                            ["label" => "Voir les médias", "url" => ["/media"]],
//                                                            ["label" => "Créer un média", "url" => ["/media/create"]],
//                                                        ],
//                                                    ],
//                                                    [
//                                                        "label" => "Entreprises-Assoc.",
//                                                        "icon" => "home",
//                                                        "url" => "#",
//                                                        "items" => [
//                                                            ["label" => "Voir les entreprises", "url" => ["/entreprise"]],
//                                                            ["label" => "Créer une entreprise", "url" => ["/entreprise/create"]],
//                                                        ],
//                                                    ],
//                                                ],
                                            ]
                                    )
                                    ?>
                                </div>
                                <?php if(Yii::$app->user->getUser()->getIsAdmin()) : ?>
                                <div class="menu_section">
                                    <h3>&Eacute;vènements</h3>
                                    <?php echo
                                    \yiister\gentelella\widgets\Menu::widget(
                                            [
                                                "items" => [ 
                                                    [
                                                    "label" => "Evènements",
                                                    "icon" => "home",
                                                    "url" => ['/evenement/']
                                                        ]
                                                    ]
                                                ]);?>
                                </div>
                                <?php endif ?>
                            </div>
                            <!-- /sidebar menu -->

                            <!-- /menu footer buttons -->
                            <div class="sidebar-footer hidden-small">
<!--                                <a data-toggle="tooltip" data-placement="top" title="Settings">
                                    <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
                                </a>
                                <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                                    <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
                                </a>
                                <a data-toggle="tooltip" data-placement="top" title="Lock">
                                    <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
                                </a>-->
                                <?=
                                Html::a('<span class="glyphicon glyphicon-off" aria-hidden="true"></span>', ['/user/logout'], ['data-method' => 'post','data-toggle' => "tooltip", 'data-placement' => "top", 'title' => "Logout"]);
                                ?>
                            </div>
                            <!-- /menu footer buttons -->
                        </div>
                    </div>
                    <!-- top navigation -->
                    <div class="top_nav">

                        <div class="nav_menu">
                            <nav class="" role="navigation">
                                <div class="nav toggle">
                                    <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                                </div>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="">
                                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                            <?= Yii::$app->user->getUser()->username ?>
                                            <span class=" fa fa-angle-down"></span>
                                        </a>
                                        <ul class="dropdown-menu dropdown-usermenu pull-right">
                                            <!--                                    <li><a href="javascript:;">  Profile</a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;">
                                                                                        <span class="badge bg-red pull-right">50%</span>
                                                                                        <span>Settings</span>
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    <a href="javascript:;">Help</a>
                                                                                </li>-->
                                            <?php if (Yii::$app->user->getUser()->getIsAdmin()) : ?>
                                                <li>
                                                    <?=
                                                    Html::a('<i class="fa fa-cog pull-right"></i> Panneau d\'administration', ['/user/admin/index']);
                                                    ?>
                                                </li>
                                                <li>
                                                    <?php echo Html::a('<i class="fa fa-upload pull-right"></i> Panneau d\'importation', ['/importation/index']);?>
                                                </li>
                                                <li>
                                                    <?php echo Html::a('<i class="fa fa-clock-o pull-right"></i> Logs généraux', ['/log/index']);?>
                                                </li>
                                                <!--<li>-->
                                                    <?php // echo Html::a('<i class="fa fa-handshake-o pull-right"></i> Suivis généraux', ['/suivi/index']);?>
                                                <!--</li>-->
                                            <?php endif; ?>
<!--                                            <li>
                                                <?=
                                                Html::a('<i class="fa fa-users pull-right"></i> L\'équipe', ['/site/equipe']);
                                                ?>
                                            </li>-->
                                            <li>
                                                <?=
                                                Html::a('<i class="fa fa-cog pull-right"></i> Paramètres du compte', ['/user/settings/account']);
                                                ?>
                                            </li>
                                            <li>
                                                <?=
                                                Html::a('<i class="fa fa-sign-out pull-right"></i> Se déconnecter', ['/user/logout'], ['data-method' => 'post']);
                                                ?>
                                            </li>

                                        </ul>
                                    </li>



                                </ul>
                            </nav>
                        </div>

                    </div>
                    <!-- /top navigation -->
                <?php endif; ?>
                <!-- page content -->
                <div class="right_col" role="main">
                    <?php if (isset($this->params['h1'])): ?>
                        <div class="page-title">
                            <div class="title_left">
                                <h1><?= $this->params['h1'] ?></h1>
                            </div>
                            <div class="title_right">
                                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Search for...">
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="button">Go!</button>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="clearfix"></div>
                    <?=
                    Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ])
                    ?>
                    <?= $content ?>
                </div>
                <!-- /page content -->
                <!-- footer content -->
                <footer>
                    <div class="pull-right">
                        CollegeDB
                    </div>
                    <div class="clearfix"></div>
                </footer>
                <!-- /footer content -->
            </div>

        </div>

        <div id="custom_notifications" class="custom-notifications dsp_none">
            <ul class="list-unstyled notifications clearfix" data-tabbed_notifications="notif-group">
            </ul>
            <div class="clearfix"></div>
            <div id="notif-group" class="tabbed_notifications"></div>
        </div>
        <!-- /footer content -->
        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage(); ?>
<?php endif;?>
