<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Producteur;
use yii\helpers\ArrayHelper;

/**
 * ProducteurSearch represents the model behind the search form about `app\models\Producteur`.
 */
class ProducteurSearch extends Producteur
{
    public $contactNom;
    public $contactPrenom;
    public $secteursID;
    public $contactCivilite;
    public $contactTelephonePrincipal;
    public $contactGSMPrincipal;
    public $contactEmailPrincipal;
    public $contactAnneeNaissance;
    public $contactCreatedAt;
    public $associesnames;
    public $exploitations;
    public $rue;
    public $localite;
    public $province;
    public $langue;
    public $codepostal;
    /*** search fields **/
    public $contactTelephonePrincipalNotNull = 0;
    public $contactGSMPrincipalNotNull = 0;
    public $contactEmailPrincipalNotNull = 0;
    public $secteursOperator = 0;
    public $prod_activite_principale = '';
    public $prod_collab_echange = '';
    public $prod_porte_parole = '';
    public $prod_accepte_stagiaire = '';
    public $prod_qualite_diff = '';
    /*** genereal ***/
    public $generalSearch;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [[/*'prod_activite_principale', 'prod_qualite_diff', 'prod_collab_echange',
                'prod_porte_parole', 'prod_accepte_stagiaire',*/ 'contactGSMPrincipalNotNull','contactTelephonePrincipalNotNull','secteursOperator',
                'contactEmailPrincipalNotNull', 'cont_id', 'prod_sigec', 'prod_numero_BCE', 'prod_statut_pro'], 'integer'],
            [['localite', 'province', 'rue', 'prod_numero_producteur', 'generalSearch'], 'string'],
            [['contactNom' , 'contactPrenom' , 'secteursID', 'contactTelephonePrincipal', 'contactGSMPrincipal', 'contactEmailPrincipal', 'secteursOperator', 
                'contactAnneeNaissance', 'contactTelephonePrincipalNotNull', 'contactGSMPrincipalNotNull',
                /*'exploitations'*/ 'prod_statut_juridique', 'prod_date_debut_activite', 'prod_nom_exploitation', 'localite', 'prod_statut_pro', 'langue', 'codepostal', 'generalSearch'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if(isset($params['sigec'])) {
            $query = Producteur::find();
        }else {
            $query = Producteur::find()->where(['prod_sigec' => 0]);
        }
        
        
        if (\app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission') {
            
            $condition = ['in', 'producteur.cont_id', ArrayHelper::map(Producteur::find()->select('producteur.cont_id')->innerJoin('travailler t', 'producteur.cont_id = t.cont_id')
                    ->innerJoin('secteur s', 't.sect_id = s.sect_id')->innerJoin('gerer g', 's.sect_id = g.sect_id')->where(['g.user_id' => \Yii::$app->user->getId()])->all(),'cont_id','cont_id')];

            $query->andFilterWhere($condition);
        }
        
        $query->joinWith(['contact c', 'contact.adresse ca', 'secteurs','associes','activites', 'associes.contact ac', 'contact.membrecollege']);
        
        $query->groupBy('producteur.cont_id');
        // add conditions that should always apply here
        
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => (array_key_exists('selectedPagination', $params)) ? ['pageSize' => $params['selectedPagination']]:[ 'pageSize' => 20 ],
        ]);
//        return $dataProvider;
        
        $dataProvider->setSort([
            'attributes' => [
                'contactNom' => [
                    'asc' => ['c.cont_nom' => SORT_ASC],
                    'desc' => ['c.cont_nom' => SORT_DESC],
                ],
                'contactPrenom' => [
                    'asc' => ['c.cont_prenom' => SORT_ASC],
                    'desc' => ['c.cont_prenom' => SORT_DESC],
                ],
                'contact.cont_civilite' => [
                    'asc' => ['c.cont_civilite' => SORT_ASC],
                    'desc' => ['c.cont_civilite' => SORT_DESC],
                ],
                'contactEmailPrincipalNotNull' => [
                    'asc' => ['c.cont_email_principal' => SORT_ASC],
                    'desc' => ['c.cont_email_principal' => SORT_DESC],
                ],
                'prod_sigec' => [
                    'asc' => ['prod_sigec' => SORT_ASC],
                    'desc' => ['prod_sigec' => SORT_DESC],
                ],
                'prod_nom_exploitation' => [
                    'asc' => ['prod_nom_exploitation' => SORT_ASC],
                    'desc' => ['prod_nom_exploitation' => SORT_DESC]
                ]

            ]
        ]);


        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
//            $query->joinWith('contact')->joinWith('contact.adresse')->joinWith('travailler')->joinWith('associes')->joinWith('activites'); //Eager loading
            return $dataProvider;
        }
        
        if(isset($params['ProducteurSearch']['prod_sigec'])) {
            $query->andFilterWhere(['prod_sigec' => $params['ProducteurSearch']['prod_sigec']]);
        }
        
        // grid filtering conditions
        $query->andFilterWhere([
//            'prod_activite_principale' => $this->prod_activite_principale,
//            'prod_collab_echange' => $this->prod_collab_echange,
//            'prod_porte_parole' => $this->prod_porte_parole,
//            'prod_accepte_stagiaire' => $this->prod_accepte_stagiaire,
//            'prod_qualite_diff' => $this->prod_qualite_diff,
            'prod_statut_pro' => $this->prod_statut_pro,
            'prod_sigec' => $this->prod_sigec,
            
//            'cont_id' => $this->cont_id,
        ]);
        
//        $query->andFilterWhere(['like', 'prod_prenom_epoux', $this->prod_prenom_epoux])
//            ->andFilterWhere(['like', 'prod_nom_epoux', $this->prod_nom_epoux]);
        
        if (isset($params['ProducteurSearch']['prod_numero_producteur']) && $params['ProducteurSearch']['prod_numero_producteur'] != '') {
            $query->andFilterWhere(['like', 'prod_numero_producteur', $this->prod_numero_producteur]);
        }
        if (isset($params['ProducteurSearch']['prod_numero_BCE']) && $params['ProducteurSearch']['prod_numero_BCE'] != '') {
            $query->andFilterWhere(['like', 'prod_numero_BCE' , $this->prod_numero_BCE]);
        }
        if (isset($params['ProducteurSearch']['prod_statut_juridique']) && $params['ProducteurSearch']['prod_statut_juridique'] != '') {
            $query->andFilterWhere(['prod_statut_juridique' => $this->prod_statut_juridique]);
        }
        if (isset($params['ProducteurSearch']['prod_nom_exploitation']) && $params['ProducteurSearch']['prod_nom_exploitation'] != '') {
            $query->andFilterWhere(['like','prod_nom_exploitation', $this->prod_nom_exploitation]);
        }
        
        if (isset($params['ProducteurSearch']['rue']) && $params['ProducteurSearch']['rue'] != '') {
//            $query->joinWith('contact.adresse');
            $query->andFilterWhere(['like','ca.adr_rue', $this->rue]);
        }
        if (isset($params['ProducteurSearch']['localite']) && $params['ProducteurSearch']['localite'] != '') {
//            $query->joinWith('contact.adresse');
            $query->andFilterWhere(['like','ca.adr_ville', $this->localite]);
        }
        if (isset($params['ProducteurSearch']['province']) && $params['ProducteurSearch']['province'] != '') {
//            $query->joinWith('contact.adresse');
            $query->andFilterWhere(['like','ca.adr_province', $this->province]);
        }
        
        
        // filter by contact nom or associes->nom
        if ((isset($params['ProducteurSearch']['contactNom']) && $params['ProducteurSearch']['contactNom'] != '') ||
                (isset($params['ProducteurSearch']['contactPrenom']) && $params['ProducteurSearch']['contactPrenom'] != '') ||
                (isset($params['ProducteurSearch']['contactEmailPrincipal']) && $params['ProducteurSearch']['contactEmailPrincipal'] != '') ||
                (isset($params['ProducteurSearch']['contactGSMPrincipal']) &&  $params['ProducteurSearch']['contactGSMPrincipal'] != '') ||
                (isset($params['ProducteurSearch']['contactGSMPrincipalNotNull']) && $params['ProducteurSearch']['contactGSMPrincipalNotNull'] == '1' ) ||
                (isset($params['ProducteurSearch']['contactEmailPrincipalNotNull']) &&  $params['ProducteurSearch']['contactEmailPrincipalNotNull'] == '1' ) ||
                (isset($params['ProducteurSearch']['contactTelephonePrincipalNotNull']) && $params['ProducteurSearch']['contactTelephonePrincipalNotNull'] == '1' )) {

//            $query->join('left join', 'associe a', 'producteur.cont_id = a.cont_id_prod')
//                    ->join('left join', 'contact ac', 'a.cont_id = ac.cont_id');
            
            if (isset($params['ProducteurSearch']['contactNom']) && $params['ProducteurSearch']['contactNom'] != '') {
                $query->andWhere(['or','c.cont_nom LIKE "%' . $this->contactNom . '%"','ac.cont_nom LIKE "%' . $this->contactNom . '%"']);
            }

            if (isset($params['ProducteurSearch']['contactPrenom']) && $params['ProducteurSearch']['contactPrenom'] != '') {
                $query->andWhere(['or', 'c.cont_prenom LIKE "%' . $this->contactPrenom . '%"', 'ac.cont_prenom LIKE "%' . $this->contactPrenom . '%"']);
            }
            
            if (isset($params['ProducteurSearch']['contactEmailPrincipal']) && $params['ProducteurSearch']['contactEmailPrincipal'] != '') {
                $query->andWhere(['or', 'c.cont_email_principal LIKE "%' . $this->contactEmailPrincipal. '%"', 'ac.cont_email_principal LIKE "%' . $this->contactEmailPrincipal. '%"']);
            }
            
            if (isset($params['ProducteurSearch']['contactGSMPrincipal']) && $params['ProducteurSearch']['contactGSMPrincipal'] != '') {
                $query->andWhere(['or', 'c.cont_gsm_principal LIKE "%' . $this->contactGSMPrincipal. '%"', 'ac.cont_gsm_principal LIKE "%' . $this->contactGSMPrincipal. '%"']);
            }
            
            if (isset($params['ProducteurSearch']['contactGSMPrincipalNotNull']) && $params['ProducteurSearch']['contactGSMPrincipalNotNull'] == '1') {
            $query->andWhere(['or',['not' , ['c.cont_gsm_principal' => null]],['not' , ['ac.cont_gsm_principal' => null]]]);
            }
            
            if (isset($params['ProducteurSearch']['contactEmailPrincipalNotNull']) &&  $params['ProducteurSearch']['contactEmailPrincipalNotNull'] == '1' ) {
                $query->andWhere(['or',['not' , ['c.cont_email_principal' => null]],['not' , ['ac.cont_email_principal' => null]]]);
            }
            
            if (isset($params['ProducteurSearch']['contactTelephonePrincipalNotNull']) && $params['ProducteurSearch']['contactTelephonePrincipalNotNull'] == '1' ) {
                $query->andWhere(['or',['not' , ['c.cont_telephone_principal' => null]],['not' , ['ac.cont_telephone_principal' => null]]]);
            }
        }
//        $query->where('c.cont_prenom LIKE "%' . $this->contactPrenom . '%"')
//                ->orWhere('ac.cont_prenom LIKE "%' . $this->contactPrenom . '%"');
        
        
        // filter by contact prenom
//        $query->joinWith(['contact' => function ($q) {
//                $q->where('contact.cont_prenom LIKE "%' . $this->contactPrenom . '%"');
//            }]);
            
        if (isset($params['ProducteurSearch']['contactTelephonePrincipal']) &&  $params['ProducteurSearch']['contactTelephonePrincipal'] != '') { //fix bug if params is empty
//            $query->joinWith(['contact' => function ($q) {
//                    $q->where('contact.cont_telephone_principal LIKE "%' . $this->contactTelephonePrincipal . '%"')
//                    ;
//                }]);
            $query->andFilterWhere(['like', 'c.cont_telephone_principal', $this->contactTelephonePrincipal]);
        }
//        if (isset($params['ProducteurSearch']['contactTelephonePrincipalNotNull']) && $params['ProducteurSearch']['contactTelephonePrincipalNotNull'] == '1' ) {
//            $query->joinWith(['contact' => function ($q) {
//                $q->where(['not' , ['contact.cont_telephone_principal' => null]]);
//            }]);
//        }
//        if (isset($params['ProducteurSearch']['contactGSMPrincipalNotNull']) && $params['ProducteurSearch']['contactGSMPrincipalNotNull'] == '1' ) {
//            $query->joinWith(['contact' => function ($q) {
//                $q->where(['not' , ['contact.cont_gsm_principal' => null]]);
//            }]);
//        }

//        if (isset($params['ProducteurSearch']['contactEmailPrincipal']) && $params['ProducteurSearch']['contactEmailPrincipal'] != '') { //fix bug if params is empty
//        $query->joinWith(['contact' => function ($q) {
//                $q->where('contact.cont_email_principal LIKE "%' . $this->contactEmailPrincipal . '%"');
//            }]);
//        }
        
//        if (isset($params['ProducteurSearch']['contactEmailPrincipalNotNull']) &&  $params['ProducteurSearch']['contactEmailPrincipalNotNull'] == '1' ) {
//            $query->joinWith(['contact' => function ($q) {
//                $q->where(['not' , ['contact.cont_email_principal' => null]]);
//            }]);
//        }
            
        if ($this->secteursID != null) {
            if (is_array($this->secteursID)) {
                if ($this->secteursOperator == 1) {
                    foreach ($this->secteursID as $secteur) {
                        $subQuery = Contact::find()->select('contact.cont_id')->joinWith('secteurs')->where(['secteur.sect_id' => $secteur]);
                        $query->andWhere(['producteur.cont_id' => $subQuery]);
                    }
                } else {
                    $query->andFilterWhere(['in', 'secteur.sect_id', $this->secteursID]);
                }
            } else {
                $subQuery = Contact::find()->select('contact.cont_id')->joinWith('secteurs')->where(['secteur.sect_id' => $this->secteursID]);
                $query->andWhere(['producteur.cont_id' => $subQuery]);
            }
        }
        
//        if((isset($params['hasExploit']) && $params['hasExploit'] == '1') || (isset($params['hasAssoc']) && $params['hasAssoc'] == '1')) {
////            $query->join('inner join', 'appartenir', ' appartenir.cont_id = producteur.cont_id')
////                        ->join('inner join', 'entreprise', 'appartenir.entr_id = entreprise.entr_id')
////                    ;
//            if (isset($params['hasExploit']) && $params['hasExploit'] == '1') {
//                $subqueryExploit = Contact::find()->select('contact.cont_id')->joinWith('entreprises')
//                        ->join('left join', 'exploitation', 'exploitation.entr_id = entreprise.entr_id')->where(['not',['exploitation.entr_id' => null]])->all();
//                $exploitArray = \yii\helpers\ArrayHelper::map($subqueryExploit, 'cont_id', 'cont_id');
//                
//                $query->andFilterWhere(['in', 'producteur.cont_id', $exploitArray]);
//            }
//            if (isset($params['hasAssoc']) && $params['hasAssoc'] == '1') {
//                $subqueryAssoc = Contact::find()->select('contact.cont_id')->joinWith('entreprises')
//                        ->join('left join', 'association', 'association.entr_id = entreprise.entr_id')->where(['not',['association.entr_id' => null]])->all();
//                $assocArray = \yii\helpers\ArrayHelper::map($subqueryAssoc, 'cont_id', 'cont_id');
//                
//                $query->andFilterWhere(['in', 'producteur.cont_id', $assocArray]);
//            }
//        }
        
//        if((isset($params['ProducteurSearch']['exploitations'])) && $params['ProducteurSearch']['exploitations'] != '') {
//            $query->joinWith('contact.entreprises.exploitation')->andWhere('entreprise.entr_nom LIKE "%' . $this->exploitations .'%"');
//        }
        if((isset($params['ProducteurSearch']['langue'])) && is_array($params['ProducteurSearch']['langue'])) {
            $query->andFilterWhere(['or like', 'c.cont_langue', $params['ProducteurSearch']['langue'] ]);
        }
        if((isset($params['ProducteurSearch']['codepostal'])) 
//                && is_array($params['ProducteurSearch']['codepostal'])
                ) {
//            $query->joinWith('contact.adresse');
            $query->andFilterWhere(['or like', 'adr_code_postal', $params['ProducteurSearch']['codepostal'] ]);
        }
        
        if(isset($params['membres']) && $params['membres'] == '1') {
            $query->join('INNER JOIN', 'membre_college', 'c.cont_id = membre_college.cont_id');
        }
        
        if($this->generalSearch) {
            $query->andFilterWhere(['or',[
                'like','prod_nom_exploitation', $this->generalSearch
            ],[
                'like','c.cont_nom', $this->generalSearch
            ],[
                'like','c.cont_prenom', $this->generalSearch
            ],[
                'like','ac.cont_nom', $this->generalSearch
            ],[
                'like','ac.cont_prenom', $this->generalSearch
            ],[
                'like','c.cont_email_principal', $this->generalSearch
            ],[
                'like','ac.cont_email_principal', $this->generalSearch
            ],[
                'like','c.cont_gsm_principal', $this->generalSearch
            ],[
                'like','ac.cont_gsm_principal', $this->generalSearch
            ],[
                'like','c.cont_telephone_principal', $this->generalSearch
            ],[
                'like','ac.cont_telephone_principal', $this->generalSearch
            ],[
                'like','ca.adr_rue', $this->generalSearch
            ],[
                'like','ca.adr_ville', $this->generalSearch
            ],[
                'like','ca.adr_province', $this->generalSearch
            ],[
                'like','adr_code_postal', $this->generalSearch
            ]
                ]
                    );
        }
        
        return $dataProvider;
    }
    

}
