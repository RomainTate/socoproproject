<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Entreprise;

/**
 * EntrepriseSearch represents the model behind the search form about `app\models\Entreprise`.
 */
class EntrepriseSearch extends Entreprise
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entr_id', 'adr_id'], 'integer'],
            [['entr_societe', 'entr_telephone', 'entr_email', 'entr_domaine_activite'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Entreprise::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'entr_id' => $this->entr_id,
            'adr_id' => $this->adr_id,
        ]);

        $query->andFilterWhere(['like', 'entr_societe', $this->entr_societe])
            ->andFilterWhere(['like', 'entr_telephone', $this->entr_telephone])
            ->andFilterWhere(['like', 'entr_email', $this->entr_email])
            ->andFilterWhere(['like', 'entr_domaine_activite', $this->entr_domaine_activite]);

        return $dataProvider;
    }
}
