<?php
namespace app\controllers;

use app\models\Suivi;
use Yii;
use yii\web\Controller;

class SuiviController extends Controller
{
    public function actionCreate()
    {
    $model = new Suivi();

    if ($model->load(Yii::$app->request->post())) {
        
        if ($model->validate()) {
            // form inputs are valid, do something here
            return;
        }
    }

    return $this->render('_form', [
        'model' => $model,
    ]);
    }

    public function actionIndex($id = null, $newID = null) {
        $post = Yii::$app->request->post();
        if ($post && !isset($post['export_type'])) {
            $newID = $this->save($post, $id);
            return $this->redirect(['index', 'id' => $id, 'newID' => $newID]);
        } else {
            $newID = null;
        }
        if ($id) {
            $newID = Yii::$app->request->get('newID');
            $prodModel = \app\models\Producteur::find()->where(['cont_id' => $id])->one();
            $suivis = Suivi::find()->where(['cont_id' => $id])->orderBy(['suivi_date' => SORT_DESC])->all();
            return $this->render('index', [
                        'suivis' => $suivis,
                        'newID' => $newID,
                        'model' => $prodModel
            ]);
        } else {
            if( !Yii::$app->user->isGuest && Yii::$app->user->getUser()->getIsAdmin()) {
                $searchModel = new \app\models\SuiviSearch();
                $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
                return $this->render('indexall', [
                            'searchModel' => $searchModel,
                            'dataProvider' => $dataProvider,
                ]);
            } else {
                 throw new \yii\web\ForbiddenHttpException("Vous n'êtes pas autorisé à effectuer cette action.");
            }
        }
    }
    
    private function save($data, $id) {
        $model = new Suivi();
        $model->load($data);
        $model->cont_id = $id;
        $model->user_id = Yii::$app->user->isGuest? null : Yii::$app->user->getUser()->id;
             
        $timezone = new \DateTimeZone(Yii::$app->timeZone);
        $model->suivi_date = \DateTime::createFromFormat("d/m/Y H:i", $data['Suivi']['suivi_date'], $timezone)->format("Y-m-d H:i"); 
        if ($model->save()) {
            foreach ((array)$model->suiviobjets_ids as $obj_id) {
                $suiviobj = new \app\models\SuiviHasObjet();
                $suiviobj->objet_id = $obj_id;
                $suiviobj->suivi_id = $model->suivi_id;
                $suiviobj->save();
            }
            return $model->suivi_id;
        } else {
            Yii::$app->session->setFlash('error', "Erreur : votre fiche n'a pas été enregistré");
            return false;
        }
    }
    
    public function actionShowcom($id) {
        $model = Suivi::find()->where(['suivi_id' => $id])->one();
        
        return $this->renderAjax('showcom',[
            'fullCom' => $model->suivi_commentaire
        ]);
    }
    
    public function actionDelete($id) {
        $model = Suivi::find()->where(['suivi_id' => $id])->one();
        $model->delete();
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionAddsuite($id) {
        $model = Suivi::find()->where(['suivi_id' => $id])->one();
        $model->suivi_suite = 1;
        $model->save();
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionRemovesuite($id) {
        $model = Suivi::find()->where(['suivi_id' => $id])->one();
        $model->suivi_suite = 0;
        $model->save();
        
        return $this->redirect(Yii::$app->request->referrer);
    }
    
    public function actionAjaxtoogleattente($suiviid) {
        $model = Suivi::find()->where(['suivi_id' => $suiviid])->one();
        if($model->suivi_suite == 0) {
            $model->suivi_suite = 1;
        } else {
            $model->suivi_suite = 0;
        }
        $model->save();
        return 0;
    }

}
