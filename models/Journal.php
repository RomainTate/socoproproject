<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "journal".
 *
 * @property string $journal_frequence
 * @property integer $entr_id
 *
 * @property Entreprise $entreprise
 */
class Journal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['entr_id'], 'required'],
            [['entr_id'], 'integer'],
            [['journal_frequence'], 'string', 'max' => 25],
            [['entr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entreprise::className(), 'targetAttribute' => ['entr_id' => 'entr_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'journal_frequence' => 'Fréquence du journal',
            'entr_id' => 'Entr ID',
            'entrepriseNom' => 'Nom',
            'entrepriseFormeJuridique' => 'Forme Juridique',
            'entrepriseTelephonePrincipal' => 'Telephone',
            'entrepriseEmailPrincipal' => 'E-mail',
            'entrepriseSiteInternet' => 'Site Web',
            'exploitationSecteurs' => 'Secteurs'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntreprise()
    {
        return $this->hasOne(Entreprise::className(), ['entr_id' => 'entr_id']);
    }
    
    public function getEntrepriseNom() {
        return $this->entreprise->entr_nom;
    }
    
    public function getEntrepriseFormeJuridique() {
        return $this->entreprise->entr_forme_juridique;
    }
    
    public function getEntrepriseTelephonePrincipal() {
        return $this->entreprise->entr_telephone_principal;
    }
    
    public function getEntrepriseEmailPrincipal() {
        return $this->entreprise->entr_email_principal;
    }
    
    public function getEntrepriseSiteInternet() {
        return $this->entreprise->entr_site_internet;
    }
}
