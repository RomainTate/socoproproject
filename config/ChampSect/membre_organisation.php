<?php

    return [
    'label' => 'Membre d\'une organisation',
    'type' => 2,
    'value' =>[ 'Groupement de producteurs', 'Coopérative', 'ASBL', 'Interprofessionnelle', 'Comice', 'CETA', 'Syndicat']
    ];