<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\export\ExportMenu;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->title = 'Recherche Sectorielle';
$this->params['breadcrumbs'][] = $this->title;
$search = false;
$get = Yii::$app->request->get();

// is a secteur selected ?
if(!empty($get)) {
    foreach ($get as $k_get => $v_get) {
        if($k_get == 'ProducteurSearchSectoriel' || $v_get == '' || $v_get == null || substr($k_get,-9) == '-operator' || $k_get == 'sort') {
            continue;
        }
        $search = true;
        break;
    }
}
?> 

<div class="producteur-search-sectoriel">
    <?php if ($search == false) :?>
    <div class="x_panel">
    <div class="x_title">
        <h1> Recherche sectorielle </h1>
    </div>
        <?php $form = ActiveForm::begin([
            'action' => 'searchsectoriel',
            'method' => 'get',
            'id' => 'searchform'
        ]); ?>

        <?php $optionsDropdown = ['class' => 'js-multiple' , 'onchange' => 'this.form.submit()'];
        if (!isset($get['ProducteurSearchSectoriel'])) $optionsDropdown['prompt'] =   'Selectionnez un secteur';
        echo $form->field($model, 'secteursID')
            ->dropDownList(ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom')
            ,$optionsDropdown)->label('Secteur')
        ?>

        <?php if (isset($get['ProducteurSearchSectoriel'])) :?>
        <?php $secteur = \app\models\Secteur::find()->where(['sect_id' => $get['ProducteurSearchSectoriel']['secteursID']])->one()->sect_nom?>
        <?= yii\bootstrap\Html::checkbox('membrefiliere', isset($get['membrecollege'])? true : false, ['id' => 'checkboxmembrefiliere'])?>
        <?= yii\bootstrap\Html::label("N'inclure que les membre de la commission filière {$secteur}", 'checkboxmembrefiliere' )?>
            <?php $fields = app\controllers\ChampsectController::getFields();
         foreach ($fields as $key => $field) {
             echo \yii\bootstrap\Html::label('> '.$field['label'],$key, ['class' => 'labelform']);
             if($field['type'] == 1) {
                 echo \yii\bootstrap\Html::checkboxList($key, isset($get[$key])? $get[$key]: null, ["1"=>"Oui","0"=>"Non"]);
             } else if($field['type'] == 2){
                 echo \yii\bootstrap\Html::checkboxList($key, isset($get[$key])? $get[$key]: null, $field['value']);
             } else if($field['type'] == 3) {
                 echo '<div>';
                 echo \yii\bootstrap\Html::textInput($key, isset($get[$key])? $get[$key]: null,['class' => 'form-control']);
                 echo '</div>';
             } else if ($field['type'] == 4) {
                 echo '<div>';
                 echo Html::dropDownList($key.'-operator', isset($get[$key.'-operator'])? $get[$key.'-operator']: null, ['=' => '=' , '>' => '>', '>=' => '>=', '<' => '<', '<=' => '<='],['class' => 'form-control', 'style' => 'width: 60px;float: left;']);
                 echo yii\bootstrap\Html::input('number', $key, isset($get[$key])? $get[$key]: null,['class' => 'form-control', 'style' => 'width: 300px;']);
                 echo '</div>';
             } else if ($field['type'] == 5) {
                 echo '<br />';
                 echo Html::label('Condition : ');
                 echo Html::dropDownList($key.'-operator', isset($get[$key.'-operator'])? $get[$key.'-operator']: null, ['AND' => 'TOUS', 'OR' => 'Au choix']);
                 echo Html::img(['css/images/info.png'], ['class' => 'infoImg', 'title' => "'TOUS' signifie que toutes les conditions cochées doivent être respéctées
'Au choix' signifie qu'au moins une condition cochée doit être respectée"]);
                 echo yii\helpers\Html::checkboxList($key, null, \app\controllers\ChampsectController::getOptions($key));
             }
             echo Html::checkbox($key.'-includeNull', isset($get[$key.'-includeNull'])? true : false, ['label' => 'inclure valeurs inconnues']);
         } ?>

    <hr/>
    <div class="x_title">
        <h1>
            Champs sectoriels spécifiques 
        </h1>
    </div>
    <?php
    $specificsFields = app\controllers\ChampsectController::getSpecificsFields($secteur);
    foreach ($specificsFields as $key => $field) {
        $key = 'SPECIFIC'.$key;
             echo \yii\bootstrap\Html::label('> '.$field['label'],$key, ['class' => 'labelform']);
             if($field['type'] == 1) {
                 echo \yii\bootstrap\Html::checkboxList($key, isset($get[$key])? $get[$key]: null, ["1"=>"Oui","0"=>"Non"]);
             } else if($field['type'] == 2){
                 echo \yii\bootstrap\Html::checkboxList($key, isset($get[$key])? $get[$key]: null, $field['value']);
             } else if($field['type'] == 3) {
                 echo '<br />';
                 echo \yii\bootstrap\Html::textInput($key, isset($get[$key])? $get[$key]: null);
                 echo '<br />';
             } else if ($field['type'] == 4) {
                 echo '<br />';
                 echo Html::dropDownList($key.'-operator', isset($get[$key.'-operator'])? $get[$key.'-operator']: null, ['=' => '=' , '>' => '>', '>=' => '>=', '<' => '<', '<=' => '<=']);
                 echo yii\bootstrap\Html::input('number', $key, isset($get[$key])? $get[$key]: null);
                 echo '<br />';
             } else if ($field['type'] == 5) {
                 echo '<br />';
                 echo Html::label('Condition : ');
                 echo Html::dropDownList($key.'-operator', isset($get[$key.'-includeNull'])? $get[$key.'-includeNull']: null, ['AND' => 'TOUS', 'OR' => 'Au choix']);
                 echo Html::img(['css/images/info.png'], ['class' => 'infoImg', 'title' => "'TOUS' signifie que toutes les conditions cochées doivent être respéctées
'Au choix' signifie qu'au moins une condition cochée doit être respectée"]);
                 echo yii\helpers\Html::checkboxList($key, null, \app\controllers\ChampsectController::getOptions(substr($key, 8), $secteur));
             }
             echo Html::checkbox($key.'-includeNull', isset($get[$key.'-includeNull'])? true : false, ['label' => 'inclure valeurs inconnues']);
             echo '<hr />';
         } ?>
    
            <?= yii\bootstrap\Html::checkbox('sigec', Yii::$app->request->get('sigec') ==1 ? true: false,['id' => 'sigecCheck']) ?>
            <?= yii\bootstrap\Html::label('Inclure les producteurs confidentiels', 'sigecCheck')?>

                <div class="form-group">
                <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
                </div>
            <?php endif;?>
        <?php ActiveForm::end(); ?>
    </div>
    <?php else :?>


        <?php   
            $gridColumns = [
                [
                    'attribute' => 'prod_sigec',
                    'visible' => (Yii::$app->request->get('sigec') == '1'),
                    'filter' => ["1" => "Oui", "0" => "Non"],
                    'value' => function ($model) {
                        if ($model->prod_sigec == 1) {
                            return 'Oui';
                        } else {
                            return 'Non';
                        }
                    }

                ],
                'prod_nom_exploitation',
                [
                    'attribute' => 'contact.cont_civilite',
                    'label' => 'Civ',
                    'format' => 'text',
                ],
                [
                    'attribute' => 'contactNom',
                    'format' => 'text',
                ],
                'contactPrenom',
                        [
                'label' => 'Associés',
                'attribute' => 'associes' ,
                'format' => 'raw',
                'width' => '150px',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesNames = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_nom || $associe->contact->cont_prenom) {
                        $associesNames[] = '<span style="display:block;"> -' .
                                ($associe->contact->cont_nom ? $associe->contact->cont_nom:'').
                                ' '. ($associe->contact->cont_prenom ? $associe->contact->cont_prenom :''). '</span>';
                        }
                    }
                    return implode("<br />", $associesNames);
                }
            ],
            'contactTelephonePrincipal',
                    [
                'label' => 'Telephone Associés',
                'hidden' => true,
                'attribute' => 'associes.tel',
                'format' => 'html',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesTels = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_telephone_principal) {
                        $associesTels[] = '<span style="display:block;"> ' .
                                $associe->contact->cont_telephone_principal. '</span>';
                        }
                    }
                    return implode("; ", $associesTels);
                }
            ],
            'contactGSMPrincipal',
                    [
                'label' => 'GSM Associés',
                'hidden' => true,
                'attribute' => 'associes.gsm',
                'format' => 'html',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesGSMs = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_gsm_principal) {
                        $associesGSMs[] = '<span style="display:block;"> ' .
                                $associe->contact->cont_gsm_principal. '</span>';
                        }
                    }
                    return implode("; ", $associesGSMs);
                }
            ],
                    [
                        'label' => 'E-mail',
            'attribute' => 'contactEmailPrincipal', 
                'format' => 'email'
                        ],
            [
                'label' => 'Email Associés',
                'hidden' => true,
                'attribute' => 'associes.email',
                'format' => 'html',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesEmails = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_email_principal) {
                        $associesEmails[] = '<span style="display:block;"> ' .
                                $associe->contact->cont_email_principal. '</span>';
                        }
                    }
                    return implode("<br />", $associesEmails);
                }
                ],
                                'rue',
                        
                    
            [
                'label' => 'Localité',
                'attribute' => 'localite'
            ],
            [
                'attribute' => 'province',
            ],
            
    //                    'cont_id'
            ]
        ?>

        <?php   
        if(array_key_exists('ProducteurSearchSectoriel', $get)) {
            $secteur = \app\models\Secteur::find()->where(['sect_id' => $get['ProducteurSearchSectoriel']['secteursID']])->one()->sect_nom;
            foreach (\Yii::$app->request->get() as $k => $g) {

                if($k == 'ProducteurSearchSectoriel' || $k == 'sigec'|| substr($k,-9) == '-operator' || $g == '' || $k == 'page' || $k == 'sort' || $k == 'membrefiliere') {
                    continue;
                }
                if(strlen($k) > 12 && substr($k, -12) == '-includeNull' ) { //if 'include null' is selected
                    if(array_key_exists(substr($k, 0, -12), $get) && $get[substr($k,0, -12)] != '') {        //if a value is also selected (don't put the column twice) 
                        continue;
                    } else {
                        $k =  substr($k, 0, -12);                           //if no value are also selected (if only 'include-null' has been selected)
                    }
                }

                $col = array();
                if (substr($k,0, 8) != 'SPECIFIC') {
                    $col['label'] = \app\controllers\ChampsectController::getLabel($k);

                    $col['value'] = function ($model) use($k, $get) {

                        $t = app\models\Travailler::find()->where(['cont_id' => $model->cont_id, 'sect_id' => $get['ProducteurSearchSectoriel']['secteursID']])->one();
                        $v = \app\controllers\ChampsectController::getValue($k, $t->$k);
                        return $v['v'];};
                } else {
                    $col['label'] = \app\controllers\ChampsectController::getLabel(substr($k, 8), $secteur);
                    $col['value'] = function ($model) use($k, $secteur) {
                        $champcleID = \app\models\ChampSectorielCle::find()->where(['champ_sectoriel_cle_field' => substr($k, 8)])->one()->champ_sectoriel_cle_id; 
                        $champVal = app\models\ChampSectorielValeur::find()->where(['cont_id' => $model->cont_id , 'champ_sectoriel_cle_id' => $champcleID])->one();
                        if ($champVal == null) {
                            return 'inconnu';
                        }
                        $val = $champVal->champ_sectoriel_valeur_val;
                        $v = \app\controllers\ChampsectController::getValue(substr($k, 8), $val, $secteur);
                        return $v['v'];
                    };
                }
                array_push($gridColumns, $col);
            }
            if(\Yii::$app->request->get('membrefiliere') != null) {
                array_push($gridColumns,
                        [
                            'label' => 'Membre filière',
                            'format' => 'html',
                            'value' => function($model) {
                                $filieres = $model->contact->secteursMembrecommission;
                                if (!$filieres) return null;
                                $filieresNames = array();
                                $filieresArray = ArrayHelper::map($filieres, 'sect_id', 'sect_nom');
                                asort($filieresArray);
                                foreach ($filieresArray as $filiere) {
                                    $filieresNames[] = '<span style="display:block;">' . $filiere . '</span>';
                                }
                            return implode(", ", $filieresNames);
                            }
                        ]);
            }    
        }
        ?>

        <?php  array_push($gridColumns, 
                ['class' => 'kartik\grid\ActionColumn','template' => '{view}{details}{delete}', 'hiddenFromExport' => true,
                'buttons' => [
                    'view' => function($url, $model) {
                        $url = \Yii::$app->urlManager->createUrl(['producteur/view', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['title' => Yii::t('yii', 'View')]);
                    },
                    'details' => function($url, $model) use($get) {
                        $url = \Yii::$app->urlManager->createUrl(['producteur/details', 'id' => $model->cont_id, 'sect_id' => array_key_exists('ProducteurSearchSectoriel', $get)? $get['ProducteurSearchSectoriel']['secteursID']:'' ]);
                        return Html::a('<span class="glyphicon glyphicon-tag text-info"></span>', $url, ['title' =>  'Details']);
                    },
                    'delete' => function($url, $model) {
                        if (!\Yii::$app->user->can('deleteProducteur')) {
                            return null;
                        }
                        $url = \Yii::$app->urlManager->createUrl(['producteur/delete', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-confirm' => 'Etes vous sûr de vouloir supprimer ce producteur?', 'data-method' => 'post']);
                    }
                ]
               ]); ?>
    <div class="x_panel">
        <div class="x_title">
            <h1>Résultats de ma recherche :</h1>
        </div>
        <div class="pull-right">
                <label>Exportation :</label>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'target' => ExportMenu::TARGET_BLANK,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false
                    ],
                    'noExportColumns' => [count($gridColumns) - 1]
                ]);
                ?>
    </div>
        <?=
        kartik\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'columns' => $gridColumns,
            'responsive' => true,
            'responsiveWrap' => false,
            'rowOptions' => function ($model) {
                if ($model->prod_sigec == '1') {
                    return ['class' => 'sigecGrid'];
                }
            },
        ]);
        ?>
    </div>
    <?php endif;?>
</div>
<?php
$this->registerJs(
    "$('.js-multiple').select2();",
    yii\web\View::POS_READY,
    'select2-prod'
); ?>