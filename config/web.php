<?php
$params = require(__DIR__ . '/params.php');
$admins = require(__DIR__ . '/admins.php');

$config = [
    'id' => 'basic',
    'timezone' => 'Europe/Paris',
    'name' => 'CollegeDB',
    'language' => 'fr-FR',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        // enter optional module parameters below - only if you need to  
        // use your own export download action or custom translation 
        // message source
        // 'downloadAction' => 'gridview/export/download',
        // 'i18n' => []
        ],
        'user' => [
            'class' => 'dektrium\user\Module',
            'enableConfirmation' => false,
            'enableUnconfirmedLogin' => true,
            'enableRegistration' => false,
            'modelMap' => [
                'User' => 'app\models\User',
                'SettingsForm' => 'app\modules\user\models\SettingsForm',
            ],
            'controllerMap' => [
                'admin' => 'app\controllers\user\AdminController',
//                'security' => 'app\controllers\user\SecurityController'
            ],
            'admins' => $admins,
            'mailer' => [
                'sender' => 'noreply@collegedesproducteurs.be'
            ],
        ],
        'rbac' => [
            'class' => 'dektrium\rbac\RbacWebModule',
            'controllerMap' => [
                'role' => 'app\modules\auth\controllers\RoleController'
            ],
        ],
        'auth' => 'app\modules\auth\Module',
        'comments' => [
            'class' => 'rmrevin\yii\module\Comments\Module',
            'userIdentityClass' => 'app\models\User',
            'useRbac' => true,
        ]
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'YFgzhBj1-a8yerNTq-9vxO77d87x87l_',
        ],
        'formatter' => [
            'dateFormat' => 'dd MMMM yyyy',
            'datetimeFormat' => "dd MMMM yyyy 'à' HH'h'mm",
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'locale' => 'fr-FR',
            'defaultTimeZone' => 'Europe/Paris'
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@dektrium/user/views' => '@app/views/user',
                    '@dektrium/rbac/views' => '@app/modules/auth/views/rbac',
                    '@vendor/rmrevin/yii2-comments/widgets/views' => '@app/views/comments'
                ],
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'app\components\User',
            'identityClass' => 'dektrium\user\models\User',
            'loginUrl' => '/user/login'
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
        ],
        'logger' => [
            'class' => 'app\components\Logger',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error'],
                ],
                'email' => [
                    'class' => 'yii\log\EmailTarget',
                    'except' => ['yii\web\HttpException:404', 'yii\web\HttpException:403'],
                    'levels' => ['error'],
                    'message' => ['from' => 'debug@collegedb.com', 'to' => 'debug.collegedb@gmail.com'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
    ],
    'as AccessBehavior' => [
        'class' => '\app\components\AccessBehavior',
        'allowedRoutes' => [
//            '/',
            ['/user/login'],
            ['/user/register'],
            ['/user/forgot'],
            ['/user/resend']
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'app\components\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
//            'allowedIPs' => ['127.0.0.1', '::1', $_SERVER['REMOTE_ADDR']],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
} else {
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
        'class' => 'app\components\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
//            'allowedIPs' => ['127.0.0.1', '::1', $_SERVER['REMOTE_ADDR']],
    ];
}

return $config;
