<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AssociationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Associations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="association-index">
   <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createAssociation')) : ?>
            <?= Html::a('Ajouter une Association', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        <?php endif; ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="x_panel">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
        'columns' => [
            'entrepriseNom', 'entrepriseFormeJuridique', 'entrepriseTelephonePrincipal',
               'entrepriseEmailPrincipal', 'entrepriseSiteInternet',

            'association_type',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
