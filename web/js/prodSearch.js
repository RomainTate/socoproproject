$("#showInfos").click(function() {
 $("#producteur-other-infos").slideDown();
 $(this).hide();
});

$("#producteursearch-secteursid").change(function() {
    if ($(this).select2('data').length > 1) {
        $("input[name = 'ProducteurSearch[secteursOperator]']").prop("disabled", false);
    } else {
        $("input[name = 'ProducteurSearch[secteursOperator]']").prop("disabled", "disabled");
    }
});