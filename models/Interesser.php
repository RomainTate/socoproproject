<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "interesser".
 *
 * @property integer $cont_id
 * @property integer $sect_id
 */
class Interesser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'interesser';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id', 'sect_id'], 'required'],
            [['cont_id', 'sect_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'sect_id' => 'Sect ID',
        ];
    }
}
