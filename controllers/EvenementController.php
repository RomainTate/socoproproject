<?php

namespace app\controllers;

use Yii;
use app\models\Evenement;
use app\models\EvenementSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EvenementController implements the CRUD actions for Evenement model.
 */
class EvenementController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['Administrateur']
                    ]
                ]
            ]
        ];
    }

    /**
     * Lists all Evenement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EvenementSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Evenement model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {       
        $contactsPresents = \yii\helpers\ArrayHelper::map(\app\models\Contact::find()->select('contact.cont_id')->joinWith('evenements', true, 'inner join')
                ->where(['evenement.evenement_id' => $id])->all(),'cont_id','cont_id');
        
        $searchModelProd = new \app\models\ProducteurSearch();
        $dataProviderProducteurs = $searchModelProd->search(Yii::$app->request->queryParams);
        $searchModelJourn = new \app\models\JournalisteSearch();
        $dataProviderJournalistes = $searchModelJourn->search(Yii::$app->request->queryParams);
        
        return $this->render('view', [
            'model' => $this->findModel($id),
            'dataProviderProducteurs' => $dataProviderProducteurs, 'dataProviderJournalistes' => $dataProviderJournalistes,
            'searchModelProd' => $searchModelProd, 'searchModelJourn' => $searchModelJourn,
            'contactsPresents'=> $contactsPresents, 
        ]);
    }

    /**
     * Creates a new Evenement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $post = Yii::$app->request->post();
        $model = new Evenement();

        if ($model->load($post)) {
            if($post['Evenement']['evenement_date'] != '') {
                $model->evenement_date = date("Y-m-d", strtotime($post['Evenement']['evenement_date']));
            } else {
                $model->evenement_date = null;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->evenement_id]);
        } else {
            return $this->render('create', [
                'model' => $model
            ]);
        }
    }

    /**
     * Updates an existing Evenement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $post = Yii::$app->request->post();
        $model = $this->findModel($id);

        if ($model->load($post)) {
            if($post['Evenement']['evenement_date'] != '') {
                $model->evenement_date = date("Y-m-d", strtotime($post['Evenement']['evenement_date']));
            } else {
                $model->evenement_date = null;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->evenement_id]);
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Evenement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Evenement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Evenement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Evenement::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionDeleteadresse($id) {
        $model = $this->findModel($id);
        $adrID = $model->adr_id;
        $model->adr_id = null;
        $model->save();
        \app\models\Adresse::find()->where(['adr_id' => $adrID])->one()->delete();
        return $this->redirect(['view', 'id' => $model->evenement_id]);
    }
    
    public function addAjaxContact($contID) {
//        <li> <a href="<?=\yii\helpers\Url::home().'producteur/view?id='.$contact->cont_id
//                                <?php // if ($contact->cont_nom != null && $contact->cont_prenom != null) {
//                                    echo $contact->cont_nom . ' ' . $contact->cont_prenom;
//                                } else {
//                                    echo 'producteur inconnu';
//                                }
//                                if ($contact->producteur->prod_nom_exploitation != null) {
//                                    echo ' ('.$contact->producteur->prod_nom_exploitation.')';
//                                }
//                            <!--</a>-->
//                        <!--</li>-->
        $newprod = [];
        $prod = \app\models\Producteur::find()->where(['cont_id' => $contID])->one();
        if ($prod->contact->cont_nom || $prod->contact->cont_prenom) {
        $newprod['nom'] = $prod->contact->cont_nom ? $prod->contact->cont_nom : 'nom inconnu';
        $newprod['prenom'] = $prod->contact->cont_prenom ? $prod->contact->cont_prenom : 'prénom inconnu';
        } else {
            $newprod['nom'] = null;
            $newprod['prenom'] = null;
        }
        
        $newprod['nom_exploitation'] = $prod->prod_nom_exploitation ? $prod->prod_nom_exploitation : null;
        return $newprod;
        
    }
    
    public function actionAddcontactajax($cont_id, $evenement_id) {
        ParticiperController::addParticipation($cont_id, $evenement_id);
        $prod = \app\models\Producteur::find()->where(['cont_id' => $cont_id])->one();
        if ($prod->contact->cont_nom || $prod->contact->cont_prenom) {
        $newprod['id'] = $cont_id;
        $newprod['nom'] = $prod->contact->cont_nom ? $prod->contact->cont_nom : 'nom inconnu';
        $newprod['prenom'] = $prod->contact->cont_prenom ? $prod->contact->cont_prenom : 'prénom inconnu';
        } else {
            $newprod['nom'] = 'producteur';
            $newprod['prenom'] = 'inconnu';
        }
        
        $newprod['nom_exploitation'] = $prod->prod_nom_exploitation ? $prod->prod_nom_exploitation : 'null';
        $newprod['evenementID'] = $evenement_id;
//        return $this->redirect(Yii::$app->request->referrer);
        return json_encode($newprod);
        
    }
    
    public function actionRemovecontactajax($cont_id, $evenement_id) {
        ParticiperController::removeParticipation($cont_id, $evenement_id);
//        $prod = \app\models\Producteur::find()->where(['cont_id' => $cont_id])->one();
//        if ($prod->contact->cont_nom || $prod->contact->cont_prenom) {
//        $newprod['id'] = $cont_id;
//        $newprod['nom'] = $prod->contact->cont_nom ? $prod->contact->cont_nom : 'nom inconnu';
//        $newprod['prenom'] = $prod->contact->cont_prenom ? $prod->contact->cont_prenom : 'prénom inconnu';
//        } else {
//            $newprod['nom'] = 'producteur';
//            $newprod['prenom'] = 'inconnu';
//        }
//        
//        $newprod['nom_exploitation'] = $prod->prod_nom_exploitation ? $prod->prod_nom_exploitation : 'null';
//        return $this->redirect(Yii::$app->request->referrer);
        return $cont_id;
        
    }
    
    public function actionImportfile($evenement_id) {
        $post = \Yii::$app->request->post();
        if ($post) {
            $file = \yii\web\UploadedFile::getInstanceByName('uploadedFile');
            if ($file == null) {
                Yii::$app->session->setFlash('error', 'Veuillez importer un fichier');
                return $this->redirect(['view', 'id' => $evenement_id]);
            }
            if ($file->extension != 'xlsx') {
                Yii::$app->session->setFlash('error', 'Veuillez importer un fichier de type Excel 2007+ (.xlsx)');
                return $this->redirect(['view', 'id' => $evenement_id]);
            }
            $file->saveAs('uploads/' . $file->name);

            set_time_limit(0);
            $inputFile = 'uploads/' . $file->name;
            try {
                $inputFileType = \PHPExcel_IOFactory::identify($inputFile);

                $obj = \PHPExcel_IOFactory::createReader($inputFileType);
                $excel = $obj->load($inputFile);
            } catch (Exception $ex) {
                die(print_r($ex));
            }
            $sheet = $excel->getSheet(0);
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();
            $rowData = $sheet->rangeToArray('A1:' . $highestColumn.$highestRow , NULL, TRUE, FALSE);
            
            $rowTitles = $rowData[0];
            foreach ($rowTitles as &$title) {
                $title = mb_strtolower(transliterator_transliterate('Any-Latin; Latin-ASCII; [\u0080-\u7fff] remove',$title));
            }
            
            $notFoundCols = [];
            $nomPrincipalIndex = array_search('nom', $rowTitles);
            if(!$nomPrincipalIndex) {
                $notFoundCols[] = 'nom';
            }
            
            $prenomPrincipalIndex = array_search('prenom', $rowTitles);
            if(!$prenomPrincipalIndex) {
                $notFoundCols[] = 'prenom';
            }
            
            if(!empty($notFoundCols)) {
                Yii::$app->session->setFlash('error', 'Impossible de trouver les colonnes : <br /> -' . implode($notFoundCols, '<br /> -'));
                return $this->redirect(['view', 'id' => $evenement_id]);
            }
            
            $errorArray = [];
            $infoArray = ['ok' => 0, 'erreur' => 0, 'ignored' => 0];
            for($i=1; $i< $highestRow; $i++) {
                $producteur = \app\models\Producteur::find()->joinWith('contact',false)
                        ->where(['cont_nom' => $rowData[$i][$nomPrincipalIndex], 'cont_prenom' => $rowData[$i][$prenomPrincipalIndex]])->all();
                
                if (count($producteur != 1)) {
                    if (count($producteur) > 1) {
                        $rowData[$i]['erreur'][] = 'Plusieurs producteurs existent avec ce nom et prénom';
                        $errorArray[] = $rowData[$i];
                        $infoArray['erreur']++;
                        continue;
                    }

                    if (count($producteur) == 0) {
                        $rowData[$i]['erreur'][] = 'Aucun producteur trouvé avec ce nom et prénom';
                        $errorArray[] = $rowData[$i];
                        $infoArray['erreur']++;
                        continue;
                    }
                    
                }
                
                if(\app\models\Participer::find()->where(['cont_id' => $producteur[0]->cont_id, 'evenement_id' => $evenement_id])->one()) {
                    $infoArray['ignored']++;
                    continue;
                }
                
                $partipation = new \app\models\Participer(['cont_id' => $producteur[0]->cont_id, 'evenement_id' => $evenement_id]);
                if($partipation->save()) {
                    $infoArray['ok']++;
                } else {
                    $rowData[$i]['erreur'][] = 'Erreur inconnue';
                    $errorArray[] = $rowData[$i];
                    $infoArray['erreur']++;
                }
            } //end for loop
            return $this->render('import', [
                'infoArray' => $infoArray,
                'rowData' => $errorArray
            ]);
        } //end if(post)
        
        return $this->redirect(['view', 'id' => $evenement_id]);
    }

}
