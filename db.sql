-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mar 25 Avril 2017 à 21:30
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `socotools`
--
CREATE DATABASE IF NOT EXISTS `socotools` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `socotools`;

-- --------------------------------------------------------

--
-- Structure de la table `adresse`
--

DROP TABLE IF EXISTS `adresse`;
CREATE TABLE `adresse` (
  `adr_id` int(11) NOT NULL,
  `adr_rue` varchar(25) NOT NULL,
  `adr_numero` int(11) NOT NULL,
  `adr_code_postal` char(5) NOT NULL,
  `adr_ville` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `adresse`
--

INSERT INTO `adresse` (`adr_id`, `adr_rue`, `adr_numero`, `adr_code_postal`, `adr_ville`) VALUES
(1, 'rue1', 11, '1000', 'brx'),
(2, 'rue2', 22, '4000', 'choo'),
(3, 'testadd', 44, '1541', 'adresse'),
(4, 'loinloin', 999, '99999', 'la bas'),
(5, 'test', 4, 'test', '5'),
(6, 'dukurtileur', 47, '50000', 'namur'),
(7, '44', 44, '54', '44'),
(8, 'aze', 58, '4578', 'olol'),
(9, 'azzz', 47, '5400', 'olol'),
(11, 'wtfaaa', 47, '4578', 'poil'),
(12, 'olol', 778, '74587', 'la bas'),
(13, 'aze', 4, 'aze', 'ae');

-- --------------------------------------------------------

--
-- Structure de la table `appartenir`
--

DROP TABLE IF EXISTS `appartenir`;
CREATE TABLE `appartenir` (
  `entr_id` int(11) NOT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `appartenir`
--

INSERT INTO `appartenir` (`entr_id`, `cont_id`) VALUES
(1, 4),
(3, 4),
(7, 4),
(9, 4),
(10, 4),
(12, 4),
(18, 4),
(21, 4),
(14, 5),
(18, 5),
(11, 12),
(18, 12),
(21, 12),
(2, 60),
(13, 61),
(2, 66),
(13, 67),
(11, 156),
(18, 156);

-- --------------------------------------------------------

--
-- Structure de la table `association`
--

DROP TABLE IF EXISTS `association`;
CREATE TABLE `association` (
  `association_type` varchar(25) NOT NULL,
  `entr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `association`
--

INSERT INTO `association` (`association_type`, `entr_id`) VALUES
('bonjour!', 18),
('null', 21),
('dunno', 22);

-- --------------------------------------------------------

--
-- Structure de la table `associe`
--

DROP TABLE IF EXISTS `associe`;
CREATE TABLE `associe` (
  `cont_id` int(11) NOT NULL,
  `cont_id_prod` int(11) NOT NULL,
  `assoc_type` varchar(15) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `associe`
--

INSERT INTO `associe` (`cont_id`, `cont_id_prod`, `assoc_type`) VALUES
(3, 4, '3'),
(116, 4, '2'),
(117, 4, '3'),
(126, 4, '2'),
(127, 4, '1'),
(146, 5, '1');

-- --------------------------------------------------------

--
-- Structure de la table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('administrateur', '1', 1488716516),
('administrateur', '10', 1493121399),
('administrateur', '12', 1493132582),
('administrateur', '13', 1493132616),
('chargé de mission', '14', 1493132787),
('chargé de mission', '3', 1488717979),
('chargé de mission', '4', 1489317609),
('chargé de mission', '6', 1489614368),
('chargé de mission', '9', 1489317220),
('Communication', '11', 1493132172);

-- --------------------------------------------------------

--
-- Structure de la table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('administrateur', 1, NULL, NULL, NULL, NULL, NULL),
('chargé de mission', 1, '', NULL, NULL, 1488715762, 1488745985),
('comments.create', 2, 'Commenter', NULL, NULL, 1492268669, 1492268669),
('comments.delete', 2, 'Supprimer les commentaires', NULL, NULL, 1492268669, 1492268669),
('comments.delete.own', 2, 'Supprimer ses propres commentaires', 'comments.its-my-comment', NULL, 1492268669, 1492268669),
('comments.update', 2, 'Modifier les commentaires', NULL, NULL, 1492268669, 1492268669),
('comments.update.own', 2, 'Modifier ses propres commentaires', 'comments.its-my-comment', NULL, 1492268669, 1492268669),
('Communication', 1, NULL, NULL, NULL, NULL, NULL),
('createAssociation', 2, 'Créer et modifier des associations', NULL, NULL, NULL, NULL),
('createConso', 2, 'Créer et modifier les consommateurs', NULL, NULL, NULL, NULL),
('createContactDivers', 2, 'Créer et modifier les contacts Divers', NULL, NULL, NULL, NULL),
('createEntreprise', 2, 'Créer et modifier les Exploitations', NULL, NULL, NULL, NULL),
('CreateEntrepriseDiverse', 2, 'Créer et modifier les Entreprises Diverses', NULL, NULL, NULL, NULL),
('createJournaliste', 2, 'Créer et modifier les journalistes et les journaux', NULL, NULL, NULL, NULL),
('createProducteur', 2, 'Créer et modifier un producteur', NULL, NULL, 1488715165, 1488715165),
('createREA', 2, 'Créer et modifier les contacts Rech/Encad/Admin', NULL, NULL, NULL, NULL),
('deleteAssociation', 2, 'Supprimer les associations', NULL, NULL, NULL, NULL),
('deleteConso', 2, 'Supprimer les consommateurs', NULL, NULL, NULL, NULL),
('deleteContactDivers', 2, 'Supprimer les contacts Divers', NULL, NULL, NULL, NULL),
('deleteEntreprise', 2, 'Supprimer les Exploitations', NULL, NULL, NULL, NULL),
('DeleteEntrepriseDiverse', 2, 'Supprimer les Entreprises Diverses', NULL, NULL, NULL, NULL),
('deleteJournaliste', 2, 'Supprimer les journalistes et les journaux', NULL, NULL, NULL, NULL),
('deleteProducteur', 2, 'Supprimer un producteur', NULL, NULL, 1488715166, NULL),
('deleteREA', 2, 'Supprimer les contacts Rech/Encad/Admin 	', NULL, NULL, NULL, NULL),
('indexConso', 2, 'Rechercher et voir les consommateurs', NULL, NULL, NULL, NULL),
('indexEntreprise', 2, 'Rechercher et voir les Exploitations', NULL, NULL, NULL, NULL),
('indexJournaliste', 2, 'Rechercher et voir les journalistes et les journaux', NULL, NULL, NULL, NULL),
('indexProducteur', 2, 'Rechercher et voir les producteurs', NULL, NULL, 1488717698, 1488717698),
('indexREA', 2, 'Rechercher et voir les contacts Rech/Encad/Admin', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `auth_item_child`
--

INSERT INTO `auth_item_child` (`parent`, `child`) VALUES
('administrateur', 'comments.create'),
('chargé de mission', 'comments.create'),
('Communication', 'comments.create'),
('administrateur', 'comments.delete'),
('chargé de mission', 'comments.delete.own'),
('Communication', 'comments.delete.own'),
('administrateur', 'comments.update'),
('chargé de mission', 'comments.update.own'),
('Communication', 'comments.update.own'),
('administrateur', 'createAssociation'),
('Communication', 'createAssociation'),
('administrateur', 'createConso'),
('Communication', 'createConso'),
('administrateur', 'createContactDivers'),
('Communication', 'createContactDivers'),
('administrateur', 'createEntreprise'),
('chargé de mission', 'createEntreprise'),
('Communication', 'createEntreprise'),
('administrateur', 'CreateEntrepriseDiverse'),
('administrateur', 'createJournaliste'),
('chargé de mission', 'createJournaliste'),
('Communication', 'createJournaliste'),
('administrateur', 'createProducteur'),
('chargé de mission', 'createProducteur'),
('administrateur', 'createREA'),
('Communication', 'createREA'),
('administrateur', 'deleteAssociation'),
('Communication', 'deleteAssociation'),
('administrateur', 'deleteConso'),
('Communication', 'deleteConso'),
('administrateur', 'deleteContactDivers'),
('Communication', 'deleteContactDivers'),
('administrateur', 'deleteEntreprise'),
('administrateur', 'DeleteEntrepriseDiverse'),
('chargé de mission', 'deleteExtreprise'),
('Communication', 'deleteExtreprise'),
('administrateur', 'deleteJournaliste'),
('Communication', 'deleteJournaliste'),
('administrateur', 'deleteProducteur'),
('administrateur', 'deleteREA'),
('Communication', 'deleteREA'),
('administrateur', 'indexConso'),
('chargé de mission', 'indexConso'),
('Communication', 'indexConso'),
('administrateur', 'indexContact'),
('administrateur', 'indexEntreprise'),
('administrateur', 'indexJournaliste'),
('chargé de mission', 'indexJournaliste'),
('Communication', 'indexJournaliste'),
('administrateur', 'indexProducteur'),
('chargé de mission', 'indexProducteur'),
('Communication', 'indexProducteur'),
('administrateur', 'indexREA'),
('chargé de mission', 'indexREA'),
('Communication', 'indexREA');

-- --------------------------------------------------------

--
-- Structure de la table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `auth_rule`
--

INSERT INTO `auth_rule` (`name`, `data`, `created_at`, `updated_at`) VALUES
('comments.its-my-comment', 0x4f3a34353a22726d726576696e5c7969695c6d6f64756c655c436f6d6d656e74735c726261635c4974734d79436f6d6d656e74223a333a7b733a343a226e616d65223b733a32333a22636f6d6d656e74732e6974732d6d792d636f6d6d656e74223b733a393a22637265617465644174223b693a313439323236383636383b733a393a22757064617465644174223b693a313439323236383636383b7d, 1492268668, 1492268668);

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `entity` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `from` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8_unicode_ci,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `comment`
--

INSERT INTO `comment` (`id`, `entity`, `from`, `text`, `deleted`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 'producteur-4', NULL, 'ici commentaire 1 (edited)\r\n', 0, 1, 1, 1492179093, 1492179159),
(3, 'producteur-4', NULL, 'là : commentaire 2', 1, 1, 1, 1492179103, 1492179148),
(4, 'producteur-4', NULL, '> ici commentaire 1\r\n> \r\n\r\net là : commentaire 1.5 (reply au 1)', 0, 1, 1, 1492179130, 1492179130),
(5, 'producteur-4', NULL, '<body onload=alert(\'test1\')><> <- fixed', 0, 1, 1, 1492195585, 1492252676),
(6, 'producteur-4', NULL, 'test\r\naa', 1, 1, 1, 1492196744, 1492196756),
(7, 'producteur-5', NULL, 'hi', 0, 1, 1, 1492258168, 1492258168),
(8, 'producteur-5', NULL, 'sdfdsf', 0, 1, 1, 1492258195, 1492258195),
(9, 'producteur-4', NULL, 'aabaa', 0, 3, 1, 1492268803, 1492269866),
(10, 'producteur-4', NULL, 'test3', 0, 3, 3, 1492284996, 1492285025),
(14, 'consommateur-182', NULL, 'doublon !', 0, 1, 1, 1492958739, 1492958739),
(16, 'rea-128', NULL, 'allo', 0, 1, 1, 1493124809, 1493124809),
(17, 'consommateur-153', NULL, 'test', 0, 1, 1, 1493125164, 1493125164);

-- --------------------------------------------------------

--
-- Structure de la table `consommateur`
--

DROP TABLE IF EXISTS `consommateur`;
CREATE TABLE `consommateur` (
  `conso_lieu_contact` int(11) DEFAULT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `consommateur`
--

INSERT INTO `consommateur` (`conso_lieu_contact`, `cont_id`) VALUES
(NULL, 153),
(NULL, 182),
(1, 119),
(10, 154),
(11, 155),
(11, 156);

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `cont_id` int(11) NOT NULL,
  `cont_prenom` varchar(25) NOT NULL,
  `cont_nom` varchar(25) NOT NULL,
  `cont_civilite` varchar(4) NOT NULL,
  `cont_annee_naissance` date DEFAULT NULL,
  `cont_created_at` timestamp NULL DEFAULT NULL,
  `cont_gsm_principal` varchar(15) DEFAULT NULL,
  `cont_email_principal` varchar(50) DEFAULT NULL,
  `adr_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `contact`
--

INSERT INTO `contact` (`cont_id`, `cont_prenom`, `cont_nom`, `cont_civilite`, `cont_annee_naissance`, `cont_created_at`, `cont_gsm_principal`, `cont_email_principal`, `adr_id`) VALUES
(3, 'Michael', 'Marshall', 'Mme', NULL, NULL, '00320479123456', 'michael@marshall.com', NULL),
(4, 'Albert', 'Chapman', 'Mr', '1942-09-09', '2017-03-08 08:38:23', '00324111112', 'albert@chapman', 1),
(5, 'Patrick', 'Moreno', 'Mr', NULL, NULL, NULL, 'pat@moreno.com', 3),
(10, 'Linda', 'Bishop', 'Mme', NULL, NULL, NULL, 'linda@bishop.com', NULL),
(11, 'Ruby', 'Ray', 'Mr', NULL, NULL, NULL, NULL, NULL),
(12, 'Mary', 'Morales', 'Mr', NULL, NULL, '512', NULL, NULL),
(15, 'Linda', 'Ruiz', 'Mr', NULL, NULL, '0032499988988', NULL, NULL),
(18, 'Louise', 'Hernandez', 'Mr', NULL, NULL, NULL, NULL, NULL),
(23, 'Walter', 'Collins', 'Mr', NULL, NULL, NULL, NULL, NULL),
(24, 'Joyce', 'Burton', 'Mr', NULL, NULL, NULL, NULL, NULL),
(25, 'Gary', 'Perkins', 'Mr', NULL, NULL, NULL, NULL, NULL),
(26, 'Brandon', 'Porter', 'Mr', NULL, NULL, NULL, NULL, NULL),
(30, 'Joshua', 'Ruiz', 'Ms', NULL, NULL, NULL, NULL, NULL),
(31, 'Robin', 'Rivera', 'Mr', NULL, NULL, NULL, NULL, NULL),
(32, 'Andrew', 'Mitchell', 'Ms', NULL, NULL, NULL, 'Andrew@Mitchell.com', NULL),
(33, 'Steven', 'Snyder', 'Ms', NULL, NULL, NULL, NULL, NULL),
(34, 'Deborah', 'Armstrong', 'Mr', NULL, NULL, NULL, NULL, NULL),
(35, 'Linda', 'Medina', 'Mr', NULL, NULL, '0478542546', 'Linda@Medina.com', NULL),
(36, 'Christina', 'Green', 'Mr', NULL, NULL, NULL, NULL, NULL),
(37, 'Tina', 'Fisher', 'Mr', NULL, NULL, NULL, NULL, NULL),
(38, 'Maria', 'Thomas', 'Mr', NULL, NULL, NULL, NULL, NULL),
(39, 'Catherine', 'James', 'Mme', NULL, NULL, NULL, NULL, NULL),
(40, 'Rebecca', 'Sims', 'Mr', NULL, NULL, NULL, NULL, NULL),
(41, 'Shirley', 'White', 'Ms', NULL, NULL, '0479854789', NULL, NULL),
(42, 'Sharon', 'Cox', 'Mr', NULL, NULL, '0497452087', 'Sharon@Cox.be', NULL),
(43, 'Bobby', 'Cook', 'Mme', NULL, NULL, NULL, 'Bobby@Cook.be', NULL),
(44, 'Kimberly', 'Montgomery', 'Mme', NULL, NULL, NULL, NULL, NULL),
(45, 'Nancy', 'Jones', 'Mme', NULL, NULL, NULL, NULL, NULL),
(46, 'Carlos', 'Carr', 'Ms', NULL, NULL, NULL, NULL, NULL),
(47, 'Margaret', 'Hansen', 'Mr', NULL, NULL, NULL, NULL, NULL),
(48, 'Elizabeth', 'Richardson', 'Mme', NULL, NULL, NULL, NULL, NULL),
(49, 'Catherine', 'Larson', 'Mr', NULL, NULL, NULL, NULL, NULL),
(50, 'Ashley', 'Castillo', 'Mme', NULL, NULL, NULL, NULL, NULL),
(51, 'Philip', 'Phillips', 'Ms', NULL, NULL, NULL, NULL, NULL),
(52, 'Howard', 'Hamilton', 'Mr', NULL, NULL, NULL, NULL, NULL),
(53, 'Margaret', 'Cruz', 'Mr', NULL, NULL, NULL, NULL, NULL),
(54, 'Tina', 'Ramos', 'Mme', NULL, NULL, NULL, NULL, NULL),
(55, 'Pamela', 'Owens', 'Mr', NULL, NULL, NULL, NULL, NULL),
(56, 'Diana', 'Webb', 'Mr', NULL, NULL, NULL, NULL, NULL),
(57, 'Martin', 'Carter', 'Mr', NULL, NULL, NULL, NULL, NULL),
(58, 'Kathy', 'Bishop', 'Mr', NULL, NULL, NULL, NULL, NULL),
(59, 'Louis', 'Larson', 'Ms', NULL, NULL, NULL, NULL, NULL),
(60, 'Patrick', 'Owens', 'Mr', '1980-01-06', NULL, '0471548597', 'test@email.be', NULL),
(61, 'Albert', 'Phillips', 'Mr', NULL, NULL, NULL, NULL, NULL),
(62, 'Teresa', 'Webb', 'Mme', NULL, NULL, NULL, NULL, NULL),
(64, 'Willie', 'Tucker', 'Mme', NULL, NULL, NULL, NULL, NULL),
(65, 'Steve', 'Howard', 'Mr', NULL, NULL, NULL, NULL, NULL),
(66, 'Catherine', 'Bennett', 'Mme', NULL, NULL, NULL, NULL, NULL),
(67, 'Willie', 'Snyder', 'Mr', NULL, NULL, NULL, NULL, NULL),
(68, 'Alan', 'Howard', 'Mme', NULL, NULL, NULL, NULL, NULL),
(70, 'Craig', 'Green', 'Ms', NULL, NULL, NULL, NULL, NULL),
(71, 'Jesse', 'Matthews', 'Mr', NULL, NULL, NULL, NULL, NULL),
(72, 'Bobby', 'Smith', 'Mr', NULL, NULL, NULL, NULL, NULL),
(73, 'Ruth', 'Lane', 'Mme', NULL, NULL, NULL, NULL, NULL),
(74, 'Shawn', 'Warren', 'Ms', NULL, NULL, NULL, NULL, NULL),
(77, 'Ernest', 'Wilson', 'Mr', NULL, NULL, NULL, NULL, NULL),
(78, 'James', 'Cole', 'Mr', NULL, NULL, NULL, NULL, NULL),
(79, 'Shawn', 'Stone', 'Mr', NULL, NULL, NULL, NULL, NULL),
(80, 'Andrea', 'Butler', 'Mr', NULL, NULL, NULL, NULL, NULL),
(81, 'Helen', 'Gray', 'Mme', NULL, NULL, NULL, NULL, NULL),
(82, 'Dorothy', 'Carr', 'Mr', NULL, NULL, NULL, NULL, NULL),
(83, 'Evelyn', 'Henderson', 'Ms', NULL, NULL, NULL, NULL, NULL),
(84, 'Maria', 'Mitchell', 'Mr', NULL, NULL, NULL, NULL, NULL),
(85, 'Diana', 'Elliott', 'Mme', NULL, NULL, NULL, NULL, NULL),
(86, 'Larry', 'Harper', 'Mme', NULL, NULL, NULL, NULL, NULL),
(87, 'Ann', 'Mcdonald', 'Mr', NULL, NULL, NULL, NULL, NULL),
(88, 'Jerry', 'Weaver', 'Ms', NULL, NULL, NULL, NULL, NULL),
(89, 'Amy', 'Hunter', 'Mr', NULL, NULL, NULL, NULL, NULL),
(90, 'Theresa', 'Woods', 'Ms', NULL, NULL, NULL, NULL, NULL),
(91, 'Kathryn', 'Edwards', 'Mr', NULL, NULL, NULL, NULL, NULL),
(92, 'Benjamin', 'Hansen', 'Mr', NULL, NULL, NULL, NULL, NULL),
(93, 'Jane', 'Simmons', 'Ms', NULL, NULL, NULL, NULL, NULL),
(94, 'Jimmy', 'Cruz', 'Mme', NULL, NULL, NULL, NULL, NULL),
(95, 'Denise', 'Gibson', 'Mr', NULL, NULL, NULL, NULL, NULL),
(96, 'Jennifer', 'Lee', 'Ms', NULL, NULL, NULL, NULL, NULL),
(97, 'Anna', 'Thomas', 'Mr', NULL, NULL, NULL, NULL, NULL),
(98, 'Ruby', 'Freeman', 'Mme', NULL, NULL, NULL, NULL, NULL),
(100, 'Jean', 'Pierce', 'Mr', NULL, NULL, NULL, NULL, NULL),
(101, 'Christina', 'Hughes', 'Mme', NULL, NULL, NULL, NULL, NULL),
(102, 'monsieur', 'pikachu', 'Mr', '1987-02-01', NULL, NULL, NULL, NULL),
(104, 'Test', 'Premier', 'Mr', NULL, NULL, NULL, NULL, NULL),
(105, 'Test', 'Deuxieme', 'Mr', NULL, NULL, NULL, NULL, NULL),
(106, 'Test', 'trois', 'Mr', NULL, NULL, NULL, NULL, NULL),
(107, 'Test', 'quatre', 'Mr', NULL, NULL, NULL, NULL, NULL),
(108, 'Test', 'quatre', 'Mr', NULL, NULL, NULL, NULL, NULL),
(109, 'Test', 'cinq', 'Mr', NULL, NULL, NULL, NULL, NULL),
(110, 'Test2', 'Premier', 'Mr', NULL, NULL, NULL, NULL, NULL),
(113, 'aze', 'poi', 'Mr', NULL, NULL, NULL, NULL, NULL),
(114, 'Test', 'Deuxieme', 'qqqq', NULL, NULL, NULL, NULL, NULL),
(116, 'zae', 'zae', 'Mr', NULL, NULL, NULL, NULL, NULL),
(117, 'Test2', 'Premier', 'Mr', NULL, NULL, NULL, NULL, NULL),
(119, 'yann', 'Baleze', 'Mr', NULL, NULL, NULL, NULL, NULL),
(121, 'sprint', 'deux', 'Mr', NULL, '2017-03-19 12:31:44', NULL, NULL, NULL),
(123, 'rzar', 'azrr', 'azr', NULL, '2017-03-19 14:17:08', '', '', NULL),
(124, 'azr', 'azr', 'azr', NULL, '2017-03-19 14:17:37', '', '', NULL),
(125, 'azeee', 'azeee', 'azee', NULL, '2017-03-19 14:18:10', '', '', NULL),
(126, 'azeee', 'azeee', 'Mr', NULL, '2017-03-19 14:18:22', NULL, NULL, NULL),
(127, 'azaaa', 'azeee', 'Mr', NULL, '2017-03-19 14:18:38', NULL, NULL, NULL),
(128, 'azeee', 'azeee', 'Mr', NULL, '2017-03-19 14:19:27', '4', NULL, NULL),
(129, 'azeee', 'azeee', 'Mr', NULL, '2017-03-19 14:19:35', NULL, NULL, NULL),
(130, 'azeeee', 'eeeeeeeeee', 'az', NULL, '2017-03-19 14:20:17', '', '', NULL),
(135, 'eee', 'zzz', 'az', NULL, '2017-03-19 21:44:43', NULL, NULL, NULL),
(136, 'testnew', 'form', 'Mr', NULL, '2017-03-21 13:51:24', NULL, NULL, NULL),
(146, 'testassocieaaaaaaaaaaaaaa', 'testingeeeeeeeeeeeeeeeeee', 'Mr', NULL, '2017-03-30 19:04:41', '003204791234', NULL, NULL),
(151, 'eee', 'Premier', 'Mr', NULL, '2017-04-04 12:50:15', NULL, NULL, NULL),
(153, 'azeeee', 'azeeza', 'Mr', NULL, '2017-04-05 12:18:47', '1', NULL, NULL),
(154, 'ash', 'ketchum', 'Mr', NULL, '2017-04-05 16:51:30', NULL, NULL, NULL),
(155, 'professeur ', 'chen', 'Mr', NULL, '2017-04-05 16:52:40', NULL, NULL, NULL),
(156, 'azzzezza', 'lumimu', 'Mr', NULL, '2017-04-06 08:35:39', NULL, NULL, NULL),
(157, 'testrea', 'reaaer', 'Mr', NULL, '2017-04-10 18:50:54', '458', NULL, NULL),
(158, 'azeaze', 'Premier', 'Mr', NULL, '2017-04-13 14:07:41', '1', NULL, NULL),
(159, 'testadd', 'somebody', 'Mr', NULL, '2017-04-20 18:47:44', NULL, NULL, NULL),
(165, 'testaddaazza', 'azaazaz', 'Mr', NULL, '2017-04-20 18:58:55', NULL, NULL, NULL),
(167, 'Patrick', 'Owens', 'Mr', NULL, '2017-04-20 19:04:01', NULL, NULL, NULL),
(168, 'Patrick', 'Owen', 'Mr', NULL, '2017-04-20 19:04:21', NULL, NULL, NULL),
(169, 'dsffdsfdfds', 'fdsdsffdsfdsfdsdsf', 'Mr', NULL, '2017-04-20 19:36:05', NULL, NULL, NULL),
(170, 'Patrick', 'Owens', 'Mr', NULL, '2017-04-20 19:36:24', NULL, NULL, NULL),
(171, 'Albert', 'Chapman', 'Mr', NULL, '2017-04-21 15:28:41', NULL, NULL, NULL),
(173, 'Albert', 'Chapman', 'Mr', NULL, '2017-04-21 15:30:56', NULL, NULL, NULL),
(174, 'Patrick', 'Moreno', 'Mr', NULL, '2017-04-21 20:01:57', NULL, NULL, NULL),
(175, 'Patrick', 'Moreno', 'Mr', NULL, '2017-04-21 20:04:12', NULL, NULL, NULL),
(177, 'poi', 'aze', 'Mr', NULL, '2017-04-23 10:49:15', NULL, NULL, NULL),
(178, 'poi', 'aze', 'Mr', NULL, '2017-04-23 11:48:49', NULL, NULL, NULL),
(179, 'poi', 'aze', 'Mr', NULL, '2017-04-23 11:50:14', NULL, NULL, NULL),
(180, 'paldd', 'nod', 'Mr', NULL, '2017-04-23 12:02:16', NULL, NULL, NULL),
(181, 'azeee', 'azeee', 'Mr', NULL, '2017-04-23 12:34:35', NULL, NULL, NULL),
(182, 'professeur', 'chen', 'Mr', NULL, '2017-04-23 12:45:27', NULL, NULL, NULL),
(183, 'addlieu', 'test', 'Mr', NULL, '2017-04-24 10:50:15', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `contact_divers`
--

DROP TABLE IF EXISTS `contact_divers`;
CREATE TABLE `contact_divers` (
  `cont_id` int(11) NOT NULL,
  `cont_divers_type` varchar(20) DEFAULT NULL,
  `cont_divers_lieu_contact` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `contact_divers`
--

INSERT INTO `contact_divers` (`cont_id`, `cont_divers_type`, `cont_divers_lieu_contact`) VALUES
(178, NULL, NULL),
(183, '', 12);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise`
--

DROP TABLE IF EXISTS `entreprise`;
CREATE TABLE `entreprise` (
  `entr_id` int(11) NOT NULL,
  `entr_nom` varchar(25) NOT NULL,
  `entr_forme_juridique` varchar(25) DEFAULT NULL,
  `entr_telephone_principal` char(25) DEFAULT NULL,
  `entr_email_principal` varchar(40) DEFAULT NULL,
  `entr_site_internet` varchar(30) DEFAULT NULL,
  `adr_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `entreprise`
--

INSERT INTO `entreprise` (`entr_id`, `entr_nom`, `entr_forme_juridique`, `entr_telephone_principal`, `entr_email_principal`, `entr_site_internet`, `adr_id`) VALUES
(1, 'La petite ferme', 'SPRL', '0245555444', NULL, NULL, 2),
(2, 'la libre be', 'SA', NULL, NULL, NULL, 5),
(3, 'testCreateEntreprise', 'test', '', '', '', NULL),
(4, 'La petite ferme', 'SPRL', '', '', '', 2),
(5, 'La petite fermeuh', 'SPRL', '', '', '', 2),
(6, 'La petite fermeuh', 'SPRL', '', '', '', 2),
(7, 'La petite ferme1', 'SPRL', '', '', '', 2),
(8, 'La petite ferme2', 'jljml', '', '', '', NULL),
(9, 'testCreateEntreprisecont4', 'dududu', '', '', '', NULL),
(10, 'newentre', 'ajax', '4', '', '', NULL),
(11, 'noadre', 'lol', NULL, NULL, 'dunno.be', NULL),
(12, 'La petite ferme sanadr', 'asbloul', '', '', '', NULL),
(13, 'testjournal', 'jj', '', '', '', NULL),
(14, 'la grosse ferme', 'SPRL', '0004445', '', 'www.test.be', NULL),
(15, 'unknowfarm', NULL, NULL, NULL, NULL, NULL),
(16, 'newnews', 'SPRL', NULL, NULL, NULL, NULL),
(17, 'the little farm', NULL, NULL, NULL, NULL, 11),
(18, 'dffdfdab', NULL, NULL, NULL, NULL, 6),
(19, 'testfarm', NULL, NULL, NULL, NULL, NULL),
(20, 'les4moul1', NULL, NULL, NULL, NULL, NULL),
(21, 'testassoc', NULL, NULL, NULL, NULL, 12),
(22, 'testassocviaprod', 'sprrlol', NULL, NULL, NULL, NULL),
(23, 'entrediverssa', 'absdl', '002445', NULL, NULL, 13),
(24, 'testHorti', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `entreprise_divers`
--

DROP TABLE IF EXISTS `entreprise_divers`;
CREATE TABLE `entreprise_divers` (
  `type_entreprise` varchar(25) DEFAULT NULL,
  `entr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `entreprise_divers`
--

INSERT INTO `entreprise_divers` (`type_entreprise`, `entr_id`) VALUES
('tranformation', 11),
('yoplàla', 23);

-- --------------------------------------------------------

--
-- Structure de la table `exploitation`
--

DROP TABLE IF EXISTS `exploitation`;
CREATE TABLE `exploitation` (
  `exploitation_visite_lieu` tinyint(1) DEFAULT NULL,
  `entr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `exploitation`
--

INSERT INTO `exploitation` (`exploitation_visite_lieu`, `entr_id`) VALUES
(1, 1),
(NULL, 14),
(NULL, 15),
(NULL, 17),
(NULL, 19),
(1, 20),
(NULL, 24);

-- --------------------------------------------------------

--
-- Structure de la table `gerer`
--

DROP TABLE IF EXISTS `gerer`;
CREATE TABLE `gerer` (
  `user_id` int(11) NOT NULL,
  `sect_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `gerer`
--

INSERT INTO `gerer` (`user_id`, `sect_id`) VALUES
(3, 1),
(3, 4),
(3, 5),
(14, 6);

-- --------------------------------------------------------

--
-- Structure de la table `journal`
--

DROP TABLE IF EXISTS `journal`;
CREATE TABLE `journal` (
  `journal_frequence` varchar(25) DEFAULT NULL,
  `entr_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `journal`
--

INSERT INTO `journal` (`journal_frequence`, `entr_id`) VALUES
('quotidien', 2),
('souvent', 13),
('', 16);

-- --------------------------------------------------------

--
-- Structure de la table `journaliste`
--

DROP TABLE IF EXISTS `journaliste`;
CREATE TABLE `journaliste` (
  `journaliste_fonction` varchar(25) DEFAULT NULL,
  `journaliste_langue` varchar(10) DEFAULT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `journaliste`
--

INSERT INTO `journaliste` (`journaliste_fonction`, `journaliste_langue`, `cont_id`) VALUES
('rédacteur web', 'nl', 60),
(NULL, 'nl', 61),
('journaliste', 'fr', 62),
('journaliste', 'fr', 64),
('rédac chef', 'fr', 65),
('rédac chef', 'fr', 66),
('journaliste', 'fr', 67),
('journaliste', 'fr', 68),
('journaliste', 'nl', 70),
('journaliste', 'fr', 71),
('journaliste', 'nl', 72),
('journaliste', 'fr', 73),
('journaliste', 'fr', 74),
('journaliste rédaction', 'fr', 77),
('journaliste rédaction', 'fr', 78),
('journaliste', 'fr/nl', 79),
('journaliste', 'fr', 80),
('rédacteur web', 'fr', 81),
('journaliste', 'fr', 82),
('journaliste', 'fr', 83),
('journaliste', 'fr', 84),
('rédac chef', 'fr', 85),
('journaliste', 'fr', 86),
('journaliste', 'fr', 87),
('rédac chef', 'fr', 88),
('journaliste', 'fr', 89),
('journaliste', 'fr', 90),
('journaliste', 'fr', 91),
('journaliste', 'nl', 92),
('journaliste', 'fr', 93),
('journaliste', 'fr', 94),
('journaliste', 'nl', 95),
('journaliste', 'fr', 96),
('journaliste', 'nl', 97),
('journaliste', 'fr', 98),
(NULL, 'fr', 158),
(NULL, NULL, 159),
(NULL, NULL, 165),
(NULL, NULL, 167),
(NULL, NULL, 168),
(NULL, NULL, 169),
(NULL, NULL, 170);

-- --------------------------------------------------------

--
-- Structure de la table `lieu_prise_contact`
--

DROP TABLE IF EXISTS `lieu_prise_contact`;
CREATE TABLE `lieu_prise_contact` (
  `lieu_prise_contact_id` int(11) NOT NULL,
  `lieu` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `lieu_prise_contact`
--

INSERT INTO `lieu_prise_contact` (`lieu_prise_contact_id`, `lieu`) VALUES
(10, 'alola'),
(13, 'buverie 2017'),
(3, 'foire de libramont 2015'),
(1, 'foire de libramont 2016'),
(12, 'làbas'),
(11, 'masara');

-- --------------------------------------------------------

--
-- Structure de la table `log`
--

DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `log_id` int(11) NOT NULL,
  `log_date` date NOT NULL,
  `log_attr_modifies` text NOT NULL,
  `id` int(11) NOT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `membre_college`
--

DROP TABLE IF EXISTS `membre_college`;
CREATE TABLE `membre_college` (
  `membre_date_debut` date DEFAULT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `migration`
--

DROP TABLE IF EXISTS `migration`;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Contenu de la table `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1492175565),
('m010101_100001_init_comment', 1492175570),
('m160629_121330_add_relatedTo_column_to_comment', 1492175570),
('m161109_092304_rename_comment_table', 1492175570),
('m161114_094902_add_url_column_to_comment_table', 1492175570),
('m150106_122229_comments', 1492178878),
('m151005_165040_comment_from', 1492178878);

-- --------------------------------------------------------

--
-- Structure de la table `producteur`
--

DROP TABLE IF EXISTS `producteur`;
CREATE TABLE `producteur` (
  `prod_sigec` tinyint(1) NOT NULL DEFAULT '0',
  `prod_activite_principale` tinyint(1) DEFAULT NULL,
  `prod_qualite_diff` tinyint(1) DEFAULT NULL,
  `prod_collab_echange` tinyint(1) DEFAULT NULL,
  `prod_porte_parole` tinyint(1) DEFAULT NULL,
  `prod_accepte_stagiaire` tinyint(1) DEFAULT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `producteur`
--

INSERT INTO `producteur` (`prod_sigec`, `prod_activite_principale`, `prod_qualite_diff`, `prod_collab_echange`, `prod_porte_parole`, `prod_accepte_stagiaire`, `cont_id`) VALUES
(0, 0, 1, 0, 1, 1, 4),
(0, 1, NULL, NULL, NULL, NULL, 5),
(0, 1, NULL, NULL, NULL, NULL, 10),
(0, 1, NULL, NULL, NULL, NULL, 15),
(0, 1, NULL, NULL, NULL, NULL, 18),
(1, 1, NULL, NULL, NULL, NULL, 23),
(1, 0, NULL, NULL, NULL, NULL, 24),
(1, 0, NULL, NULL, NULL, NULL, 25),
(0, 0, NULL, NULL, NULL, NULL, 26),
(0, NULL, NULL, NULL, NULL, NULL, 104),
(0, NULL, NULL, NULL, NULL, NULL, 105),
(0, NULL, NULL, NULL, NULL, NULL, 106),
(0, NULL, NULL, NULL, NULL, NULL, 107),
(0, NULL, NULL, NULL, NULL, NULL, 108),
(0, NULL, NULL, NULL, NULL, NULL, 109),
(0, NULL, NULL, NULL, NULL, NULL, 110),
(0, NULL, NULL, NULL, NULL, NULL, 113),
(0, NULL, NULL, NULL, NULL, NULL, 121),
(0, NULL, NULL, NULL, NULL, NULL, 135),
(0, NULL, NULL, NULL, NULL, NULL, 136),
(0, NULL, NULL, NULL, NULL, NULL, 151),
(0, NULL, NULL, NULL, NULL, NULL, 171),
(0, NULL, NULL, NULL, NULL, NULL, 173),
(0, NULL, NULL, NULL, NULL, NULL, 174),
(0, NULL, NULL, NULL, NULL, NULL, 175),
(0, NULL, NULL, NULL, NULL, NULL, 177),
(0, NULL, NULL, NULL, NULL, NULL, 179),
(0, NULL, NULL, NULL, NULL, NULL, 180);

-- --------------------------------------------------------

--
-- Structure de la table `rech_admin_encad`
--

DROP TABLE IF EXISTS `rech_admin_encad`;
CREATE TABLE `rech_admin_encad` (
  `rae_type` tinyint(1) NOT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `rech_admin_encad`
--

INSERT INTO `rech_admin_encad` (`rae_type`, `cont_id`) VALUES
(3, 128),
(2, 129),
(1, 130),
(2, 157),
(1, 181);

-- --------------------------------------------------------

--
-- Structure de la table `representant_college`
--

DROP TABLE IF EXISTS `representant_college`;
CREATE TABLE `representant_college` (
  `representant_annee` char(25) NOT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `secteur`
--

DROP TABLE IF EXISTS `secteur`;
CREATE TABLE `secteur` (
  `sect_id` int(11) NOT NULL,
  `sect_nom` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `secteur`
--

INSERT INTO `secteur` (`sect_id`, `sect_nom`) VALUES
(1, 'Aquaculture'),
(2, 'Lait'),
(3, 'Ovins'),
(4, 'Caprins'),
(5, 'Horticulture comestible'),
(6, 'Horticulture Ornementale'),
(7, 'Grandes cultures'),
(8, 'Porcin'),
(9, 'Avicole'),
(10, 'Cunicole'),
(11, 'Viande Bovine'),
(12, 'Pomme de terre'),
(13, 'Bio');

-- --------------------------------------------------------

--
-- Structure de la table `token`
--

DROP TABLE IF EXISTS `token`;
CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(3, 'R4xDb8C-vaHCSJflbYzqx--X057Z5ryK', 1488555789, 0);

-- --------------------------------------------------------

--
-- Structure de la table `traiter`
--

DROP TABLE IF EXISTS `traiter`;
CREATE TABLE `traiter` (
  `entr_id` int(11) NOT NULL,
  `sect_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `traiter`
--

INSERT INTO `traiter` (`entr_id`, `sect_id`) VALUES
(1, 1),
(19, 1),
(20, 1),
(1, 2),
(17, 2),
(20, 2),
(20, 3),
(20, 4),
(24, 6);

-- --------------------------------------------------------

--
-- Structure de la table `travailler`
--

DROP TABLE IF EXISTS `travailler`;
CREATE TABLE `travailler` (
  `sect_id` int(11) NOT NULL,
  `cont_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `travailler`
--

INSERT INTO `travailler` (`sect_id`, `cont_id`) VALUES
(1, 4),
(2, 4),
(3, 4),
(2, 5),
(3, 5),
(2, 10),
(3, 10),
(3, 11),
(1, 15),
(3, 15),
(2, 113),
(3, 121),
(4, 121),
(3, 135),
(1, 136),
(1, 151),
(1, 156),
(3, 156),
(1, 171),
(1, 173),
(2, 174),
(1, 175),
(1, 177),
(4, 179),
(2, 180);

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL,
  `user_admin` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`, `user_admin`) VALUES
(1, 'admin', 'admin@admin.be', '$2y$10$WQgxkoxoiHsiGpcKUxy/N.psOtCZ4wz3ekDCcprEfcQnk0.RZYOGm', 'VWcC1r7QiPqvvorbYxBDvxNdADbCX8az', 1488561287, NULL, NULL, '127.0.0.1', 1488561287, 1488561287, 0, 1493107745, 1),
(3, 'toto', 'toto@toto.com', '$2y$10$J0iNJuTAgPHiRgPNytG10eXfTPbeE/V7.B/co/y/hnmx1/Fyc32G6', 'uR7s1etrf3-AiNfGQ7ZAtIqVTj-XMuOo', 1, NULL, NULL, '127.0.0.1', 1488555789, 1488555789, 0, 1492979293, 0),
(11, 'laura.roost', 'laura.roost@collegedesproducteurs.be', '$2y$10$aZA1eBGJxfEwac/gHcQM5uw8fRwz2yRIjlM0YbRotYUPaTsjfNXqC', 'QQff_4Sd49BxJe9W8HsjPbWPinBeGKTT', 1493131869, NULL, NULL, '127.0.0.1', 1493131869, 1493131869, 0, 1493146893, NULL),
(12, 'emmanuel.grosjean', 'emmanuel.grosjean@collegedesproducteurs.be', '$2y$10$BlV2EoQbzWmhAoJL5O3zqeUe7sbmsNtnzmW7PdMU8WxoKKhBmlsjm', 'bJybu737AZPx9C0JEpxjr4g5oDUJhZNX', 1493132576, NULL, NULL, '127.0.0.1', 1493132576, 1493132576, 0, NULL, NULL),
(13, 'isabelle.monnart', 'isabelle.monnart@collegedesproducteurs.be', '$2y$10$ptg58M0JAR4oQUXfd0rdquVG4scnVgxiJpoabrujbeIOIc/a7A70.', 'wVieklCmef0lu5tfI_im70u-nRIFyEMr', 1493132612, NULL, NULL, '127.0.0.1', 1493132612, 1493132612, 0, NULL, NULL),
(14, 'alain.grifnee', 'alain.grifnee@collegedesproducteurs.be', '$2y$10$b0jlPuQkZE3zN2Y00J240eyOJlMNEfpujWPCgg0nCxSFzC9H9m1ei', 'jE0ezNPxDa4CzyaXyYkqVlC7SzjvvH5y', 1493132740, NULL, NULL, '127.0.0.1', 1493132740, 1493132740, 0, 1493147008, NULL),
(15, 'christel.daniaux', 'christel.daniaux@collegedesproducteurs.be', '$2y$10$lA2MlrhL2OHZcDohFZswmuAA8wXoDMgAWrdhHkc5cRxrkGiGSVkRy', 'Os2fDy0gTp887HnhFMaPque05Hb1DVve', 1493132843, NULL, NULL, '127.0.0.1', 1493132843, 1493132843, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Doublure de structure pour la vue `v_contact`
-- (Voir ci-dessous la vue réelle)
--
DROP VIEW IF EXISTS `v_contact`;
CREATE TABLE `v_contact` (
`cont_id` int(11)
,`cont_prenom` varchar(25)
,`cont_civilite` varchar(4)
,`NATURE` varchar(11)
);

-- --------------------------------------------------------

--
-- Structure de la vue `v_contact`
--
DROP TABLE IF EXISTS `v_contact`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_contact`  AS  select `c`.`cont_id` AS `cont_id`,`c`.`cont_prenom` AS `cont_prenom`,`c`.`cont_civilite` AS `cont_civilite`,(case when (`p`.`cont_id` is not null) then 'Producteur' when (`j`.`cont_id` is not null) then 'Journaliste' else 'inconnu !' end) AS `NATURE` from ((`contact` `c` left join `producteur` `p` on((`c`.`cont_id` = `p`.`cont_id`))) left join `journaliste` `j` on((`c`.`cont_id` = `j`.`cont_id`))) ;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `adresse`
--
ALTER TABLE `adresse`
  ADD PRIMARY KEY (`adr_id`);

--
-- Index pour la table `appartenir`
--
ALTER TABLE `appartenir`
  ADD PRIMARY KEY (`entr_id`,`cont_id`),
  ADD KEY `FK_appartenir_cont_id` (`cont_id`);

--
-- Index pour la table `association`
--
ALTER TABLE `association`
  ADD PRIMARY KEY (`entr_id`);

--
-- Index pour la table `associe`
--
ALTER TABLE `associe`
  ADD PRIMARY KEY (`cont_id`),
  ADD KEY `FK_Associe_cont_id_prod` (`cont_id_prod`);

--
-- Index pour la table `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`) USING BTREE;

--
-- Index pour la table `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`) USING BTREE;

--
-- Index pour la table `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx_entity` (`entity`),
  ADD KEY `idx_created_by` (`created_by`),
  ADD KEY `idx_created_at` (`created_at`);

--
-- Index pour la table `consommateur`
--
ALTER TABLE `consommateur`
  ADD PRIMARY KEY (`cont_id`),
  ADD KEY `FK_consommateur_lieu_prise_contact` (`conso_lieu_contact`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`cont_id`),
  ADD KEY `FK_contact_adr_id` (`adr_id`);

--
-- Index pour la table `contact_divers`
--
ALTER TABLE `contact_divers`
  ADD PRIMARY KEY (`cont_id`),
  ADD KEY `FK_cont_divers_lieu_prise_contact` (`cont_divers_lieu_contact`) USING BTREE;

--
-- Index pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD PRIMARY KEY (`entr_id`),
  ADD KEY `FK_entreprise_adr_id` (`adr_id`);

--
-- Index pour la table `entreprise_divers`
--
ALTER TABLE `entreprise_divers`
  ADD PRIMARY KEY (`entr_id`);

--
-- Index pour la table `exploitation`
--
ALTER TABLE `exploitation`
  ADD PRIMARY KEY (`entr_id`);

--
-- Index pour la table `gerer`
--
ALTER TABLE `gerer`
  ADD PRIMARY KEY (`user_id`,`sect_id`),
  ADD KEY `FK_gerer_sect_id` (`sect_id`);

--
-- Index pour la table `journal`
--
ALTER TABLE `journal`
  ADD PRIMARY KEY (`entr_id`);

--
-- Index pour la table `journaliste`
--
ALTER TABLE `journaliste`
  ADD PRIMARY KEY (`cont_id`);

--
-- Index pour la table `lieu_prise_contact`
--
ALTER TABLE `lieu_prise_contact`
  ADD PRIMARY KEY (`lieu_prise_contact_id`),
  ADD UNIQUE KEY `lieu` (`lieu`);

--
-- Index pour la table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `FK_Log_id` (`id`),
  ADD KEY `FK_Log_cont_id` (`cont_id`);

--
-- Index pour la table `membre_college`
--
ALTER TABLE `membre_college`
  ADD PRIMARY KEY (`cont_id`);

--
-- Index pour la table `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Index pour la table `producteur`
--
ALTER TABLE `producteur`
  ADD PRIMARY KEY (`cont_id`);

--
-- Index pour la table `rech_admin_encad`
--
ALTER TABLE `rech_admin_encad`
  ADD PRIMARY KEY (`cont_id`);

--
-- Index pour la table `representant_college`
--
ALTER TABLE `representant_college`
  ADD PRIMARY KEY (`cont_id`);

--
-- Index pour la table `secteur`
--
ALTER TABLE `secteur`
  ADD PRIMARY KEY (`sect_id`);

--
-- Index pour la table `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Index pour la table `traiter`
--
ALTER TABLE `traiter`
  ADD PRIMARY KEY (`entr_id`,`sect_id`),
  ADD KEY `FK_Traiter_sect_id` (`sect_id`);

--
-- Index pour la table `travailler`
--
ALTER TABLE `travailler`
  ADD PRIMARY KEY (`sect_id`,`cont_id`),
  ADD KEY `FK_travailler_cont_id` (`cont_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `adresse`
--
ALTER TABLE `adresse`
  MODIFY `adr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `cont_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=185;
--
-- AUTO_INCREMENT pour la table `entreprise`
--
ALTER TABLE `entreprise`
  MODIFY `entr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT pour la table `lieu_prise_contact`
--
ALTER TABLE `lieu_prise_contact`
  MODIFY `lieu_prise_contact_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `log`
--
ALTER TABLE `log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `secteur`
--
ALTER TABLE `secteur`
  MODIFY `sect_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `appartenir`
--
ALTER TABLE `appartenir`
  ADD CONSTRAINT `FK_appartenir_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`),
  ADD CONSTRAINT `FK_appartenir_entr_id` FOREIGN KEY (`entr_id`) REFERENCES `entreprise` (`entr_id`);

--
-- Contraintes pour la table `association`
--
ALTER TABLE `association`
  ADD CONSTRAINT `FK_Association_entr_id` FOREIGN KEY (`entr_id`) REFERENCES `entreprise` (`entr_id`);

--
-- Contraintes pour la table `associe`
--
ALTER TABLE `associe`
  ADD CONSTRAINT `FK_Associe_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`),
  ADD CONSTRAINT `FK_Associe_cont_id_prod` FOREIGN KEY (`cont_id_prod`) REFERENCES `producteur` (`cont_id`);

--
-- Contraintes pour la table `consommateur`
--
ALTER TABLE `consommateur`
  ADD CONSTRAINT `FK_consommateur_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`),
  ADD CONSTRAINT `FK_consommateur_lieu_prise_contact` FOREIGN KEY (`conso_lieu_contact`) REFERENCES `lieu_prise_contact` (`lieu_prise_contact_id`);

--
-- Contraintes pour la table `contact`
--
ALTER TABLE `contact`
  ADD CONSTRAINT `FK_contact_adr_id` FOREIGN KEY (`adr_id`) REFERENCES `adresse` (`adr_id`);

--
-- Contraintes pour la table `contact_divers`
--
ALTER TABLE `contact_divers`
  ADD CONSTRAINT `FK_Divers_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`),
  ADD CONSTRAINT `FK_cont_divers_lieu_prise_contact` FOREIGN KEY (`cont_divers_lieu_contact`) REFERENCES `lieu_prise_contact` (`lieu_prise_contact_id`);

--
-- Contraintes pour la table `entreprise`
--
ALTER TABLE `entreprise`
  ADD CONSTRAINT `FK_entreprise_adr_id` FOREIGN KEY (`adr_id`) REFERENCES `adresse` (`adr_id`);

--
-- Contraintes pour la table `entreprise_divers`
--
ALTER TABLE `entreprise_divers`
  ADD CONSTRAINT `FK_entreprise_entr_id` FOREIGN KEY (`entr_id`) REFERENCES `entreprise` (`entr_id`);

--
-- Contraintes pour la table `exploitation`
--
ALTER TABLE `exploitation`
  ADD CONSTRAINT `FK_Ferme_entr_id` FOREIGN KEY (`entr_id`) REFERENCES `entreprise` (`entr_id`);

--
-- Contraintes pour la table `gerer`
--
ALTER TABLE `gerer`
  ADD CONSTRAINT `FK_gerer_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `FK_gerer_sect_id` FOREIGN KEY (`sect_id`) REFERENCES `secteur` (`sect_id`);

--
-- Contraintes pour la table `journal`
--
ALTER TABLE `journal`
  ADD CONSTRAINT `FK_Journal_entr_id` FOREIGN KEY (`entr_id`) REFERENCES `entreprise` (`entr_id`);

--
-- Contraintes pour la table `journaliste`
--
ALTER TABLE `journaliste`
  ADD CONSTRAINT `FK_journaliste_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`);

--
-- Contraintes pour la table `log`
--
ALTER TABLE `log`
  ADD CONSTRAINT `FK_Log_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`),
  ADD CONSTRAINT `FK_Log_id` FOREIGN KEY (`id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `membre_college`
--
ALTER TABLE `membre_college`
  ADD CONSTRAINT `FK_Membre_college_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`);

--
-- Contraintes pour la table `producteur`
--
ALTER TABLE `producteur`
  ADD CONSTRAINT `FK_producteur_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`);

--
-- Contraintes pour la table `rech_admin_encad`
--
ALTER TABLE `rech_admin_encad`
  ADD CONSTRAINT `FK_Rech_admin_encad_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`);

--
-- Contraintes pour la table `representant_college`
--
ALTER TABLE `representant_college`
  ADD CONSTRAINT `FK_Representant_college_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`);

--
-- Contraintes pour la table `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `traiter`
--
ALTER TABLE `traiter`
  ADD CONSTRAINT `FK_Traiter_entr_id` FOREIGN KEY (`entr_id`) REFERENCES `entreprise` (`entr_id`),
  ADD CONSTRAINT `FK_Traiter_sect_id` FOREIGN KEY (`sect_id`) REFERENCES `secteur` (`sect_id`);

--
-- Contraintes pour la table `travailler`
--
ALTER TABLE `travailler`
  ADD CONSTRAINT `FK_travailler_cont_id` FOREIGN KEY (`cont_id`) REFERENCES `contact` (`cont_id`),
  ADD CONSTRAINT `FK_travailler_sect_id` FOREIGN KEY (`sect_id`) REFERENCES `secteur` (`sect_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
