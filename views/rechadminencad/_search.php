<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RechAdminEncadSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rech-admin-encad-search">

    <?php $form = ActiveForm::begin([
        'action' => 'search',
        'method' => 'get',
    ]); ?>
    <?= $form->field($model, 'contactGSMPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Numéro GSM');?>
    <?= $form->field($model, 'contactEmailPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Adresse e-mail');?>
    
    <?= $form->field($model, 'rae_type')->checkboxList(['1' =>'Recherche', '2' => 'Administratif', '3' => 'Encadrement']) ?>


    <div class="form-group">
        <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
