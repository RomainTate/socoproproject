<?php

    return [
    'label' => 'Modèle de production',
    'type' => 2,
    'value' =>[ 'Bio', 'Conventionnel', 'Intégrée']
    ];