<?php

namespace app\controllers;

use Yii;
use app\models\Contact;
use app\models\ContactSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ContactController implements the CRUD actions for Contact model.
 */
class ContactController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'indexmembres', 'indexmembrescommission'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['addentreprise' , 'deleteentreprise' , 'quickupdate', 'addevenement', 'addadresse', 'managenewsletters', 'changefonction'],
                        'roles' => ['@'],
                    ],
                ]
            ]
        ];
    }

    /**
     * Lists all Contact models.
     * @return mixed
     */
    public function actionIndex() {
        if(!Yii::$app->user->getUser()->getIsAdmin()) {
            throw new NotFoundHttpException();
        }
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionView($id) {
        $model = $this->findModel($id);
        $nature = $model->getNature('url');
        
        return $this->redirect(yii\helpers\Url::home().$nature.'/view?id='.$id);
    }
    
    public function actionSearch() {
        return $this->actionIndex();
    }

    public function actionAddentreprise($contactId) {
        $post = Yii::$app->request->post();
        if ($post) {
            $this->addContactToEntr($contactId, $post['entrepriseId'], $post['fonctionContact']); 
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        } else {
                    return $this->renderAjax('addentreprisedivers', [
                    'contactId' => $contactId,
        ]);
        }
        
    }
    
    public function actionDeleteentreprise($contactId, $entr_id) {
        if(AppartenirController::deleteContactEntreprise($contactId, $entr_id)){
            return $this->redirect(Yii::$app->request->referrer);
        }
    }
    
    
    protected function findModel($id)
    {
        if (($model = Contact::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionQuickupdate($id, $attribute) {
        $model = $this->findModel($id);
        $type = $model->getTableSchema()->getColumn($attribute)->type;
        $post = Yii::$app->request->post();
        if ($model->getNature() == 'producteur') {
            $model->setScenario('typeProducteur');
        } else if ($model->getNature() == 'journaliste') {
            $model->setScenario('typeJournaliste');
        }
        if ($post) {
            if ($type =='date') {
                $model->$attribute = date("Y-m-d", strtotime($post['Contact'][$attribute]));
            } else {
                if ($attribute == 'cont_langue') {
                    
                        $model->cont_langue = $this::setLangue($post['Contact']['cont_langue']);
                } else {
                    $model->$attribute = $post['Contact'][$attribute];
                }
            }
            
            $model->save();
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        } else {
            return $this->renderAjax('quickform/updateform', [
                        'model' => $model,
                        'attribute' => $attribute,
                        'type' => $type
            ]);
        }
    }
    
    public function actionAddevenement($cont_id, $evenement_id) {
        ParticiperController::addParticipation($cont_id, $evenement_id);
        return $this->redirect(Yii::$app->request->referrer);
        
    }
    
    public function actionAddadresse($id) {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($post) {
            $model->adr_id = AdresseController::CreateAdresse($post);
            $model->save(false);
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        }
        return $this->renderAjax('/adresse/create', [
            'model' => new \app\models\Adresse(),
            'relatedModel' => null
            ]);
    }
    
    public function actionManagenewsletters($id) {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if($post) {
            if(array_key_exists('newsletters', $post)) {
                $newsletters = $post['newsletters'];
            } else {
                $newsletters = null;
            }
            $this::manageNewsletters($id, $newsletters);
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        }
        return $this->renderAjax('/newsletter/update', [
            'model' => $model
            ]);
    }
    
    public static function setLangue($cont_langue) {
        $langue = '';
        if (in_array('FR', $cont_langue)) {
            $langue .= 'FR';
        }
        if (in_array('NL', $cont_langue)) {
            $langue == '' ? $langue .= 'NL' : $langue .= '/NL';
        }
        if (in_array('DE', $cont_langue)) {
            $langue == '' ? $langue .= 'DE' : $langue .= '/DE';
        }
        if (in_array('EN', $cont_langue)) {
            $langue == '' ? $langue .= 'EN' : $langue .= '/EN';
        }
        return $langue;
    } 
    
    public static function manageNewsletters($cont_id, $newslettersID = null) {
        \app\models\Recevoir::deleteAll(['cont_id' => $cont_id]);
        if($newslettersID) {
            foreach ($newslettersID as $newletter_id) {
                $recevoir = new \app\models\Recevoir();
                $recevoir->cont_id = $cont_id;
                $recevoir->newsletter_id = $newletter_id;
                $recevoir->save();
            }
        }
        return 1;
    }
    
    public static function addContactToEntr($cont_id, $entr_id, $fonction) {
        AppartenirController::addContactEntreprise($cont_id, $entr_id, $fonction);
        return 1;
    }
    
    public function actionChangefonction($cont_id, $entr_id) {
        $model = $this->findModel($cont_id);
        $post = Yii::$app->request->post();
        $appartenir = \app\models\Appartenir::find()->where(['cont_id' => $cont_id, 'entr_id' => $entr_id])->one();
        
        if($post) {
//            die(var_dump($post));
            if (!Yii::$app->user->can('create'.$model->getNature('label'))) {
                throw new \yii\web\ForbiddenHttpException();
            }
            AppartenirController::changeFonction($cont_id, $entr_id, $post['Appartenir']['fonction']);
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        }
        
        return $this->renderAjax('/contact/changefonction', [
            'appartenir' => $appartenir
            ]);
    }
    
    public function actionIndexmembres() {
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $membresCollege = \app\models\MembreCollege::find()->select('cont_id')->all();
        if($membresCollege) {
            $dataProvider->query->andFilterWhere(['in', 'contact.cont_id', \yii\helpers\ArrayHelper::map($membresCollege, 'cont_id', 'cont_id')]);
        } else {
            $dataProvider->query->where('0=1');
        }
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'title' => 'Membres du collège'
        ]);
    }
    public function actionIndexmembrescommission() {
        $searchModel = new ContactSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        $dataProvider->query->innerJoin('membre_commission_filliere mcf', 'contact.cont_id = mcf.cont_id');
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'title' => 'Membres Commissions Filières'
        ]);
    }
}
