<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Producteur */
/* @var $contactModel app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
if (Yii::$app->session->hasFlash('error')):
    ?>
    <div class="alert alert-warning alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <h5><i class="icon fa fa-times"></i> Attention !</h5>
        <?= Yii::$app->session->getFlash('error') . '<br />' ?>
    </div>
    <?php
endif;

$this->title = 'Ajouter Producteur';
$this->params['breadcrumbs'][] = ['label' => 'Producteurs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producteur-create">

    <h3><?= Html::encode($this->title) ?></h3>
    <div class="x_panel">
        <div class="producteur-form">

            <?php $form = ActiveForm::begin([
                        'id' => 'addSecteursForm',
                        'action' => 'addsecteurs',
                        'method' => 'post'
            ]); ?>

            <div>
                Le producteur <?= $contactModel->cont_nom . ' ' . $contactModel->cont_prenom ?> existe déjà dans les secteurs : <br />
                <?php
                foreach ($existingProd->secteurs as $secteur) {
                    echo $secteur->sect_nom . '<br />';
                }
                ?>
            </div>
            <hr />
            <div>
                <p>Voulez vous ajouter <b><?= implode(' , ', app\models\Secteur::getSecteursNames($secteursToAdd)) ?></b>  au producteur existant ? </p>
            </div>
            
            <?= \yii\bootstrap\Html::hiddenInput('cont_id', $existingProd->cont_id) ?>
            
            <?php foreach ($secteursToAdd as $secteurToAdd) {
                echo '<input type="hidden" name="secteurs[]" value="' . $secteurToAdd . '">';
            }?>
            
            <div class="form-group">
                <?= Html::submitButton('OK', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod'); ?>