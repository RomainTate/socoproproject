<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "travailler".
 *
 * @property integer $cont_id
 * @property integer $sect_id
 *
 * @property Contact $contact
 * @property Secteur $secteur
 */
class Travailler extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'travailler';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id', 'sect_id'], 'required'],
            [['cont_id', 'sect_id'], 'integer'],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['sect_id'], 'exist', 'skipOnError' => true, 'targetClass' => Secteur::className(), 'targetAttribute' => ['sect_id' => 'sect_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'sect_id' => 'Sect ID',
        ];
    }
    
    public function beforeSave($insert) {
        if($insert && $this->contact->getNature() == 'producteur' && !$this->contact->producteur->needConfirm) {
            $logs['new' . 'sect'] = $this->secteur->sect_nom;
            \Yii::$app->get('logger')->log($this->contact->cont_id, json_encode($logs), 0);
        }
        return parent::beforeSave($insert);
    }
    
    public function beforeDelete() {
        if($this->contact->getNature() == 'producteur' && !$this->contact->producteur->needConfirm) {
            $logs['old' . 'sect'] = $this->secteur->sect_nom;
            \Yii::$app->get('logger')->log($this->contact->cont_id, json_encode($logs), 2);
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecteur()
    {
        return $this->hasOne(Secteur::className(), ['sect_id' => 'sect_id']);
    }
}
