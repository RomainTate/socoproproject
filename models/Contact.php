<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use codeonyii\yii2validators\AtLeastValidator;

/**
 * This is the model class for table "contact".
 *
 * @property integer $cont_id
 * @property string $cont_prenom
 * @property string $cont_nom
 * @property string $cont_civilite
 * @property string $cont_annee_naissance
 * @property string $cont_created_at
 * @property string $cont_telephone_principal
 * @property string $cont_gsm_principal
 * @property string $cont_email_principal
 * @property string $cont_langue
 * @property integer $adr_id 
 *
 * @property Appartenir[] $appartenirs
 * @property Entreprise[] $entreprises
 * @property Evenement[] $evenements
 * @property Autre $autre
 * @property Adresse $adresse
 * @property Producteur $producteur
 * @property Travailler[] $travaillers
 * @property Secteur[] $secteurs
 * @property Membrecollege $membrecollege
 * @property Newsletter[] $newsletters
 * @property Suivis[] $suivis
 */
class Contact extends \yii\db\ActiveRecord
{
    public $nature;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_prenom', 'cont_nom'], 'required', 'except' => ['typeAssocie', 'typeProducteur', 'typeJournaliste', 'foire']],
            [['cont_prenom', 'cont_nom', 'cont_gsm_principal','cont_email_principal','cont_telephone_principal'], AtLeastValidator::className()
                , 'in' => ['cont_prenom', 'cont_nom', 'cont_gsm_principal','cont_email_principal','cont_telephone_principal'], 'min' => 1, 'on' => 'typeAssocie',
                'except' => 'foire', 'message' => 'au moins un attribut doit être renseigné'],
            [['cont_prenom', 'cont_nom', 'cont_gsm_principal','cont_email_principal','cont_telephone_principal'], AtLeastValidator::className()
                , 'in' => ['cont_prenom', 'cont_nom', 'cont_gsm_principal','cont_email_principal','cont_telephone_principal'], 'min' => 1, 'on' => 'typeJournaliste',
                'except' => 'foire', 'message' => 'au moins un attribut doit être renseigné'],
            [['cont_civilite', /*'cont_annee_naissance',*/ 'cont_created_at', 'cont_gsm_principal', 'cont_email_principal', 'cont_langue'], 'safe'],
            [['cont_prenom', 'cont_nom'], 'string', 'max' => 50],
            [['cont_civilite'], 'string', 'max' => 15],
            [['cont_email_principal'], 'string', 'max' => 75],
            [['cont_email_principal'], 'email', 'message' => 'L\'adresse e-mail n\'est pas valide', 'except' => 'foire'],
            [['nature'],'safe'],
            [['cont_gsm_principal'], 'number', 'message' => 'Le numéro GSM n\'est pas valide', 'except' => 'foire',],
            [['cont_telephone_principal'], 'number', 'message' => 'Le numéro de téléphone n\'est pas valide', 'except' => 'foire',],
            [['cont_gsm_principal','cont_telephone_principal'], 'match', 'pattern' => '/^(0032)/', 'message' => 'Le numéro doit commencer par 0032', 'on' => ['typeAssocie', 'typeProducteur']],
            [['cont_gsm_principal'], 'string', 'length' => 13, 'notEqual' => 'Le numéro GSM doit comporter 13 chiffres', 'on' => ['typeAssocie', 'typeProducteur']],
            [['cont_telephone_principal'], 'string', 'length' => 12, 'notEqual' => 'Le numéro de téléphone doit comporter 12 chiffres', 'on' => ['typeAssocie', 'typeProducteur']]
        ];
    }
    
//    public function checkExists() {
//        if(Contact::find()->where(['cont_email_principal' => $this->cont_email_principal])) {
//            Yii::$app->session->setFlash('notice', ' gaffe au mail');
//        }
//    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'cont_prenom' => 'Prénom',
            'cont_nom' => 'Nom',
            'cont_civilite' => 'Civilité',
//            'cont_annee_naissance' => 'Année de Naissance',
            'cont_telephone_principal' => 'Telephone',
            'cont_gsm_principal' => 'GSM',
            'cont_email_principal' => 'Email',
            'cont_langue' => 'Langue',
            'cont_created_at' => 'Creation',
            'nature' => 'nature',
            
        ];
    }
    
    public static function getLabelAttribute($attribute) {
        $cont = New Contact();
        return $cont->getAttributeLabel($attribute);
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    \yii\db\ActiveRecord::EVENT_BEFORE_INSERT => ['cont_created_at'],
                ],
                'value' => function() { return date("Y-m-d H:i:s"); }
            ],
        ];
    }
    
    public function beforeSave($insert) {
        
        $this->cont_nom = mb_strtoupper($this->cont_nom);
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        if (!$insert) {
            if (empty($this->getDirtyAttributes())) {
                return parent::beforeSave($insert);
            }

            if ($this->getNature() == 'contact divers') {
                return parent::beforeSave($insert);
            }
            
            $FIELDSTOLOG = ['cont_gsm_principal', 'cont_email_principal'];
            //die(var_dump($this->getDirtyAttributes())); //array ['nom_attr_modifié' => 'nouvelle valeur']
            //die(var_dump($this->getOldAttributes())); //array ['nom_attr' => 'old valeur']
            foreach ($this->getDirtyAttributes() as $dirtyKey => $dirtyValue) {
                $logs = array();
                if (in_array($dirtyKey, $FIELDSTOLOG)) {
                    if ($this->getOldAttribute($dirtyKey) != null) {
                        $action = 1;
                        $logs['old' . $dirtyKey] = $this->getOldAttribute($dirtyKey);
                    } else {
                        $action = 0;
                    }
                    if ($dirtyValue) {
                        $logs['new' . $dirtyKey] = $dirtyValue;
                    } else {
                        $action = 2;
                    }
                }
                if(!empty($logs)) {
                    \Yii::$app->get('logger')->log($this->cont_id, json_encode($logs), $action);
                }
            }
        }


        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppartenirs()
    {
        return $this->hasMany(Appartenir::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntreprises()
    {
        return $this->hasMany(Entreprise::className(), ['entr_id' => 'entr_id'])->viaTable('appartenir', ['cont_id' => 'cont_id']);
    }
    public function getEvenements()
    {
        return $this->hasMany(Evenement::className(), ['evenement_id' => 'evenement_id'])->viaTable('participer', ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutre()
    {
        return $this->hasOne(Autre::className(), ['cont_id' => 'cont_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdresse()
    {
        return $this->hasOne(Adresse::className(), ['adr_id' => 'adr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducteur()
    {
        return $this->hasOne(Producteur::className(), ['cont_id' => 'cont_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContactdivers()
    {
        return $this->hasOne(Contactdivers::className(), ['cont_id' => 'cont_id']);
    }
    public function getConsommateur()
    {
        return $this->hasOne(Consommateur::className(), ['cont_id' => 'cont_id']);
    }
    public function getJournaliste()
    {
        return $this->hasOne(Journaliste::className(), ['cont_id' => 'cont_id']);
    }
    public function getAssocie()
    {
        return $this->hasOne(Associe::className(), ['cont_id' => 'cont_id']);
    }
    
    public function getMembrecollege()
    {
        return $this->hasOne(MembreCollege::className(), ['cont_id' => 'cont_id']);
    }
    public function getMembrecommission()
    {
        return $this->hasMany(MembreCommissionFilliere::className(), ['cont_id' => 'cont_id']);
    }
    public function getSecteursMembrecommission()
    {
        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable(MembreCommissionFilliere::tableName(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTravaillers()
    {
        return $this->hasMany(Travailler::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecteurs()
    {
        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable('travailler', ['cont_id' => 'cont_id']);
    }
    
    public function getNewsletters()
    {
        return $this->hasMany(Newsletter::className(), ['newsletter_id' => 'newsletter_id'])->viaTable('recevoir', ['cont_id' => 'cont_id']);
    }
    
    public function getSuivis() {
        return $this->hasMany(Suivi::className(), ['cont_id' => 'cont_id']);
    }
    
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['cont_id' => 'cont_id']);
    }
    
    public function getNature($option = null)
    {
        if($this->producteur) {
            if($option == 'label') {
                return 'Producteur';
            } else {
                return 'producteur';
            }
        }
        if($this->journaliste) {
            if($option == 'label') {
                return 'Journaliste';
            } else {
                return 'journaliste';
            }
        }
        if($this->contactdivers) {
             if($option == 'url') {
                 return 'contactdivers';
             } elseif ($option == 'label') {
                 return 'ContactDivers';
             } else {
                 return 'contact divers';
             }
        }
        if($this->consommateur) {
            if($option == 'label') {
                return 'Conso';
            } else {
                return 'consommateur';
            }
        }
        if($this->associe) {
            if($option == 'label') {
                return 'Associe';
            } else {
                return 'associe';
            }
        }
        return 'inconnu';
    }
    
    public function alreadyExists() 
    {
        if($this->find()->where(['cont_prenom' => $this->cont_prenom , 'cont_nom' => $this->cont_nom])->one())
                return true;
        return false;
    }
}
