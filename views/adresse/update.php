<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Adresse */

$this->title = 'Mettre à jour une adresse ';
$this->params['breadcrumbs'][] = ['label' => 'Adresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->adr_id, 'url' => ['view', 'id' => $model->adr_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="adresse-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'relatedModel' => $relatedModel
    ]) ?>

</div>
