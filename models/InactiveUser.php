<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "inactive_user".
 *
 * @property int $id
 * @property string $email
 * @property string $Nom
 * @property string $Prenom
 * @property string $GSM
 * @property string $Telephone
 */
class InactiveUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inactive_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email', 'Nom', 'Prenom'], 'required'],
            [['Nom', 'Prenom'], 'string', 'max' => 30],
            [['GSM', 'Telephone'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'Nom' => 'Nom',
            'Prenom' => 'Prenom',
            'GSM' => 'Gsm',
            'Telephone' => 'Telephone',
        ];
    }
    
    public function getfullName()
        {
                return $this->Nom.' '.$this->Prenom;
        }
}
