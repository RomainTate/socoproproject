<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//$suggest = yii\helpers\ArrayHelper::map(\app\models\Contactdivers::find()->select('cont_divers_type')
//        ->where(['not' , ['cont_divers_type' => null]])->all() , 'cont_divers_type', 'cont_divers_type');
//$suggest = array_values($suggest);  

//$suggestLieu = yii\helpers\ArrayHelper::map(\app\models\Lieuprisecontact::find()->select('lieu')
//        ->where(['not' , ['lieu' => null]])->all() , 'lieu', 'lieu');
//$suggestLieu = array_values($suggestLieu); 
/* @var $this yii\web\View */
/* @var $model app\models\ContactdiversSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contactdivers-search">

    <?php $form = ActiveForm::begin([
        'action' => ['search'],
        'method' => 'get',
    ]); ?>
    
    <?= $form->field($model, 'contactGSMPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Numéro GSM');?>
    <?= $form->field($model, 'contactEmailPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Adresse e-mail');?>

<?php /*    
    <?= $form->field($model, 'cont_divers_type')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => $suggest,
        ],
        'options' => [
            'class' => 'form-control'
        ],

    ]); ?>
*/?>    
    <?php // echo $form->field($model, 'contDiversLieuContactName')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
//        'clientOptions' => [
//            'source' => $suggestLieu,
//        ],
//        'options' => [
//            'class' => 'form-control'
//        ],
//    ]); ?>
    <?= $form->field($model, 'nomentrs')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Entreprise::find()->all(),'entr_societe' , 'entr_societe')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Sociétés')?>
    
    <?= $form->field($model, 'fcts')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Appartenir::find()->all(),'fonction' , 'fonction')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Fonctions')?>



    <div class="form-group">
        <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs(
    "$('.js-multiple').select2();",
    yii\web\View::POS_READY,
    'select2-prod'
); ?>