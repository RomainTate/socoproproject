<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

/**
 * Description of ParticiperController
 *
 * @author rom-PC
 */
class ParticiperController extends \yii\web\Controller{
    public static function addParticipation($cont_id, $evenement_id) {
        $participation = new \app\models\Participer;
        $participation->cont_id = $cont_id;
        $participation->evenement_id = $evenement_id;
        $participation->save();
    }
    public static function removeParticipation($cont_id, $evenement_id) {
        $participation = \app\models\Participer::find()->where(['cont_id' => $cont_id, 'evenement_id' => $evenement_id])->one();
        if($participation) {
            $participation->delete();
        }
    }
}
