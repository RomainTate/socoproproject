<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title = Yii::t('rbac', 'Ajouter un champ');
$this->params['breadcrumbs'][] = ['label' => 'Administration : champs sectoriels', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-warning alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h5><i class="icon fa fa-times"></i> Attention !</h5>
    <?= Yii::$app->session->getFlash('error') . '<br />' ?>
    </div>

<?php endif; ?>

<?php if ($sectID != 'all') : ?>
    <h1> Ajout d'un champ sectoriel pour le Secteur <?= app\models\Secteur::findOne($sectID)->sect_nom ?></h1>
<?php else :?>
    <h1> Ajout d'un champ sectoriel transversal</h1>
<?php endif;?>
    
<?php $form = yii\bootstrap\ActiveForm::begin(['action' => ['create', 'sectID' => $sectID], 'method' => 'post']) ?>

<?= $form->field($champSect, 'labelField');?>
<?= $form->field($champSect, 'type')->dropDownList(['1' => 'Vrai/Faux', '2' => 'Liste avec un seul choix possible', '3' => 'Texte libre', '4' => 'Nombre libre', '5' => 'Liste avec plusieurs choix possible']);?>
<div id="divNombreElements" style="display: none">
    <?= $form->field($champSect, 'nombreElements')->dropDownList(['1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9','10' => '10','11' => '11','12' => '12','13' => '13','14' => '14','15' => '15','16' => '16','17' => '17','18' => '18','19' => '19','20' => '20','21' => '21','22' => '22','23' => '23','24' => '24','25' => '25'], ['prompt' => 'Selectionner le nombre d\'élements']);?>
</div>
<div id='ListElem'> <!--- auto generated fields go here --->
</div>
<div id="errorElem" style="display: none; color: red;">
    Tous les éléments doivent être renseignés.
</div>
<div id="errorElemSyntaxe" style="display: none; color: red;">
    Vérifiez la syntaxe de vos champs.
</div>
<div id="errorElemNbElem" style="display: none; color: red;">
    Choisissez le nombre d'éléments à intégrer.
</div>
<?= \yii\helpers\Html::submitButton('Créer', ['id' => 'endFormButton', 'class' => 'btn btn-success']);?>
<?php yii\bootstrap\ActiveForm::end() ?>

<?php
$this->registerJsFile(\yii\helpers\Url::home().'js/champsSect.js', ['depends' => [yii\web\JqueryAsset::className()]]);