<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
use yii\bootstrap\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Secteur;
use app\components\WhResponsive;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProducteurSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$search = Yii::$app->request->pathInfo == 'producteur/search' ? true : false;
$this->title = $search ? 'Recherche globale Producteurs' : 'Producteurs' ;
$this->params['breadcrumbs'][] = $this->title;

$getToShow = [];
if(Yii::$app->request->get('show') != null) $getToShow = Yii::$app->request->get('show');
?>

<div class="producteur-index x_panel">
    <div id="gridHeader">
        <h1 class="x_title"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createProducteur')) : ?>
            <?= Html::a('Ajouter un Producteur', ['create'], ['class' => 'btn btn-success btn-add']) ?>
        <?php endif; ?>
    </div>

    <?php if ($search && Yii::$app->request->get('ProducteurSearch') == null) : ?>
        <?php echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php endif;

    if (!$search || Yii::$app->request->get('ProducteurSearch') != null) :
        ?>

        <?php   
        $gridColumns = [
//            [
//                'class' => 'kartik\grid\SerialColumn',
//                'hiddenFromExport' => true,
//            ],
            
            [
                'label' => 'Sigec',
                'attribute' => 'prod_sigec',
                'visible' => (Yii::$app->request->get('sigec') == '1'),
                'filter' => ["1" => "Oui", "0" => "Non"],
                'value' => function ($model) {
                    if ($model->prod_sigec == 1) {
                        return 'Oui';
                    } else {
                        return 'Non';
                    }
                }
                
            ],
            [
                'label' => 'num. Prod',
                'attribute' => 'prod_numero_producteur',
                'hidden' =>  (in_array('prod_numero_producteur', $getToShow) == false),
            ],
            [
                'label' => 'num. BCE',
                'attribute' => 'prod_numero_BCE',
                'hidden' =>  (in_array('prod_numero_BCE', $getToShow) == false),
            ],
            [
                'label' => 'Exploitation',
                'attribute' => 'prod_nom_exploitation', 
                'format' => 'raw'
            ],
//            [
//                'attribute' => 'contact.cont_civilite',
//                'label' => 'Civ',
//                'format' => 'text',
//            ],
            [
                'label' => 'Nom',
                'attribute' => 'contactNom', 
                'format' => 'raw'
//                'filterInputOptions' => [
//                    'class'       => 'form-control',
//                    'placeholder' => 'Recherche par Nom'
//                ],
            ],
                    [
                        'label' => 'Prenom',
            'attribute' => 'contactPrenom', 
                'format' => 'raw'
                        ],
            [
                'label' => 'Associés',
                'attribute' => 'associes' ,
                'format' => 'html',
                'width' => '150px',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesNames = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_nom || $associe->contact->cont_prenom) {
                        $associesNames[] = '<span style="display:block;"> -' .
                                ($associe->contact->cont_nom ? $associe->contact->cont_nom:'').
                                ' '. ($associe->contact->cont_prenom ? $associe->contact->cont_prenom :''). '</span>';
                        }
                    }
                    return implode("<br />", $associesNames);
                }
            ],
            [
                'label' => 'langue', 
                'attribute' => 'langue',
                'hidden' => true
            ],
            [
                'label' => 'Boite',
                'attribute' => 'rue',
                'format' => 'raw'
 //               'hidden' => true
            ],
            [
                'label' => 'Localité',
                'format' => 'raw',
                'attribute' => 'localite'
            ],
            [
                'label' => 'C.P.',
                'attribute' => 'codepostal', 
                'format' => 'raw'
 //               'hidden' => true
            ],
            [
                'label' => 'Province',
                'attribute' => 'province', 
                'format' => 'raw'
            ],
            [
                'label' => 'Tel',
            'attribute' => 'contactTelephonePrincipal',
                ],
            [
                'label' => 'Telephone Associés',
                'hidden' => true,
                'attribute' => 'associes.tel',
                'format' => 'html',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesTels = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_telephone_principal) {
                        $associesTels[] = '<span style="display:block;"> ' .
                                $associe->contact->cont_telephone_principal. '</span>';
                        }
                    }
                    return implode("; ", $associesTels);
                }
            ],
                    [
            'label' => 'GSM',
                        'attribute' => 'contactGSMPrincipal'
                        ],
            [
                'label' => 'GSM Associés',
                'hidden' => true,
                'attribute' => 'associes.gsm',
                'format' => 'html',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesGSMs = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_gsm_principal) {
                        $associesGSMs[] = '<span style="display:block;"> ' .
                                $associe->contact->cont_gsm_principal. '</span>';
                        }
                    }
                    return implode("; ", $associesGSMs);
                }
            ],
                    [
                        'label' => 'E-mail',
            'attribute' => 'contactEmailPrincipal', 
                'format' => 'email'
                        ],
            [
                'label' => 'Email Associés',
                'hidden' => true,
                'attribute' => 'associes.email',
                'format' => 'html',
                'value' => function($model) {
                if (empty($model->associes)) return null;
                    $associesEmails = array();
                    foreach ($model->associes as $associe) {
                        if($associe->contact->cont_email_principal) {
                        $associesEmails[] = '<span style="display:block;"> ' .
                                $associe->contact->cont_email_principal. '</span>';
                        }
                    }
                    return implode("<br />", $associesEmails);
                }
            ],
            [
                'label' => 'Membre College',
                'hidden' => true,
                'value' => function ($model) {
                if ($model->contact->membrecollege) {
                    return 'Oui';
                }
                return 'Non';
                }
            ],
            
            [
                'label' => 'statut juridique',
                'attribute' => 'prod_statut_juridique',
                'hidden' =>  (in_array('prod_statut_juridique', $getToShow) == false),
                'filter' => array("1"=>"SA","2"=>"SPRL","3"=>"SPRLS","4"=>"SCRL","5"=>"SNC","6"=>"ASBL"),
                'value' => function ($model) {
                    switch ($model->prod_statut_juridique) {
                                    case 1 : return 'SA';
                                    case 2 : return "SPRL";
                                    case 3 : return "SPRLS";
                                    case 4 : return "SCRL";
                                    case 5 : return "SNC";
                                    case 6 : return "ASBL";
                                    default : return null;
                                }
                }
            ],
            [
                'label' => 'statut pro',
                'attribute' => 'prod_statut_pro', 
                'format' => 'raw',
                'hidden' =>  (in_array('prod_statut_pro', $getToShow) == false),
                'filter' => array("1"=>"Actif", "0"=>"Retraité"),
                'value' => function ($model) {
                if ($model->prod_statut_pro === 1) {
                        return 'Actif';
                    } elseif ($model->prod_statut_pro === 0) {
                        return 'Retraité';
                    } else {
                        return 'Inconnu';
                    }
                }
            ],
            [
                'label' => 'Secteurs',
                'attribute' => 'secteursID',
                'format' => 'html',
                'filter' => ArrayHelper::map(Secteur::find()->asArray()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom'),
                'value' => function($model) {
                    $secteurNames = array();
                    $secteursArray = ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
                    asort($secteursArray);
                    foreach ($secteursArray as $secteur) {
                        $secteurNames[] = '<span style="display:block;">' . $secteur . '</span>';
                    }
                    return implode(", ", $secteurNames);
                }
            ],
            [ 
                'label' => 'debut activités',
                'attribute' => 'prod_date_debut_activite',
                'hidden' => true,
                'value' => function($model) {
                if ($model->prod_date_debut_activite) {
                    return Yii::$app->formatter->asDate($model->prod_date_debut_activite);
                }
                return 'Inconnue';
                }
            ],
            [
                'label' => 'Autres activités',
                'hidden' => true,
                'value' => function ($model) {
                if($model->activites) { 
                    return implode('; ', ArrayHelper::map($model->activites, 'activite_id', 'activite_nom'));
                    }
                return 'Aucune';
                }
            ]
        ];
        ?>
        <?php
        $allGridColumns = array_merge($gridColumns
//                ,$attributes
                );
        array_push($allGridColumns, ['class' => 'kartik\grid\ActionColumn','template' => '{view}{details}{delete}', 'hiddenFromExport' => true,
            'buttons' => [
                'view' => function($url, $model) {
                    $url = \Yii::$app->urlManager->createUrl(['producteur/view', 'id' => $model->cont_id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['title' => Yii::t('yii', 'View'), 'class' => 'action']);
                },
                'details' => function($url, $model) {
                    if(count($model->secteurs) == 1 && $model->secteurs[0]->sect_nom == 'Agriculture Bio') return null; // no details if only in Bio
                    $url = \Yii::$app->urlManager->createUrl(['producteur/details', 'id' => $model->cont_id]);
                    return Html::a('<span class="glyphicon glyphicon-tag text-info"></span>', $url, ['title' =>  'Details', 'class' => 'action']);
                },
//                'update' => function($url, $model) {
//                    if (!\Yii::$app->user->can('createProducteur')) {
//                        return null;
//                    }
//                    $url = \Yii::$app->urlManager->createUrl(['producteur/update', 'id' => $model->cont_id]);
//                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['title' => Yii::t('yii', 'Update')]);
//                },
                'delete' => function($url, $model) {
                    if (!\Yii::$app->user->can('deleteProducteur')) {
                        return null;
                    }
                    $url = \Yii::$app->urlManager->createUrl(['producteur/delete', 'id' => $model->cont_id]);
                    return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['class' => 'action', 'title' => Yii::t('yii', 'Delete'), 'data-confirm' =>  'Etes vous sûr de vouloir supprimer ce producteur?', 'data-method' => 'post']);
                }
            ]
        ])
        ?>

                <?php if (Yii::$app->session->hasFlash('success')): ?>
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php endif; ?>

                <?php if (Yii::$app->session->hasFlash('danger')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?= Yii::$app->session->getFlash('danger') ?>
                </div>
            <?php endif; ?>

    <div class="pull-right">
                <label>Exportation :</label>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $allGridColumns,
                    'target' => ExportMenu::TARGET_POPUP,
//                    'stream' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
//                        'encoding' => 'utf-8'
                    ],
                    'noExportColumns' => [count($allGridColumns) - 1]
                ]);
                ?>
    </div>
    
    <div class="pull-left">
            <?php ActiveForm::begin(['action' => \yii\helpers\Url::home() . Yii::$app->request->pathInfo, 'method' => 'get', 'id' => 'displayFieldsForm']) ?>
            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#displayFields">Options d'affichage</button>
            <div id="displayFields" class="collapse">
                <?= Html::checkboxList('show', Yii::$app->request->get('show'), \yii\helpers\ArrayHelper::map($attributes, 'attribute', 'label')) ?>
                <p> Afficher par :
                    <?= Html::dropDownList('selectedPagination', Yii::$app->request->get('selectedPagination') ? Yii::$app->request->get('selectedPagination') : null, ['20' => 20 , '30' => 30, '50' => 50, '75' => 75 , '100' => 100]);?>
                </p>
                <?php
                foreach (Yii::$app->request->get() as $key => $value) {
                    if ($key != 'show' && $key != 'selectedPagination') {
                        if (is_array($value)) {
                            foreach ($value as $k => $val) {
                                if (is_array($val)) {
                                    foreach ($val as $v) {
                                        echo yii\helpers\Html::hiddenInput($key . "[$k][]", $v);
                                    }
                                } else {
                                    echo yii\helpers\Html::hiddenInput($key . "[$k]", $val);
                                }
                            }
                        } else {
                            echo yii\helpers\Html::hiddenInput($key, $value);
                        }
                    }
                }
                ?>
            <?= Html::input('submit', null, null, ['class' => 'btn btn-success btn-sm']) ?>
            </div>
            <?php ActiveForm::end(); ?>
            </div>
    <div class="clear"></div>
    <hr />
        <?php
    if (!$search) {
        $generealForm = \yii\widgets\ActiveForm::begin([
            'id' => 'generalSearch',
            'method' => 'GET',
            'action' => \yii\helpers\Url::home() . Yii::$app->request->pathInfo
        ]);?>
        <?= $generealForm->field($searchModel, 'generalSearch', [ 'options' => ['id' => 'generalSearchInput'] ])
                ->label('Recherche générale')->textInput(['placeholder' => "Rechercher par Nom, Prénom, Téléphone, Adresse, ..."])?>
        <?php \yii\widgets\ActiveForm::end();
    }
    ?>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'prodGrid',
                'filterModel' => $search? null:$searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
                'columns' => $allGridColumns,
//                'responsive' => false,
                'responsiveWrap' => false,
                'pager' => [
                    'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
                    'prevPageLabel' => 'Précédent',   // Set the label for the "previous" page button
                    'nextPageLabel' => 'Suivant',   // Set the label for the "next" page button
                    'firstPageLabel'=>'Début',   // Set the label for the "first" page button
                    'lastPageLabel'=>'Fin',    // Set the label for the "last" page button
                    'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
                    'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
                    'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
                    'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
                    'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
            ],      
                'rowOptions' => function ($model) {
                    if ($model->prod_sigec == '1') {
                        return ['class' => 'sigecGrid'];
            }
        },
            ]);
            ?>

    <?php if(!$search && Yii::$app->request->get('sigec') != '1') echo Html::a('Inclure les producteurs confidentiels', \yii\helpers\Url::to(\yii\helpers\Url::current(['sigec' => 1])),['class' => 'btn btn-danger']) ?>
    <?php if(!$search && Yii::$app->request->get('sigec') == '1') echo Html::a('Cacher les producteurs confidentiels', \yii\helpers\Url::to(\yii\helpers\Url::current(['sigec' => 0])),['class' => 'btn btn-danger']);?>
<?php endif; ?>

</div>
<?php
if (!$search || Yii::$app->request->get('ProducteurSearch') != null) :
    $responsiveCSS = WhResponsive::writeResponsiveCss($gridColumns, 'prodGrid');
    $this->registerCss($responsiveCSS);
endif;

$this->registerJsFile(\yii\helpers\Url::home().'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(\yii\helpers\Url::home().'js/generalSearch.js', ['depends' => [yii\web\JqueryAsset::className()]]);
//$this->registerJs("$(window).on('load', function() {  $('ul:first', $('#sidebar-menu').find('li.active')).slideUp(); console.log('ok');   });", $this::POS_HEAD, 'testjs');