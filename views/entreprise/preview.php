<?=
\yii\widgets\DetailView::widget([
    'model' => $entreprise,
    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
    'attributes' => [
        [
          'label' => 'Fonction du contact',
            'format' => 'raw',
          'value' => function ($entreprise) use ($model) {
            $fct = \app\models\Appartenir::find()->where(['entr_id' => $entreprise->entr_id, 'cont_id' => $model->cont_id])->one()->fonction;
            if(!$fct) $fct = 'inconnue';
            if (!Yii::$app->user->can('create'.$model->contact->getNature('label'))) {
                return $fct;
            }
            return $fct . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/changefonction', 'cont_id' => $model->cont_id , 'entr_id' => $entreprise->entr_id]),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
          }  
        ],
        'entr_telephone',
                [
                    'attribute' => 'entr_email',
                    'format' => 'html',
                    'value' => function ($model) {
                        if(!$model->entr_email) return null;
                        return \yii\helpers\Html::a(yii\helpers\BaseStringHelper::truncate($model->entr_email, 40, '...'), 'mailto:'.$model->entr_email) ;
                    }
                ],
//                                                'entr_site_internet',
        'entr_domaine_activite',
        [
            'label' => 'Adresse',
            'format' => 'raw',
            'value' => function ($entreprise) {
                if (!$entreprise->adresse) {
                    if (!Yii::$app->user->can('createEntreprise')) {
                        return 'adresse inconnue';
                    }
                    return 'adresse inconnue' . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/entreprise/addadresse?id=' . $entreprise->entr_id),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                }
                //else
                $adr = "<ul>";
                foreach ($entreprise->adresse->getAttributes(null, ['adr_id']) as $attr => $value) {
                    $adr .= "<li>" . $entreprise->adresse->getAttributeLabel($attr) . ": ";
                    $adr .= $value ? $value : 'inconnu';
                    $adr .= "</li>";
                }
                $adr .= "</ul>";
                if (!Yii::$app->user->can('createEntreprise')) {
                    return $adr;
                }
                //else
                return $adr
                        . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/adresse/update?id=' . $entreprise->adr_id),
                            'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
            }
        ],
    ],
]);
?>