<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
use kartik\grid\GridView;
use app\models\Secteur;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contact-index">
    <div class="x_panel">
        <div class="x_title">
            <h3><?= Html::encode($this->title) ?></h3>
        </div>
        <div class="x_content">
    <?php $gridColumns =[
//            'cont_id',
//            ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'cont_civilite',
                    'label' => 'Civ',
                ],
                [
                    'attribute' => 'cont_nom',
                    'label' => 'Nom',
                ],
                [
                    'attribute' => 'cont_prenom',
                    'label' => 'Prénom',
                ],
                [
                    'attribute' => 'cont_telephone_principal',
                    'label' => 'Téléphone',
                ],
                [
                    'attribute' => 'cont_gsm_principal',
                    'label' => 'GSM',
                ],
                [
                    'attribute' => 'cont_email_principal',
                    'label' => 'E-mail',
                ],
                [
                    'attribute' => '_membrecollege',
                    'label' => 'Membre du collège',
                    'filter' => ['0' => 'Non', '1' => 'Oui'],
                    'value' => function($model) {
                        if ($model->membrecollege) {
                            return 'oui';
                        }
                        return 'non';
                    }
                ],
                [
                    'attribute' => 'nature',
                    'label' => 'Type contact',
                ],
                [
                    'attribute' => 'membrefiliere',
                    'label' => 'Membre Commission Filière',
                    'filter' => ArrayHelper::map(Secteur::find()->orderBy('sect_nom')->asArray()->all(), 'sect_id', 'sect_nom'),
                    'format' => 'html',
                    'value' => function ($model) {
                        if (!$model->secteursMembrecommission)
                            return 'Non';
                        foreach ($model->secteursMembrecommission as $filiere) {
                            $filieresNames[] = '<span style="display:block;"> -' . $filiere->sect_nom . '</span>';
                        }
                        return implode("<br/> ", $filieresNames);
                    }
                ],
                [
                    'label' => 'Secteurs',
                    'attribute' => '_secteurs',
                    'format' => 'html',
                    'filter' => ArrayHelper::map(Secteur::find()->asArray()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom'),
                    'value' => function($model) {
                        $secteurNames = array();
                        $secteursArray = ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
                        asort($secteursArray);
                        foreach ($secteursArray as $secteur) {
                            $secteurNames[] = '<span style="display:block;">' . $secteur . '</span>';
                        }
                        return implode(", ", $secteurNames);
                    }
                ],
            ];?>

            <div class="pull-right">
                <label>Exportation :</label>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'target' => ExportMenu::TARGET_POPUP,
//                    'stream' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
//                        'encoding' => 'utf-8'
                    ],
//                    'noExportColumns' => [count($allGridColumns) - 1]
                ]);
                ?>
            </div>
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'membresGrid',
            'filterModel' => $searchModel,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
//        'rowOptions' => function ($model) {
//            if ($model->nature == "Producteur") {
//                return ['class' => 'danger'];
//            }
//        },
            'columns' => $gridColumns,
            'responsiveWrap' => false
        ]);
        ?>
        </div>
    </div>
</div>
<?php
$responsiveCSS = \app\components\WhResponsive::writeResponsiveCss($gridColumns, 'membresGrid');
$this->registerCss($responsiveCSS);
$this->registerJsFile(\yii\helpers\Url::home() . 'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);
