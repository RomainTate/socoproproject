<?php

namespace app\controllers;

use Yii;
use app\models\Associe;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AssocieController implements the CRUD actions for Associe model.
 */
class AssocieController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'update', 'delete','create'],
                        'roles' => ['@'],
                    ],
                ]
            ]
        ];
    }

    /**
     * Lists all Associe models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Associe::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Associe model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Associe model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($prod_id)
    {
        $model = new Associe();
        $contactModel = new \app\models\Contact(['scenario' => 'typeAssocie']);
        $post = Yii::$app->request->post();
        
        if ($contactModel->load($post) && $model->load($post)) {
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
                $model->cont_id_prod = $prod_id;
                if ($model->save(false))
                    { // save(false) bypass validation (already did 2 lines ago)
                    return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
                } 
            } else {
                throw new \yii\base\ErrorException;
            }
        } 
            return $this->renderAjax('create', [
                'model' => $model,
                'contactModel' => $contactModel
            ]);
    }

    /**
     * Updates an existing Associe model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {//TODO : log
        $model = $this->findModel($id);
        $contactModel = $model->contact;
        $contactModel->setScenario('typeAssocie');
        $post = Yii::$app->request->post();
        
        if ($contactModel->load($post) && $model->load($post)) {
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                if ($model->save(false)){ // save(false) bypass validation (already did 2 lines ago)
                    return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
                } 
            } else {
                throw new \yii\base\ErrorException;
            }
        }
        
        return $this->renderAjax('update', [
            'model' => $model,
            'contactModel' => $contactModel
        ]);
    }
    

    /**
     * Deletes an existing Associe model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
    }

    /**
     * Finds the Associe model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Associe the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Associe::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
