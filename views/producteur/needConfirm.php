<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $errors string */

$this->title = 'Ajouter un producteur';
$this->params['breadcrumbs'][] = ['label' => 'Producteurs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="producteur-needconfirm">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="col-sm-6">
            <?= Yii::$app->session->getFlash('error')?>
            Ce producteur est maintenant en attente et est accessible par les administrateurs.
        </div>
    </div>
</div>