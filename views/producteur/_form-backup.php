<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Producteur */
/* @var $contactModel app\models\Contact */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="producteur-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($contactModel, 'cont_civilite')->textInput(['maxlength' => true]) ?>

    <?= $form->field($contactModel, 'cont_prenom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($contactModel, 'cont_nom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'secteurs')
        ->dropDownList(ArrayHelper::map(\app\models\Secteur::find()->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple']) 
    ?>


    <div id="producteur-other-infos" style="display: none">
        <?= $form->field($contactModel, 'cont_gsm_principal')->textInput(['maxlength' => true]) ?>
        <?= $form->field($contactModel, 'cont_email_principal')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'prod_activite_principale')->dropDownList(["1"=>"Principale","0"=>"Complémentaire"],['prompt' => '']);?>
        <?php
        foreach ($model->getAttributes(null, ['cont_id', 'prod_activite_principale']) as $key => $value) {
        echo $form->field($model, $key);
        }
        ?>
    </div>

    <?= Html::button('More infos', ['id' => 'showInfos', 'class' => 'btn btn-info']) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJsFile('js/test.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?>
<?php
$this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');?>