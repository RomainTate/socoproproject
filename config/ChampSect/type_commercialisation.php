<?php

    return [
    'label' => 'Type de Commercialisation',
    'type' => 2,
    'value' =>[ 'Circuit court', 'Magasin à la ferme', 'Marché', 'GMS', 'Exportation']
    ];