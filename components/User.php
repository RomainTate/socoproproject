<?php


namespace app\components;

class User extends \yii\web\User
{
    public function getUser()
    {
        $id = $this->getId();
        return \app\models\User::findOne($id);
    }
    
    public function canSee($cont_id) {
        if ($this->getUser()->getIsAdmin()) return true;
        
        $role = \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()]);
        if($role->item_name == 'Communication') {
            return true;
        }
        
        if($role->item_name == 'Chargé de mission') {
            if(\app\models\Producteur::find()->joinWith(['secteurs.gerers' => function ($q) {
                    $q->where('gerer.user_id =' .\Yii::$app->user->getId() );
                }])->where(['producteur.cont_id' => $cont_id])->one())
                            return true;
        }
        
        return false;
    }
    
    public function getSecteurs() {
        $role = \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()]);
        if($role->item_name == 'Chargé de mission') {
            $secteurs = \yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->joinWith(['gerers' => function ($q) {
                    $q->where(['gerer.user_id' => \Yii::$app->user->getId()] );
                }])->all(), 'sect_id' , 'sect_nom');
        } else {
            $secteurs = \yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom');
        }
        return $secteurs;
    }
    
//    $secteurshandled = ArrayHelper::map(Gerer::find()->select('sect_id')->where(['user_id' => Yii::$app->user->getId()])->all(), 'sect_id', 'sect_id');

}