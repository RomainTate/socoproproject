<?php
use yii\helpers\Html;
use yii\helpers\Url;
$this->title = 'CollegeDB';
?>
<?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success">
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
<?php $home = \yii\helpers\Url::home();
echo "<script>setTimeout(\"location.href = '".$home."';\",4000);</script>";?>
<?php else : ?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Bienvenue!</h1>
        <p class="lead">Bienvenue sur l'application du collège.</p>
    </div>
    <div class="row">
        <div class="col-md-6 col-xs-12">
            <div class="x_panel">
                <img class="pull-left" src="css/images/farmer.png" alt="producteur" />
                <h2>Producteur</h2>
                <?= Html::a('Ajouter un producteur', Url::to(['/producteur/create']), ['class' => 'btn btn-success' , 'style' => 'margin-left: 15px']) ?>
            </div>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="x_panel">
                <img class="pull-left" src="css/images/basket.png" alt="consommateur" />
                <h2>Consommateur</h2>
                <?= Html::a('Ajouter un consommateur', Url::to(['/consommateur/create']) , ['class' => 'btn btn-success', 'style' => 'margin-left: 15px']) ?>
            </div>
        </div>
    </div>
</div>
<?php endif;?>