$("#ListElem").on("change paste keyup","input[name='DynamElemField[]']", function() {
    $("#errorElem").hide();
});

$('input').on("change paste keyup", function () {
    $("#errorElemSyntaxe").hide();
});

$('#champsectorielcle-nombreelements').change(function (e) {
    $("#errorElemNbElem").hide();
    var nb = $('#champsectorielcle-nombreelements').val();
        $('#ListElem').empty();
        for (i = 1; i <= nb; i++) {
            $('#ListElem').append('<label class="control-label">élement '+i+'</label> <div> <input type="text" class="form-control" name="DynamElemField[]" /> </div> ');
    }
});

$("form").submit(function(e){
    $('input').each(function(index,data) {
        var value = $(this).val();
        if($(this).attr('name') != '_csrf') {
           if(value.match(/[",]/)) {
                $("#errorElemSyntaxe").show();
                e.preventDefault();
            }
        }
    });
    
    if($('#champsectorielcle-type').val() == '2' || $('#champsectorielcle-type').val() == '5') {
        if ($("#champsectorielcle-nombreelements :selected").val() == ''){
             $("#errorElemNbElem").show();
        }
        for (var i=0; i < $("input[name='DynamElemField[]']").length; i++) {
            if($("input[name='DynamElemField[]']")[i].value == '') {
                $("#errorElem").show();
                e.preventDefault();
            } 
        }
    }
});

$('#champsectorielcle-type').change(function(){
    if((this).value == '2' || (this).value == '5') {
        $('#divNombreElements').show();
        $("#ListElem").show();
    } else {
        $('#divNombreElements').hide();
        $("#ListElem").hide();
    }
});

$(function() {
    $('#champsectorielcle-type').trigger( "change" );
    $('#champsectorielcle-nombreelements').trigger( "change" );
});