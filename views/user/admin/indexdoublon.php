<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use app\models\Secteur;
use kartik\grid\GridView;

$this->title = 'Administration : gestion des doublons';
$this->params['breadcrumbs'][] = $this->title;?>

<div class="x_panel">
    <div class="x_title">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <ul id="w0" class="nav-tabs nav" style="margin-bottom: 15px">
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/index">Utilisateurs</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>rbac/role/index">Rôles</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>champsect/">Champs sectoriels</a></li>
        <li class="active"><a href="<?= \yii\helpers\Url::home()?>user/admin/indexdoublon">Gestion doublons</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexnewsletters">Newsletters</a></li>
        <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foires <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireprod">Prod</a></li>
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireconso">Conso</a></li>
            </ul>
        </li>
    </ul>

        <?php $gridColumns = [

            [
                'label' => 'num. Prod',
                'attribute' => 'prod_numero_producteur',
            ],
            [
                'label' => 'num. BCE',
                'attribute' => 'prod_numero_BCE',
            ],
            [
                'label' => 'Exploitation',
                'attribute' => 'prod_nom_exploitation', 
                'format' => 'raw'
            ],

            [
                'label' => 'Nom',
                'attribute' => 'contactNom', 
                'format' => 'raw'
            ],
            [
                'label' => 'Prenom',
                'attribute' => 'contactPrenom', 
                'format' => 'raw'
            ],
            [
                'label' => 'Boite',
                'attribute' => 'rue',
                'format' => 'raw'
            ],
            [
                'label' => 'Localité',
                'format' => 'raw',
                'attribute' => 'localite'
            ],
            [
                'label' => 'C.P.',
                'attribute' => 'codepostal', 
                'format' => 'raw'
            ],
            [
                'label' => 'Province',
                'attribute' => 'province', 
                'format' => 'raw'
            ],
            [
                'label' => 'Tel',
                'attribute' => 'contactTelephonePrincipal',
            ],
            [
                'label' => 'GSM',
                'attribute' => 'contactGSMPrincipal'
            ],

            [
                'label' => 'E-mail',
                'attribute' => 'contactEmailPrincipal', 
                'format' => 'email'
            ],
            [
                'label' => 'Membre College',
                'hidden' => true,
                'value' => function ($model) {
                if ($model->contact->membrecollege) {
                    return 'Oui';
                }
                return 'Non';
                }
            ],
            [
                'label' => 'statut juridique',
                'attribute' => 'prod_statut_juridique',
                'filter' => array("1"=>"SA","2"=>"SPRL","3"=>"SPRLS","4"=>"SCRL","5"=>"SNC","6"=>"ASBL"),
                'value' => function ($model) {
                    switch ($model->prod_statut_juridique) {
                        case 1 : return 'SA';
                        case 2 : return "SPRL";
                        case 3 : return "SPRLS";
                        case 4 : return "SCRL";
                        case 5 : return "SNC";
                        case 6 : return "ASBL";
                        default : return null;
                    }
                }
            ],
            [
                'label' => 'statut pro',
                'attribute' => 'prod_statut_pro', 
                'format' => 'raw',
                'filter' => array("1"=>"Actif", "0"=>"Retraité"),
                'value' => function ($model) {
                if ($model->prod_statut_pro === 1) {
                        return 'Actif';
                    } elseif ($model->prod_statut_pro === 0) {
                        return 'Retraité';
                    } else {
                        return 'Inconnu';
                    }
                }
            ],
            [
                'label' => 'Secteurs',
                'attribute' => 'secteursID',
                'format' => 'html',
                'filter' => ArrayHelper::map(Secteur::find()->asArray()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom'),
                'value' => function($model) {
                    $secteurNames = array();
                    $secteursArray = ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
                    asort($secteursArray);
                    foreach ($secteursArray as $secteur) {
                        $secteurNames[] = '<span style="display:block;">' . $secteur . '</span>';
                    }
                    return implode(", ", $secteurNames);
                }
            ],
            [ 
                'label' => 'debut activités',
                'attribute' => 'prod_date_debut_activite',
                'hidden' => true,
                'value' => function($model) {
                if ($model->prod_date_debut_activite) {
                    return Yii::$app->formatter->asDate($model->prod_date_debut_activite);
                }
                return 'Inconnue';
                }
            ],
            [
                'label' => 'Autres activités',
                'hidden' => true,
                'value' => function ($model) {
                if($model->activites) { 
                    return implode('; ', ArrayHelper::map($model->activites, 'activite_id', 'activite_nom'));
                    }
                return 'Aucune';
                }
            ]
        ];
        array_push($gridColumns, ['class' => 'kartik\grid\ActionColumn','template' => '{view}{details}{delete}', 'hiddenFromExport' => true,
            'buttons' => [
                'view' => function($url, $model) {
                    $url = \Yii::$app->urlManager->createUrl(['producteur/view', 'id' => $model->cont_id]);
                    return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['title' => Yii::t('yii', 'View'), 'class' => 'action']);
                },
                'delete' => function($url, $model) {
                    if (!\Yii::$app->user->can('deleteProducteur')) {
                        return null;
                    }
                    $url = \Yii::$app->urlManager->createUrl(['producteur/delete', 'id' => $model->cont_id]);
                    return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['class' => 'action', 'title' => Yii::t('yii', 'Delete'), 'data-confirm' =>  'Etes vous sûr de vouloir supprimer ce producteur (doublon) ?', 'data-method' => 'post']);
                }
            ]
        ])
        ?>
    <div class="clear"></div>
            <?=
            GridView::widget([
                'dataProvider' => $dataProvider,
                'id' => 'prodGrid',
                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
                'columns' => $gridColumns,
                'responsive' => false,
                'responsiveWrap' => false,
                'pager' => [
                    'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
                    'prevPageLabel' => 'Précédent',   // Set the label for the "previous" page button
                    'nextPageLabel' => 'Suivant',   // Set the label for the "next" page button
                    'firstPageLabel'=>'Début',   // Set the label for the "first" page button
                    'lastPageLabel'=>'Fin',    // Set the label for the "last" page button
                    'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
                    'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
                    'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
                    'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
                    'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
                ],      
            ]);
            ?>
</div>

<?php $this->registerJsFile(\yii\helpers\Url::home().'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);