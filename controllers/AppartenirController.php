<?php

namespace app\controllers;

class AppartenirController extends \yii\web\Controller {

    public static function addContactEntreprise($cont_id, $entr_id, $fonction) {
        $contactEntreprise = new \app\models\Appartenir();
        $contactEntreprise->cont_id = $cont_id;
        $contactEntreprise->entr_id = $entr_id;
        $contactEntreprise->fonction = $fonction;
        if ($contactEntreprise->save()) {
            return true;
        }
        return false;
    }
    
    public static function deleteContactEntreprise($cont_id, $entr_id) {
        $contactEntreprise = \app\models\Appartenir::findOne(['cont_id' => $cont_id , 'entr_id' => $entr_id]);
        if (!$contactEntreprise)  {
            return true;
        }
        if ($contactEntreprise->delete()) {
            return true;
        }
        return false;
    }

    public static function changeFonction($cont_id, $entr_id, $fonction) {
         $contactEntreprise = \app\models\Appartenir::findOne(['cont_id' => $cont_id , 'entr_id' => $entr_id]);
         $contactEntreprise->fonction = $fonction;
         $contactEntreprise->save();
         return true;
    }
}
