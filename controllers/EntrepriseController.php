<?php

namespace app\controllers;

use Yii;
use app\models\Entreprise;
use app\models\EntrepriseSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EntrepriseController implements the CRUD actions for Entreprise model.
 */
class EntrepriseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search', 'view'],
                        'roles' => ['indexEntreprise'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'addmoreinfos' , 'addadresse', 'quickupdate'],
                        'roles' => ['createEntreprise'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteEntreprise'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Entreprise models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EntrepriseSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Entreprise model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Entreprise model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    { 
        $model = new Entreprise();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            if(isset($post['contactToAdd'])) { //if user adds Entreprise from contact view
                $fonction = isset($post['fonctionContact']) ? $post['fonctionContact'] : null;
                ContactController::addContactToEntr($post['contactToAdd'], $model->entr_id, $fonction);
                $natureContact = \app\models\Contact::findOne($post['contactToAdd'])->getNature('url');
                return $this->redirect([$natureContact.'/view', 'id' => $post['contactToAdd']]);
            }
            return $this->redirect(['view', 'id' => $model->entr_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Entreprise model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return \yii\widgets\ActiveForm::validate($model);
        }
        $post = Yii::$app->request->post();
        
        if ($model->load($post) && $model->save()) {
                    if (isset($post['ajax']) && $post['ajax'] == 1) {
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                    return $this->redirect(['view', 'id' => $model->entr_id]);
        } 
        
        if (!Yii::$app->request->isAjax) {
            return $this->render('update', [
                    'model' => $model,
            ]);
        }
        return $this->renderAjax('update', [
                'model' => $model,
        ]);
    }
    
    public function actionQuickupdate($id, $attribute) {
        $model = $this->findModel($id);
        $type = $model->getTableSchema()->getColumn($attribute)->type;
        $post = Yii::$app->request->post();
        if ($post) {
            if ($type =='date') {
                $model->$attribute = date("Y-m-d", strtotime($post['Entreprise'][$attribute]));
            } else {
                $model->$attribute = $post['Entreprise'][$attribute];
            }
            $model->save();
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        } else {
            return $this->renderAjax('quickform/updateform', [
                        'model' => $model,
                        'attribute' => $attribute,
                        'type' => $type
            ]);
        }
    }
    
    public function actionAddmoreinfos ($entr_id) {
        $post = Yii::$app->request->post();
        $model = $this->findModel($entr_id);
        if($model->load($post)) {
            if($model->save()) {
                return $this->redirect(Yii::$app->request->referrer);
            }
        } else {
            return $this->renderAjax('_formInfos', [
                        'model' => $model,
            ]);
        }
    }
    /**
     * Deletes an existing Entreprise model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->request->referrer && (strpos(Yii::$app->request->referrer, 'index') !== false)) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Entreprise model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Entreprise the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Entreprise::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAddadresse($id) {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();
        if ($post) {
            $model->adr_id = AdresseController::CreateAdresse($post);
            $model->save();
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        }
        return $this->renderAjax('/adresse/create', [
            'model' => new \app\models\Adresse(),
            'relatedModel' => $model
            ]);
    }
    
    public static function createEntreprise($nom) {
        $model = New \app\models\Entreprise();
        $model->entr_societe = $nom;
        $model->save();
        return $model->entr_id;
    }
}
