<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Evenement */

$this->title = $model->evenement_nom;
$this->params['breadcrumbs'][] = ['label' => 'Evenements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evenement-view">

    <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>

    <div class="pull-right">

        <?=
        Html::a('Supprimer', ['delete', 'id' => $model->evenement_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Êtes-vous sûr de vouloir supprimer cet élément ?',
                'method' => 'post',
            ],
        ])
        ?>
    </div>
    
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="pull-left">
                    <ul>
                        <li>
                            date de l'évènement : <?= Yii::$app->formatter->asDate($model->evenement_date) ?>
                        </li>
                        <li>
                            Type de l'évènement : 
                            <?php switch ($model->evenement_type) {
                                    case 1 : echo 'Assemblée sectorielle';
                                        break;
                                    case 2 : echo "Assemblée générale";
                                        break;
                                    case 3 : echo "Réunion Collège";
                                        break;
                                    case 4 : echo "Voyage / Colloque";
                                        break;
                                    case 5 : echo "Autre";
                                        break;
                                    default : echo "Inconnu";
                                }?>
                        </li>
                    </ul>
                </div>
                <div class="pull-right">
                    <?= Html::a('Modifier', ['update', 'id' => $model->evenement_id], ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>
    <?php if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-danger alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?= Yii::$app->session->getFlash('error') ?>
    </div>
    <?php endif; ?>
    <div class="row">
        <div class="col-sm-12 col-xs-12">
            <div class="x_panel">
                    <h2 class="x_title">Importer un fichier de présence</h2>
                <?php    \kartik\form\ActiveForm::begin([
                    'action' => ['importfile', 'evenement_id' => $model->evenement_id],
                        'options' => [
                            'enctype' => 'multipart/form-data'
                        ]
                ]) ?>
                <?=  kartik\file\FileInput::widget([
                    'name' => 'uploadedFile',
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false
                        ]
                ]);?>
                    <?= Html::submitButton('envoyer', ['class' => 'btn btn-success'])?>
                    <?php    \kartik\form\ActiveForm::end() ?>
            </div>
        </div>
        <div class="col-sm-12 col-xs-12">
            <div class="x_panel">
                <h2>
                    Contacts présents (<span id='nbContact'><?=count($model->contacts)?></span>)
                </h2>
                <ul id="listeContactsPresents" class="list-group">
                        <?php if ($model->contacts == null) :?>
                        <li id="noContact"> Aucun contact</li>
                        <?php else : ?>
                        <?php foreach ($model->contacts as $contact) : ?>
                        <li class="list-group-item"> 
                            <a title="Retirer" style="color: red" value="<?= Yii::$app->urlManager->createUrl( ['evenement/removecontactajax', 'cont_id' => $contact->cont_id, 'evenement_id' => Yii::$app->request->get('id')] ) ?>" class="removeProd" contID= "<?=$contact->cont_id?>" homeUrl="<?=\yii\helpers\Url::home()?>">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </a>
                            <a href="<?=\yii\helpers\Url::home().'producteur/view?id='.$contact->cont_id?>">
                                <?php if ($contact->cont_nom != null && $contact->cont_prenom != null) {
                                    echo $contact->cont_nom . ' ' . $contact->cont_prenom;
                                } else {
                                    echo 'producteur inconnu';
                                }
                                if ($contact->producteur->prod_nom_exploitation != null) {
                                    echo ' ('.$contact->producteur->prod_nom_exploitation.')';
                                }?>
                            </a>
                            
                        </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif ?>
            </div>
            <div class="x_panel">
                        <h2>Ajouter des producteurs</h2>
                        <?php
//                        yii\widgets\Pjax::begin(['id' => 'gridProd']);
                        echo kartik\grid\GridView::widget([
                            'dataProvider' => $dataProviderProducteurs,
                            'filterModel' => $searchModelProd,
                            'id' => 'eventProdGrid',
                            'pjax' => true,
                            'pjaxSettings' =>[
                            'neverTimeout'=>true,
                            'options'=>[
                                    'id'=>'pjax_id',
                                ]
                            ],
                            'columns' => [
                                'contactNom',
                                'contactPrenom',
                                'contact.adresse.adr_ville',
                                'contact.adresse.adr_province',
                                'contactGSMPrincipal',
                                'contactEmailPrincipal',
                                [
                                    'label' => 'Secteurs',
                                    'attribute' => 'secteursID',
                                    'format' => 'html',
                                    'filter' => \yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->asArray()->all(), 'sect_id', 'sect_nom'),
                                    'value' => function($model) {
                                        $secteurNames = array();
                                        foreach ($model->secteurs as $secteur) {
                                            $secteurNames[] = '<span style="display:block;">' . $secteur->sect_nom . '</span>';
                                        }
                                        return implode(",", $secteurNames);
                                    }
                                ],
                                [
                                    'class' => 'kartik\grid\ActionColumn',
                                    'template' => '{ajouter}',
                                    'buttons' => [
                                        'ajouter' => function($url, $model) use ($contactsPresents) {
                                            if (array_key_exists($model->cont_id, $contactsPresents)) {
                                                $url = Yii::$app->urlManager->createUrl(['evenement/removecontactajax', 'cont_id' => $model->cont_id, 'evenement_id' => Yii::$app->request->get('id')
                                                    , 'tab' => 'Producteurs']);
                                                return Html::a('<span class="btn btn-danger glyphicon glyphicon-minus text-info"></span>',null, ['title' => 'Retirer', 'value' => $url, 'class' => 'removeProd', 'contID' => $model->cont_id, 'homeUrl' => \yii\helpers\Url::home()]);
//                                                return  '<span class="btn btn-danger glyphicon glyphicon-minus text-info"></span>';
                                            }
                                            $url = \Yii::$app->urlManager->createUrl(['evenement/addcontactajax', 'cont_id' => $model->cont_id, 'evenement_id' => Yii::$app->request->get('id')
                                                    , 'tab' => 'Producteurs'
                                                ]);
                                            return Html::a('<span class="btn btn-success glyphicon glyphicon-plus text-info"></span>',null, ['title' => 'Ajouter', 'value' => $url, 'class' => 'addProd', 'contID' => $model->cont_id, 'homeUrl' => \yii\helpers\Url::home()]);
                                        }
                                    ]
                                ],
                            ]
                        ]);
//                        yii\widgets\Pjax::end();
                        ?>
                </div>
            </div>
        </div>
    </div>
<?php
$this->registerJsFile(\yii\helpers\Url::home().'js/evenementview.js', ['depends' => [yii\web\JqueryAsset::className()]]);