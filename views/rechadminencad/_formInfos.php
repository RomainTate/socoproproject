<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Contactdivers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conso-infos-form">

    <?php $form = ActiveForm::begin([
        'id' => 'reaInfosForm'
    ]); ?>


    <?= $form->field($model, 'secteurs')
        ->dropDownList(yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Intérêts') 
    ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');?>