<?php
 
/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */
namespace app\modules\user\models;
 
use dektrium\user\models\SettingsForm as BaseModel;
use dektrium\user\Mailer;
/**
 * SettingsForm gets user's username, email and password and changes them.
 *
 * @property User $user
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class SettingsForm extends BaseModel
{
    /** @var string */
    public $GSM;
    public $Nom;
    public $Prenom;
    public $Telephone;
    public function __construct(Mailer $mailer, $config = [])
    {
        $this->mailer = $mailer;
        $this->setAttributes(['GSM' => $this->user->GSM], false);
        $this->setAttributes(['Nom' => $this->user->Nom], false);
        $this->setAttributes(['Prenom' => $this->user->Prenom], false);
        $this->setAttributes(['Telephone' => $this->user->Telephone], false);
        parent::__construct($mailer, $config);
    }
    /** @inheritdoc */
    public function rules()
    {
        $rules = parent::rules();
        //        var_dump($rules);die;
        $GSM = [ ['GSM', 'filter', 'filter' => 'trim'], ['GSM', 'match', 'pattern' => '/^[0-9]+$/'], ['GSM', 'string', 'min' => 3, 'max' => 20]];
        $Nom = [ ['Nom', 'filter', 'filter' => 'trim'], ['Nom', 'string', 'min' => 3, 'max' => 30]];
        $Prenom = [ ['Prenom', 'filter', 'filter' => 'trim'], ['Prenom', 'string', 'min' => 3, 'max' => 30]];
        $Telephone = [ ['Telephone', 'filter', 'filter' => 'trim'], ['Telephone', 'match', 'pattern' => '/^[0-9]+$/'], ['Telephone', 'string', 'min' => 3, 'max' => 20]];
        $rules = array_merge($rules, $GSM);
        $rules = array_merge($rules, $Nom);
        $rules = array_merge($rules, $Prenom);
        $rules = array_merge($rules, $Telephone);
        return $rules;
    }
    /** @inheritdoc */
    public function attributeLabels()
    {
        return ['email' => \Yii::t('user', 'Email'), 'username' => \Yii::t('user', 'Login'), 'new_password' => \Yii::t('user', 'New password'), 'current_password' => \Yii::t('user', 'Current password')];
    }
    /**
     * Saves new account settings.
     *
     * @return bool
     */
    public function save()
    {
        if ($this->validate()) {
            $this->user->scenario = 'settings';
            $this->user->username = $this->username;
            $this->user->GSM = $this->GSM;
            $this->user->Nom = $this->Nom;
            $this->user->Prenom = $this->Prenom;
            $this->user->Telephone = $this->Telephone;
            $this->user->password = $this->new_password;
            if ($this->email == $this->user->email && $this->user->unconfirmed_email != null) {
                $this->user->unconfirmed_email = null;
            } else {
                if ($this->email != $this->user->email) {
                    switch ($this->module->emailChangeStrategy) {
                        case Module::STRATEGY_INSECURE:
                            $this->insecureEmailChange();
                            break;
                        case Module::STRATEGY_DEFAULT:
                            $this->defaultEmailChange();
                            break;
                        case Module::STRATEGY_SECURE:
                            $this->secureEmailChange();
                            break;
                        default:
                            throw new \OutOfBoundsException('Invalid email changing strategy');
                    }
                }
            }
            return $this->user->save();
        }
        return false;
    }
}