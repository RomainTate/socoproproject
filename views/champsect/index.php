<?php
use yii\bootstrap\Html;

$this->title = Yii::t('rbac', 'Administration :  gérer les champs sectoriels');
$this->params['breadcrumbs'][] = $this->title;
$get = Yii::$app->request->get();
$secteursList = yii\helpers\ArrayHelper::map(app\models\Secteur::find()->all(), 'sect_id', 'sect_nom');
$secteursList['all'] = 'Tous';
unset($secteursList[13]); // no field for Bio secteur
?>

<div class="x_panel">
    <div class="x_title">
            <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <ul id="w0" class="nav-tabs nav" style="margin-bottom: 15px">
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/index">Utilisateurs</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>rbac/role/index">Rôles</a></li>
        <li class="active"><a href="<?= \yii\helpers\Url::home()?>champsect/">Champs sectoriels</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexdoublon">Gestion doublons</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexnewsletters">Newsletters</a></li>
        <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foires <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireprod">Prod</a></li>
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireconso">Conso</a></li>
            </ul>
        </li>
    </ul>



    <?php yii\bootstrap\ActiveForm::begin(['action' => ['index'],'method' => 'get']) ?>
    <p>
        <?= Html::dropDownList('secteurID', isset($get['secteurID']) ? $get['secteurID'] : null, $secteursList,
                isset($get['secteurID']) ?['onchange' => 'this.form.submit()', 'class' => 'form-control']:
            ['prompt' => 'Selectionnez un secteur', 'onchange' => 'this.form.submit()', 'class' => 'form-control']);
        ?>
    </p>
    <?php yii\bootstrap\ActiveForm::end(); ?>

    <?php if (isset($get['secteurID'])) : ?>
        <table class="table table-bordered">
            <tr>
                <th> Champs </th>
                <th> Actions </th>
            </tr>
            <?php if ($get['secteurID'] == 'all') : ?>
                <?php foreach (\app\models\Travailler::getTableSchema()->columnNames as $columnTable) : ?>
                    <?php if ($columnTable == 'cont_id' || $columnTable == 'sect_id') continue; ?>
            <?php $type = app\controllers\ChampsectController::getType($columnTable);
            $actions = array();
            $actions[] = yii\helpers\Html::a('<i class="fa fa-trash" aria-hidden="true"></i> Supprimer', \yii\helpers\Url::to(['delete', 'attr' => $columnTable])); 
            if($type == '2' || $type == '5') {
                array_unshift($actions, yii\helpers\Html::a('<i class="fa fa-pencil" aria-hidden="true"></i> Modifier &nbsp;&nbsp;&nbsp;', \yii\helpers\Url::to(['update', 'attr' => $columnTable])));
            }?>
                    <tr>
                        <?= '<td>' . app\controllers\ChampsectController::getLabel($columnTable) . '</td>' ?>
                        <?= '<td>' ?> 
                        <?php foreach ($actions as $action) {
                            echo $action;
                        }?>
                        <?= '</td>' ?>
                    </tr>
                <?php endforeach; ?>
            <?php else : ?>
                <?php foreach (yii\helpers\ArrayHelper::map(app\models\ChampSectorielCle::find()->where(['sect_id' => $get['secteurID']])->all(), 'champ_sectoriel_cle_id', 'champ_sectoriel_cle_field') as $key => $value) : ?>
                    <tr>
                        <?= '<td>' . app\controllers\ChampsectController::getLabel($value, $secteursList[$get['secteurID']]) . '</td>' ?>
                        <td> 
                            <?php
                            $type = app\controllers\ChampsectController::getType($value, $secteursList[$get['secteurID']]);
                            $actions = array();
                            $actions[] = yii\helpers\Html::a('<i class="fa fa-trash" aria-hidden="true"></i> Supprimer', \yii\helpers\Url::to(['delete', 'attr' => $value, 'sect_id' => $get['secteurID']]));
                            if ($type == '2' || $type == '5') {
                                array_unshift($actions, yii\helpers\Html::a('<i class="fa fa-pencil" aria-hidden="true"></i> Modifier &nbsp;&nbsp;&nbsp;', \yii\helpers\Url::to(['update', 'attr' => $value, 'secteurName' => $secteursList[$get['secteurID']]])));
                            }
                            ?>
                            <?php foreach ($actions as $action) {
                            echo $action;
                        }?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
        </table>
        <?= Html::a('Ajouter', yii\helpers\Url::to(['create', 'sectID' => $get['secteurID']]), ['class' => 'btn btn-success']);?>
    <?php endif; ?>
</div>
