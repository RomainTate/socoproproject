<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "adresse".
 *
 * @property integer $adr_id
 * @property string $adr_rue
 * @property string $adr_code_postal
 * @property string $adr_ville
 * @property string $adre_province 
 *
 * @property Entreprise[] $entreprises
 * @property Habiter[] $habiters
 * @property Contact[] $contacts
 */
class Adresse extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'adresse';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adr_ville', 'adr_code_postal', 'adr_province'], \codeonyii\yii2validators\AtLeastValidator::className()
                , 'in' => ['adr_ville', 'adr_code_postal', 'adr_province'],'except' => ['evenement', 'formContact', 'app\models\Media'], 'min' => 1 , 'message' => 'au moins un attribut doit être renseigné'],
            [['adr_ville', 'adr_province'], 'string', 'max' => 50],
            [['adr_rue'], 'string', 'max' => 125],
            [['adr_code_postal'], 'string', 'max' => 5],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'adr_id' => 'Adr ID',
            'adr_rue' => 'Rue',
            'adr_code_postal' => 'Code Postal',
            'adr_ville' => 'Ville',
            'adr_province' => 'Province'
        ];
    }
    
    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        if ($this->adr_ville) {
            $this->adr_ville = mb_strtoupper($this->adr_ville);
        }
        
        return parent::beforeSave($insert);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedia()
    {
        return $this->hasMany(Media::className(), ['adr_id' => 'adr_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContacts()
    {
        return $this->hasMany(Contact::className(), ['cont_id' => 'cont_id']);
    }
}
