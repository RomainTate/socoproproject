<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Evenement;

/**
 * EvenementSearch represents the model behind the search form about `app\models\Evenement`.
 */
class EvenementSearch extends Evenement
{
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['evenement_id', 'evenement_type'], 'integer'],
            [['evenement_nom', 'evenement_date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Evenement::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'evenement_id' => $this->evenement_id,
            'evenement_date' => $this->evenement_date,
            'evenement_type' => $this->evenement_type,
        ]);

        $query->andFilterWhere(['like', 'evenement_nom', $this->evenement_nom]);

        return $dataProvider;
    }
}
