$(function () {
    
    $("button[name=editButton]").click(function () {
        $(this).parent().parent().find($("div[name=editForm]")).first().show();
        $(this).parent().hide();
        

    });
    
    $('button[name=resetEdit]').click(function () {
        $(this).parent().parent().hide();
        $(this).parent().parent().parent().find($("div[name=divShow]")).show();
    })
});