<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "participer".
 *
 * @property integer $cont_id
 * @property integer $evenement_id
 */
class Participer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'participer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id', 'evenement_id'], 'required'],
            [['cont_id', 'evenement_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'evenement_id' => 'Evenement ID',
        ];
    }
}
