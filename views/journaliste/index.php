<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;
//use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\JournalisteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$search = Yii::$app->request->pathInfo == 'journaliste/search'? true : false;
if(Yii::$app->request->post('export_type')) {
    $exporting = true;
} else {
    $exporting = false;
}
$this->title = 'Journalistes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journaliste-index">
    <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createJournaliste')) : ?>
                <?= Html::a('Ajouter un Journaliste', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        <?php endif; ?>
    </div>

    
        <?php if($search) : ?>
            <div class="x_panel">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
            </div>
        <?php endif;?>
    
    <?php if (!$search || Yii::$app->request->get('JournalisteSearch') != null) : ?>
    <?php 
     $gridColumns = [
         'langue',
            [
                'label' => 'Date contact',
                'attribute' => 'journaliste_date_contact',
                'hidden' => true,
                'value' => function ($model) {
                    if (!$model->journaliste_date_contact) {
                        return null;
                    }
                    return Yii::$app->formatter->asDate($model->journaliste_date_contact);
                }
            ],
            [
                'attribute' => 'contact.cont_civilite',
                'label' => 'Civ',
                'format' => 'text',
            ],
            [
                'label' => 'Nom',
                'attribute' => 'contactNom',
                'format' => 'text',
            ],
            [
                'label' => 'Prénom',
                'attribute' => 'contactPrenom',
                'format' => 'text',
            ],
                    [
                'label' => 'Téléphone',
                'attribute' => 'contactTelephonePrincipal',
                'format' => 'text',
            ],
             [
                'label' => 'GSM',
                'attribute' => 'contactGSMPrincipal',
                'format' => 'text',
            ],       
             [
                 'label' => 'E-mail',
                 'attribute' => 'contactEmailPrincipal',
                 'format' => 'html',
                 'value' => function ($model) use($exporting) {
                    if($exporting) return $model->contactEmailPrincipal; 
                    return Html::a(yii\helpers\BaseStringHelper::truncate($model->contactEmailPrincipal, 50, '...'), 'mailto:'.$model->contactEmailPrincipal);
                },
             ],
             [
                 'label' => 'Rubrique',
                 'attribute' => 'journaliste_rubrique',
                 'hidden' => true,
             ],
             [
                 'label' => 'Fonction',
                 'attribute' => 'journaliste_fonction',
                 'hidden' => true,
             ],       
            
             [
                'attribute' => 'societemedia',
                'header' => 'Société <br/> Grp Presse',
                'label' => 'Société / Grp Presse',
                'format' => 'html'
             ],
            [
                 'label' => 'Nom Média',
                 'attribute' => 'nommedia',
//                 'hidden' => true,
             ],
             [
                 'label' => 'Audience Média',
                 'attribute' => 'audiencemedia',
                 'hidden' => true,
             ],
             [
                 'label' => 'Rue Média',
                 'attribute' => 'ruemedia',
                 'hidden' => true,
             ],
             [
                 'label' => 'Code Postal Média',
                 'attribute' => 'codepostalmedia',
                 'hidden' => true,
             ],
             [
                 'label' => 'Localité Média',
                 'attribute' => 'localitemedia',
                 'hidden' => true,
             ],
             [
                 'label' => 'Site internet Média',
                 'attribute' => 'sitemedia',
                 'hidden' => true,
             ],
             [
                 'label' => 'Type Presse',
                 'attribute' => 'typepressemedia',
                 'hidden' => true,
             ],
             [
                 'label' => 'Type Support',
                 'attribute' => 'typesupportmedia',
//                 'hidden' => true,
             ],
             [
                 'label' => 'Périodicité',
                 'attribute' => 'periodicitemedia',
//                 'hidden' => true,
             ],
             [
                 'label' => 'Pays Média',
                 'attribute' => 'paysmedia',
                 'hidden' => true,
             ]
         ]
         ?>
    <?php array_push($gridColumns, ['class' => 'kartik\grid\ActionColumn', 'hiddenFromExport' => true ,
            'buttons' => [
                    'view' => function($url, $model) {
                        $url = \Yii::$app->urlManager->createUrl(['journaliste/view', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['class' => 'action','title' => Yii::t('yii', 'View')]);
                    },
                    'update' => function($url, $model) {
                        if(!\Yii::$app->user->can('createJournaliste')) {
                            return null;
                        }
                        $url = \Yii::$app->urlManager->createUrl(['journaliste/update', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'action','title' => Yii::t('yii', 'Update')]);
                    },
                    'delete' => function($url, $model) {
                        if(!\Yii::$app->user->can('deleteJournaliste')) {
                            return null;
                        }
                        $url = \Yii::$app->urlManager->createUrl(['journaliste/delete', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['class' => 'action','title' => Yii::t('yii', 'Delete'), 'data-confirm' => 'Etes vous sûr de vouloir supprimer ce journaliste?', 'data-method' => 'post']);
                    }
                ]
            ])?>
    <hr />
    <div class="x_panel">
        <div class="pull-right">
            Exportation :
        <?=
        ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'exportConfig' => [
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_EXCEL => false
            ],
            'noExportColumns' => [ count($gridColumns) - 1]
        ]);
        ?>
        </div>
    </div>
    
    <div class="x_panel">

        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?= Yii::$app->session->getFlash('success') ?>
            </div>
        <?php endif; ?>

        <?php if (Yii::$app->session->hasFlash('danger')): ?>
            <div class="alert alert-danger alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <?= Yii::$app->session->getFlash('danger') ?>
            </div>
        <?php endif; ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'journGrid',
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
        'columns' => $gridColumns,
//        'responsive' => false,
        'responsiveWrap' => false,
    ]); ?>
    </div>
    <?php endif; ?>
</div>
<?php if (!$search || Yii::$app->request->get('JournalisteSearch') != null) :
    $responsiveCSS = \app\components\WhResponsive::writeResponsiveCss($gridColumns, 'journGrid');
    $this->registerCss($responsiveCSS);
    $this->registerJsFile(\yii\helpers\Url::home().'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);
endif;?>