<?php

namespace app\controllers;

use Yii;
use app\models\Adresse;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AdresseController implements the CRUD actions for Adresse model.
 */
class AdresseController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Adresse models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Adresse::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Adresse model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

   
    public static function CreateAdresse($data) {
        $model = new Adresse();
        $model->load($data);
        if(!$model->adr_code_postal && !$model->adr_rue && !$model->adr_ville && !$model->adr_province) {
            return null;
        }
        $model->save();
        return $model->adr_id;
    }

    /**
     * Updates an existing Adresse model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id, $relatedModelClass = null, $relatedModelId = null) {
        
        $model = $this->findModel($id);
        $relatedModel = null;
        $post = Yii::$app->request->post();
        if ($relatedModelClass && $relatedModelId) {
            $model->setScenario($relatedModelClass);
            if ($relatedModelClass == 'app\models\Media') {
                $relatedModel = \app\models\Media::find()->where(['media_id' => $relatedModelId])->one();
            }
        }
        if ($model->load($post) && $model->save()) {
            if ($relatedModel) {
                $relatedModel->load($post);
                $relatedModel->save();
            }
            if (!$model->adr_code_postal && !$model->adr_rue && !$model->adr_ville && !$model->adr_province) {
                $model->delete();
                return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
            }
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
        } else {
            return $this->renderAjax('update', [
                        'model' => $model,
                        'relatedModel' => $relatedModel
            ]);
        }
    }

    /**
     * Deletes an existing Adresse model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Adresse model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Adresse the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Adresse::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
