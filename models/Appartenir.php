<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "appartenir".
 *
 * @property integer $entr_id
 * @property integer $cont_id
 * @property string $fonction
 *
 * @property Contact $contact
 * @property Entreprise $entreprise
 */
class Appartenir extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appartenir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entr_id', 'cont_id'], 'required'],
            [['entr_id', 'cont_id'], 'integer'],
            [['fonction'], 'string'],
            [['fonction'], 'safe'],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['entr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entreprise::className(), 'targetAttribute' => ['entr_id' => 'entr_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entr_id' => 'Entr ID',
            'cont_id' => 'Cont ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntreprise()
    {
        return $this->hasOne(Entreprise::className(), ['entr_id' => 'entr_id']);
    }
}
