<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$suggestFonction = yii\helpers\ArrayHelper::map(\app\models\Contactdivers::find()->select('divers_fonction')
                        ->where(['not', ['divers_fonction' => null]])->all(), 'divers_fonction', 'divers_fonction');
$suggestFonction = array_values($suggestFonction);
//$suggestLieu = yii\helpers\ArrayHelper::map(\app\models\Lieuprisecontact::find()->select('lieu')->all() , 'lieu', 'lieu');
//$suggestLieu = array_values($suggestLieu);
/* @var $this yii\web\View */
/* @var $model app\models\Contactdivers */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="conso-infos-form">

    <?php
    $form = ActiveForm::begin([
                'id' => 'contactdiversInfosForm'
    ]);
    ?>
    
    <div class="form-group customradio">
        <?= Html::label('Membre du college', null, ['class' => 'control-label']); ?>
        <?= Html::radioList('membrecollege',  ($model->contact->membrecollege)?'1': '0', ['0' => 'non', '1' => 'oui']) ?>
    </div>
    <div class="form-group">
        <?= Html::label('Membre commission filière',null, ['class' => 'control-label']);?>
        <?= Html::dropDownList('membrefiliere',
                yii\helpers\ArrayHelper::map(app\models\MembreCommissionFilliere::find()->where(['cont_id' => $model->cont_id])->all(),'sect_id', 'sect_id'), 
                yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(),'sect_id' , 'sect_nom'),['class' => 'js-multiple' , 'multiple'=>'multiple']);
        ?>
    </div>
    <?= $form->field($model, 'secteurs')
            ->dropDownList(yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom'), ['class' => 'js-multiple', 'multiple' => 'multiple'])->label('Intérêts')?>
     <?php /*   
    <?= $form->field($model, 'divers_fonction')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
        'clientOptions' => [
            'source' => $suggestFonction,
            'appendTo' => '#modalContent'
        ],
        'options' => [
            'class' => 'form-control'
        ]
    ])?>

    // echo $form->field($model, 'contDiversLieuContactName')->textInput()->widget(\yii\jui\AutoComplete::classname(), [
//        'clientOptions' => [
//            'source' => $suggestLieu,
//            'appendTo'=>'#modalContent'
//        ],
//        'options' => [
//            'class' => 'form-control'
//        ],
//    ]); 
    */?>
    <?= $form->field($model, 'divers_AS_AG')->dropDownList(['1' => 'AS' , '2' => 'AG' , '3' => 'AS / AG' ], ['prompt' => 'non']);?>
    <div class="form-group">
<?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod'); ?>