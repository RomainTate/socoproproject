<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Producteur */
if($model->needConfirm == 1 && Yii::$app->user->getUser()->getIsAdmin()) :
    $doublon = true;
else :
    $doublon = false;
endif;

if($model->needConfirm == 2 && Yii::$app->user->getUser()->getIsAdmin()) :
    $needConfirm = true;
else :
    $needConfirm = false;
endif;

$this->title = ($model->contact->cont_nom != null || $model->contact->cont_prenom) ? $model->contact->cont_civilite . ' ' . $model->contact->cont_nom . ' ' . $model->contact->cont_prenom : 'Profil producteur';
if($doublon) :
    $this->params['breadcrumbs'][] = ['label' => 'Admin : gestion doublons', 'url' => ['user/admin/indexdoublon']];
elseif($needConfirm):
    $this->params['breadcrumbs'][] = ['label' => 'Admin : gestion foire', 'url' => ['user/admin/indexfoireprod']];
else:
    $this->params['breadcrumbs'][] = ['label' => 'Producteurs', 'url' => ['index']];
endif;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producteur-view">
    <div class="x_panel">
        <h1 class="pull-left x_title"><?= $this->title ?></h1>
        <?php if (Yii::$app->user->can('deleteProducteur')) : ?>
            <div class="pull-right">
                <?php  if ($doublon) $contentbtn = 'Supprimer le doublon'; else $contentbtn = 'Supprimer le contact';
                $dataconfirm = 'Êtes-vous sûr de vouloir supprimer ce ';
            if ($doublon) {
                $dataconfirm .= 'doublon ?';
                $pageToRedirect = 'doublon';
            } elseif ($needConfirm) {
                $dataconfirm .= 'producteur en attente';
                $pageToRedirect = 'foire';
            } else {
                $dataconfirm .= 'contact ?';
                $pageToRedirect = '';
            }
            ?>
                <?= Html::a($contentbtn, ['delete', 'id' => $model->cont_id, 'pageToRedirect' => $pageToRedirect], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => $dataconfirm,
                        'method' => 'post',
                    ],
                    'alt' => 'Supprimer',
                    'title' => 'Supprimer'
                ]);?>
            </div>
            <?php endif    ?>
        <div class="clearfix"></div>
<?php if(!$doublon && !$needConfirm) :?>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <!--<a class="navbar-brand" href="#">Brand</a>-->
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="#">Profil <span class="sr-only">(current)</span></a></li>
                        <li><?= Html::a('Champs sectoriels', \yii\helpers\Url::to(['/producteur/details', 'id' => $model->cont_id])) ?></li>
                        <li> <?= Html::a('Logs', \yii\helpers\Url::to(['/log', 'LogSearch[contID]' => $model->cont_id])) ?> </li>
                        <?php if( !Yii::$app->user->isGuest &&  (Yii::$app->user->getUser()->getIsAdmin() || \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission' )) : ?>
                        <li> <?= Html::a('Suivis', \yii\helpers\Url::to(['/suivi', 'id' => $model->cont_id])) ?> </li>
                        <?php endif ?>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <?php /*
        <div class="statsProd">    
            <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#statsProd">Voir les statistiques</button>
            <div id="statsProd" class="collapse">
                <?php
                $nbSuivis = count($model->contact->suivis);
                $nbSuivisMonth = count($model->contact->lastmonthsuivis);
                $percentSuiviMonth = $nbSuivisMonth ? number_format($nbSuivisMonth / $nbSuivis * 100, 2, '.', '') : '0';
                $nbSuivisYear = count($model->contact->lastyearsuivis);
                $percentSuiviYear = $nbSuivisYear ? number_format($nbSuivisYear / $nbSuivis * 100, 2, '.', '') : '0';
                ?>
                <div class="row">
                    <div class="col-lg-6 col-xs-12">
                        <div class="row tile_count">
                            <div class="col-xs-4 tile_stats_count">
                                <span class="count_top"><i class="fa fa-handshake-o"></i> échanges</span>
                                <div class="count"><?= $nbSuivis ?></div>
                                <span class="count_bottom"> en tout </span>
                            </div>
                            <div class="col-xs-4 tile_stats_count">
                                <span class="count_top"><i class="fa fa-clock-o"></i> depuis un mois</span>
                                <div class="count"><?= $nbSuivisMonth ?></div> 
                                <span class="count_bottom"><i class="green"><?= $percentSuiviMonth ?>% </i> </span>
                            </div>
                            <div class="col-xs-4 tile_stats_count" style="border-right: 2px solid #ADB2B5">
                                <span class="count_top"><i class="fa fa-user"></i> depuis un an</span>
                                <div class="count"><?= $nbSuivisYear ?></div>
                                <span class="count_bottom"><i class="green"><?= $percentSuiviYear ?> % </i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                        <div class="row tile_count">
                            <div class="col-xs-6 tile_stats_count">
                                <span class="count_top"><i class="fa fa-user"></i> &Eacute;vènements</span>
                                <div class="count">4</div>
                                <span class="count_bottom"> en tout</span>
                            </div>
                            <div class="col-xs-6 tile_stats_count" style="border-right: 2px solid #ADB2B5">
                                <span class="count_top"><i class="fa fa-user"></i> depuis un an</span>
                                <div class="count">2</div>
                                <span class="count_bottom"><i class="green">50%</i></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-2 col-xs-12">
                        <div class="row tile_count">
                            <div class="col-xs-12 tile_stats_count" style="border-right: 2px solid #ADB2B5">
                                <span class="count_top"><i class="fa fa-user"></i> Newsletters</span>
                                <div class="count">3</div>
                                <span class="count_bottom"> Inscriptions</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
         * 
         */?>
<?php endif;?>
    </div>
    
    <?php if($doublon) :?>
    <div class="x_panel" style="background-color: #fffad6">
        <?php if(empty($otherProds)) :?>
            Aucun doublon sur base du nom + prénom ou sur base du nom d'exploitation n'a été trouvé.
        <?php else : ?>
            <h3>&#9888; <?= count($otherProds)?> contact(s) existe(nt) déjà avec les mêmes informations :</h3>
            <ul class="list-group">
            <?php foreach ($otherProds as $otherProd) :
                $prod = '';
                if($otherProd->contact->cont_nom && $otherProd->contact->cont_prenom) :
                    $prod .= $otherProd->contact->cont_nom . ' '. $otherProd->contact->cont_prenom;
                endif;
                if($otherProd->prod_nom_exploitation) :
                    $prod .= ' ('. $otherProd->prod_nom_exploitation .')';
                endif;?>
                <li class="list-group-item"> <?= Html::a($prod, yii\helpers\Url::to(['/producteur/view', 'id' => $otherProd->cont_id]),['target' => 'blank'])?> </li>
            <?php endforeach;?>
            </ul>
        <?php endif;?>
            <div class="pull-right">
                <?=Html::a('Ajouter quand même', yii\helpers\Url::to(['/producteur/accept', 'id' => $model->cont_id]), ['class' => 'btn btn-warning']) ?>
            </div>
    </div>
    <?php endif ?>
    
    <?php if($needConfirm) :?>
    <div class="x_panel" style="background-color: #fffad6">
        <h2 class="x_title"> Remarques : </h2>
        <?php
        $remarques = [];
        if(!$model->prod_nom_exploitation && !$model->contact->cont_prenom && !$model->contact->cont_nom) {
            $remarques[] = 'Le producteur n\'a pas de nom, pas de prénom ni de nom d\'exploitation';
        }
        $model->validate();
        foreach((array)$model->getErrors() as $modelErrors) {
            foreach ($modelErrors as $modelError) {
                $remarques[] = $modelError;
            }
        }
        $model->contact->setScenario('typeProducteur');
        $model->contact->validate();
        foreach((array)$model->contact->getErrors() as $modelErrors) {
            foreach ($modelErrors as $modelError) {
                $remarques[] = $modelError;
            }
        }
        if(!$model->secteurs) {
            $remarques[] = 'Aucun secteur n\'est assigné';
        }
        if(empty($remarques)) :?>
            Aucun problème détecté, ce Producteur peut être validé
            <div class="pull-right">
                <?=Html::a('Valider', yii\helpers\Url::to(['/producteur/accept', 'id' => $model->cont_id]), ['class' => 'btn btn-warning']) ?>
            </div>
        <?php else:?>
            <ul>
            <?php foreach ($remarques as $remarque) :?>
                <li> -<?=$remarque?></li>
            <?php endforeach;?>
            </ul>
        <?php endif; 
            if(!empty($otherProds)) :?>
        <h2 class="x_title"> Attention : </h2>
            <h3>&#9888; <?= count($otherProds)?> contact(s) existe(nt) déjà avec les mêmes informations :</h3>
            <ul class="list-group">
            <?php foreach ($otherProds as $otherProd) :
                $prod = '';
                if($otherProd->contact->cont_nom && $otherProd->contact->cont_prenom) :
                    $prod .= $otherProd->contact->cont_nom . ' '. $otherProd->contact->cont_prenom;
                endif;
                if($otherProd->prod_nom_exploitation) :
                    $prod .= ' ('. $otherProd->prod_nom_exploitation .')';
                endif;?>
                <li class="list-group-item"> <?= Html::a($prod, yii\helpers\Url::to(['/producteur/view', 'id' => $otherProd->cont_id]),['target' => 'blank'])?> </li>
            <?php endforeach;?>
            </ul>
            <?php endif;?>
    </div>
    <?php endif ?>
    
    <?php if($model->prod_sigec) : ?>
    <div class="x_panel" style="background-color: #fffad6">
        <h2 class="x_title"> Attention : </h2>
        Ce producteur est confidentiel !
    </div>
    <?php endif ?>
    <?php if (Yii::$app->session->hasFlash('notice')): ?>
        <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?= Yii::$app->session->getFlash('notice') . '<br />' ?>
        </div>
    <?php endif;?>
    
    <div class="row">

        <div class="col-lg-6 col-xs-12 profile_left">
            <div class="x_panel" >
                <div class="x_content">
                    <div class="text-right">
                        <p style="font-size: 11px">
                            <?php echo ($model->contactCreatedAt) ? 'Ajouté le ' . Yii::$app->formatter->asDatetime($model->contactCreatedAt) : 'date d\'ajout inconnue'; ?>
                        </p>
                    </div>
                    <div class="img-profil">
                        <img src="../css/images/farmer.png" alt="producteur" />
                    </div>

                    <h3><?php if ($model->contact->cont_nom != null || $model->contact->cont_prenom) : ?>
                            <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?>
                        <?php else : ?>
                            <?= 'Nom et Prénom inconnus' ?>
                        <?php endif; ?>
                    </h3>

                    <div class="mainInfos">
                        <p> <i class="fa fa-lg fa-phone" aria-hidden="true"></i>
                            <?php if ($model->contactTelephonePrincipal) : ?> 
                                <?= $model->contactTelephonePrincipal ?>
                            <?php else : ?>
                                Numéro Téléphone inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createProducteur'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_telephone_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 
                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->contactGSMPrincipal) : ?> 
                                <?= $model->contactGSMPrincipal ?>
                            <?php else : ?>
                                Numéro GSM inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createProducteur'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_gsm_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope" aria-hidden="true"></i> 
                            <?php if ($model->contactEmailPrincipal) : ?>
                            <?= Html::a(yii\helpers\BaseStringHelper::truncate($model->contactEmailPrincipal, 40, '...'), 'mailto:'.$model->contactEmailPrincipal) ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createProducteur'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_email_principal']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>

                        <p> <i class="fa fa-flag" aria-hidden="true"></i> 
                            <?php
                            if ($model->contactLangue) :
                                echo 'Langue : ' . $model->contactLangue;
                            else :
                                ?>
                                Langue inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createProducteur'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_langue']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>


                        <?php /*
                          <p> <i class="fa fa-calendar" aria-hidden="true"></i>
                          <?php if ($model->contactAnneeNaissance) : ?>
                          <?= Yii::$app->formatter->asDate($model->contactAnneeNaissance) ?>
                          <?php else : ?>
                          Année de naissance inconnue
                          <?php endif; ?>
                          <?php
                          if (Yii::$app->user->can('createProducteur'))
                          echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_annee_naissance']),
                          'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                          ?>
                          </p>
                         */ ?>
                        <h3>Adresse</h3>

                            <?php if ($model->contact->adresse) : ?>
                            <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                <?php if ($model->contact->adresse->adr_rue != null && $model->contact->adresse->adr_rue != ''): ?>
                                    <?= $model->contact->adresse->adr_rue ?>
                                <?php else : ?>
                                    <?= 'Numéro et Rue inconnus' ?>
                                <?php endif; ?>
                                <?php
                                if (Yii::$app->user->can('createProducteur')) {
                                    echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update', 'id' => $model->contact->adr_id]),
                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                }
                                ?>
                            </p>

                                <?php if ($model->contact->adresse->adr_code_postal != null && $model->contact->adresse->adr_code_postal != '') : ?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                    <?= 'Code Postal: ' . $model->contact->adresse->adr_code_postal ?>
                                    <?php
                                    if (Yii::$app->user->can('createProducteur')) {
                                        echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update', 'id' => $model->contact->adr_id]),
                                            'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                    }
                                    ?>
                                </p>
                                <?php endif ?>

                                <?php if ($model->contact->adresse->adr_ville != null && $model->contact->adresse->adr_ville != '') : ?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                <?= 'Localite: ' . $model->contact->adresse->adr_ville ?>
                                <?php
                                if (Yii::$app->user->can('createProducteur')) {
                                    echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update', 'id' => $model->contact->adr_id]),
                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                }
                                ?>
                                </p>
                                <?php endif; ?>

                            <?php if ($model->contact->adresse->adr_province != null && $model->contact->adresse->adr_province != '') : ?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                <?= 'Province: ' . $model->contact->adresse->adr_province ?>
                                    <?php
                                    if (Yii::$app->user->can('createProducteur')) {
                                        echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update', 'id' => $model->contact->adr_id]),
                                            'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                    }
                                    ?>
                                </p>
                            <?php endif; ?>
<?php else : ?>
                            <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                        <?= 'Adresse inconnue'; ?>
                        <?php
                        if (Yii::$app->user->can('createProducteur')) {
                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/addadresse', 'id' => $model->cont_id]),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                        }
                        ?>
                            </p>
<?php endif; ?>

                    </div>
                    <?php
                    if (Yii::$app->user->can('createProducteur'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations du contact', ['value' => \yii\helpers\Url::to(['/producteur/update', 'id' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                </div>
            </div>

            <div class="x_panel mainInfos" >
                <div class="headerInfos">
                    <h2 class="pull-left"> Associé(es)</h2>
                        <?php
                        if (Yii::$app->user->can('createProducteur'))
                            echo \yii\bootstrap\Html::button('Créer un associé', ['value' => \yii\helpers\Url::to(['/associe/create', 'prod_id' => $model->cont_id]),
                                'name' => 'modalButton', 'class' => 'btn btn-success pull-right']);
                        ?>
                </div>
<?php if (!$model->associes) : ?> <div class="x_content"> Aucun associé </div> <?php else : ?>
    <?php foreach ($model->associes as $associe) : ?>
                        <div class="x_content">
                            <h4>
                                <?php
                                if (($associe->contact->cont_prenom == null || $associe->contact->cont_prenom == '') &&
                                        ($associe->contact->cont_nom == null || $associe->contact->cont_nom == '')) :
                                    echo "Nom et Prénom inconnus";
                                else :
                                    echo $associe->contact->cont_civilite . ' ' . $associe->contact->cont_prenom . ' ' . $associe->contact->cont_nom;
                                endif;
                                ?>
                            </h4>

                            <p>
                                <b>Relation :</b> <?php
                                switch ($associe->assoc_type) {
                                    case 1: echo '&Eacute;poux/&Eacute;pouse';
                                        break;
                                    case 2: echo 'famille';
                                        break;
                                    default : echo 'autre';
                                        break;
                                }
                                ?>
                            </p>
                            <?php if ($associe->contact->cont_gsm_principal) : ?>
                                <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                                <?= $associe->contact->cont_gsm_principal ?>
                                </p> 
                            <?php endif; ?>

                            <?php if ($associe->contact->cont_email_principal) : ?>    
                                <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                                    <?= $associe->contact->cont_email_principal ?> 
                                </p>
                                <?php endif; ?>
                                <?php /*
                                  <?php if ($associe->contact->cont_annee_naissance) : ?>
                                  <p> <i class="fa fa-calendar" aria-hidden="true"></i>
                                  <?= $associe->contact->cont_annee_naissance ?>
                                  </p>
                                  <?php endif; ?>
                                 */ ?>
                                <?php if (Yii::$app->user->can('createProducteur')) : ?>
                                <div class="text-right">
                                <?php
                                echo \yii\bootstrap\Html::button('Modifier', ['value' => \yii\helpers\Url::to(['/associe/update', 'id' => $associe->cont_id]),
                                    'name' => 'modalButton', 'class' => "btn btn-info btn-xs"]);
                                echo Html::a('Supprimer', ['/associe/delete', 'id' => $associe->cont_id], [
                                    'class' => 'btn btn-danger btn-xs',
                                    'data' => [
                                        'confirm' => 'Êtes-vous sûr de vouloir supprimer cet associé?',
                                        'method' => 'post',
                                    ],
                                ]);
                                ?>
                                </div>
        <?php endif; ?>
                        </div>
    <?php endforeach; ?>
<?php endif; ?>
            </div>
        </div>

        <div class="col-lg-6 col-xs-12">
            <div class="x_panel secondInfos">
                <div class="x_content">
                    <h2>Autres informations</h2> 

                    <ul class="autres-infos">
                        <li>
                            <h3>
                                <u>Membre du collège</u> : <?php echo ($model->contact->membrecollege) ? 'oui' : 'non'; ?>
                            </h3>
                        </li>
                        <hr />
                        <li>
                            <h3> Secteurs :
                                <?php if (Yii::$app->user->can('createProducteur')) :?>
                                    <?= Html::button('',['value' => \yii\helpers\Url::to(['/producteur/addothersecteurs', 'id' => $model->cont_id]),
                                    'name' => 'modalButton', 'class' => 'fa fa-plus-circle text-success'])?> 
                                <?php endif ?>
                            </h3>
                            <ul class="fa-ul">
                        <?php
                        foreach ($model->secteursOrdered as $secteur) {
                            $show = "<div class='sectshow'> <li>" . "<i class='glyphicon glyphicon-chevron-right'></i>" . $secteur->sect_nom  
                                    ."<span class='sectremove'>";
                            if(Yii::$app->user->can('createProducteur') && in_array($secteur->sect_nom, Yii::$app->user->getSecteurs())) {
                                $dataconfirm = 'Êtes-vous sûr de vouloir supprimer ce secteur?';
                                if (count($model->secteurs) == 1) {
                                    $dataconfirm .= ' Puisque ce producteur n\'aura plus de secteur, il sera donc supprimé';
                                } 
                                $show .= Html::a('', yii\helpers\Url::to(['/producteur/removesecteur', 'id' => $model->cont_id, 'sectID' => $secteur->sect_id]), ['class' => 'fa fa-times' , 'style' => 'color : red;', 
                                        'data' => ['confirm' => $dataconfirm, 'method' => 'post']])
                                    .'</span>' . "</li> </div>" ;
                            }
                            echo $show; 
                                    
                        }
                        ?>
                            </ul>
                        </li>
                        <hr/>
                            <?php if (!$model->autreinfos) : ?>
                            <li>
                                Site internet : inconnu
                            </li>

                            <li>
                                Page Facebook : inconnu
                            </li>
                            <?php else : ?>
                            <li>
                                Site internet :
                            <?php
                            if ($model->autreinfos->site_internet) :
//                                $siteUrl = substr($model->autreinfos->site_internet, 0, 7) == 'http://'  $model->autreinfos->site_internet : ( (substr($model->autreinfos->site_internet, 0, 8) == 'https://' ? $model->autreinfos->site_internet : 'http://' . $model->autreinfos->site_internet);
                                if(substr($model->autreinfos->site_internet, 0, 7) == 'http://') {
                                    $siteUrl = $model->autreinfos->site_internet;
                                } elseif (substr($model->autreinfos->site_internet, 0, 8) == 'https://' ) {
                                    $siteUrl = $model->autreinfos->site_internet;
                                } else {
                                     $siteUrl = 'http://'. $model->autreinfos->site_internet;
                                }
                                Yii::setAlias('@siteprod', $siteUrl);
//                                echo $model->autreinfos->site_internet;
                                echo Html::a($siteUrl, yii\helpers\Url::to('@siteprod'), ['target' => 'blank']);
//                                echo '<a href='.$model->autreinfos->site_internet.' target=blanck>'.$model->autreinfos->site_internet.'</a>';?>
                                <!--<a href="eza.com" > hey </a>-->
                                    <?php
                            else :
                                echo 'inconnu';
                            endif;
                            ?>
                            </li>
                            <li>
                                Page Facebook :
                                <?php
                                if ($model->autreinfos->page_facebook) :
                                    echo $model->autreinfos->page_facebook;
                                else :
                                    echo 'inconnu';
                                endif;
                                ?>
                            </li>
                            <?php endif; ?>


                        <li>
                            Statut professionnel :
                            <?php
                            if ($model->prod_statut_pro === 1) {
                                echo 'Actif';
                            } elseif ($model->prod_statut_pro === 0) {
                                echo 'Retraité';
                            } else {
                                echo 'Inconnu';
                            }
                            ?>
                        </li>

                        <li>
                            Numéro Producteur :
                            <?php
                            if ($model->prod_numero_producteur != '' && $model->prod_numero_producteur != null) {
                                echo $model->prod_numero_producteur;
                            } else {
                                echo "inconnu";
                            }
                            ?>
                        </li>

                        <li>
                            Numéro banque carrefour entreprise :
                            <?php
                            if ($model->prod_numero_BCE != '' && $model->prod_numero_BCE != null) {
                                echo $model->prod_numero_BCE;
                            } else {
                                echo "inconnu";
                            }
                            ?>
                        </li>

                        <li>
                            Exploitation :
                            <?php
                            if ($model->prod_nom_exploitation != '' && $model->prod_nom_exploitation != null) {
                                echo $model->prod_nom_exploitation;
                            } else {
                                echo "inconnue";
                            }
                            ?>
                        </li>

                        <li>
                            Statut juridique :
                            <?php
                            if ($model->prod_statut_juridique != 0 && $model->prod_statut_juridique != null) {
                                // ["1"=>"SA","2"=>"SPRL","3"=>"SPRLS","4"=>"SCRL","5"=>"SNC","6"=>"ASBL"]
                                switch ($model->prod_statut_juridique) {
                                    case 1 : echo 'SA';
                                        break;
                                    case 2 : echo "SPRL";
                                        break;
                                    case 3 : echo "SPRLS";
                                        break;
                                    case 4 : echo "SCRL";
                                        break;
                                    case 5 : echo "SNC";
                                        break;
                                    case 6 : echo "ASBL";
                                        break;
                                    default : echo "inconnu";
                                }
                            } else {
                                echo "inconnu";
                            }
                            ?>
                        </li>

                        <li>
                            Date début activité :
                            <?php
                            if ($model->prod_date_debut_activite != '' && $model->prod_date_debut_activite != null) {
                                echo Yii::$app->formatter->asDate($model->prod_date_debut_activite);
                            } else {
                                echo "inconnue";
                            }
                            ?>
                        </li>
                        <li>
                            Autres activités :
                            <?php if ($model->activites) : ?>
                                <ul class="fa-ul">
                                <?php
                                foreach ($model->activites as $activite) {
                                    echo "<li>" . "<i class='glyphicon glyphicon-chevron-right'></i>" . $activite->activite_nom . "</li>";
                                }
                                ?>
                                </ul>
                            <?php else : ?>
                                aucune
                            <?php endif; ?>
                        </li>

                    </ul>
                    <?php
                    if (Yii::$app->user->can('createProducteur'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/producteur/addmoreinfos', 'contactId' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                    <hr />
                    <h2>Newsletters</h2>
                    <ul>
                        <li>
                    <?php if ($model->contact->newsletters) : ?>
                                <ul class="fa-ul">
                        <?php foreach ($model->contact->newsletters as $newsletter) : ?>
                                        <li><i class='glyphicon glyphicon-chevron-right'></i> <?= $newsletter->newsletter_nom ?> </li>
                        <?php endforeach; ?>
                                </ul>
                    <?php else : ?>
                                aucune
                    <?php endif; ?>
                        </li>
                    </ul>
                    <?php
                    if (Yii::$app->user->can('createProducteur'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/contact/managenewsletters', 'id' => $model->cont_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                    <hr />
                    <h2>Entreprises-Associations </h2>
                    <?php if (Yii::$app->user->can('createProducteur')): ?>
                        <div class="text-right">
                        <?= Html::button('Ajouter une entreprise', ['value' => \yii\helpers\Url::to(['/contact/addentreprise', 'contactId' => $model->cont_id]), 'class' => 'btn btn-success', 'name' => 'modalButton']) ?> 
                        </div>
                    <?php
                    endif;
                    if (!$model->contact->entreprises) {
                        echo 'Aucune Entreprise';
                    } else {
                        foreach ($model->contact->entreprises as $entreprise) :
                            ?>
                            <h4> <?= $entreprise->entr_societe ?></h4> 
                            <?=
                            $this->render('../entreprise/preview', array('model' => $model, 'entreprise' => $entreprise));

                            if ((Yii::$app->user->can('createEntreprise')) || Yii::$app->user->can('createProducteur')) {
                                echo '<div class="text-right">';
                                if (Yii::$app->user->can('createEntreprise'))
                                    echo \yii\bootstrap\Html::button('Modifier', ['value' => \yii\helpers\Url::to('/entreprise/update?id=' . $entreprise->entr_id), 'name' => 'modalButton', 'class' => "btn btn-info"]);
                                if (Yii::$app->user->can('createProducteur'))
                                    echo \yii\bootstrap\Html::a("Enlever l'entreprise", \yii\helpers\Url::to(['/contact/deleteentreprise', 'contactId' => $model->cont_id, 'entr_id' => $entreprise->entr_id]), ['class' => 'btn btn-danger', 'data-confirm' => 'Retirer ce contact de cette entreprise ? Ceci ne supprimera pas l\'entreprise de la base de données']);
                                echo '</div>';
                            }
                            echo '<hr />';
                        endforeach;
                    }
                    ?>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
<?php
echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
    'entity' => (string) 'producteur-' . $model->cont_id, // type and id
]);
?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home() . 'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>