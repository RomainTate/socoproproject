$(document).ready(function () {
    
    $('body').on('click','.addProd', function () {
        var values = $(this).attr('value');
        var homeUrl = $(this).attr('homeUrl');
        $.ajax({
                url :values,
                dataType: 'json',
                success : function (json) {
                    if(json != null) {
                        $.pjax.reload({container:'#pjax_id'});
                        $("#noContact").remove();
                        var nbContacts = parseInt($('#nbContact').html());
                        $('#nbContact').html(nbContacts +1);
                        var exploit = '';
                        if(json.nom_exploitation != 'null') {
                            exploit = ' ('+json.nom_exploitation+' )';
                        }
                        $("#listeContactsPresents").append(
                                '<li class="list-group-item">' +
                                '<a title="Retirer" style="color: red" value="'+homeUrl+'evenement/removecontactajax?cont_id='+json.id+'&evenement_id='+json.evenementID+'" class="removeProd" contID="'+json.id+'" homeUrl="'+homeUrl+'"> <i class="fa fa-times" aria-hidden="true"></i> </a>' +
                                '<a href="'+homeUrl+'producteur/view?id=' +json.id + '">' + json.nom + ' ' +json.prenom + exploit + '</a></li>');
                    }
                }
            });
    });
    
    $('body').on('click','.removeProd', function () {
        var values = $(this).attr('value');
        var homeUrl = $(this).attr('homeUrl');
        console.log(values);
        $.ajax({
                url :values,
                dataType: 'text',
                success : function (contID) {
                    if(contID != null) {
                        $.pjax.reload({container:'#pjax_id'});
                        var nbContacts = parseInt($('#nbContact').html());
                        $('#nbContact').html(nbContacts -1);
                        $('a[href="'+homeUrl+'producteur/view?id='+contID+'"]').parent().remove();
                        if(nbContacts == 1) {
                            $("#listeContactsPresents").append('<li id="noContact"> Aucun contact </li>');
                        }
                    }
                }
            });
    });
});