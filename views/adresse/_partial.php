<?php

/* @var $this yii\web\View */
/* @var $adrModel app\models\Adresse */
?>

<div class="adresse-form">

    <?= $form->field($adrModel, 'adr_rue')->textInput(['maxlength' => true]) ?>

    <?= $form->field($adrModel, 'adr_code_postal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($adrModel, 'adr_ville')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($adrModel, 'adr_province')->dropDownList(['ANVERS'=>'ANVERS', 'BRABANT WALLON'=>'BRABANT WALLON', 'BRABANT FLAMAND'=>'BRABANT FLAMAND',
        'BRUXELLES'=>'BRUXELLES', 'FLANDRE OCCIDENTALE'=>'FLANDRE OCCIDENTALE', 'FLANDRE ORIENTALE'=>'FLANDRE ORIENTALE', 'HAINAUT'=>'HAINAUT',
        'LIMBOURG'=>'LIMBOURG', 'LIÈGE'=>'LIÈGE', 'LUXEMBOURG'=>'LUXEMBOURG', 'NAMUR'=>'NAMUR'], ['prompt' => 'inconnue']) ?>
    
    <?php if (isset($relatedModel) && $relatedModel && get_class($relatedModel) ==  "app\models\Media") :?>
        <?= $form->field($relatedModel, 'media_pays')->textInput(['maxlength' => true]) ?>
    <?php endif ?>

</div>
