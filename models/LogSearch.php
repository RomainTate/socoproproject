<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use app\models\Log;

/**
 * LogSearch represents the model behind the search form about `app\models\Log`.
 */
class LogSearch extends Log
{
    public $createdAt;
    public $userID;
    public $infos;
    public $natureContact;
    public $contID;
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['log_id', 'id', 'cont_id', 'created_at', 'action'], 'integer'],
            [['log_attr_modifies', 'createdAt', 'userID', 'infos', 'natureContact', 'contID'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {   
        $query = Log::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
//            'sort'=> ['defaultOrder' => ['created_at'=>SORT_DESC]]
        ]);
        
        $query->joinWith(['user']);
        $query->joinWith(['contact']);
        $query->joinWith(['contact.producteur']);
        $query->joinWith(['contact.associe ca']);
        $query->joinWith(['contact.journaliste']);
        $query->joinWith(['contact.consommateur']);
        $query->joinWith(['contact.contactdivers']);
        $this->load($params);
        
        
        $dataProvider->setSort([
        'attributes' => [
            'date'=> [
                'asc' => ['created_at' => SORT_ASC],
                'desc' => ['created_at' => SORT_DESC],
            ],
        ]
    ]);

        $query->orderBy(['created_at' => SORT_DESC]);
        
        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        
        if($this->createdAt) {
            $query->andFilterWhere(['>', 'log.created_at', strtotime($this->createdAt)]);
        }
        
        if($this->infos != '') {
            switch ($this->infos) {
            case 0:
                $query->andFilterWhere(['like', 'log_attr_modifies', 'cont_gsm_principal']);
                break;
            case 1:
                $query->andFilterWhere(['like', 'log_attr_modifies', 'cont_email_principal']);
                break;
            case 2:
                $query->andFilterWhere(['like', 'log_attr_modifies', 'prod_statut_pro']);
                break;
            case 3:
                $query->andFilterWhere(['like', 'log_attr_modifies', 'sect']);
                break;
            case 4:
                $query->andFilterWhere(['like', 'log_attr_modifies', 'newcont_entity']);
                break;
            case 5:
                $query->andFilterWhere(['like', 'log_attr_modifies', 'oldcont_entity']);
                break;
            }
        }
        
        if($this->natureContact != '') {
            switch ($this->natureContact) {
            case 0:
                $query->andFilterWhere(['or',
                    ['in', 'log.cont_id', ArrayHelper::map(Producteur::find()->select('cont_id')->all(), 'cont_id', 'cont_id')],
                    ['in', 'log.cont_id', ArrayHelper::map(Associe::find()->select('cont_id')->all(), 'cont_id', 'cont_id')],
                    ['like', 'log_attr_modifies', 'prod'],
                    ]);
                break;
            case 1:
                $query->andFilterWhere(['in', 'log.cont_id',  ArrayHelper::map(Journaliste::find()->select('cont_id')->all(), 'cont_id', 'cont_id')]);
                break;
            case 2:
                $query->andFilterWhere(['in', 'log.cont_id',  ArrayHelper::map(Consommateur::find()->select('cont_id')->all(), 'cont_id', 'cont_id')]);
                break;
            case 3:
                $query->andFilterWhere(['in', 'log.cont_id',  ArrayHelper::map(Contactdivers::find()->select('cont_id')->all(), 'cont_id', 'cont_id')]);
                break;
            }
        }
        
        $query->andFilterWhere(['user.id' => $this->userID]);
        $query->andFilterWhere(['action' => $this->action]);
        
        
        if($this->contID) {
            $ttcont = ArrayHelper::map(Associe::find()->select('cont_id')->where(['cont_id_prod' => $this->contID])->all(), 'cont_id', 'cont_id');
            $ttcont[] = (int)$this->contID;
            $query->andFilterWhere(['log.cont_id' => $ttcont]);
        }

//        $query->andFilterWhere(['like', 'log_attr_modifies', $this->log_attr_modifies]);

        return $dataProvider;
    }
}
