<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gerer".
 *
 * @property integer $user_id
 * @property integer $sect_id
 * @property Secteur $secteur
 * @property User $user
 */
class Gerer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gerer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'sect_id'], 'required'],
            [['user_id', 'sect_id'], 'integer'],
            [['sect_id'], 'exist', 'skipOnError' => true, 'targetClass' => Secteur::className(), 'targetAttribute' => ['sect_id' => 'sect_id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'sect_id' => 'Sect ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecteur()
    {
        return $this->hasOne(Secteur::className(), ['sect_id' => 'sect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
