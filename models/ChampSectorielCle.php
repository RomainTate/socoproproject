<?php

namespace app\models;


/**
 * This is the model class for table "champ_sectoriel_cle".
 *
 * @property integer $champ_sectoriel_cle_id
 * @property integer $sect_id
 * @property string $champ_sectoriel_cle_field
 * @property integer $priority
 *
 * @property Secteur $secteur
 * @property ChampSectorielValeur[] $champSectValue
 */
class ChampSectorielCle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $labelField;
    public $type;
    public $nombreElements;
    
    public static function tableName()
    {
        return 'champ_sectoriel_cle';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['champ_sectoriel_cle_field', 'labelField', 'type'], 'required'],
            [['type', 'nombreElements'], 'integer'],
            [['nombreElements'],'number', 'max' => 35],
            [['champ_sectoriel_cle_field', 'labelField'], 'string', 'max' => 55],
            [['sect_id'], 'exist', 'skipOnError' => true, 'targetClass' => Secteur::className(), 'targetAttribute' => ['sect_id' => 'sect_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'champ_sectoriel_cle_id' => 'Champ Sectoriel Cle ID',
            'sect_id' => 'Champ Sectoriel Cle Sect ID',
            'champ_sectoriel_cle_field' => 'Nom technique du champ sectoriel',
            /******* attributes *****/
            'labelField' => 'Label du champ sectoriel',
            'type' => 'Type du champ',
            'nombreElements' => 'Nombre d\'élements'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecteur()
    {
        return $this->hasOne(Secteur::className(), ['sect_id' => 'sect_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChampSectorielValeurs()
    {
        return $this->hasMany(ChampSectorielValeur::className(), ['champ_sectoriel_cle_id' => 'champ_sectoriel_cle_id']);
    }
}
