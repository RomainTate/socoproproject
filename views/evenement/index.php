<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EvenementSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Evenements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="evenement-index">

    <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
                <?= Html::a('Ajouter un Evènement', ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="x_panel">
    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//            'evenement_id',
            'evenement_nom',
            [
                'label' => 'Type',
                'attribute' => 'evenement_type',
                'filter' => ['1' => 'Assemblée sectorielle', '2' => 'Assemblée générale', '3' => 'Réunion Collège', '4' => 'Voyage / Colloque', '5' => 'Autre' ],
                'value' => function ($model) {
                    switch ($model->evenement_type) {
                                    case 1 : return 'Assemblée sectorielle';
                                    case 2 : return "Assemblée générale";
                                    case 3 : return "Réunion Collège";
                                    case 4 : return "Voyage / Colloque";
                                    case 5 : return "Autre";
                                    default : return null;
                                }
                }
            ],
//            'adr_id',
            [
                'attribute' => 'evenement_date',
                'value' => function ($model) {
                return Yii::$app->formatter->asDate($model->evenement_date);},
                'format' => 'raw',
                'filter' => yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'evenement_date',
                    'dateFormat' => 'yyyy-MM-dd',
//                    'dateFormat' => 'dd MMMM yyyy',
                    ]),
            ],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]);
    ?>
    </div>
</div>
