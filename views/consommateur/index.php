<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ConsommateurSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$search = Yii::$app->request->pathInfo == 'consommateur/search'? true : false;
if(Yii::$app->request->post('export_type')) {
    $exporting = true;
} else {
    $exporting = false;
}
$this->title = 'Consommateurs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consommateur-index">

    <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createConso')) : ?>
                <?= Html::a('Ajouter un Consommateur', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        <?php endif; ?>
    </div>
    <?php if($search) : ?>
        <div class="x_panel">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    <?php endif;?>
    
    <?php if (Yii::$app->session->hasFlash('success')): ?>
    <div class="alert alert-success alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?= Yii::$app->session->getFlash('success') ?>
    </div>
    <?php endif; ?>

    <?php if (Yii::$app->session->hasFlash('danger')): ?>
    <div class="alert alert-danger alert-dismissable">
        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
        <?= Yii::$app->session->getFlash('danger') ?>
    </div>
    <?php endif ?>
    
    <?php if (!$search || Yii::$app->request->get('ConsommateurSearch') != null) : 
     $gridColumns = [
            [
                'attribute' => 'contact.cont_civilite',
                'label' => 'Civ',
                'format' => 'text',
            ],
//            'conso_lieu_contact',
//            'cont_id',
         [
                'attribute' => 'contactNom',
                'label' => 'Nom',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactPrenom',
                'label' => 'Prénom',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactTelephonePrincipal',
                'label' => 'Téléphone',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactGSMPrincipal',
                'label' => 'GSM',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactEmailPrincipal',
                'label' => 'E-mail',
                'format' => 'html',
                 'value' => function ($model) use($exporting) {
                    if($exporting) return $model->contactEmailPrincipal; 
                    return Html::a(yii\helpers\BaseStringHelper::truncate($model->contactEmailPrincipal, 50, '...'), 'mailto:'.$model->contactEmailPrincipal);
                },
            ],
         
//            'consoLieuContactName',
            [
                'attribute' => 'rue',
                'label' => 'Rue',
                'format' => 'text',
            ],
            [
                'attribute' => 'localite',
                'label' => 'Localité',
                'format' => 'text',
            ],
            [
                'attribute' => 'province',
                'label' => 'Province',
                'format' => 'text',
            ],
            [
                'attribute' => 'newsletter',
                'label' => 'Newsletters',
                'format' => 'html',
                'value' => function($model) {
                    if (!$model->contact->newsletters) {
                        return null;
                    }
                    foreach ($model->contact->newsletters as $newsletter) {
                        $newslettersNames[] = '<span style="display:block;">' . $newsletter->newsletter_nom . '</span>';
                    }
                    return implode(",", $newslettersNames);
                }
            ],
//            [
//                'attribute' => 'nomentr',
//                'label' => 'Entreprises / Fonctions',
//                'format' => 'html',
//                'value' => function($model) {
//                    if(!$model->contact->entreprises) {
//                        return null;
//                    }
//                    foreach ($model->contact->entreprises as $entreprise) {
//                        
//                        $fonction = app\models\Appartenir::find()->where(['entr_id' => $entreprise->entr_id, 'cont_id' => $model->cont_id])->one()->fonction;
//                        if (!$fonction) $fonction = 'inconnue';
//                        $entreprisesNames[] = '<span style="display:block;"> -' . $entreprise->entr_societe . ' ('. $fonction .')</span>';
//                    }
//                    return implode("<hr />", $entreprisesNames);
//                }
//            ],
            [
                'attribute' => 'conso_fonction',
                'label' => 'Fonction',
                'format' => 'text',
            ],
            [
                'attribute' => 'conso_societe',
                'label' => 'Société',
                'format' => 'text',
            ],
            [
                'label' => 'Intérêts',
                'attribute' => 'secteursID',
                'format' => 'html',
                'filter' => yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->asArray()->all(), 'sect_id', 'sect_nom'),
                'value' => function($model) {
                    $secteurNames = array();
                    $secteursArray = \yii\helpers\ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
                    asort($secteursArray);
                    foreach ($secteursArray as $secteur) {
                        $secteurNames[] = '<span style="display:block;">' . $secteur . '</span>';
                    }
                    return implode(", ", $secteurNames);
                }
            ],
            ['class' => 'kartik\grid\ActionColumn', 'hiddenFromExport' => true,
                'buttons' => [
                    'view' => function($url, $model) {
                        $url = \Yii::$app->urlManager->createUrl(['consommateur/view', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['class' => 'action', 'title' => Yii::t('yii', 'View')]);
                    },
                    'update' => function($url, $model) {
                        if (!\Yii::$app->user->can('createConso')) {
                            return null;
                        }
                        $url = \Yii::$app->urlManager->createUrl(['consommateur/update', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'action', 'title' => Yii::t('yii', 'Update')]);
                    },
                    'delete' => function($url, $model) {
                        if (!\Yii::$app->user->can('deleteConso')) {
                            return null;
                        }
                        $url = \Yii::$app->urlManager->createUrl(['consommateur/delete', 'id' => $model->cont_id]);
                        return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['class' => 'action', 'title' => Yii::t('yii', 'Delete'), 'data-confirm' => 'Etes vous sûr de vouloir supprimer ce contact?', 'data-method' => 'post']);
                    }
                ],
            ],
                ]
        ?>
    <hr />
    <div class="x_panel">
        <div class="pull-right">
            Exportation :
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'exportConfig' => [
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_EXCEL => false
            ],
            'noExportColumns' => [ count($gridColumns) - 1]
        ]);?>
        </div>
    </div>

    <div class="x_panel">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'id' => 'consoGrid',
        'filterModel' => $searchModel,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
        'columns' => $gridColumns,
        'responsiveWrap' => false
    ]); ?>
    </div>
    <?php endif;?>
</div>
<?php if (!$search || Yii::$app->request->get('ConsommateurSearch') != null) : 
    $responsiveCSS = \app\components\WhResponsive::writeResponsiveCss($gridColumns, 'consoGrid');
    $this->registerCss($responsiveCSS);
    $this->registerJsFile(\yii\helpers\Url::home().'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);
endif ?>