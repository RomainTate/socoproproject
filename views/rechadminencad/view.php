<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RechAdminEncad */

$this->title = $model->contact->cont_nom . ' ' . $model->contact->cont_prenom;
$this->params['breadcrumbs'][] = ['label' => 'Rech Admin Encads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rech-admin-encad-view">

    <h1 class="pull-left">Profil de <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?></h1>
    <div class="pull-right">
        <?php
        if (Yii::$app->user->can('deleteREA'))
            echo Html::a('Supprimer le contact', ['delete', 'id' => $model->cont_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Êtes-vous sûr de vouloir supprimer ce contact?',
                    'method' => 'post',
                ],
            ])
            ?>
    </div> 
    <div class="clearfix"></div>
    <div class="row">

        <div class="col-md-3 col-sm-3 col-xs-12 profile_left" >
            <div class="x_panel" >
                <div class="x_content">
                    <div class="text-right">
                        <p style="font-size: 11px">
                            <?php echo ($model->contactCreatedAt) ? 'Ajouté le ' . Yii::$app->formatter->asDatetime($model->contactCreatedAt) : 'date d\'ajout inconnue'; ?>
                        </p>
                    </div>

                    <div class="img-profil">
                        <img src="../css/images/manager.png" alt="producteur" />
                    </div>

                    <h3>
                        <?= $model->contact->cont_civilite . ' ' . $model->contact->cont_prenom . ' ' . $model->contact->cont_nom ?>
                    </h3>
                    <div class="mainInfos">
                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->contactGSMPrincipal) : ?> 
                                <?= $model->contactGSMPrincipal ?>
                            <?php else : ?>
                                Numéro GSM inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createREA'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_gsm_principal']),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                            <?php if ($model->contactEmailPrincipal) : ?>
                                <?= $model->contactEmailPrincipal ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createREA'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_email_principal']),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>

                        <p> <i class="fa fa-calendar" aria-hidden="true"></i> 
                            <?php if ($model->contactAnneeNaissance) : ?>
                                <?= Yii::$app->formatter->asDate($model->contactAnneeNaissance) ?> 
                            <?php else : ?>
                                Année de naissance inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createREA'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/contact/quickupdate', 'id' => $model->cont_id, 'attribute' => 'cont_annee_naissance']),
                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                            ?>
                        </p>

                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> <?php if ($model->contact->adresse) : ?> 
                                <?=
                                $model->contact->adresse->adr_numero . ' ' . $model->contact->adresse->adr_rue . ' ' .
                                $model->contact->adresse->adr_code_postal . ' ' . $model->contact->adresse->adr_ville
                                ?> 
                                <?php
                            if (Yii::$app->user->can('createREA'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/adresse/update?id=' . $model->contact->adr_id),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                ?>

                            <?php else : ?>
                                Adresse inconnue <?php
                            if (Yii::$app->user->can('createREA'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/adresse/create?entr_id=' . $model->contact->adr_id),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                ?>
                            <?php endif; ?></p>
                    </div>
                    <?php
                            if (Yii::$app->user->can('createREA'))
                                echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations du contact', ['value' => \yii\helpers\Url::to(['/rechadminencad/update', 'id' => $model->cont_id]),
                        'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                    ?>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-sm-9 col-xs-12">
            <div class="x_panel secondInfos">

                <div class="x_content">
                    <h2>Autres informations
                    </h2> 

                    <ul class="autres-infos">

                        <li>
                            <i class="glyphicon glyphicon-chevron-right"></i>
                            <?php if ($model->secteurs) : ?>
                                Centres d'intérêts :
                                <ul class="fa-ul">
                                    <?php
                                    foreach ($model->secteurs as $secteur) {
                                        echo "<li>" . $secteur->sect_nom . "</li>";
                                    }
                                    ?>
                                </ul>
                            <?php else : ?> 
                                aucun centre d'intérêt
                            <?php endif; ?> 

                        </li>
                    </ul>
                    <?php
                    if (Yii::$app->user->can('createREA'))
                        echo '<div class="text-right">' .
                        \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/rechadminencad/addmoreinfos', 'contactId' => $model->cont_id]),
                        'name' => 'modalButton', 'class' => "btn btn-info"]). '</div>'
                    ?>
                    <hr />
                    <h2>Entreprises </h2>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item active">
                            <a class="nav-link active" data-toggle="tab" href="#Associations" role="tab" aria-controls="Associations">Associations</a>
                        </li><li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#Divers" role="tab" aria-controls="Divers">Autres Entreprises</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="Associations"  role="tabpanel">
                                <?php if (Yii::$app->user->can('createProducteur')) : ?>
                                <div class="text-right">
                                <?= Html::button('Ajouter Association', ['value' => \yii\helpers\Url::to(['/contact/addentreprise', 'contactId' => $model->cont_id, 'type' => 'association']), 'class' => 'btn btn-success', 'name' => 'modalButton']) ?> 
                                </div>
                                <?php
                            endif;
                            if (!$model->contact->entreprises) {
                                echo 'Aucune Association';
                            } else {
                                $associations = array();
                                $diversEntreprises = array();
                                foreach ($model->contact->entreprises as $entreprise) {
                                    if ($entreprise->association) {
                                        array_push($associations, $entreprise);
                                    } else if ($entreprise->divers) {
                                        array_push($diversEntreprises, $entreprise);
                                    }
                                }
                                if (!$associations) {
                                    echo 'Aucune Association';
                                } else {
                                    foreach ($associations as $association) {
                                        echo "<h5>" . $association->entr_nom . "</h5>";
                                        echo DetailView::widget([
                                            'model' => $association,
                                            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
                                            'attributes' => [
                                                'entr_forme_juridique',
                                                'entr_telephone_principal',
                                                'entr_email_principal',
                                                'entr_site_internet',
                                                'association.association_type',
                                                [
                                                    'label' => 'Adresse',
                                                    'format' => 'raw',
                                                    'value' => function ($association) {
                                                        if (!$association->adresse) {
                                                            if (!Yii::$app->user->can('createAssociation')) {
                                                                return 'adresse inconnue';
                                                            }
                                                            return 'adresse inconnue' . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/adresse/create?entr_id=' . $association->entr_id),
                                                                        'name' => 'modalButton', 'class' => "fa fa-pencil"]);
                                                        }
                                                        if (Yii::$app->user->can('createAssociation'))
                                                            return $association->adresse->adr_numero . ' ' . $association->adresse->adr_rue . ' ' . $association->adresse->adr_code_postal . ' ' . $association->adresse->adr_ville
                                                                    . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/adresse/update?id=' . $association->adr_id),
                                                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                                        return $association->adresse->adr_numero . ' ' . $association->adresse->adr_rue . ' ' . $association->adresse->adr_code_postal . ' ' . $association->adresse->adr_ville;
                                                    }
                                                ],
                                            ],
                                        ]);
                                        if ((Yii::$app->user->can('createAssociation')) || Yii::$app->user->can('createREA')) {
                                            echo '<div class="text-right">';
                                            if (Yii::$app->user->can('createAssociation'))
                                                echo \yii\bootstrap\Html::button('Modifier', ['value' => \yii\helpers\Url::to('/association/update?id=' . $association->entr_id), 'name' => 'modalButton', 'class' => "btn btn-info"]);
                                            if (Yii::$app->user->can('createREA'))
                                                echo \yii\bootstrap\Html::a("Enlever l'association", \yii\helpers\Url::to(['/contact/deleteentreprise', 'contactId' => $model->cont_id, 'entr_id' => $association->entr_id]), ['class' => 'btn btn-danger', 'data-confirm' => 'Retirer ce contact de cette exploitation ? Ceci ne supprimera pas l\'exploitation de la base de données']);
                                            echo '</div>';
                                        }
                                        echo '<hr />';
                                    }
                                }
                            }
                            ?>
                        </div>
                        <div class="tab-pane" id="Divers"  role="tabpanel">
                                <?php if (Yii::$app->user->can('createREA')) : ?>
                                <div class="text-right">
                                <?= Html::button('Ajouter Entreprise', ['value' => \yii\helpers\Url::to(['/contact/addentreprise', 'contactId' => $model->cont_id, 'type' => 'entreprisedivers']), 'class' => 'btn btn-success', 'name' => 'modalButton']) ?> 
                                </div>
                                <?php
                            endif;
                            if (!$model->contact->entreprises) {
                                echo 'Aucune Entreprise';
                            } else {
                                if (!$diversEntreprises) {
                                    echo 'Aucune Entreprise';
                                } else {
                                    foreach ($diversEntreprises as $diversEntreprise) {
                                        echo "<h5>" . $diversEntreprise->entr_nom . "</h5>";
                                        echo DetailView::widget([
                                            'model' => $diversEntreprise,
                                            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
                                            'attributes' => [
                                                'entr_forme_juridique',
                                                'entr_telephone_principal',
                                                'entr_email_principal',
                                                'entr_site_internet',
                                                'divers.type_entreprise',
                                                [
                                                    'label' => 'Adresse',
                                                    'format' => 'raw',
                                                    'value' => function ($entreprise) {
                                                        if (!$entreprise->adresse) {
                                                            if(!Yii::$app->user->can('createEntrepriseDivers')) {
                                                                return 'adresse inconnue' ;
                                                            }
                                                            return 'adresse inconnue' . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/adresse/create?entr_id=' . $entreprise->entr_id),
                                                                        'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                                        }
                                                        if(!Yii::$app->user->can('createEntrepriseDivers')) {
                                                            return $entreprise->adresse->adr_numero . ' ' . $entreprise->adresse->adr_rue . ' ' . $entreprise->adresse->adr_code_postal . ' ' . $entreprise->adresse->adr_ville;
                                                        }
                                                        return $entreprise->adresse->adr_numero . ' ' . $entreprise->adresse->adr_rue . ' ' . $entreprise->adresse->adr_code_postal . ' ' . $entreprise->adresse->adr_ville
                                                                . \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to('/adresse/update?id=' . $entreprise->adr_id),
                                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                                    }
                                                ],
                                            ],
                                        ]);
                                        if (Yii::$app->user->can('createREA') || Yii::$app->user->can('createEntrepriseDivers')) {
                                            echo '<div class="text-right">';
                                            if(Yii::$app->user->can('createEntrepriseDivers'))
                                                echo \yii\bootstrap\Html::button('Modifier', ['value' => \yii\helpers\Url::to('/entreprisedivers/update?id=' . $diversEntreprise->entr_id), 'name' => 'modalButton', 'class' => "btn btn-info"]);
                                            if(Yii::$app->user->can('createREA'))
                                                echo \yii\bootstrap\Html::a("Enlever l'association", \yii\helpers\Url::to(['/contact/deleteentreprise', 'contactId' => $model->cont_id, 'entr_id' => $diversEntreprise->entr_id]), ['class' => 'btn btn-danger', 'data-confirm' => 'Retirer ce contact de cette entreprise ? Ceci ne supprimera pas l\'entreprise de la base de données']);
                                            echo '</div>';
                                        }
                                        echo '<hr />';
                                    }
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php
                    echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
                        'entity' => (string) 'rea-' . $model->cont_id, // type and id
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile('js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>