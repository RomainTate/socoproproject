<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Producteur */
/* @var $contactModel app\models\Contact */

$this->title = 'Ajouter un producteur';
if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username != 'foire') {
    $this->params['breadcrumbs'][] = ['label' => 'Producteurs', 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="producteur-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="col-sm-6">
            <?php
            if (isset($existingContacts)) :
                echo $this->render('_form', [
                    'model' => $model,
                    'contactModel' => $contactModel,
                    'existingContacts' => $existingContacts,
                    'autreInfos' => $autreInfos,
                    'adrModel'=>$adrModel
                ]);
            else:
                echo $this->render('_form', [
                    'model' => $model,
                    'contactModel' => $contactModel,
                    'autreInfos' => $autreInfos,
                    'adrModel'=>$adrModel
                ]);
            endif; ?>
        </div>
    </div>
</div>
