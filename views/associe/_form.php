<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Associe */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="associe-form">

    <?php $form = ActiveForm::begin([
        'id' => 'associeForm'
    ]); ?>
    <?= $form->field($contactModel, 'cont_civilite')->textInput(['maxlength' => true])->dropDownList(['Mr' => 'Mr' , 'Mme' => 'Mme']) ?>

    <?= $form->field($contactModel, 'cont_nom')->textInput(['maxlength' => true]) ?>
    <?= $form->field($contactModel, 'cont_prenom')->textInput(['maxlength' => true]) ?>

    <?= $form->field($contactModel, 'cont_gsm_principal')->textInput(['maxlength' => true]) ?>
    <?= $form->field($contactModel, 'cont_email_principal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'assoc_type')->dropDownList(["3"=>"Autre","2"=>"Famille","1"=>"Epoux/Epouse"]); ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'créer un associé' : 'mettre à jour l\'associé', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
