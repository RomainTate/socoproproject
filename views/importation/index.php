<?php

if (Yii::$app->session->hasFlash('error')): ?>
    <div class="alert alert-warning alert-dismissable">
    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
    <h5><i class="icon fa fa-times"></i> Attention !</h5>
    <?= Yii::$app->session->getFlash('error') . '<br />' ?>
    </div>
<?php endif;?>


<div class="x_panel">
<h2>Importation de contacts</h2>
<?php
yii\bootstrap\ActiveForm::begin([
//    'action' => 'import',
    'method' => 'post',
    'options' => [
        'enctype' => 'multipart/form-data'
    ]
]);

echo kartik\file\FileInput::widget([
                    'name' => 'uploadedFile',
                        'pluginOptions' => [
                            'showPreview' => false,
                            'showCaption' => true,
                            'showRemove' => true,
                            'showUpload' => false
                        ]
                ]);
?>
<div>
    <?=    \yii\helpers\Html::radioList('type', null,['journ' => 'journalistes' ,'conso' => 'consommateurs', 'prod' => 'producteurs'] ,['label' => 'type de contacts', 'id' => 'typeContact'])?>
    <div id ='secteurs' style="display : none;">
    <?php foreach (\yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->all(), 'sect_id', 'sect_nom') as $sect_id => $sect_nom) {
        if($sect_nom == 'Ovins') {
            echo \yii\helpers\Html::radio('secteur', null, ['label' => 'Ovins / Caprins' , 'value' => $sect_id]);
        } else if($sect_nom == 'Caprins') {
            continue;
        } else {
            echo \yii\helpers\Html::radio('secteur', null, ['label' => $sect_nom , 'value' => $sect_id]);
        }
    }?>
    </div>
</div>
<?php //echo yii\bootstrap\Html::radioList('secteur', null, \yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->all(),'sect_id', 'sect_nom'));
echo yii\bootstrap\Html::submitButton('Envoyer', ['class' => 'btn btn-success']);
yii\bootstrap\ActiveForm::end();
?>

</div>
<?php
$this->registerJsFile(\yii\helpers\Url::home().'js/importation.js', ['depends' => [yii\web\JqueryAsset::className()]]);
