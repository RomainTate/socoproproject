<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Entreprise;

/**
 * EntrepriseSearch represents the model behind the search form about `app\models\Entreprise`.
 */
class EntrepriseSearch extends Entreprise
{
    public $nature;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entr_id', 'adr_id'], 'integer'],
            [['entr_nom', 'entr_forme_juridique', 'entr_telephone_principal', 'entr_email_principal', 'entr_site_internet', 'nature'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Entreprise::find()->select([
                'entreprise.entr_id', 'entreprise.entr_nom', 'entreprise.entr_forme_juridique', 'entreprise.entr_telephone_principal',
                'entreprise.entr_email_principal', 'entreprise.entr_site_internet', 'entreprise.adr_id', 'nature' => "(CASE 
                WHEN association.entr_id IS NOT NULL THEN 'Association' 
                WHEN exploitation.entr_id IS NOT NULL THEN 'Exploitation' 
                WHEN journal.entr_id IS NOT NULL THEN 'Journal' 
                ELSE 'entreprise !' END)"])
                ->leftJoin('Association', 'association.entr_id = entreprise.entr_id')
                ->leftJoin('Exploitation', 'exploitation.entr_id = entreprise.entr_id')
                ->leftJoin('Journal', 'journal.entr_id = entreprise.entr_id');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'entr_id' => $this->entr_id,
            'adr_id' => $this->adr_id,
        ]);

        $query->andFilterWhere(['like', 'entr_nom', $this->entr_nom])
            ->andFilterWhere(['like', 'entr_forme_juridique', $this->entr_forme_juridique])
            ->andFilterWhere(['like', 'entr_telephone_principal', $this->entr_telephone_principal])
            ->andFilterWhere(['like', 'entr_email_principal', $this->entr_email_principal])
            ->andFilterWhere(['like', 'entr_site_internet', $this->entr_site_internet]);

        return $dataProvider;
    }
}
