<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "objet".
 *
 * @property integer $objet_id
 * @property integer $objet_nom
 *
 * @property Suivi[] $suivis
 */
class Objet extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'objet';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['objet_nom'], 'required'],
            [['objet_nom'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'objet_id' => 'Objet ID',
            'objet_nom' => 'Objet Nom',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuivis()
    {
        return $this->hasMany(Suivi::className(), ['suivi_id' => 'suivi_id'])->viaTable('suivi_has_objet', ['objet_id' => 'objet_id']);
    }
}
