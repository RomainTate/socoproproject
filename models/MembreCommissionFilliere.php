<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "membre_commission_filliere".
 *
 * @property integer $cont_id
 * @property integer $sect_id
 *
 * @property Contact $contact
 * @property Secteur $secteur
 */
class MembreCommissionFilliere extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'membre_commission_filliere';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id', 'sect_id'], 'required'],
            [['cont_id', 'sect_id'], 'integer'],
            [['cont_id', 'sect_id'], 'unique', 'targetAttribute' => ['cont_id', 'sect_id'], 'message' => 'The combination of Cont ID and Sect ID has already been taken.'],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['sect_id'], 'exist', 'skipOnError' => true, 'targetClass' => Secteur::className(), 'targetAttribute' => ['sect_id' => 'sect_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'sect_id' => 'Sect ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecteur()
    {
        return $this->hasOne(Secteur::className(), ['sect_id' => 'sect_id']);
    }
}
