<?php

namespace app\models;


/**
 * This is the model class for table "consommateur".
 *
 * @property integer $conso_lieu_contact
 * @property string $conso_fonction 
 * @property string $conso_societe 
 * @property integer $cont_id
 * @property interger $needConfirm 
 *
 * @property Contact $contact
 * @property Lieuprisecontact $consoLieuContact
 */
class Consommateur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consommateur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conso_lieu_contact', 'cont_id', 'needConfirm'], 'integer'],
//            [['cont_id'], 'required'],
            [['conso_fonction', 'conso_societe'], 'string', 'max' => 100],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['conso_lieu_contact'], 'exist', 'skipOnError' => true, 'targetClass' => Lieuprisecontact::className(), 'targetAttribute' => ['conso_lieu_contact' => 'lieu_prise_contact_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'conso_lieu_contact' => 'Lieu prise de contact',
            'cont_id' => 'Cont ID',
            'conso_fonction' => 'Fonction',
            'conso_societe' => 'Société',
            /* related attributes*/
            'contactNom' => 'Nom',
            'contactPrenom' => 'Prénom',
            'contactTelephonePrincipal' => 'Téléphone',
            'contactGSMPrincipal' => 'Numéro GSM',
            'contactEmailPrincipal' => 'E-mail',
            'consoLieuContactName' => 'Lieu prise de contact',
            /*   */
            'nomentr' => 'Societe',
            'fct' => 'Fonction'
        ];
    }
    
    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        return parent::beforeSave($insert);
    }
    public function afterSave($insert, $changedAttributes) {
        if (!$insert && array_key_exists("conso_lieu_contact", $changedAttributes)) {
            if($changedAttributes['conso_lieu_contact'] != '' && 
                    $this->find()->where(['conso_lieu_contact' => $changedAttributes['conso_lieu_contact']])->one() == null ) {
                Lieuprisecontact::find()->where(['lieu_prise_contact_id' => $changedAttributes['conso_lieu_contact']])->one()->delete();
            }
        }
        return parent::afterSave($insert, $changedAttributes);
    }
    
    public function beforeDelete() {
        \rmrevin\yii\module\Comments\models\Comment::deleteAll(['entity' => 'consommateur-'.$this->cont_id]);
        Interesser::deleteAll(['cont_id' => $this->cont_id]);
        Appartenir::deleteAll(['cont_id' => $this->cont_id]);
        return parent::beforeDelete();
    }

    public function afterDelete() {
        Contact::findOne($this->cont_id)->delete();
        return parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact() {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
    
    /* Getter for contact nom */
    public function getContactNom() {
        return $this->contact->cont_nom;
    }
    
    /* Getter for contact prenom */
    public function getContactPrenom() {
        return $this->contact->cont_prenom;
    }

    public function getContactCivilite() {
        return $this->contact->cont_civilite;
    }
    
    public function getContactTelephonePrincipal() {
        return $this->contact->cont_telephone_principal;
    }

    public function getContactGSMPrincipal() {
        return $this->contact->cont_gsm_principal;
    }
    
    public function getContactEmailPrincipal() {
        return $this->contact->cont_email_principal;
    }
    
//    public function getContactAnneeNaissance() {
//        return $this->contact->cont_annee_naissance;
//    }
    public function getContactCreatedAt() {
        return $this->contact->cont_created_at;
    }
    
    public function getRue() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_rue;
    }
    public function getLocalite() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_ville;
    }
    
    public function getProvince() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_province;
    }
    
    public function getCodepostal() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_code_postal;
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsoLieuContact()
    {
        return $this->hasOne(Lieuprisecontact::className(), ['lieu_prise_contact_id' => 'conso_lieu_contact']);
    }
    
    public function getConsoLieuContactName()
    {
        if (!$this->consoLieuContact) return null;
        return $this->consoLieuContact->lieu;
    }
    
    public function getSecteurs()
    {
        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable('interesser', ['cont_id' => 'cont_id']);
    }
    
//    public function setSecteurs($v)
//    {     
//        $this->secteurs = $v;
//    }
    
//    public function getSecteursID() {
//        if($this->secteurs == null) return null;
//        return $this->secteurs->sect_id;
//    }
    
//    public function getNomentr() {
//        return null;
//    }
//    public function getFct() {
//        return null;
//    }
    
    public function getNewsletter() {
        if(!$this->contact)
            return null;
        if(!$this->contact->newsletters)
            return null;
        return $this->contact->newsletters;
    }
    
}
