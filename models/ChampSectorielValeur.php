<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "champ_sectoriel_valeur".
 *
 * @property integer $champ_sectoriel_cle_id
 * @property integer $cont_id
 * @property string $champ_sectoriel_valeur_val
 *
 * @property Contact $contact
 * @property ChampSectorielCle $champSectorielCle
 */
class ChampSectorielValeur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'champ_sectoriel_valeur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['champ_sectoriel_valeur_val'], 'required'],
            [['champ_sectoriel_cle_id', 'cont_id'], 'integer'],
            [['champ_sectoriel_valeur_val'], 'string', 'max' => 25],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['champ_sectoriel_cle_id'], 'exist', 'skipOnError' => true, 'targetClass' => ChampSectorielCle::className(), 'targetAttribute' => ['champ_sectoriel_cle_id' => 'champ_sectoriel_cle_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'champ_sectoriel_cle_id' => 'Champ Sectoriel Cle ID',
            'cont_id' => 'Cont ID',
            'champ_sectoriel_valeur_val' => 'Champ Sectoriel Valeur Val',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChampSectorielCle()
    {
        return $this->hasOne(ChampSectorielCle::className(), ['champ_sectoriel_cle_id' => 'champ_sectoriel_cle_id']);
    }
}
