<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Consommateur */

$this->title = 'Ajouter Consommateur';
if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username != 'foire') {
    $this->params['breadcrumbs'][] = ['label' => 'Consommateurs', 'url' => ['index']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="consommateur-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <?= $this->render('_form', [
        'model' => $model,
        'contactModel' => $contactModel,
        'adrModel' => $adrModel
    ]) ?>
    </div>
</div>
