 

<div id="result">
    <table class="table table-bordered table-condensed table-responsive" id="table2excel" style="font-size: 12px" >
        <?php foreach ($dataImport as $row) : ?>
            <tr>
            <?php foreach ($row as $k => $v) : ?>
                <?php if (!is_array($v)) : ?>
                <td>
                    <?= $v ?> 
                </td>
                <?php else : ?>
                <td colspan="11">
                    <?= implode(', ', $v) ?>
                </td>
                <?php endif; ?>
            <?php endforeach; ?>
            </tr>
        <?php endforeach; ?>
    </table>
</div>
<button>Exporter</button>
<?=
$this->registerJsFile(\yii\helpers\Url::home() . 'js/jquery.table2excel.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJsFile(\yii\helpers\Url::home() . 'js/exportTable.js', ['depends' => [yii\web\JqueryAsset::className()]]);
