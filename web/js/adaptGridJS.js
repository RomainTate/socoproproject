var CURRENT_URL = window.location.href.split('#')[0].split('?')[0],
    $BODY = $('body'),
    $SIDEBAR_MENU = $('#sidebar-menu'),
    $SIDEBAR_FOOTER = $('.sidebar-footer'),
    $LEFT_COL = $('.left_col'),
    $RIGHT_COL = $('.right_col'),
    $NAV_MENU = $('.nav_menu'),
    $FOOTER = $('footer');
    

var setContentHeight = function () {
	// reset height
	$RIGHT_COL.css('min-height', $(window).height());

	var bodyHeight = $BODY.outerHeight(),
		footerHeight = $BODY.hasClass('footer_fixed') ? -10 : $FOOTER.height(),
		leftColHeight = $LEFT_COL.eq(1).height() + $SIDEBAR_FOOTER.height(),
		contentHeight = bodyHeight < leftColHeight ? leftColHeight : bodyHeight;

	// normalize content
	contentHeight -= $NAV_MENU.height() + footerHeight;

	$RIGHT_COL.css('min-height', contentHeight);
};

var forceSmallMenu = function () {
    if ($BODY.hasClass('nav-md')) {
        $SIDEBAR_MENU.find('li.active ul').hide();
        $SIDEBAR_MENU.find('li.active').addClass('active-sm').removeClass('active');
        var $li = $SIDEBAR_MENU.find('a').parent();
            $SIDEBAR_MENU.find('ul.child_menu').hide();
            $li.removeClass('active active-sm');
            $('ul:first', $li).hide();
            $BODY.toggleClass('nav-md nav-sm');
    }

    
    setContentHeight();
};  

 $(window).on('load resize', function () {
   $(".kv-grid-table").css('font-size', "13px");
   var f = $(".kv-grid-table").width() / $('.kv-grid-table').parent().width() * 100;
   var size = $(".kv-grid-table").css('font-size');
   size = parseInt(size, 10);
//   alert(size);
   if (f > 100) {
        forceSmallMenu();
   }
   f = $(".kv-grid-table").width() / $('.kv-grid-table').parent().width() * 100;
   
   while(f > 100 && size > 10) {
       size = size - 1;
       $('.kv-grid-table').css("font-size",(size) + "px");
       f = $(".kv-grid-table").width() / $('.kv-grid-table').parent().width() * 100;
//       alert(f);    
       size = $(".kv-grid-table").css('font-size');
       size = parseInt(size, 10);
//       alert(size);
   }
});
