<div class="x_panel">
    <h2 class="x_title">
        Rapport de l'importation du fichier
    </h2>
    <div class="x_content">
        <ul class="list-group">
            <li class="list-group-item list-group-item-success">
                Nombre de participants ajoutés : <?= $infoArray['ok']?>
            </li>
            <li class="list-group-item list-group-item-warning">
                Nombre de participants ignorés : <?= $infoArray['ignored']?>
            </li>
            <li class="list-group-item list-group-item-danger">
                Nombre de participants n'ayant pas pu être ajoutés : <?= $infoArray['erreur']?>
            </li>
        </ul>
        <?php if ($infoArray['erreur'] != 0) : ?>
        <div id="result">
            <table class="table table-bordered table-condensed table-responsive" id="table2excel">
                <tr>
                    <th>
                        Nom
                    </th>
                    <th>
                        Prénom
                    </th>
                    <th>
                        Erreurs
                    </th>
                </tr>
                <?php foreach ($rowData as $row) : ?>
                    <tr>
                        <?php foreach ($row as $rowItem) : ?>
                            <td>
                                <?php if (is_array($rowItem)) : ?>
                                    <?= implode('<br />', $rowItem) ?>
                                <?php else : ?>
                                    <?= $rowItem ?>
                                <?php endif ?>
                            </td>
                        <?php endforeach; ?>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <?php endif; ?>
        <a href="<?=yii\helpers\Url::home()?>evenement/view?id=<?=Yii::$app->request->get('evenement_id')?>" class="btn btn-primary active">Revenir sur la page de l'évènement</a>
        <?php if ($infoArray['erreur'] != 0) : ?>
        <button class="btn btn-info">Exporter les erreurs</button>
        <?php endif; ?>
    </div>
</div>

<?= $this->registerJsFile(\yii\helpers\Url::home().'js/jquery.table2excel.js', ['depends' => [yii\web\JqueryAsset::className()]]);
 $this->registerJsFile(\yii\helpers\Url::home().'js/exportTable.js', ['depends' => [yii\web\JqueryAsset::className()]]);