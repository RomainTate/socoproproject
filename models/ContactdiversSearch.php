<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Contactdivers;

/**
 * ContactdiversSearch represents the model behind the search form about `app\models\Contactdivers`.
 */
class ContactdiversSearch extends Contactdivers
{
    public $contactNom;
    public $contactPrenom;
    public $contactCivilite;
    public $contactTelephonePrincipal;   
    public $contactGSMPrincipal;
    public $contactEmailPrincipal;
//    public $contactAnneeNaissance;
    public $contactCreatedAt;
    public $secteursID;
    public $rue;
    public $localite;
    public $province;
    public $codepostal;
    public $nomentr;
    public $nomentrs;
    public $fcts;
    public $membrecollege;
    public $membrefiliere;
                    /*** ! **/
    public $contactGSMPrincipalNotNull = 0;
    public $contactEmailPrincipalNotNull = 0;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id',  'contactGSMPrincipalNotNull', 'contactEmailPrincipalNotNull', 'contactTelephonePrincipal', 'divers_AS_AG', 'membrecollege'], 'integer'],
            [[/*'cont_divers_type',*/ 'divers_fonction','localite', 'province', 'rue', 'codepostal'], 'string'],
            [['secteursID', 'contactNom', 'contactPrenom', 'contactGSMPrincipal', 'contactEmailPrincipal', 'contactTelephonePrincipal', 'divers_AS_AG', 'nomentr', 'nomentrs', 'membrefiliere'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Contactdivers::find();
        $query->joinWith('contact'); //Eager loading
        $query->joinWith('contact.adresse'); //Eager loading
        $query->joinWith('contact.entreprises'); //Eager loading
        $query->joinWith('contact.membrecollege'); //Eager loading
        $query->joinWith('secteurs'); //Eager loading
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'divers_fonction' => $this->divers_fonction,
//        ]);
//        
//        
        // filter by contact nom
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_nom LIKE "%' . $this->contactNom . '%"');
            }]);
            
        // filter by contact prenom
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_prenom LIKE "%' . $this->contactPrenom . '%"');
            }]);
            
        if (isset($params['ContactdiversSearch']['rue']) && $params['ContactdiversSearch']['rue'] != '') {
            $query->andFilterWhere(['like','adr_rue', $this->rue]);
        }
        if (isset($params['ContactdiversSearch']['localite']) && $params['ContactdiversSearch']['localite'] != '') {
            $query->andFilterWhere(['like','adr_ville', $this->localite]);
        }
        if (isset($params['ContactdiversSearch']['province']) && $params['ContactdiversSearch']['province'] != '') {
            $query->andFilterWhere(['like','adr_province', $this->province]);
        }
        if (isset($params['ContactdiversSearch']['codepostal']) && $params['ContactdiversSearch']['codepostal'] != '') {
            $query->andFilterWhere(['like','adr_code_postal', $this->codepostal]);
        }
        
        if (isset($params['ContactdiversSearch']['contactTelephonePrincipal']) && $params['ContactdiversSearch']['contactTelephonePrincipal'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_telephone_principal LIKE "%' . $this->contactTelephonePrincipal . '%"')
                    ;
                }]);
        }
        if (isset($params['ContactdiversSearch']['contactGSMPrincipal']) && $params['ContactdiversSearch']['contactGSMPrincipal'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_gsm_principal LIKE "%' . $this->contactGSMPrincipal . '%"')
                    ;
                }]);
        }

        if (isset($params['ContactdiversSearch']['contactEmailPrincipal']) && $params['ContactdiversSearch']['contactEmailPrincipal'] != '') { //fix bug if params is empty
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_email_principal LIKE "%' . $this->contactEmailPrincipal . '%"');
            }]);
        }
        
        
        if (isset($params['ContactdiversSearch']['contactGSMPrincipalNotNull']) && $params['ContactdiversSearch']['contactGSMPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_gsm_principal' => null]]);
            }]);
        }
        
        if (isset($params['ContactdiversSearch']['contactEmailPrincipalNotNull']) &&  $params['ContactdiversSearch']['contactEmailPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_email_principal' => null]]);
            }]);
        }
        if (isset($params['ContactdiversSearch']['divers_AS_AG']) &&  $params['ContactdiversSearch']['divers_AS_AG'] != '' ) {
            if($this->divers_AS_AG == 1) {
                $query->andFilterWhere(['divers_AS_AG' => [1,3]]);
            } else if ($this->divers_AS_AG == 2) {
                $query->andFilterWhere(['divers_AS_AG' => [2,3]]);
            } else {
                $query->andFilterWhere(['divers_AS_AG' => $this->divers_AS_AG]);
            }
        }
        if (isset($params['ContactdiversSearch']['membrecollege']) &&  $params['ContactdiversSearch']['membrecollege'] != '' ) {
            $membres = \yii\helpers\ArrayHelper::map(MembreCollege::find()->all(), 'cont_id', 'cont_id');
//                $query->joinWith(['contact.membrecollege cm'], true, 'inner join');
            if($params['ContactdiversSearch']['membrecollege'] == 1) {
                $query->andFilterWhere(['in','divers.cont_id', $membres ]);
            } else {
                $query->andFilterWhere(['not in','divers.cont_id', $membres ]);
            }
        }
        
        if (isset($params['ContactdiversSearch']['nomentr']) && $params['ContactdiversSearch']['nomentr'] != '') {
            $query->andFilterWhere(['or like','entr_societe', $params['ContactdiversSearch']['nomentr']]);
        }
        if (isset($params['ContactdiversSearch']['nomentrs']) && is_array($params['ContactdiversSearch']['nomentrs'])) {
            $query->andFilterWhere(['or like','entr_societe', $params['ContactdiversSearch']['nomentrs']]);
        }
        if (isset($params['ContactdiversSearch']['fcts']) && is_array($params['ContactdiversSearch']['fcts'])) {
            $query->andFilterWhere(['or like','fonction', $params['ContactdiversSearch']['fcts']]);
        }
        
//        $query->andFilterWhere(['like', 'divers_fonction', $this->divers_fonction]);
        
        if ($this->secteursID != null) {
            if (is_array($this->secteursID)) {
                foreach ($this->secteursID as $secteur_id) {
                    $query->orFilterWhere(['=', 'secteur.sect_id', $secteur_id]);
                }
            } else {
                $query->andFilterWhere(['=', 'secteur.sect_id', $this->secteursID]);
            }
        }
        if ($this->membrefiliere != null) {
            $query->joinWith('contact.secteursMembrecommission sm');
            if (is_array($this->membrefiliere)) {
                foreach ($this->secteursID as $secteur_id) {
                    $query->orFilterWhere(['=', 'sm.sect_id', $secteur_id]);
                }
            } else {
                $query->andFilterWhere(['=', 'sm.sect_id', $this->membrefiliere]);
            }
        }


        return $dataProvider;
    }
}
