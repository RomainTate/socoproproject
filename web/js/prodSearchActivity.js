
function removeParam(key, sourceURL) {
    var rtn = sourceURL.split("?")[0],
        param,
        params_arr = [],
        queryString = (sourceURL.indexOf("?") !== -1) ? sourceURL.split("?")[1] : "";
    if (queryString !== "") {
        params_arr = queryString.split("&");
        for (var i = params_arr.length - 1; i >= 0; i -= 1) {
            param = params_arr[i].split("=")[0];
            if (param === key) {
                params_arr.splice(i, 1);
            }
        }
        rtn = rtn + "?" + params_arr.join("&");
    }
    return rtn;
}

$("#producteursearchactivity-secteursid").change(function() {
    if ($(this).select2('data').length > 1) {
        $("input[name = 'ProducteurSearchActivity[secteursOperator]']").prop("disabled", false);
    } else {
        $("input[name = 'ProducteurSearchActivity[secteursOperator]']").prop("disabled", "disabled");
    }
});

$("#minInteraction").on('change', function () {
   var nbInteract = $('#minInteraction').val();
       if( $.isNumeric(nbInteract) && nbInteract > 0){
        $('#interactionMinDate').prop("disabled", false);
        $('#interactionMaxDate').prop("disabled", false);
    } else {
        $('#interactionMinDate').prop("disabled", "disabled");
        $('#interactionMaxDate').prop("disabled", "disabled");
    }
    
});

$("#minEvent,#minEventAS,#minEventAG,#minEventCOLLEGE,#minEventVOYAGE,#minEventAUTRE").on('change', function () {
    var nbEvent = Math.max ($('#minEvent').val(), $('#minEventAS').val(), $('#minEventAG').val(), $('#minEventCOLLEGE').val(), $('#minEventVOYAGE').val(), $('#minEventAUTRE').val());
    if( $.isNumeric(nbEvent) && nbEvent > 0){
        $('#eventMinDate').prop("disabled", false);
        $('#eventMaxDate').prop("disabled", false);
    } else {
        $('#eventMinDate').prop("disabled", "disabled");
        $('#eventMaxDate').prop("disabled", "disabled");
    }
});

$("#minSuivi,#minSuiviTEL,#minSuiviEMAIL,#minSuiviVISITE,#minSuiviFOIREEVENT").on('change', function () {
    var nbSuivi = Math.max ($('#minSuivi').val(), $('#minSuiviTEL').val(), $('#minSuiviEMAIL').val(), $('#minSuiviVISITE').val(), $('#minSuiviFOIREEVENT').val());
    if( $.isNumeric(nbSuivi) && nbSuivi > 0){
        $('#suiviMinDate').prop("disabled", false);
        $('#suiviMaxDate').prop("disabled", false);
    } else {
        $('#suiviMinDate').prop("disabled", "disabled");
        $('#suiviMaxDate').prop("disabled", "disabled");
    }
});

$(function() {
    $("#minEvent").trigger('change');
    $("#minSuivi").trigger('change');
    $("#minInteraction").trigger('change');
});

$("#allSuiviSort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'allSuivi') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-allSuivi";
            } else if (url[i].substring(5) == '-allSuivi') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=allSuivi";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-allSuivi";
            }
        }
    }
    
    if (noSort == 1) {
        //add 'sort=-allSuivi' param
        window.location.href = window.location.href + "&sort=-allSuivi";
    }
    
});

$("#nbInteractionSort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'nbInteraction') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-nbInteraction";
            } else if (url[i].substring(5) == '-nbInteraction') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=nbInteraction";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-nbInteraction";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-nbInteraction";
    }
});

$("#allEventSort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'allEvent') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-allEvent";
            } else if (url[i].substring(5) == '-allEvent') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=allEvent";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-allEvent";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-allEvent";
    }
});

$("#interactionQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'interactionQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-interactionQuery";
            } else if (url[i].substring(5) == '-interactionQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=interactionQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-interactionQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-interactionQuery";
    }
});
$("#suiviQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'suiviQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviQuery";
            } else if (url[i].substring(5) == '-suiviQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=suiviQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-suiviQuery";
    }
});
$("#suiviTelQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'suiviTelQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviTelQuery";
            } else if (url[i].substring(5) == '-suiviTelQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=suiviTelQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviTelQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-suiviTelQuery";
    }
});
$("#suiviEmailQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'suiviEmailQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviEmailQuery";
            } else if (url[i].substring(5) == '-suiviEmailQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=suiviEmailQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviEmailQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-suiviEmailQuery";
    }
});
$("#suiviVisiteQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'suiviVisiteQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviVisiteQuery";
            } else if (url[i].substring(5) == '-suiviVisiteQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=suiviVisiteQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviVisiteQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-suiviVisiteQuery";
    }
});
$("#suiviFoireEventQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'suiviFoireEventQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviFoireEventQuery";
            } else if (url[i].substring(5) == '-suiviFoireEventQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=suiviFoireEventQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-suiviFoireEventQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-suiviFoireEventQuery";
    }
});
$("#eventQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'eventQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventQuery";
            } else if (url[i].substring(5) == '-eventQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=eventQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-eventQuery";
    }
});
$("#eventASQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'eventASQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventASQuery";
            } else if (url[i].substring(5) == '-eventASQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=eventASQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventASQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-eventASQuery";
    }
});
$("#eventAGQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'eventAGQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventAGQuery";
            } else if (url[i].substring(5) == '-eventAGQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=eventAGQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventAGQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-eventAGQuery";
    }
});
$("#eventCollegeQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'eventCollegeQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventCollegeQuery";
            } else if (url[i].substring(5) == '-eventCollegeQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=eventCollegeQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventCollegeQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-eventCollegeQuery";
    }
});
$("#eventVoyageQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'eventVoyageQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventVoyageQuery";
            } else if (url[i].substring(5) == '-eventVoyageQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=eventVoyageQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventVoyageQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-eventVoyageQuery";
    }
});
$("#eventAutreQuerySort").on('click', function () {
    var url = location.search.substring(1).split('&');
    var noSort = 1;
    
    for (i= 0; i < url.length ; i++) {
        if (url[i].substring(0,4) == 'sort') {
            noSort = 0;
            if (url[i].substring(5) == 'eventAutreQuery') {
                //delete param and add '-allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventAutreQuery";
            } else if (url[i].substring(5) == '-eventAutreQuery') {
                //delete param and add 'allSuivi'
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=eventAutreQuery";
            } else {
                var newUrl = removeParam('sort', location.search);
                window.location.href = newUrl + "&sort=-eventAutreQuery";
            }
        }
    }
    
    if (noSort == 1) {
        window.location.href = window.location.href + "&sort=-eventAutreQuery";
    }
});