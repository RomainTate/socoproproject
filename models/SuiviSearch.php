<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Suivi;

/**
 * SuiviSearch represents the model behind the search form about `app\models\Suivi`.
 */
class SuiviSearch extends Suivi
{
    public $userID;
    public $prodName;
    public $suividates;
    public $suiviobjet_id;
    private $_minDate;
    private $_maxDate;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['suivi_id', 'cont_id', 'user_id', 'suivi_type_contact', 'userID', 'suivi_suite'], 'integer'],
            [['prodName', 'suividates', 'suiviobjet_id'], 'string'],
            [['suivi_commentaire', 'suivi_date', 'userID', 'suividates', 'suiviobjet_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Suivi::find();

        // add conditions that should always apply here
        $query->joinWith('contact.producteur');
        $query->joinWith('user');
        $query->joinWith('objets');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
        $dataProvider->setSort([
        'attributes' => [
//            'suividates'=> [
//                'asc' => ['suivi_date' => SORT_ASC],
//                'desc' => ['suivi_date' => SORT_DESC],
//            ],
            'suivi_commentaire'=>  false
//                'asc' => ['created_at' => SORT_ASC],
//                'desc' => ['created_at' => SORT_DESC],
//            ],
//            'defaultOrder' => ['suividates' => SORT_DESC]
        ]
    ]);
        $query->groupBy(['suivi_id']);
        $query->orderBy(['suivi_date' => SORT_DESC]); //empeche autres sort

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'suivi_id' => $this->suivi_id,
            'cont_id' => $this->cont_id,
            'user_id' => $this->user_id,
            'suivi_type_contact' => $this->suivi_type_contact,
            'suivi_date' => $this->suivi_date,
            'user.id' => $this->userID,
            'suivi_suite' => $this->suivi_suite
        ]);

//        $query->andFilterWhere(['like', 'suivi_objet', $this->suivi_objet]);
            $query->andFilterWhere(['like', 'suivi_commentaire', $this->suivi_commentaire]);
        
        if($this->prodName) {
            $query->andFilterWhere(['or', ['like', 'contact.cont_nom', $this->prodName], ['like', 'contact.cont_prenom', $this->prodName], 
                 ['like', 'producteur.prod_nom_exploitation', $this->prodName]]);
        }
        
//        if($this->minDate) {
//            $timezone = new \DateTimeZone(Yii::$app->timeZone);
//        $mysqlDate = \DateTime::createFromFormat("d-m-Y", $this->minDate, $timezone)->format("Y-m-d");
//        $query->andFilterWhere(['>=', 'suivi_date', $mysqlDate]);
//        }
        
        if($this->suividates) {
            $timezone = new \DateTimeZone(Yii::$app->timeZone);
            $explodeDates = explode(' | ', $this->suividates);
            $this->_minDate = \DateTime::createFromFormat("d-m-Y", $explodeDates[0], $timezone)->format("Y-m-d");
            $this->_maxDate = \DateTime::createFromFormat("d-m-Y", $explodeDates[1], $timezone)->format("Y-m-d");
            $query->andFilterWhere(['BETWEEN', 'suivi_date', $this->_minDate, $this->_maxDate ]);
        }
        
        if($this->suiviobjet_id) {
            $query->andFilterWhere(['objet.objet_id' => $this->suiviobjet_id]);
        }
        
        

        return $dataProvider;
    }
}
