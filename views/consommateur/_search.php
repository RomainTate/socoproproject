<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\ConsommateurSearch */
/* @var $form yii\widgets\ActiveForm */
//$suggestLieu = ArrayHelper::map(\app\models\Lieuprisecontact::find()->select('lieu')
//        ->where(['not' , ['lieu' => null]])->all() , 'lieu', 'lieu');
//$suggestLieu = array_values($suggestLieu); 

//$consos = \app\models\Consommateur::find()->select(['conso_fonction', 'conso_societe'])->all();
?>

<div class="consommateur-search">

    <?php $form = ActiveForm::begin([
        'action' => ['search'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'contactGSMPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Numéro GSM');?>
    <?= $form->field($model, 'contactEmailPrincipalNotNull')->radioList([ 1 => 'Renseigné' , 0 => 'Tous'])->label('Adresse e-mail');?>
    
    <?php 
    //echo $form->field($model, 'consoLieuContactName')->widget(\yii\jui\AutoComplete::classname(), [
//        'clientOptions' => [
//            'source' => $suggestLieu,
//        ],
//        'options' => [
//            'class' => 'form-control'
//        ],
//    ]);
    ?> 
    
    <?= $form->field($model, 'consoLieuContactName')->dropDownList(ArrayHelper::map(app\models\Lieuprisecontact::find()->orderBy('lieu')->all(), 'lieu_prise_contact_id', 'lieu'),
            ['class' => 'js-multiple' , 'multiple'=>'multiple'])?>
    
    <?= $form->field($model, 'secteursID')
        ->dropDownList(ArrayHelper::map(\app\models\Secteur::find()->all(),'sect_id' , 'sect_nom')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Intérêts')
    ?>
    
    <?= $form->field($model, 'conso_societes')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Consommateur::find()->where(['not',['conso_societe' => [null, '']]])->all(),'conso_societe' , 'conso_societe')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Sociétés')?>
    
    <?= $form->field($model, 'conso_fonctions')->dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Consommateur::find()->where(['not',['conso_fonction' => [null, '']]])->all(),'conso_fonction' , 'conso_fonction')
        ,['class' => 'js-multiple' , 'multiple'=>'multiple'])->label('Fonctions')?>


    <div class="form-group">
        <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php $this->registerJs(
    "$('.js-multiple').select2();",
    yii\web\View::POS_READY,
    'select2-prod'
); ?>