<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "autre_infos".
 *
 * @property integer $cont_id
 * @property string $site_internet
 * @property string $page_facebook
 */
class AutreInfos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autre_infos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id'], 'required'],
            [['cont_id'], 'integer'],
            [['site_internet'], 'string', 'max' => 200],
            [['page_facebook'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'site_internet' => 'Site Internet',
            'page_facebook' => 'Page Facebook',
        ];
    }
}
