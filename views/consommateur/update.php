<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Consommateur */

$this->title = 'Mise à jour de '. $contactModel->cont_civilite  .' '. $contactModel->cont_nom .' '. $contactModel->cont_prenom;
$this->params['breadcrumbs'][] = ['label' => 'Consommateurs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $contactModel->cont_nom. ' '. $contactModel->cont_prenom, 'url' => ['view', 'id' => $model->cont_id]];
$this->params['breadcrumbs'][] = 'Mise à jour';
?>
<div class="consommateur-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <?php if(!Yii::$app->request->isAjax) :?>
        <div class="consoUpdate">
        <?php            endif;?>
    <?= $this->render('_form', [
        'model' => $model,
        'contactModel' => $contactModel,
    ]) ?>
        <?php if(!Yii::$app->request->isAjax) :?>
        </div>
        <?php            endif;?>
    </div>
</div>
