<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RechAdminEncad */
/* @var $form yii\widgets\ActiveForm */
    if (Yii::$app->session->hasFlash('error')): ?>
        <div class="alert <?php echo Yii::$app->user->getUser()->getIsAdmin()? " alert-warning ": " alert-danger ";?> alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h5><i class="icon fa fa-times"></i><?php echo Yii::$app->user->getUser()->getIsAdmin()? " Attention !" : " Erreur !"; ?></h5>
            <?= Yii::$app->session->getFlash('error') ?>
        </div>
    <?php endif; ?>
<div class="rech-admin-encad-form">

<?php $form = ActiveForm::begin([
        'id' => 'raeForm'
    ]); ?>
    
    <?= $form->field($contactModel, 'cont_civilite')->textInput(['maxlength' => true])->dropDownList(['Mr' => 'Mr' , 'Mme' => 'Mme']) ?>

    <?= $form->field($contactModel, 'cont_nom')->textInput(['maxlength' => true]) ?>
    <?= $form->field($contactModel, 'cont_prenom')->textInput(['maxlength' => true]) ?>


    <?= $form->field($contactModel, 'cont_gsm_principal')->textInput(['maxlength' => true]) ?>
    <?= $form->field($contactModel, 'cont_email_principal')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'rae_type')->dropDownList(["1" => "Recherche", "2" => "Administratif", "3" => "Encadrement"]); ?>


    <div class="form-group">
        <?php if (Yii::$app->session->hasFlash('error') && Yii::$app->user->getUser()->getIsAdmin()) echo Html::submitButton($model->isNewRecord ? 'Créer quand même' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'value' => 'forceAdd', 'name'=>'forceadd']);
              else echo Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
