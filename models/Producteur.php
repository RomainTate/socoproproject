<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "producteur".
 *
 * @property boolean $prod_sigec
 * @property string $prod_numero_producteur
 * @property string $prod_numero_BCE
 * @property string $prod_nom_exploitation
 * @property integer $prod_statut_juridique
 * @property string $prod_date_debut_activite
 * @property boolean $prod_statut_pro
 * @property integer $needConfirm 
 * @property integer $cont_id
 * 
 * @property Associe[] $associes
 * @property Contact $contact
 * @property Activite[] $activites
 */
class Producteur extends \yii\db\ActiveRecord
{
    public $nbInteraction;
    public $allSuivi;
    public $allEvent;
    public $interactionQuery;
    public $suiviQuery;
    public $suiviTelQuery;
    public $suiviEmailQuery;
    public $suiviVisiteQuery;
    public $suiviFoireEventQuery;
    public $eventQuery;
    public $eventASQuery;
    public $eventAGQuery;
    public $eventCollegeQuery;
    public $eventVoyageQuery;
    public $eventAutreQuery;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'producteur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['secteurs'], 'required', 'on' => 'form'],
            [['prod_sigec', 'prod_statut_juridique', 'cont_id', 'prod_statut_pro', 'needConfirm'], 'integer'],
            [['prod_date_debut_activite', 'nbInteraction', 'allSuivi'], 'safe'],
            [['nbInteraction', 'allSuivi'], 'integer'],
//            [['cont_id'], 'required'],
            [['prod_numero_producteur', 'prod_numero_BCE'], 'string', 'max' => 50],
            [['prod_nom_exploitation'], 'string', 'max' => 50],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            ['secteurs', 'validateSecteurs', 'on' => 'form'], 
//            function ($attribute, $params, $validator) {
//                    var_dump($attribute);
//                if (!ctype_alnum($this->$attribute)) {
//                    $this->addError($attribute, 'The token must contain letters or digits.');
//                }
//            }],
            ];
    }
    
    public function validateSecteurs() {
        $user = \Yii::$app->user->getId();
        $role = \app\modules\auth\models\AuthAssignment::findOne(['user_id' => $user])->item_name; // ok
        if($role == 'Chargé de mission') {
            $userSecteurs  = \yii\helpers\ArrayHelper::map(Gerer::find()->select('sect_id')->where(['user_id' => $user])->all(), 'sect_id', 'sect_id'); //ok
            if(empty(array_intersect($userSecteurs, $this->secteurs))) {        
                $this->addError('secteurs', "Aucun de vos secteurs n'est selectionné");
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'oldcont_entity' => 'Type Contact',
            'prod_entity' => 'Contact Producteur',
            'prod_numero_producteur' => 'Numero Producteur',
            'prod_numero_BCE' => 'Numero Bce',
            'prod_nom_exploitation' => 'Nom Exploitation',
            'prod_statut_juridique' => 'Statut Juridique',
            'prod_date_debut_activite' => 'Date Debut Activite',
            'cont_id' => 'Cont ID',
            'prod_sigec' => 'Confidentiel',
            'prod_statut_pro' => 'Statut Professionel',
            /* related attributes*/
            'contactNom' => 'Nom',
            'contactPrenom' => 'Prénom',
            'contactTelephonePrincipal' => 'Téléphone',
            'contactGSMPrincipal' => 'Numéro GSM',
            'contactEmailPrincipal' => 'E-mail',
            'codepostal' => 'C.P.'
        ];
    }
    
    public static function getLabelAttribute($attribute) {
        $prod = New Producteur();
        return $prod->getAttributeLabel($attribute);
    }
    
    public function getName() {
        if($this->contactNom && $this->contactPrenom) {
            return $this->contactNom.' '.$this->contactPrenom;
        } else {
            if ($this->prod_nom_exploitation) {
                return 'Producteur ayant pour exploitation "'.$this->prod_nom_exploitation.'"';
            } else {
                return 'Producteur anonyme';
            }
        }
    }

    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        
        if (empty($this->getDirtyAttributes()) || $this->needConfirm) {
            return parent::beforeSave($insert);
        }
        
        if($insert) {
            \Yii::$app->get('logger')->log($this->cont_id, json_encode(['newcont_entity' => 'prod' ]), 3);
        }

        $FIELDSTOLOG = ['prod_statut_pro'];
//            die(var_dump($this->getDirtyAttributes())); //array ['nom_attr_modifié' => 'nouvelle valeur']
//            die(var_dump($this->getOldAttributes())); //array ['nom_attr' => 'old valeur']
        foreach ($this->getDirtyAttributes() as $dirtyKey => $dirtyValue) {
            $logs = array();
            if (in_array($dirtyKey, $FIELDSTOLOG)) {

                if ($this->getOldAttribute($dirtyKey) !== null) {
                    $action = 1;

                    if($dirtyKey == 'prod_statut_pro') {
                        if($this->getOldAttribute($dirtyKey) == $dirtyValue) {
                            continue;
                        }
                        switch ($this->getOldAttribute($dirtyKey)) {
                        case '0':
                            $oldAttr = 'Retraité';
                            break;
                        case '1' :
                            $oldAttr = 'Actif';
                            break;
                        }
                        $logs['old' . $dirtyKey] = $oldAttr;
                    } else {
                        $logs['old' . $dirtyKey] = $this->getOldAttribute($dirtyKey);
                    }

                } else {
                    $action = 0;
                }

                if ($dirtyValue !== null) {

                    if($dirtyKey == 'prod_statut_pro') {
                        switch ($dirtyValue) {
                        case '0':
                            $newAttr = 'Retraité';
                            break;
                        case '1' :
                            $newAttr = 'Actif';
                            break;
                        }
                        $logs['new' . $dirtyKey] = $newAttr;
                    } else {
                        $logs['new' . $dirtyKey] = $dirtyValue;
                    }

                } else {
                    $action = 2;
                }
            }
            if(!empty($logs)) {
                \Yii::$app->get('logger')->log($this->cont_id, json_encode($logs), $action);
            }
        }
        return parent::beforeSave($insert);
    }
    
    public function beforeDelete() {
        if(!$this->needConfirm) {
        //log some infos before delete contact
            $FIELDSTOLOG = ['contactNom', 'contactPrenom' , 'prod_nom_exploitation', 'contactEmailPrincipal', 'contactGSMPrincipal'];
            $logs['oldcont_entity'] = 'Producteur';
            foreach ($FIELDSTOLOG as $field) {
                if($this->$field)
                $logs[$field] = $this->$field;
            }

            if($this->secteurs) {
                foreach ($this->secteurs as $secteur) {
                    $logs['oldsect'][] = $secteur->sect_nom;
                }
            }
            if(!empty($logs)) {
                \Yii::$app->get('logger')->log(null, json_encode($logs), 4);
            }
        }
        
        $associes = Associe::find()->where(['cont_id_prod' => $this->cont_id])->all();
        foreach ($associes as $associe) {
            $associeId = $associe->cont_id;
            $associe->delete();
            Contact::deleteAll(['cont_id' => $associeId]);
        }
        
        Travailler::deleteAll(['cont_id' => $this->cont_id]);
        
        \rmrevin\yii\module\Comments\models\Comment::deleteAll(['entity' => 'producteur-'.$this->cont_id]);
        Appartenir::deleteAll(['cont_id' => $this->cont_id]);
        
        
        return parent::beforeDelete();
    }
    
    public function afterDelete() {
        Contact::findOne($this->cont_id)->delete();
        return parent::afterDelete();
    }

    public function getAssocies() {
        return $this->hasMany(Associe::className(), ['cont_id_prod' => 'cont_id'])
                ;
    }
    
//    public function getAllSuivi() {
//        return $this->hasMany(Suivi::className(), ["cont_id" => "cont_id"])->count();
//    }
//    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
    
    public function getAutreinfos() {
        return $this->hasOne(AutreInfos::className(), ['cont_id' => 'cont_id']);
    }
    
    /* Getter for contact nom */
    public function getContactNom() {
        return $this->contact->cont_nom;
    }
    
    /* Getter for contact prenom */
    public function getContactPrenom() {
        return $this->contact->cont_prenom;
    }
    
    public function getContactCivilite() {
        return $this->contact->cont_civilite;
    }
    
    public function getContactTelephonePrincipal() {
        return $this->contact->cont_telephone_principal;
    }
    public function getContactGSMPrincipal() {
        return $this->contact->cont_gsm_principal;
    }
    
    public function getContactEmailPrincipal() {
        return $this->contact->cont_email_principal;
    }
    
    public function getContactLangue() {
        return $this->contact->cont_langue;
    }
    
    public function getContactCreatedAt() {
        return $this->contact->cont_created_at;
    }
    
    public function getSecteurs()
    {
        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable('travailler', ['cont_id' => 'cont_id']);
    }
    
    public function getSecteursOrdered()
    {
        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable('travailler', ['cont_id' => 'cont_id'])
                ->orderBy('sect_nom')
                ;
    }
    
    public function setSecteurs($v)
    {     
        $this->secteurs = $v;
    }
    
    public function getSecteursID() {
        if($this->secteurs == null) return null;
        return $this->secteurs->sect_id;
    }
    
    public function getRue() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_rue;
    }
    public function getLocalite() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_ville;
    }
    
    public function getProvince() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_province;
    }
    public function getCodepostal() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_code_postal;
    }
    
    public function getLangue() {
        return $this->contact->cont_langue;
    }
    
    
    public function getChampSectorielValeurs() {
        return $this->hasMany(ChampSectorielValeur::className(), ['cont_id' => 'cont_id']);
    }

    public function getActivites()
    {
        return $this->hasMany(Activite::className(), ['activite_id' => 'activite_id'])->viaTable('avoir', ['cont_id' => 'cont_id']);
    }
    
//    public function getProdNumerosProducteur()
//    {
//        return $this->hasMany(ProdNumeroProducteur::className(), ['cont_id' => 'cont_id']);
//    }
    
    public function getNbinteraction(){
        if($this->contact) { 
            $this->nbinteraction = 0;
            $this->nbinteraction += count($this->contact->suivis);
            $this->nbinteraction += count($this->contact->evenements);
            return $this->nbinteraction;
        }
    }
}
