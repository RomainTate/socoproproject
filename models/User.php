<?php

namespace app\models;

use dektrium\user\models\User as BaseUser;

class User extends BaseUser   implements          \rmrevin\yii\module\Comments\interfaces\CommentatorInterface{
    
        public function scenarios()
    {
        $scenarios = parent::scenarios();
        // add field to scenarios
        $scenarios['create'][]   = 'GSM';
        $scenarios['create'][]   = 'Nom';
        $scenarios['create'][]   = 'Prenom';
        $scenarios['create'][]   = 'Telephone';
        $scenarios['update'][]   = 'GSM';
        $scenarios['update'][]   = 'Nom';
        $scenarios['update'][]   = 'Prenom';
        $scenarios['update'][]   = 'Telephone';
        $scenarios['register'][] = 'GSM';
        $scenarios['register'][] = 'Nom';
        $scenarios['register'][] = 'Prenom';
        $scenarios['register'][] = 'Telephone';
        return $scenarios;
    }

    public function rules()
    {
        $rules = parent::rules();
        // add some rules
//        $rules['fieldRequired'] = ['field', 'required'];
        $rules['fieldLength']   = ['GSM', 'string', 'max' => 20];
        $rules['fieldLength']   = ['Nom', 'string', 'max' => 30];
        $rules['fieldLength']   = ['Prenom', 'string', 'max' => 30];
        $rules['fieldLength']   = ['Telephone', 'string', 'max' => 20];
        
        return $rules;
    }

    public function getIsAdmin($id = null) {
        $admins = require (\Yii::$app->basePath . '/config/admins.php');
        if ($id) {
            $user = User::findOne(['id' => $id]);
            return in_array($user->username, $admins) ?  true :  false;
//            return (User::findOne([
//                    'id' => $id,
//                    'user_admin' => 1
//                ]) ? true : false );
        }
        return in_array(\Yii::$app->user->getUser()->username, $admins) ?  true :  false;
//        return (User::findOne([
//                    'username' => \Yii::$app->user->getUser()->username,
//                    'user_admin' => 1
//                ]) ? true : false );
    }
    
        //ignorer parent::afterSave 
    public function afterSave($insert, $changedAttributes)
    {
        return true;
        
    }
    
    // User was admin ? remove him 
    // Delete Child rows
    public function beforeDelete() {
        $currentAdmins = require (\Yii::$app->basePath . '/config/admins.php');
        if (in_array($this->username, $currentAdmins)) {
            $content = "<?php return [";
            foreach ($currentAdmins as $currentAdmin) {
                if ($currentAdmin != $this->username) {
                    $content .= " '" . $currentAdmin . "', ";
                }
            }
            $content .= "];";
            file_put_contents(\Yii::$app->basePath . '/config/admins.php', $content);
        }
        
        $gerer = Gerer::deleteAll(['user_id' => $this->id]);
        return parent::beforeDelete();
    }

    public function getCommentatorAvatar()
    {
        return false;
    }
    
    public function getCommentatorName()
    {
        return $this->username;
    }
    
    public function getCommentatorUrl()
    {
        return false; // or false, if user does not have a public page
    }

}
