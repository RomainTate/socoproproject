<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * @var $dataProvider array
 * @var $filterModel  dektrium\rbac\models\Search
 * @var $this         yii\web\View
 */

$this->title = Yii::t('rbac', 'Administration :  gérer les Rôles');
$this->params['breadcrumbs'][] = $this->title;
$selectedRole = \Yii::$app->request->get('name');
?>
<div class="x_panel">
        <div class="x_title">
            <h1><?= Html::encode($this->title) ?></h1>
        </div>
<ul id="w0" class="nav-tabs nav" style="margin-bottom: 15px">
    <li><a href="<?= \yii\helpers\Url::home()?>user/admin/index">Utilisateurs</a></li>
    <li class="active"><a href="<?= \yii\helpers\Url::home()?>rbac/role/index">Rôles</a></li>
    <li><a href="<?= \yii\helpers\Url::home()?>champsect/">Champs sectoriels</a></li>
    <li><a href="<?= \yii\helpers\Url::home()?>user/admin/checkdoublon">Gestion doublons</a></li>
    <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexnewsletters">Newsletters</a></li>
    <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foires <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireprod">Producteurs</a></li>
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireconso">Consommateurs</a></li>
            </ul>
    </li>
</ul>

<?php if (Yii::$app->session->hasFlash('success')): ?>
  <div class="alert alert-success alert-dismissable">
  <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
  <h5><i class="icon fa fa-check"></i>Sauvegardé!</h5>
  <?= Yii::$app->session->getFlash('success') ?>
  </div>
<?php endif; ?>

<div class="role-indexform">
    <?php $roles = ArrayHelper::map($dataProvider->getModels(), 'name', 'name'); 
    unset($roles['Administrateur']); // //unset admin for the dropDownList to disable changes on admin role
    unset($roles['Foire']); // //unset foire for the dropDownList to disable changes on foire role
    
    ActiveForm::begin(['action' => \yii\helpers\Url::home().'rbac/role/index','method' => 'get']); ?>

    <label>Selectionner un rôle </label>
 
    <?= Html::dropDownList('name', $selectedRole, $roles, 
    is_null($selectedRole)? ['prompt' => 'Selectionnez un rôle', 'onchange' => 'this.form.submit()','class' => 'form-control'] : ['onchange' => 'this.form.submit()','class' => 'form-control'] ) ?>
    <?php
    ActiveForm::end(); 


    if (is_array($authItems) && is_array($activatedAuth)) : 
        ?>

        <?php ActiveForm::begin(['action' => 'update/?role=' . $selectedRole])?>

    <fieldset style="margin: 10px 0">
        <div>
        Commentaires :
        <?= Html::checkboxList('name', $activatedAuth, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'comments'])->all(),'name', 'description'))?>
    </div>
    <div>
        Consommateurs :
        <?= Html::checkboxList('name', $activatedAuth, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'conso'])->all(),'name', 'description'))?>
    </div>
    <div>
        Contacts divers :
        <?= Html::checkboxList('name', $activatedAuth, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'divers'])->all(),'name', 'description'))?>
    </div>
    <div>
        Journalistes et médias :
        <?= Html::checkboxList('name', $activatedAuth, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'journaliste'])->all(),'name', 'description'))?>
    </div>
    <div>
        Producteurs :
        <?= Html::checkboxList('name', $activatedAuth, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'prod'])->all(),'name', 'description'))?>
    </div>
    <div>
        Entreprises-Associations :
        <?= Html::checkboxList('name', $activatedAuth, ArrayHelper::map(\app\modules\auth\models\AuthItem::find()->where(['type' => '2'])->andWhere(['like', 'name' , 'entreprise'])->all(),'name', 'description'))?>
    </div>
    </fieldset>
    <div class="form-group">
        <?= Html::submitButton('Modifier le rôle', ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Supprimer le rôle', ['delete', 'name' => $selectedRole], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        <?php ActiveForm::end(); ?>
    </div>
    
    
    <?php // ActiveForm::begin(['action' => 'update/?role=' . $selectedRole])?>
    
    <?php // ActiveForm::end(); ?>
    
    <?php endif; ?>
        <p style="text-align: right; margin-top: 10px">
        <?= Html::a('Ajouter un Rôle', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
</div>
