<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Association */
/* @var $form yii\widgets\ActiveForm */
if (!isset($model)) {
    $model = new app\models\Association();
    $entrepriseModel = new app\models\Entreprise();
    $action = \yii\helpers\Url::home().'association/create';
} else {
    $action = '';
}
?>

<div class="association-form">
    <div class="x_panel">
        <?php
        $form = ActiveForm::begin([
                    'action' => $action,
                    'id' => 'associationForm'
        ]);
        ?>


        <?php if (Yii::$app->request->isAjax) echo yii\bootstrap\Html::hiddenInput('ajax', 1); ?>

        <?= $form->field($entrepriseModel, 'entr_nom')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_forme_juridique')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_telephone_principal')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_email_principal')->textInput(['maxlength' => true]) ?>

        <?= $form->field($entrepriseModel, 'entr_site_internet')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'association_type')->textInput(['maxlength' => true]) ?>

        <?php
        if (isset($contactId)) {
            echo \yii\bootstrap\Html::hiddenInput('contactToAdd', $contactId);
        }
        ?>


        <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Créer' : 'Mettre à jour', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>

<?php ActiveForm::end(); ?>
    </div>
</div>
