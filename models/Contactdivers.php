<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_divers".
 * @property string $divers_fonction 
 * @property integer $divers_AS_AG 
 * @property integer $cont_id
 * 
 * @property Contact $contact
 */
class Contactdivers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'divers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['cont_id'], 'required'],
            [['divers_AS_AG', 'cont_id'], 'integer'],
            [['divers_fonction'], 'string', 'max' => 25],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'divers_fonction' => 'Fonction', 
            'divers_AS_AG' => 'Invitation As/Ag', 
//            'cont_divers_type' => 'Type de Contact',
            /* related attributes*/
            'contactNom' => 'Nom',
            'contactPrenom' => 'Prénom',
            'contactTelephonePrincipal' => 'Téléphone',
            'contactGSMPrincipal' => 'GSM',
            'contactEmailPrincipal' => 'E-mail',
            'nomentr' => 'Societe',
            'fct' => 'Fonction'
        ];
    }
    
    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        return parent::beforeSave($insert);
    }
    
//    public function afterSave($insert, $changedAttributes) {
//        if (!$insert && array_key_exists("cont_divers_lieu_contact", $changedAttributes)) {
//            if($changedAttributes['cont_divers_lieu_contact'] != '' &&
//                    $this->find()->where(['cont_divers_lieu_contact' => $changedAttributes['cont_divers_lieu_contact']])->one() == null &&
//                    Consommateur::find()->where(['conso_lieu_contact' => $changedAttributes['cont_divers_lieu_contact']])->one() == null) {
//                Lieuprisecontact::find()->where(['lieu_prise_contact_id' => $changedAttributes['cont_divers_lieu_contact']])->one()->delete();
//            }
//        }
//        return parent::afterSave($insert, $changedAttributes);
//    }
    
    public function beforeDelete() {
        \rmrevin\yii\module\Comments\models\Comment::deleteAll(['entity' => 'contactDivers-'.$this->cont_id]);
        Interesser::deleteAll(['cont_id' => $this->cont_id]);
        Appartenir::deleteAll(['cont_id' => $this->cont_id]);
        return parent::beforeDelete();
    }

    public function afterDelete() {
        Contact::findOne($this->cont_id)->delete();
        return parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
    
     /* Getter for contact nom */
    public function getContactNom() {
        return $this->contact->cont_nom;
    }
    
    /* Getter for contact prenom */
    public function getContactPrenom() {
        return $this->contact->cont_prenom;
    }

    public function getContactCivilite() {
        return $this->contact->cont_civilite;
    }
    
    public function getContactTelephonePrincipal() {
        return $this->contact->cont_telephone_principal;
    }
    public function getContactGSMPrincipal() {
        return $this->contact->cont_gsm_principal;
    }
    
    public function getContactEmailPrincipal() {
        return $this->contact->cont_email_principal;
    }
    
//    public function getContactAnneeNaissance() {
//        return $this->contact->cont_annee_naissance;
//    }
    public function getContactCreatedAt() {
        return $this->contact->cont_created_at;
    }
    
        /**
     * @return \yii\db\ActiveQuery
     */
    
    public function getSecteurs()
    {
        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable('interesser', ['cont_id' => 'cont_id']);
    }
    
//    public function setSecteurs($v)
//    {     
//        $this->secteurs = $v;
//    }
    
    public function getSecteursID() {
        if($this->secteurs == null) return null;
        return $this->secteurs->sect_id;
    }
    
    public function getRue() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_rue;
    }
    public function getLocalite() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_ville;
    }
    
    public function getProvince() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_province;
    }
    public function getCodepostal() {
        if(!$this->contact->adresse) 
            return null;
        return $this->contact->adresse->adr_code_postal;
    }

    public function alreadyExists($nom, $prenom) {
        if($this->find()->joinWith('contact', true, 'inner join')->where(['cont_prenom' => $prenom , 'cont_nom' => $nom])->one())
                return true;
        return false;
    }
    
    public function getNomentr() {
        return null;
    }
    public function getFct() {
        return null;
    }
    

}
