<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\MediaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="media-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'media_id') ?>

    <?= $form->field($model, 'media_societe') ?>

    <?= $form->field($model, 'media_titre') ?>

    <?= $form->field($model, 'media_telephone') ?>

    <?= $form->field($model, 'media_email') ?>

    <?php echo $form->field($model, 'media_site_internet') ?>

    <?php echo $form->field($model, 'media_type_presse') ?>

    <?php echo $form->field($model, 'media_type_support') ?>

    <?php echo $form->field($model, 'media_frequence') ?>


    <?php echo $form->field($model, 'media_audience') ?>

    <?php echo $form->field($model, 'media_langue') ?>
    <?php echo $form->field($model, 'media_pays') ?>

    <?php // echo $form->field($model, 'adr_id') ?>

    <div class="form-group">
        <?= Html::submitButton('Rechercher', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
