<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if( (!Yii::$app->user->isGuest) &&Yii::$app->user->getUser()->username == 'foire') {
            return $this->render('indexfoire');
        }
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return string
     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }
    
    public function actionEquipe() {
        $users = Yii::$app->getDb()->createCommand(
                "SELECT Nom, Prenom, email, GSM, Telephone, 'Active' FROM `user` WHERE user.id != 1 UNION SELECT Nom, Prenom, email, GSM, Telephone, 'Inactive' FROM `inactive_user`"
                )->queryAll();
        
        return $this->render('equipe', [
            'users' => $users,
        ]);
    }
    
    public function actionAddinactiveuserform()
    {
        $model = new \app\models\InactiveUser();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) {
                Yii::$app->session->setFlash('success', "Informations enregistrées");
            } else {
                Yii::$app->session->setFlash('error', "Erreur, les informations n'ont pas pu être enregistrées");
            }
        }

        return $this->redirect(['/site/equipe']);
    }
    
    public function actionDeleteinactiveuserform() {
        $post = Yii::$app->request->post();
        if($post) {
            $iUser = \app\models\InactiveUser::find()->where(['id' => $post['idInactiveUser']])->one();
            if (!$iUser) {
                Yii::$app->session->setFlash('error', "Erreur, les informations n'ont pas pu être supprimées");
                return $this->redirect(['/site/equipe']);
            }
            if(!$iUser->delete()){
                Yii::$app->session->setFlash('error', "Erreur, les informations n'ont pas pu être supprimées");
                return $this->redirect(['/site/equipe']);
            } else {
                Yii::$app->session->setFlash('success', "Informations supprimées");
                return $this->redirect(['/site/equipe']);
            }
        }
        
        return $this->redirect(['/site/equipe']);
    }
}