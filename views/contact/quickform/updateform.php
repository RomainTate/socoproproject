<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
?>

<div class="x_panel">

    <?php $form = ActiveForm::begin(['id' => 'updateContactForm']); ?>

    <?php if ($type == 'string' && $attribute != 'cont_langue') : ?>
        <?= $form->field($model, $attribute)->textInput(['maxlength' => true]) ?>
    <?php elseif ($type == 'date') : ?>
        <?= $form->field($model, $attribute)->widget(\yii\jui\DatePicker::className(),[
//            'model' => $model,
//            'attribute' => $attribute,
            'dateFormat' => 'dd-MM-yyyy',
        ]);
        elseif ($attribute == 'cont_langue') :
            echo $form->field($model, $attribute)->checkboxList(['FR' => 'FR', 'NL' => 'NL', 'DE' => 'DE', 'EN' => 'EN'],['item'=>function ($index, $label, $name, $checked, $value) use ($model){
                if(strpos($model->cont_langue, $value) !== false) {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                return "<label><input type='checkbox' {$checked} name='{$name}' value='{$value}'>{$label}</label>";
                                        }]);

        ?>
    
    <?php endif; ?>
    
    <div class="form-group">
        <?= Html::submitButton('Mettre à jour', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>