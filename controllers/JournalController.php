<?php

namespace app\controllers;

use Yii;
use app\models\Journal;
use app\models\JournalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JournalController implements the CRUD actions for Journal model.
 */
class JournalController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Journal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JournalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Journal model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Journal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Journal();
        $entrepriseModel = new \app\models\Entreprise();
        $post = Yii::$app->request->post();

        if ($entrepriseModel->load($post) && $model->load($post)) {
            if ($model->validate() && $entrepriseModel->save()) { //validate $model BEFORE saving $entrepriseModel
                $model->entr_id = $entrepriseModel->entr_id;
                if ($model->save(false)) { // save(false) bypass validation (already did 2 lines ago)
                    if (isset($post['contactToAdd'])) { //if user adds Journal from contact view
                        if (AppartenirController::addContactEntreprise($post['contactToAdd'], $model->entr_id)) {
                            return $this->redirect(['journaliste/view', 'id' => $post['contactToAdd']]);
                        }
                    }
                    return $this->redirect(['view', 'id' => $model->entr_id]);
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        }
        
        return $this->render('create', [
                    'model' => $model,
                    'entrepriseModel' => $entrepriseModel
        ]);
    }

    /**
     * Updates an existing Journal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
        { //TODO : log
        $model = $this->findModel($id);
        $entrepriseModel = $model->entreprise;
        $post = Yii::$app->request->post();
        
        if ($entrepriseModel->load($post) && $model->load($post)) {
            if ($model->validate() && $entrepriseModel->save()) { //validate $model BEFORE saving $entrepriseModel
                if ($model->save(false)){ // save(false) bypass validation (already did 2 lines ago)
                    if (isset($post['ajax']) && $post['ajax'] == 1) {
                        return $this->redirect(Yii::$app->request->referrer);
                    }
                    return $this->redirect(['view', 'id' => $entrepriseModel->entr_id]);
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        } 
        
        if (!Yii::$app->request->isAjax) {
            return $this->render('update', [
                    'model' => $model,
                    'entrepriseModel' => $entrepriseModel
            ]);
        }
        return $this->renderAjax('update', [
                'model' => $model,
                'entrepriseModel' => $entrepriseModel
        ]);
    }

    /**
     * Deletes an existing Journal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Journal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Journal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Journal::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
