<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "traiter".
 *
 * @property integer $entr_id
 * @property integer $sect_id
 *
 * @property Entreprise $entrentreprise
 * @property Secteur $secteur
 */
class Traiter extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'traiter';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['entr_id', 'sect_id'], 'required'],
            [['entr_id', 'sect_id'], 'integer'],
            [['entr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entreprise::className(), 'targetAttribute' => ['entr_id' => 'entr_id']],
            [['sect_id'], 'exist', 'skipOnError' => true, 'targetClass' => Secteur::className(), 'targetAttribute' => ['sect_id' => 'sect_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'entr_id' => 'Entr ID',
            'sect_id' => 'Sect ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntreptrise()
    {
        return $this->hasOne(Entreprise::className(), ['entr_id' => 'entr_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSecteur()
    {
        return $this->hasOne(Secteur::className(), ['sect_id' => 'sect_id']);
    }
}
