<?php

use yii\bootstrap\Html;
use kartik\grid\GridView;

/**
 * @var \yii\web\View $this
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \dektrium\user\models\UserSearch $searchModel
 */
$this->title = 'Administration : Gérér les newsletters';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="x_panel">
    <div class="x_title">
        <h1><?= Html::encode($this->title) ?></h1>
    </div>
    <ul id="w0" class="nav-tabs nav" style="margin-bottom: 15px">
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/index">Utilisateurs</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>rbac/role/index">Rôles</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>champsect/">Champs sectoriels</a></li>
        <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexdoublon">Gestion doublons</a></li>
        <li class="active"><a href="<?= \yii\helpers\Url::home()?>user/admin/indexnewsletters">Newsletters</a></li>
        <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#">Foires <span class="caret"></span></a>
            <ul class="dropdown-menu">
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireprod">Prod</a></li>
                <li><a href="<?= \yii\helpers\Url::home()?>user/admin/indexfoireconso">Conso</a></li>
            </ul>
        </li>
    </ul>
    

    
    <?php $gridColumns = [

            [
                'label' => 'Nom',
                'attribute' => 'newsletter_nom',
            ],
            [
                'label' => 'Nombre inscrits',
                'value' => function ($model) {
                    if(!$model->contacts) return 0;
                    return count($model->contacts);
                }
            ],
//            [
//                'label' => 'Modifier',
//                'format' => 'Html',
//                'content' => function ($model) {
//                    return Html::button('', ['class' => 'glyphicon glyphicon-eye-open text-info']);
//                }
//            ],
                ];
            
            array_push($gridColumns, ['class' => 'kartik\grid\ActionColumn','template' => '{update}{delete}', 'hiddenFromExport' => true,
            'buttons' => [
                'update' => function($url, $model) {
                    $url = \Yii::$app->urlManager->createUrl(['newsletter/update', 'id' => $model->newsletter_id]);
                    return Html::a('<span class="glyphicon glyphicon-pencil text-info"></span>', '#', ['title' => Yii::t('yii', 'Update'), 'name' => 'modalButton', 'value' => yii\helpers\Url::to(['/newsletter/update', 'id' => $model->newsletter_id])]);
                },
                'delete' => function($url, $model) {
                    $url = \Yii::$app->urlManager->createUrl(['newsletter/delete', 'id' => $model->newsletter_id]);
                    return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-confirm' =>  'Etes vous sûr de vouloir supprimer cette newsletter ?', 'data-method' => 'post']);
                }
            ]
        ])
        ?>
                
        <div class="clear"></div>
        <div id="gridHeader">
            <?= Html::a('Ajouter une Newsletter', ['/newsletter/create'], ['class' => 'btn btn-success']) ?>
        </div>
            <?=
            GridView::widget([
                'dataProvider' => $newsletters,
                'id' => 'prodGrid',
//                'filterModel' => $searchModel,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
                'columns' => $gridColumns,
                'responsive' => false,
                'responsiveWrap' => false,
                'pager' => [
                    'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
                    'prevPageLabel' => 'Précédent',   // Set the label for the "previous" page button
                    'nextPageLabel' => 'Suivant',   // Set the label for the "next" page button
                    'firstPageLabel'=>'Début',   // Set the label for the "first" page button
                    'lastPageLabel'=>'Fin',    // Set the label for the "last" page button
                    'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
                    'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
                    'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
                    'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
                    'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
                ],      
            ]);
            ?>
    
</div>

<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile('js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>