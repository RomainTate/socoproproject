<?php


namespace app\modules\auth\controllers ;


use yii\helpers\ArrayHelper;
use dektrium\rbac\models\Search;

use dektrium\rbac\controllers\RoleController as BaseRoleController;


class RoleController extends BaseRoleController
{

    public function actionIndex($name = null) {
        $authItems = null; $activatedAuth = null;
        
        if ($name != null) {
            $authManager = \Yii::$app->authManager;
            $authItems = ArrayHelper::map($authManager->getPermissions(), 'name' , 'description');
            $activatedAuth = ArrayHelper::map($authManager->getPermissionsByRole($name), 'name' , 'name');
        }
        $filterModel = new Search($this->type);
        return $this->render('index', [
                    'filterModel' => $filterModel,
                    'dataProvider' => $filterModel->search(\Yii::$app->request->get()),
                    'authItems' => $authItems,
                    'activatedAuth' => $activatedAuth
        ]);
    }
    
    public function actionCreate() {

        $model = new \app\modules\auth\models\AuthItem;
        $post = \Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            if ($post['children'] != '') {
                $auth = \Yii::$app->authManager;
                foreach ($post['children'] as $authItem) {
                    $auth->addChild($auth->getRole($model), $auth->getPermission($authItem));
                }
            }
            return $this->redirect(['role/index', 'name' => $model->name]);
            
        } else {
            return $this->render('create', [
                        'model' => $model,
            ]);
        }
    }

    public function actionUpdate($role) {
        // get all auth_item_child where 'parent' = $role & delete them
        \app\modules\auth\models\AuthItemChild::deleteAll(['parent' => $role]);
        
        // 3 foreach name : add auth_item_child to $role
        if (\Yii::$app->getRequest()->post('name')) {
            $auth = \Yii::$app->authManager; 
            foreach (\Yii::$app->getRequest()->post('name') as $authItem) {
                $auth->addChild($auth->getRole($role), $auth->getPermission($authItem));
            }
            \Yii::$app->session->setFlash('success', "Le rôle à été mis à jour !");
        }
        return $this->redirect(['role/index', 'name' => $role]);
    }
    
    public function delete($role) {
        $authItem = \app\modules\auth\models\AuthItem::findOne(['name' => $role]);
        $authItem->delete();
    }

}