<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

use Yii;
use yii\debug\Module as DebugModule;
use models\User;
 
class Module extends DebugModule
{
    private $_basePath;
 
    protected function checkAccess()
    {
        $user = Yii::$app->user->getUser();
        
        if ($user && $user->username == 'romain') {
            return true;
        }
//        return parent::checkAccess();
        return false ;
    }
 
    /**
     * Returns the root directory of the module.
     * It defaults to the directory containing the module class file.
     * @return string the root directory of the module.
     */
    public function getBasePath()
    {
        if ($this->_basePath === null) {
            $class = new \ReflectionClass(new yii\debug\Module('debug'));
            $this->_basePath = dirname($class->getFileName());
        }
 
        return $this->_basePath;
    }
}