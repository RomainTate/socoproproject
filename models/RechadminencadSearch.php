<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Rechadminencad;

/**
 * RechAdminEncadSearch represents the model behind the search form about `app\models\RechAdminEncad`.
 */
class RechadminencadSearch extends Rechadminencad
{
    public $contactNom;
    public $contactPrenom;
    public $contactCivilite;
    public $contactGSMPrincipal;
    public $contactEmailPrincipal;
    public $contactAnneeNaissance;
    public $contactCreatedAt;    
            /*** ! **/
    public $contactGSMPrincipalNotNull = 0;
    public $contactEmailPrincipalNotNull = 0;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rae_type', 'contactNom' ,
                'contactPrenom' , 'contactGSMPrincipal' , 'contactEmailPrincipal','contactAnneeNaissance','contactCreatedAt'], 'safe'],
            [['cont_id', 'contactGSMPrincipalNotNull', 'contactEmailPrincipalNotNull'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Rechadminencad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cont_id' => $this->cont_id,
        ]);

//        $query->andFilterWhere(['like', 'rae_type', $this->rae_type]);
        
        if (isset($params['RechadminencadSearch']['contactGSMPrincipalNotNull']) && $params['RechadminencadSearch']['contactGSMPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_gsm_principal' => null]]);
            }]);
        }
        
        if (isset($params['RechadminencadSearch']['contactEmailPrincipalNotNull']) &&  $params['RechadminencadSearch']['contactEmailPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_email_principal' => null]]);
            }]);
        }
        
        if ($this->rae_type != null) {
            if (is_array($this->rae_type)) {
                foreach ($this->rae_type as $rae_type) {
                    $query->orFilterWhere(['rae_type' => $this->rae_type]);
                }
            } else {
                $query->andFilterWhere(['like', 'rae_type', $this->rae_type]);
            }
        }

        return $dataProvider;
    }
}
