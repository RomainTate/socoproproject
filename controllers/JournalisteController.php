<?php

namespace app\controllers;

use Yii;
use app\models\Journaliste;
use app\models\JournalisteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * JournalisteController implements the CRUD actions for Journaliste model.
 */
class JournalisteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search', 'view'],
                        'roles' => ['indexJournaliste'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'addmoreinfos' , 'addjournal', 'deletemedia'],
                        'roles' => ['createJournaliste'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteJournaliste'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Journaliste models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JournalisteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch() {
        return $this->actionIndex();
    }

    /**
     * Displays a single Journaliste model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Journaliste model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Journaliste();
        $contactModel = new \app\models\Contact();
        $contactModel->setScenario('typeJournaliste');
        $post = Yii::$app->request->post();
        
        if ($contactModel->load($post) && $model->load($post)) {
            if($contactModel->cont_langue != '') {
                $contactModel->cont_langue = ContactController::setLangue($contactModel->cont_langue);
            }
            if ($contactModel->cont_email_principal && $model->mailExist($contactModel->cont_email_principal)) {
                if (!isset($post['forceadd'])) {
                    Yii::$app->session->setFlash('error', "Un journaliste avec cet e-mail existe déjà !");
                    return $this->render('create', [
                                'model' => $model,
                                'contactModel' => $contactModel,
                    ]);
                }
            }
            $contactModel->cont_nom = strtoupper($contactModel->cont_nom);
            $contactModel->cont_created_at = date("Y-m-d H:i:s");
            if ($model->journaliste_date_contact != '') {
                $model->journaliste_date_contact = date("Y-m-d", strtotime($model->journaliste_date_contact));
            }
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
                if ($model->save(false)){ // save(false) bypass validation (already did 2 lines ago)
                    if($post['Journaliste']['nommedia'] != '') {
                        $media_id = $this->addMedia($post['Journaliste']['nommedia']);
                        if($media_id) 
                        {
                            $model->media_id = $media_id;
                            $model->save(false);
                        }
                    }
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                }
            } else {
                throw new \yii\base\ErrorException;
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'contactModel' => $contactModel,
            ]);
        }
    }

    /**
     * Updates an existing Journaliste model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        $model = $this->findModel($id);
        $contactModel = $model->contact;
        $contactModel->setScenario('typeJournaliste');
        $post = Yii::$app->request->post();

        if ($contactModel->load($post) ) {
            if($contactModel->cont_langue != '') {
                $contactModel->cont_langue = ContactController::setLangue($contactModel->cont_langue);
            }
            $contactModel->cont_nom = strtoupper($contactModel->cont_nom);
            $contactModel->save(); 
            return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                
        }
        
        if (!Yii::$app->request->isAjax) {
            return $this->render('update', [
                        'model' => $model,
                        'contactModel' => $contactModel
            ]);
        }
        return $this->renderAjax('update', [
                    'model' => $model,
                    'contactModel' => $contactModel
        ]);
    }

    /**
     * Deletes an existing Journaliste model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if ($this->findModel($id)->delete()) {
            \Yii::$app->session->setFlash('success', "La suppression à été effectuée !");
        } else {
            \Yii::$app->session->setFlash('danger', "Erreur lors de la suppression !");
        }
        if (Yii::$app->request->referrer && (strpos(Yii::$app->request->referrer, 'index') !== false)) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Journaliste model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Journaliste the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Journaliste::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('La page demandée n\'existe pas.');
        }
    }
    
    public function actionAddmoreinfos ($contactId) {
        $post = Yii::$app->request->post();
        $model = $this->findModel($contactId);
        if($model->load($post)) {
            if ($model->journaliste_date_contact != '') {
                $model->journaliste_date_contact = date("Y-m-d", strtotime($model->journaliste_date_contact));
            }
            if($model->save()) {
                return $this->redirect(['view', 'id' => $contactId]);
            } else {
                throw new \yii\base\ErrorException();
            }
        } else {
            return $this->renderAjax('_formInfos', [
                        'model' => $model,
            ]);
        }
    }
    
    public function actionAddjournal($contactId) {
        $model = $this->findModel($contactId);
        $post = Yii::$app->request->post();
        
        if ($post) {
            $model->media_id = $post['entrepriseId'] ;
            $model->save();
            return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
            
        } else {
                    return $this->renderAjax('addJournal', [
                    'contactId' => $contactId
        ]);
        }
    }
    
    public function actionDeletemedia($id) {
        $model = $this->findModel($id);
        $model->media_id = null;
        $model->save();
        return $this->redirect(Yii::$app->request->referrer); //redirect to previous page
    }

    public function addMedia($mediaTitre) {
        
        $media = \app\models\Media::find()->where(['media_titre' => $mediaTitre])->one();
        if($media) {
            return $media->media_id;
        } else {
            $media = new \app\models\Media();
            $media->media_titre = $mediaTitre;
            $media->save();
            return $media->media_id;
        }
        
        
    }

}
