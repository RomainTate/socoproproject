<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers\user;

use app\models\Secteur;
use app\models\Gerer;
use yii\helpers\ArrayHelper;
use dektrium\user\models\UserSearch;
use dektrium\user\controllers\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{

    public function actionIndex($id = null)
    {
        $searchModel  = \Yii::createObject(UserSearch::className());
        $dataProvider = $searchModel->search(\Yii::$app->request->get());
        $authRoles = ArrayHelper::map(\Yii::$app->authManager->getRoles(), 'name' , 'name');
        unset($authRoles['Foire']);
        
        $secteurs = ArrayHelper::map(Secteur::find()->all(), 'sect_id', 'sect_nom');
        $gerer = ArrayHelper::map(Gerer::findAll(['user_id' => $id]),'sect_id' , 'sect_id' ); //can be empty
        
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel'  => $searchModel,
            'authRoles' => $authRoles,
            'secteurs' => $secteurs,
            'gerer' => $gerer
        ]);
    }
    
    private function updateAdmins($id, $post) {
        if (!$id || !$post) {
            throw new \yii\base\ErrorException;
        }
        
        $currentAdmins = require (\Yii::$app->basePath . '/config/admins.php');
        $user = \app\models\User::findOne(['id' =>$id]);

        if ($post['role'] == 'Administrateur') {
            if (!in_array($user->username, $currentAdmins)) {
                array_push($currentAdmins, $user->username);
                $content = "<?php return [";
                foreach ($currentAdmins as $currentAdmin) {
                    $content .= " '" . $currentAdmin . "', ";
                }
                $content .= "];";
                file_put_contents(\Yii::$app->basePath . '/config/admins.php', $content);
            }
        } else {
            if (in_array($user->username, $currentAdmins)) {
                $content = "<?php return [";
                foreach ($currentAdmins as $currentAdmin) {
                    if ($currentAdmin != $user->username) {
                        $content .= " '" . $currentAdmin . "', ";
                    }
                }
                $content .= "];";
                file_put_contents(\Yii::$app->basePath . '/config/admins.php', $content);
            }
        }
    }

    private function updateSecteurs($id , $post) {
        $links = (array_key_exists('gerer', $post)) ? $post['gerer'] : null;
        \app\models\Gerer::deleteAll(['user_id' => $id]);

        if ($post['role'] != 'Administrateur' && $links) {
            foreach ($links as $link) {
                $gerer = new \app\models\Gerer();
                $gerer->user_id = $id;
                $gerer->sect_id = $link;
                $gerer->save();
            }
        }
    }
    
    public function actionUpdaterole($id) {
        $post = \Yii::$app->request->post();
        $this->updateAdmins($id , $post);
        
        $this->updateSecteurs($id , $post);
        
        $assignment = \app\modules\auth\models\AuthAssignment::findOne(['user_id' => $id]);

         if($assignment) {
            $assignment->item_name = $post['role'];
            if ($assignment->save()) {
                \Yii::$app->session->setFlash('success', "L'assignation à été mise à jour !");
            }
        } else {
            $authManager = \Yii::$app->authManager;
            if($authManager->assign($authManager->getRole($post['role']), $id)) {
                \Yii::$app->session->setFlash('success', "L'assignation à été mis à jour !");
            }
        }
        
        return $this->redirect(['index', 'id'=>  $id]);
    }
    
    public function actionCreate() {
            
        /** @var User $user */
        $user = \Yii::createObject([
            'class'    => \app\models\User::className(),
            'scenario' => 'create',
        ]);

        if ($user->load(\Yii::$app->request->post()) && $user->create()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'User has been created'));
            return $this->redirect(['index', 'id' => $user->id]);
        }

        return $this->render('create', [
            'user' => $user,
        ]);
    }
    
    public function actionIndexdoublon () {
        $searchModel = new \app\models\ProducteurSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['needConfirm' => 1]);
        
        return $this->render('indexdoublon', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }
    public function actionIndexfoireprod () {
        $searchModel = new \app\models\ProducteurSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['needConfirm' => 2]);
        
        return $this->render('indexfoireprod', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }
    
    public function actionIndexfoireconso () {
        $searchModel = new \app\models\ConsommateurSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['needConfirm' => 2]);
        
        return $this->render('indexfoireconso', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel
        ]);
    }
    
    public function actionIndexnewsletters() {
        $newsletters = new \yii\data\ArrayDataProvider([
            'allModels' => \app\models\Newsletter::find()->all(),
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => ['id', 'name'],
            ],
        ]);
        
        return $this->render('indexnewsletters', [
            'newsletters' => $newsletters,
        ]);
    }
    
}
