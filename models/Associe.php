<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "associe".
 *
 * @property integer $cont_id 
 * @property integer $cont_id_prod
 * @property integer $assoc_type
 *
 * @property Contact $contact
 * @property Producteur $producteur
 */
class Associe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'associe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
//            [['cont_id', 'cont_id_prod'], 'required'],
            [['cont_id', 'cont_id_prod', 'assoc_type'], 'integer'],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
            [['cont_id_prod'], 'exist', 'skipOnError' => true, 'targetClass' => Producteur::className(), 'targetAttribute' => ['cont_id_prod' => 'cont_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'cont_id_prod' => 'Cont Id Prod',
            'assoc_type' => 'Relation'
        ];
    }
    
    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        return parent::beforeSave($insert);
    }
    
    public function afterDelete() {
        Contact::findOne($this->cont_id)->delete();
        return parent::afterDelete();
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducteur()
    {
        return $this->hasOne(Producteur::className(), ['cont_id' => 'cont_id_prod']);
    }
}
