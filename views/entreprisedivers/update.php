<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Entreprisedivers */

$this->title = 'Mettre à jour l\'Entreprise ' . $model->entreprise->entr_nom;
$this->params['breadcrumbs'][] = ['label' => 'Entreprisedivers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->entreprise->entr_nom, 'url' => ['view', 'id' => $model->entr_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="entreprisedivers-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'entrepriseModel' => $entrepriseModel
    ]) ?>

</div>
