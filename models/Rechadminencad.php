<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "rech_admin_encad".
 *
 * @property integer $rae_type
 * @property integer $cont_id
 *
 * @property Contact $contact
 */
class Rechadminencad extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rech_admin_encad';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rae_type'], 'required'],
            [['cont_id','rae_type'], 'integer'],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rae_type' => 'Type de contact',
            'cont_id' => 'Cont ID',
            /* related attributes*/
            'contactNom' => 'Nom',
            'contactPrenom' => 'Prénom',
            'contactGSMPrincipal' => 'Numéro GSM',
            'contactEmailPrincipal' => 'E-mail'
        ];
    }
    
    public function beforeDelete() {
        \rmrevin\yii\module\Comments\models\Comment::deleteAll(['entity' => 'rea-'.$this->cont_id]);
        Appartenir::deleteAll(['cont_id' => $this->cont_id]);
        return parent::beforeDelete();
    }

    public function afterDelete() {
        Contact::findOne($this->cont_id)->delete();
        return parent::afterDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
    
      /* Getter for contact nom */
    public function getContactNom() {
        return $this->contact->cont_nom;
    }
    
    /* Getter for contact prenom */
    public function getContactPrenom() {
        return $this->contact->cont_prenom;
    }
    

    public function getContactCivilite() {
        return $this->contact->cont_civilite;
    }
    
    public function getContactGSMPrincipal() {
        return $this->contact->cont_gsm_principal;
    }
    
    public function getContactEmailPrincipal() {
        return $this->contact->cont_email_principal;
    }
    
    public function getContactAnneeNaissance() {
        return $this->contact->cont_annee_naissance;
    }
    public function getContactCreatedAt() {
        return $this->contact->cont_annee_naissance;
    }
    
    
    public function getSecteurs()
    {
        return $this->hasMany(Secteur::className(), ['sect_id' => 'sect_id'])->viaTable('travailler', ['cont_id' => 'cont_id']);
    }
    
//    public function setSecteurs($v)
//    {     
//        $this->secteurs = $v;
//    }
    
    public function getSecteursID() {
        if($this->secteurs == null) return null;
        return $this->secteurs->sect_id;
    }
}
