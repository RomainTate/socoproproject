<?php

namespace app\controllers;

use Yii;
use app\models\Consommateur;
use app\models\ConsommateurSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ConsommateurController implements the CRUD actions for Consommateur model.
 */
class ConsommateurController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'search', 'view'],
                        'roles' => ['indexConso'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'update', 'addmoreinfos' , 'addjournal', 'deletemedia'],
                        'roles' => ['createConso'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => ['deleteConso'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['accept'],
                        'roles' => ['Administrateur'],
                    ],
                ],
            ]
        ];
    }

    /**
     * Lists all Consommateur models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ConsommateurSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['needConfirm' => 0]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionSearch() {
        return $this->actionIndex();
    }

    /**
     * Displays a single Consommateur model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if( ((!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') || 
                ($model->needConfirm && (!\Yii::$app->user->getUser()->getIsAdmin())) ) {
            throw new NotFoundHttpException('La page demandée n\'existe pas.');
        }
        if ($model->needConfirm != 0) {
            $otherConsos = $this->getDoublons($model);
        } else {
            $otherConsos = null;
        }
        return $this->render('view', [
            'model' => $model,
            'otherConsos' => $otherConsos
        ]);
    }

    /**
     * Creates a new Consommateur model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $foire = Yii::$app->user->getUser()->username == 'foire'; // true or false
        $model = new Consommateur();
        $contactModel = new \app\models\Contact();
        $adrModel = new \app\models\Adresse();
        $adrModel->setScenario('formContact');
        if($foire) {
            $contactModel->setScenario('foire');
            $model->needConfirm = 2;
        }
        $post = Yii::$app->request->post();
        
        if ($contactModel->load($post) && $model->load($post)&& $adrModel->load($post)) {
            if (!$foire) {
                if ($contactModel->alreadyExists()) {
                    if (!isset($post['forceadd'])) {
                        Yii::$app->session->setFlash('error', "Ce contact existe déjà !");
                        return $this->render('create', [
                                    'model' => $model,
                                    'contactModel' => $contactModel,
                                    'adrModel' => $adrModel
                        ]);
                    }
                }
            }
            if(!empty($post['Consommateur']['consoLieuContactName'])&& $post['Consommateur']['consoLieuContactName'] != '') {
                $model->conso_lieu_contact = LieuprisecontactController::actionCreate($post['Consommateur']['consoLieuContactName']);
            } else {
                $model->conso_lieu_contact = null;
            } 
            $contactModel->cont_nom = strtoupper($contactModel->cont_nom);
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
//                if(isset($post['Consommateur']['nomentr'])) {
//                    if($post['Consommateur']['fct'] == '') {
//                        $fonction = null;
//                    } else {
//                        $fonction = $post['Consommateur']['fct'];
//                    }
//                    $societe = \app\models\Entreprise::find()->where(['entr_societe' => $post['Consommateur']['nomentr']])->one();
//                    if($societe) {
//                        AppartenirController::addContactEntreprise($model->cont_id, $societe->entr_id, $fonction);
//                    } else {
//                        $societe_id = EntrepriseController::createEntreprise($post['Consommateur']['nomentr']);
//                        AppartenirController::addContactEntreprise($model->cont_id, $societe_id, $fonction);
//                    }
//                }
                if ($model->save(false)&&($this->actionLinkSecteurs($model->cont_id,$post['Consommateur']['secteurs'])))
                    { // save(false) bypass validation (already did 2 lines ago)
                    if(array_key_exists('newsletters', $post)) {
                        ContactController::manageNewsletters($model->cont_id, $post['newsletters']);
                    }
                    if($adrModel->adr_rue != '' || $adrModel->adr_code_postal != '' || $adrModel->adr_ville != '' || $adrModel->adr_province != '') {
                        $adrModel->save();
                        $contactModel->adr_id = $adrModel->adr_id;
                        $contactModel->save();
                    }
                    if($foire) {
                        if($post['comment'] != '') {
                            $comment = new \rmrevin\yii\module\Comments\models\Comment();
                            $comment->entity = 'consommateur-'.$model->cont_id;
                            $comment->text = $post['comment'];
                            $comment->created_by = \dektrium\user\models\User::find()->where(['username' => 'foire'])->one()->id;
                            $comment->updated_by = \dektrium\user\models\User::find()->where(['username' => 'foire'])->one()->id;
                            $comment->save();
                        }
                        Yii::$app->session->setFlash('success', 'Merci de vous être enregistré <br /> Redirection vers la page d\'accueil en cours <br /> <a href="'.\yii\helpers\Url::home().'"> Cliquez ici si vous n\'êtes pas redirigé </a>');
                        return $this->redirect(\yii\helpers\Url::home());
                    }
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                } else {
                    throw new \yii\base\ErrorException; // should never been thrown but we never know
                }
            } else { 
                throw new \yii\base\ErrorException;
            }
        } else {
            return $this->render('create', [
                        'model' => $model,
                        'contactModel' => $contactModel,
                        'adrModel' => $adrModel
            ]);
        }
    }

    /**
     * Updates an existing Consommateur model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) { 
        if( (!Yii::$app->user->isGuest) && Yii::$app->user->getUser()->username == 'foire') {
            throw new NotFoundHttpException;
        }
        $model = $this->findModel($id);
        $contactModel = $model->contact;
        $post = Yii::$app->request->post();
        
        if ($contactModel->load($post) && $model->load($post)) {
            if(!empty($post['Consommateur']['consoLieuContactName'])&& $post['Consommateur']['consoLieuContactName'] != '') {
                $model->conso_lieu_contact = LieuprisecontactController::actionCreate($post['Consommateur']['consoLieuContactName']);
            } else {
                $model->conso_lieu_contact = null;
            } 
            $contactModel->cont_nom = strtoupper($contactModel->cont_nom);
            if ($model->validate() && $contactModel->save()) { //validate $model BEFORE saving $contactModel
                $model->cont_id = $contactModel->cont_id;
                if ($model->save(false)&&($this->actionLinkSecteurs($model->cont_id,$post['Consommateur']['secteurs'])))
                    { // save(false) bypass validation (already did 2 lines ago)
                    if(array_key_exists('newsletters', $post)) {
                        $newsletters = $post['newsletters'];
                    } else {
                        $newsletters = null;
                    }
                    ContactController::manageNewsletters($model->cont_id, $newsletters);
                    return $this->redirect(['view', 'id' => $contactModel->cont_id]);
                } else {
                    throw new \yii\base\ErrorException; // should never been thrown but we never know
                }
            } else { 
                throw new \yii\base\ErrorException;
            }
        }
        
        if (!Yii::$app->request->isAjax) {
            return $this->render('update', [
                        'model' => $model,
                        'contactModel' => $contactModel
            ]);
        }
        
        return $this->renderAjax('update', [
                    'model' => $model,
                    'contactModel' => $contactModel
        ]);
    }
    
    public function actionAddmoreinfos ($contactId) {
        $post = Yii::$app->request->post();
        $model = $this->findModel($contactId);
        if($model->load($post)) {
            if(!empty($post['Consommateur']['consoLieuContactName'])&& $post['Consommateur']['consoLieuContactName'] != '') {
                $model->conso_lieu_contact = LieuprisecontactController::actionCreate($post['Consommateur']['consoLieuContactName']);
            } else {
                $model->conso_lieu_contact = null;
            } 
            if($model->save()&&($this->actionLinkSecteurs($model->cont_id,$post['Consommateur']['secteurs']))) {
                return $this->redirect(['view', 'id' => $contactId]);
            }
        } else {
            return $this->renderAjax('_formInfos', [
                        'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Consommateur model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id, $pageToRedirect = '')
    {
        $this->findModel($id)->delete();
        if($pageToRedirect == 'foire') {
            return $this->redirect(\yii\helpers\Url::to(['/user/admin/indexfoireconso']));
        }
        if (Yii::$app->request->referrer && (strpos(Yii::$app->request->referrer, 'index') !== false)) {
            return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Consommateur model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Consommateur the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Consommateur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    private function actionLinkSecteurs($id, $secteurs) {
        // get all auth_item_child where 'parent' = $role & delete them
        \app\models\Interesser::deleteAll(['cont_id' => $id]);

        if (is_array($secteurs)) {
            foreach ($secteurs as $secteur) {
                $travail = new \app\models\Interesser();
                $travail->cont_id = $id;
                $travail->sect_id = $secteur;
                if (!$travail->save()) {
                    return false;
                }
            }
        }
        return true;
    }
    
    public function getDoublons($model) {
        $otherConsos = [];
        if($model->contact->cont_nom && $model->contact->cont_prenom) {
            foreach (Consommateur::find()->joinWith('contact')->where(['cont_nom' => $model->contact->cont_nom, 'cont_prenom' => $model->contact->cont_prenom, 'needConfirm' => 0])
                    ->andWhere(['not', ['consommateur.cont_id' => $model->cont_id]])->all() as $v) {
                $otherConsos[] = $v;
            }
        }
        return $otherConsos;
    }
    
    public function actionAccept($id) {
        $model = $this->findModel($id);
        $model->needConfirm = 0;
        $model->save();
        Yii::$app->session->setFlash('notice', "Ce contact à bien été validé <br /> <a href='../user/admin/indexfoireconso'> Cliquez ici pour revenir à la liste des Consommateurs en attente de validation </a>");
        return $this->redirect(['view', 'id' => $model->cont_id]);
    }
}
