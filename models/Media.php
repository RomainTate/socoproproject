<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "media".
 *
 * @property integer $media_id
 * @property string $media_societe
 * @property string $media_titre
 * @property string $media_telephone
 * @property string $media_email
 * @property string $media_site_internet
 * @property string $media_type_presse
 * @property string $media_type_support
 * @property string $media_frequence
 * @property string $media_audience
 * @property string $media_audience_cible
 * @property string $media_pays
 * @property string $media_langue
 * @property integer $adr_id
 *
 * @property Journaliste[] $journalistes
 * @property Adresse $adresse
 */
class Media extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'media';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adr_id'], 'integer'],
            [['media_titre'], 'required'],
            [['media_titre'], 'unique'],
            [['media_societe', 'media_titre', 'media_type_support', 'media_frequence'], 'string', 'max' => 25],
            [['media_telephone'], 'string', 'max' => 12],
            [['media_email', 'media_site_internet'], 'string', 'max' => 30],
            [['media_type_presse', 'media_pays'], 'string', 'max' => 20],
            [['media_audience', 'media_audience_cible'], 'string', 'max' => 50],
            [['media_email'], 'unique'],
            [['adr_id'], 'exist', 'skipOnError' => true, 'targetClass' => Adresse::className(), 'targetAttribute' => ['adr_id' => 'adr_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
//            'media_id' => 'Media ID',
            'media_societe' => 'Société / Groupe de Presse',
            'media_titre' => 'Média',
            'media_telephone' => 'Téléphone',
            'media_email' => 'Email',
            'media_site_internet' => 'Site Internet',
            'media_type_presse' => 'Type Presse',
            'media_type_support' => 'Type Support',
            'media_frequence' => 'Périodicité',
            'media_audience' => 'Audience Média',
            'media_audience_cible' => 'Audience Cible',
            'media_langue' => 'Langue Média',
            'media_pays' => 'Pays',
//            'adr_id' => 'Adr ID',
        ];
    }
    
//    public function checkTitreExists($titre) {
//        $this->addError($attribute);
//    }
    
    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        return parent::beforeSave($insert);
    }
    
    public function beforeDelete() {
        if (!empty($this->journalistes)) {
            foreach ($this->journalistes as $journaliste) {
                $journ = Journaliste::find()->where(['cont_id' => $journaliste->cont_id])->one();
                $journ->media_id = null;
                $journ->save();
            }
        }
        if($this->adresse) {
            Adresse::find()->where(['adr_id' => $this->adr_id])->one()->delete();
        }
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJournalistes()
    {
        return $this->hasMany(Journaliste::className(), ['media_id' => 'media_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdresse()
    {
        return $this->hasOne(Adresse::className(), ['adr_id' => 'adr_id']);
    }
}
