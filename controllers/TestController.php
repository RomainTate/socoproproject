<?php
namespace app\controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use yii\web\Controller;

/**
 * Description of TestController
 *
 * @author Utilisateur
 */
class TestController extends Controller {

    //put your code here
    public function actionIndex() {
        
        $post = \Yii::$app->request->post();
        if($post) {
//            die(var_dump($post));
            $timestamp_debut = time(true);
            var_dump($timestamp_debut);
            for($i = 0; $i < $post['testInput']; $i++) {
                var_dump(time(true));
                sleep(1);
                if (time(true) > $timestamp_debut + 10) {
                    $timeleft = $post['testInput'] - $i;
                    return $this->render('left', 
                        ['timeleft' => $timeleft]);
                }
            }
        }
        
        return $this->render('index');
    }

}
