<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "avoir".
 *
 * @property integer $cont_id
 * @property integer $newsletter_id
 *
 * @property Newsletter $newsletter
 * @property Contact $contact
 */
class Recevoir extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'recevoir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id', 'newsletter_id'], 'required'],
            [['cont_id', 'newsletter_id'], 'integer'],
            [['newsletter_id'], 'exist', 'skipOnError' => true, 'targetClass' => Newsletter::className(), 'targetAttribute' => ['newsletter_id' => 'newsletter_id']],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cont_id' => 'Cont ID',
            'newsletter_id' => 'Newsletter ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsletter()
    {
        return $this->hasOne(Newsletter::className(), ['newsletter_id' => 'newsletter_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
}
