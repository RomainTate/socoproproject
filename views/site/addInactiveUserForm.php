<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\InactiveUser */
/* @var $form ActiveForm */
?>
<div class="site-addInactiveUserForm">

    <?php $form = ActiveForm::begin([
        'action' => ['/site/addinactiveuserform']
    ]); ?>

        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'Nom') ?>
        <?= $form->field($model, 'Prenom') ?>
        <?= $form->field($model, 'GSM') ?>
        <?= $form->field($model, 'Telephone') ?>
    
        <div class="form-group">
            <?= Html::submitButton('Envoyer', ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- site-addInactiveUserForm -->
