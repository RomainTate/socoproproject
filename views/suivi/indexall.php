<?php
use yii\helpers\Html;
use kartik\export\ExportMenu;
if(Yii::$app->request->post('export_type')) {
    $exporting = true;
} else {
    $exporting = false;
}
$this->title = 'Suivi global';
?>

<?php 
    $gridColumns = [
        [
            'label' => 'Date',
//            'attribute' => 'suividates',
//            'filter' => \yii\jui\DatePicker::widget([
//                    'model'=>$searchModel,
//                    'attribute'=>'minDate',
//                    'language' => 'fr',
//                    'dateFormat' => 'dd-MM-yyyy',
//                    'options' => ['class' => 'form-control', 'placeholder' => 'A partir du ...']
//                ]),
            'filter' => \kartik\daterange\DateRangePicker::widget([
                'model' => $searchModel,
                'attribute' => 'suividates',
                'convertFormat'=>true,
                'pluginOptions' => [
                    'locale' => [
                        'format' => 'd-m-Y', //01-11-2017
                        'separator' => ' | '
                    ]
                ]
            ]),
            'value' => function ($model) {
                return Yii::$app->formatter->asDatetime($model->suivi_date);
            }
        ],
        [
            'label' => 'Producteur',
            'attribute' => 'prodName',
            'format' => 'raw',
            'value' => function ($model) {
            if (!$model->contact) return 'Inconnu/Supprimé';
            if (!$model->contact->producteur) return 'Inconnu/Supprimé';
                return $model->contact->producteur->getName();
            }
        ],
        [
            'label' => 'Auteur',
            'attribute' => 'userID',
            'filter' => yii\helpers\ArrayHelper::map(app\models\User::find()->select(['id', 'username'])->all(), 'id', 'username'),
            'value' => function ($model) {
            if(!$model->user) return 'Inconnu/Supprimé';
                return $model->user->username;
            }
        ],
        [
            'label' => 'Type',
            'format' => 'raw',
            'attribute' => 'suivi_type_contact',
            'filter' => ['1' => 'Téléphonique', '2' => 'E-mail', '3' => 'Visite', '4' => 'Foire/Evènement'],
            'value' => function ($model) {
            if($model->suivi_type_contact == 1) return 'Téléphonique';
            if($model->suivi_type_contact == 2) return 'E-mail';
            if($model->suivi_type_contact == 2) return 'Visite';
            if($model->suivi_type_contact == 2) return 'Foire/Evènement';
            }
        ],
        [
            'label' => 'Objet',
            'attribute' => 'suiviobjet_id',
            'filter' =>  \yii\helpers\ArrayHelper::map(\app\models\Objet::find()->all(), 'objet_id', 'objet_nom'),
            'value' => function ($model) {
                return implode(", ",yii\helpers\ArrayHelper::map($model->objets, 'objet_id','objet_nom'));
            }
            //dropDownList(\yii\helpers\ArrayHelper::map(\app\models\Objet::find()->all(), 'objet_id', 'objet_nom'),
//        $form->field($model, 'suiviobjets_ids')->dropDownList($model->suiviobjets_ids
//                ['class' => 'js-multiple' , 'multiple'=>'multiple'])
        ],
        [
            'label' => 'En attente',
            'attribute' => 'suivi_suite',
            'filter' => ['0' => 'Non', '1' => 'Oui'],
            'value' => function ($model) {
            return $model->suivi_suite? 'Oui' : 'Non' ;
            }
        ],
        [
            'label' => 'Commentaire',
            'attribute' => 'suivi_commentaire',
            'format' => 'raw',
            'value' => function ($model) use($exporting) {
            if($exporting) return $model->suivi_commentaire; 
            return 
            '<div class="tdCommentSuivi">'.
                yii\helpers\BaseStringHelper::truncate(htmlspecialchars($model->suivi_commentaire), 100, Html::a(' ... lire la suite', null, [ 'name' => "modalButton", 'value'=>\yii\helpers\Url::to(['/suivi/showcom', 'id' => $model->suivi_id])]))
            .'</div>';
            },
        ],
    ];
?>
    <div class="x_panel">
        <div id="gridHeader">
            <h1 class="x_title"><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="pull-right">
            <label>Exportation :</label>
            <?=
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumns,
                'target' => ExportMenu::TARGET_POPUP,
                'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
//                    'stream' => false,
                'exportConfig' => [
                    ExportMenu::FORMAT_HTML => false,
                    ExportMenu::FORMAT_TEXT => false,
                    ExportMenu::FORMAT_PDF => false,
                    ExportMenu::FORMAT_EXCEL => false,
//                        'encoding' => 'utf-8'
                ],
//                'noExportColumns' => [count($gridColumns) - 1]
            ]);
            ?>
        </div>
        
        <?= kartik\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'suiviGrid',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'filterModel' => $searchModel,
            'columns' => $gridColumns,
            'responsiveWrap' => false,
//            'tableOptions' => ['style' => 'table-layout: fixed']
        ]);?>
    </div>

<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home() . 'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
$this->registerJs("$('.js-multiple').select2();", yii\web\View::POS_READY, 'select2-prod');