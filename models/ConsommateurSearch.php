<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Consommateur;

/**
 * ConsommateurSearch represents the model behind the search form about `app\models\Consommateur`.
 */
class ConsommateurSearch extends Consommateur
{
    public $contactNom;
    public $contactPrenom;
    public $contactCivilite;
    public $contactTelephonePrincipal;    
    public $contactGSMPrincipal;
    public $contactEmailPrincipal;
    public $contactAnneeNaissance;
    public $contactCreatedAt;
    public $consoLieuContactName;
    public $secteursID;
    public $rue;
    public $localite;
    public $province;
//    public $nomentr;
//    public $nomentrs;
//    public $fcts;
    public $newsletter;
    public $conso_societes;
    public $conso_fonctions;
                /*** ! **/
    public $contactGSMPrincipalNotNull = 0;
    public $contactEmailPrincipalNotNull = 0;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['conso_lieu_contact', 'cont_id', 'contactGSMPrincipalNotNull', 'contactEmailPrincipalNotNull',  'contactTelephonePrincipal' ], 'integer'],
            [['secteursID', 'contactNom', 'contactPrenom', 'contactGSMPrincipal', 'contactEmailPrincipal','contactAnneeNaissance', 'consoLieuContactName',
                 'contactTelephonePrincipal', 'newsletter', 'conso_societes', 'conso_fonctions'
//                , 'nomentr', 'nomentrs'
                ], 'safe'],
            [['localite', 'province', 'rue', 'conso_societe', 'conso_fonction'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Consommateur::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!($this->load($params) && $this->validate())) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            $query->joinWith('contact.newsletters'); //Eager loading
            $query->joinWith('contact.adresse'); //Eager loading
            $query->joinWith('secteurs');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'conso_lieu_contact' => $this->conso_lieu_contact,
//            'cont_id' => $this->cont_id,
//        ]);
        
        // filter by contact nom
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_nom LIKE "%' . $this->contactNom . '%"');
            }]);
            
        // filter by contact prenom
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_prenom LIKE "%' . $this->contactPrenom . '%"');
            }]);
        
        if (isset($params['ConsommateurSearch']['consoLieuContactName']) && $params['ConsommateurSearch']['consoLieuContactName'] != '') {    
            $query->joinWith(['consoLieuContact' => function ($q) {
//                    $q->where('lieu_prise_contact.lieu LIKE "%' . $this->consoLieuContactName . '%"');
                $q->where(['lieu_prise_contact_id' => $this->consoLieuContactName]);
                }]);
        }
        
        if (isset($params['ConsommateurSearch']['contactTelephonePrincipal']) && $params['ConsommateurSearch']['contactTelephonePrincipal'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_telephone_principal LIKE "%' . $this->contactTelephonePrincipal . '%"')
                    ;
                }]);
        }
        if (isset($params['ConsommateurSearch']['contactGSMPrincipal']) && $params['ConsommateurSearch']['contactGSMPrincipal'] != '') { //fix bug if params is empty
            $query->joinWith(['contact' => function ($q) {
                    $q->where('contact.cont_gsm_principal LIKE "%' . $this->contactGSMPrincipal . '%"')
                    ;
                }]);
        }

        if (isset($params['ConsommateurSearch']['contactEmailPrincipal']) && $params['ConsommateurSearch']['contactEmailPrincipal'] != '') { //fix bug if params is empty
        $query->joinWith(['contact' => function ($q) {
                $q->where('contact.cont_email_principal LIKE "%' . $this->contactEmailPrincipal . '%"');
            }]);
        }
        
        if (isset($params['ConsommateurSearch']['contactGSMPrincipalNotNull']) && $params['ConsommateurSearch']['contactGSMPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_gsm_principal' => null]]);
            }]);
        }
        
        if (isset($params['ConsommateurSearch']['contactEmailPrincipalNotNull']) &&  $params['ConsommateurSearch']['contactEmailPrincipalNotNull'] == '1' ) {
            $query->joinWith(['contact' => function ($q) {
                $q->where(['not' , ['contact.cont_email_principal' => null]]);
            }]);
        }
        
        if (isset($params['ConsommateurSearch']['rue']) && $params['ConsommateurSearch']['rue'] != '') {
            $query->joinWith('contact.adresse');
            $query->andFilterWhere(['like','adr_rue', $this->rue]);
        }
        if (isset($params['ConsommateurSearch']['localite']) && $params['ConsommateurSearch']['localite'] != '') {
            $query->joinWith('contact.adresse');
            $query->andFilterWhere(['like','adr_ville', $this->localite]);
        }
        if (isset($params['ConsommateurSearch']['province']) && $params['ConsommateurSearch']['province'] != '') {
            $query->joinWith('contact.adresse');
            $query->andFilterWhere(['like','adr_province', $this->province]);
        }

        if (isset($params['ConsommateurSearch']['newsletter']) && $params['ConsommateurSearch']['newsletter'] != '') {
            $query->joinWith('contact.newsletters');
            $query->andFilterWhere(['or like','newsletter_nom', $params['ConsommateurSearch']['newsletter']]);
        }
//        if (isset($params['ConsommateurSearch']['nomentr']) && $params['ConsommateurSearch']['nomentr'] != '') {
//            $query->joinWith('contact.entreprises');
//            $query->andFilterWhere(['or like','entr_societe', $params['ConsommateurSearch']['nomentr']]);
//        }
//        if (isset($params['ConsommateurSearch']['nomentrs']) && is_array($params['ConsommateurSearch']['nomentrs'])) {
//            $query->joinWith('contact.entreprises');
//            $query->andFilterWhere(['or like','entr_societe', $params['ConsommateurSearch']['nomentrs']]);
//        }
//        if (isset($params['ConsommateurSearch']['fcts']) && is_array($params['ConsommateurSearch']['fcts'])) {
//            $query->joinWith('contact.entreprises');
//            $query->andFilterWhere(['or like','fonction', $params['ConsommateurSearch']['fcts']]);
//        }
        
        $query->andFilterWhere(['like', 'conso_societe', $this->conso_societe]);
        $query->andFilterWhere(['like', 'conso_fonction', $this->conso_fonction]);
        
        if($this->conso_societes) {
            $query->andFilterWhere(['OR LIKE', 'conso_societe', $this->conso_societes]);
        }
        if($this->conso_fonctions) {
            $query->andFilterWhere(['OR LIKE', 'conso_societe', $this->conso_fonctions]);
        }
        
        if ($this->secteursID != null) {
            $query->joinWith('secteurs');
            if (is_array($this->secteursID)) {
                foreach ($this->secteursID as $secteur_id) {
                    $query->orFilterWhere(['=', 'secteur.sect_id', $secteur_id]);
                }
            } else {
                $query->andFilterWhere(['=', 'secteur.sect_id', $this->secteursID]);
            }
        }

        return $dataProvider;
    }
}
