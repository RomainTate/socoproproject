<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "journaliste".
 *
 * @property string $journaliste_fonction
 * @property date $journaliste_date_contact 
 * @property integer $cont_id
 *
 * @property Contact $contact
 * @property Media $media
 */
class Journaliste extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'journaliste';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cont_id'], 'integer'],
            [['journaliste_date_contact'], 'safe'],
            [['journaliste_rubrique'], 'string', 'max' => 50],
            [['journaliste_fonction'], 'string', 'max' => 75],
            [['cont_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contact::className(), 'targetAttribute' => ['cont_id' => 'cont_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'journaliste_fonction' => 'Fonction',
            'journaliste_date_contact' => 'Date contact',
            'journaliste_rubrique' => 'Rubrique',
            'cont_id' => 'Cont ID',
            'societemedia' => 'Société / Groupe de presse',
            /* related attributes*/
            'contactNom' => 'Nom',
            'contactPrenom' => 'Prénom',
            'contactTelephonePrincipal' => 'Téléphone',
            'contactGSMPrincipal' => 'GSM',
            'contactEmailPrincipal' => 'E-mail',
            'nommedia' => 'Titre Media',
            'ruemedia' => 'Rue Média',
            'codepostalmedia' => 'Code Postal Média',
            'localitemedia' => 'Localité Média',
            'sitemedia' => 'Site Internet Média',
            'audiencemedia' => 'Audience Média',
            'typepressemedia' => 'Type Presse',
            'typesupportmedia' => 'Type Support',
            'periodicitemedia' => 'Périodicité',
            'paysmedia' => 'Pays Média',
            'langue' => 'Langues'
        ];
    }
    
    public function beforeSave($insert) {
        foreach ($this->attributes as  $key => $value) {
            if ($value === '') {
                $this->$key = NULL;
            }
        }
        return parent::beforeSave($insert);
    }
    
    public function beforeDelete() {
        \rmrevin\yii\module\Comments\models\Comment::deleteAll(['entity' => 'journaliste-'.$this->cont_id]);
        Appartenir::deleteAll(['cont_id' => $this->cont_id]);
        return parent::beforeDelete();
    }

    public function afterDelete() {
        Contact::findOne($this->cont_id)->delete();
        return parent::afterDelete();
    }
   /**
     * @return \yii\db\ActiveQuery
     */
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->hasOne(Contact::className(), ['cont_id' => 'cont_id']);
    }
    
    /* Getter for contact nom */
    public function getContactNom() {
        return $this->contact->cont_nom;
    }
    
    /* Getter for contact prenom */
    public function getContactPrenom() {
        return $this->contact->cont_prenom;
    }
    
    public function getLangue() {
        return $this->contact->cont_langue;
    }
    

    public function getContactCivilite() {
        return $this->contact->cont_civilite;
    }
    
    public function getContactTelephonePrincipal() {
        return $this->contact->cont_telephone_principal;
    }
    
    public function getContactGSMPrincipal() {
        return $this->contact->cont_gsm_principal;
    }
    
    public function getContactEmailPrincipal() {
        return $this->contact->cont_email_principal;
    }
    
//    public function getContactAnneeNaissance() {
//        return $this->contact->cont_annee_naissance;
//    }
    public function getContactCreatedAt() {
        return $this->contact->cont_created_at;
    }
    
//    public function getJournal() {
//        if(!$this->contact->entreprises) return null;
//        return $this->contact->entreprises[0]->entr_nom;
//    }
    
    public function getMedia() {
        return $this->hasOne(Media::className(), ['media_id' => 'media_id']);
    }
    public function getSocietemedia() {
        if(!$this->media) return null;
        return $this->media->media_societe;
    }
    
    public function getNommedia() {
        if(!$this->media) return null;
        return $this->media->media_titre;
    }
    public function getSitemedia() {
        if(!$this->media) return null;
        return $this->media->media_site_internet;
    }
    public function getPaysmedia() {
        if(!$this->media) return null;
        return $this->media->media_pays;
    }
    public function getAudiencemedia() {
        if(!$this->media) return null;
        return $this->media->media_audience;
    }
    public function getTypepressemedia() {
        if(!$this->media) return null;
        return $this->media->media_type_presse;
    }
    
    public function getTypesupportmedia() {
        if(!$this->media) return null;
        return $this->media->media_type_support;
    }
    
    public function getPeriodicitemedia() {
        if(!$this->media) return null;
        return $this->media->media_frequence;
    }
    
    public function getRuemedia() {
        if(!$this->media) return null;
        if(!$this->media->adresse) return null;
        return $this->media->adresse->adr_rue;
    }
    
    public function getCodepostalmedia() {
        if(!$this->media) return null;
        if(!$this->media->adresse) return null;
        return $this->media->adresse->adr_code_postal;
    }
    public function getLocalitemedia() {
        if(!$this->media) return null;
        if(!$this->media->adresse) return null;
        return $this->media->adresse->adr_ville;
    }
    
    public function getContactLangue() {
        return $this->contact->cont_langue;
    }
    
    public function mailExist($email) {
            if($this::find()->joinWith('contact')->where(['cont_email_principal' => $email])->one()) {
                return true;
            }
            return false;
    }
}
