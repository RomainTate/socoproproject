<?php

namespace app\controllers;

use Yii;
use app\models\Log;
use app\models\LogSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LogController implements the CRUD actions for Log model.
 */
class LogController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Log models.
     * @return mixed
     */
    public function actionIndex()
    {
        
        $getRequest = Yii::$app->request->get('LogSearch');
        $specific =  $getRequest != null ? (array_key_exists('contID', $getRequest) && $getRequest['contID'] != null )? true : false : false;
        
        if(!$specific) {
            if ( (!(Yii::$app->user->can('indexProducteur') && Yii::$app->user->can('indexJournaliste') && Yii::$app->user->can('indexConso'))) 
                    || \app\modules\auth\models\AuthAssignment::findOne(['user_id' => \Yii::$app->user->getId()])->item_name == 'Chargé de mission') {
                throw new \yii\web\ForbiddenHttpException(" Vous n'êtes pas autorisé à effectuer cette action." );
            }
        }
        
        $contID = $specific? $getRequest['contID'] : null;
        $model = $specific ? \app\models\Contact::find()->where(['cont_id' => $contID])->one() : null ;
        
        if($model) {
            if (!Yii::$app->user->can('index'.$model->getNature('label'))) {
                throw new NotFoundHttpException('La page demandée n\'existe pas.');
            }

            if ($model->getNature() == 'producteur') {
                if (!\Yii::$app->user->canSee($model->cont_id)) {
                    throw new NotFoundHttpException('La page demandée n\'existe pas.');
                }
            }
        }
        
        $searchModel = new LogSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'specific' => $specific,
            'model' => $model
        ]);
    }


    /**
     * Deletes an existing Log model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        if (Yii::$app->request->referrer && (strpos(Yii::$app->request->referrer, 'index') !== false)) {
        return $this->redirect(Yii::$app->request->referrer);
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Log model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Log the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Log::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
