<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Entreprise */

$this->title = $model->entr_societe;
$this->params['breadcrumbs'][] = ['label' => 'Entreprises-Associations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entreprisedivers-view">
    <h1 class="pull-left">Profil de l'entreprise <?= $model->entr_societe ?></h1>
    <div class="pull-right">
        <?php
        if (Yii::$app->user->can('deleteEntreprise'))
            echo Html::a('Supprimer l\'entreprise', ['delete', 'id' => $model->entr_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Êtes-vous sûr de vouloir supprimer cette entreprise?' . (!empty($model->contacts)? ' Attention : des contacts font partie de cette entreprise, ils seront détachés de celle ci':''),
                    'method' => 'post',
                ],
            ])
            ?>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-lg-5 col-xs-12 profile_left">
            <div class="x_panel" >
                <div class="x_content">
                    <div class="img-profil">
                        <img src="../css/images/flats.png" alt="producteur" />
                    </div>

                    <h3> <?= $model->entr_societe ?> </h3>
                    <div class="mainInfos">


                        <p> <i class="fa fa-lg fa-mobile" aria-hidden="true"></i>
                            <?php if ($model->entr_telephone) : ?> 
                                <?= $model->entr_telephone ?>
                            <?php else : ?>
                                Numéro de téléphone inconnu
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createEntreprise'))
                                echo\yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/entreprise/quickupdate', 'id' => $model->entr_id, 'attribute' => 'entr_telephone']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>
                        </p> 

                        <p> <i class="fa fa-envelope-o" aria-hidden="true"></i> 
                            <?php if ($model->entr_email) : ?>
                                <?= $model->entr_email ?> 
                            <?php else : ?>
                                Adresse e-mail inconnue
                            <?php endif; ?>
                            <?php
                            if (Yii::$app->user->can('createEntreprise'))
                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/entreprise/quickupdate', 'id' => $model->entr_id, 'attribute' => 'entr_email']),
                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn btn-edit"]);
                            ?>

                        <h3>Adresse</h3>
                                <?php if($model->adresse) : ?>
                                    <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                        <?php if($model->adresse->adr_rue != null && $model->adresse->adr_rue != '' ):?>
                                            <?php if (strlen($model->adresse->adr_rue) < 40) :?>
                                                <?= $model->adresse->adr_rue ?>
                                            <?php else : ?>
                                                <?php
                                                $explodedAdr = explode(' ', $model->adresse->adr_rue);
                                                $okRue = [];
                                                $indexOkRue = 0;
                                                for($i=0; $i < count($explodedAdr); $i++) { //ok
                                                    $okRue[$indexOkRue] = $explodedAdr[$i];
                                                    while( isset($explodedAdr[$i+1]) && ((strlen($okRue[$indexOkRue]) + strlen($explodedAdr[$i+1])) < 40) ) {
                                                        $okRue[$indexOkRue] .= ' ' . $explodedAdr[$i+1];
                                                        $i++;
                                                    }
                                                    $indexOkRue++;
                                                }
                                                for($j=0; $j < count($okRue); $j++) {
                                                    echo $okRue[$j];
                                                    if(isset ($okRue[$j+1])) {
                                                        echo '</p>';
                                                        echo '<p><i class="fa fa-map-marker" aria-hidden="true"></i> ';
                                                    }
                                                }
                                                ?>
                                            <?php endif ?>
                                        <?php else : ?>
                                            <?= 'Numéro et Rue inconnus'?>
                                        <?php endif; ?>
                                        <?php if (Yii::$app->user->can('createEntreprise')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                    </p>

                                    <?php if ($model->adresse->adr_code_postal != null && $model->adresse->adr_code_postal != '') :?>
                                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                            <?= 'Code Postal: '. $model->adresse->adr_code_postal?>
                                            <?php if (Yii::$app->user->can('createEntreprise')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                        </p>
                                    <?php endif?>

                                    <?php if ($model->adresse->adr_ville != null && $model->adresse->adr_ville != '') :?>
                                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                            <?= 'Localite: '. $model->adresse->adr_ville?>
                                            <?php if (Yii::$app->user->can('createEntreprise')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                        </p>
                                    <?php endif;?>

                                    <?php if ($model->adresse->adr_province != null && $model->adresse->adr_province != '') :?>
                                        <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                            <?= 'Province: '. $model->adresse->adr_province?>
                                            <?php if (Yii::$app->user->can('createEntreprise')) {
                                                echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/adresse/update' , 'id' => $model->adr_id]),
                                                    'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                            }?>
                                        </p>
                                    <?php endif;?>
                                
                                        
                            <?php else :?>
                                <p><i class="fa fa-map-marker" aria-hidden="true"></i> 
                                    <?= 'Adresse inconnue';?>
                                    <?php if (Yii::$app->user->can('createEntreprise')){
                                            echo \yii\bootstrap\Html::button('', ['value' => \yii\helpers\Url::to(['/entreprise/addadresse','id' => $model->entr_id]),
                                                'name' => 'modalButton', 'class' => "fa fa-pencil btn-edit"]);
                                    }?>
                                </p>
                            <?php endif;?>
                    </div>
                    <?php
                    if (Yii::$app->user->can('createEntreprise'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations de l\'entreprise', ['value' => \yii\helpers\Url::to(['/entreprise/update', 'id' => $model->entr_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                </div>
            </div>
        </div>
        <div class="col-lg-7 col-xs-12">
            <div class="x_panel secondInfos">
                <div class="x_content">
                    <h2> Autres informations </h2> 

                    <ul class="autres-infos">
                        <li>
                            Domaine d'activité : <?php echo $model->entr_domaine_activite == null ? 'inconnue' : $model->entr_domaine_activite ?>
                        </li>
                    </ul>
                    <?php
                    if (Yii::$app->user->can('createEntreprise'))
                        echo '<div class="text-right">' . \yii\bootstrap\Html::button('Modifier les informations', ['value' => \yii\helpers\Url::to(['/entreprise/addmoreinfos', 'entr_id' => $model->entr_id]),
                            'name' => 'modalButton', 'class' => "btn btn-info"]) . '</div>'
                        ?>
                    <hr />
                    <h2> Contacts associés </h2>
                    <?php
                    if (!$model->contacts) : echo 'Aucun contact associé à cette exploitation';
                    else :?>
                        <table class="table table-bordered">
                        <th>
                            Nom et Prenom
                        </th>
                        <th>
                            Fonction
                        </th>
                        <th>
                            Email employé
                        </th>
                        <th>
                            GSM employé
                        </th>
                        <th>
                            Membre collège
                        </th>
                        <th>
                            Membre commission filière
                        </th>
                        <?php foreach ($model->contacts as $contact) : ?>
                        <tr>
                            <td>
                                <a class="text-primary" href="<?php echo\yii\helpers\Url::home()?>contact/view?id=<?= $contact->cont_id ?>"> <?=  $contact->cont_nom . ' ' . $contact->cont_prenom  ?> </a>
                            </td>
                            <td>
                                <?php echo ($contact->getNature() == 'producteur')? 'Producteur': app\models\Appartenir::find()->where(['entr_id' => $model->entr_id, 'cont_id' => $contact->cont_id])->one()->fonction?>
                            </td>
                            <td>
                                <?php if ($contact->cont_email_principal) {
                                    $nbChar = strlen($contact->cont_email_principal);
                                    if ($nbChar > 30) {
                                        $style = ['style' => 'font-size : 11px'];
                                    } else {
                                        $style = '';
                                    }
                                    echo Html::a(yii\helpers\BaseStringHelper::truncate($contact->cont_email_principal, 40, '...'), 'mailto:'.$contact->cont_email_principal,$style);
                                }?>
                            </td>
                            <td>
                                <?= $contact->cont_gsm_principal ?>
                            </td>
                            <td>
                                <?php echo ($contact->membrecollege)? 'oui' : 'non';?>
                            </td>
                            <td>
                                <?php echo ($contact->membrecommission)? 'oui' : 'non';?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                    </table>
                    <?php endif; ?>                    
                </div>
            </div>
            <div class="x_panel">
                <div class="x_content">
                    <?php
                    echo rmrevin\yii\module\Comments\widgets\CommentListWidget::widget([
                        'entity' => (string) 'entreprise-' . $model->entr_id, // type and id
                    ]);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
yii\bootstrap\Modal::begin(['id' => 'modal']);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
$this->registerJsFile(\yii\helpers\Url::home().'js/modal.js', ['depends' => [yii\web\JqueryAsset::className()]]);
?>