<?php

namespace app\controllers;

use Yii;
use app\models\Producteur;
use app\models\ProducteurSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class ImportationController extends Controller {

    public $acceptedCiv = ['mr', 'monsieur', 'mme', 'madame', 'messieurs', 'mesdames', 'm.', 'mlle'];
    public $acceptedProvince = ['Anvers', 'Brabant Wallon', 'Brabant Flamand', 'Bruxelles', 'Flandre Occidentale', 'Flandre Orientale', 'Hainaut', 'Limbourg', 'Liege', 'Liège', 'Luxembourg', 'Namur'];
    public $strReplaceTel = ['/', ' ', '_', '+', '(', ')', '.',];

    public function actionIndex() {
        $post = Yii::$app->request->post();
        if ($post) {
            $file = \yii\web\UploadedFile::getInstanceByName('uploadedFile');
            if ($file == null) {
                Yii::$app->session->setFlash('error', 'Veuillez importer un fichier');
                return $this->render('index');
            }
            if ($file->extension != 'xlsx') {
                Yii::$app->session->setFlash('error', 'Veuillez importer un fichier de type Excel 2007+ (.xlsx)');
                return $this->render('index');
            }
            $file->saveAs('uploads/' . $file->name);

            set_time_limit(0);
            $inputFile = 'uploads/' . $file->name;
            try {
                $inputFileType = \PHPExcel_IOFactory::identify($inputFile);

                $obj = \PHPExcel_IOFactory::createReader($inputFileType);
                $excel = $obj->load($inputFile);
            } catch (Exception $ex) {
                die(print_r($ex));
            }

            $sheet = $excel->getSheet(0);
            if ($post['type'] == 'journ') {
                $dataImport = $this->importJourn($sheet);
            } elseif ($post['type'] == 'conso') {
                $dataImport = $this->importConso($sheet);
                unlink('uploads/' . $file->name);
                return $this->render('importConso', ['dataImport' => $dataImport]);
            } else {
                if (strtolower($sheet->getCell('J1')->getValue()) == 'fax')
                    $sheet->removeColumn('J');
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $dataImport = array();

                for ($row = 1; $row <= $highestRow; $row++) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    if ($row == 1)
                        continue;
                    if ($rowData[0][1] == '' && $rowData[0][3] == '' && $rowData[0][4] == '')
                        continue;
                    $rowData[0] = $this->CheckErrors($rowData[0]);
                    $rowData[0] = $this->CheckConcordance($rowData[0]);
                    $rowData[0] = $this->TryImportRow($rowData[0]);
                    if (array_key_exists('errors', $rowData[0]) || array_key_exists('concordance', $rowData[0]))
                        $dataImport[] = $rowData[0];
                }
            }
            unlink('uploads/' . $file->name);
            return $this->render('import', ['dataImport' => $dataImport]);
        } else {
            return $this->render('index');
        }
    }

    public function CheckErrors($rowData) {

        //civilité
        $civ = strtolower($rowData[1]);
        if ($civ != '') {
            $arrayCiv = \app\components\Functions::multiexplode([' et ', '&', ',', '-'], $civ);
            foreach ($arrayCiv as $singleCiv) {
                if (!(in_array(trim($singleCiv), $this->acceptedCiv))) {
                    $rowData['messages'][] = 'Civilité incorrecte';
                    $rowData['errors'][] = 'Civ';
                    $rowData['errors']['index'][] = 1;
                    break;
                }
            }
        }

        //code postal
        $codePostal = $rowData[5];
        if ($codePostal != '') {
            if (strlen($codePostal) != 4) {
                $rowData['messages'][] = 'Code postal incorrect';
                $rowData['errors'][] = 'codePostal';
                $rowData['errors']['index'][] = 5;
            }
        }

        //province
        $province = ucwords(mb_strtolower($rowData[7]));
        if ($province != '') {
            if (!(in_array(trim($province), $this->acceptedProvince))) {
                $rowData['messages'][] = 'Province incorrecte';
                $rowData['errors'][] = 'Province';
                $rowData['errors']['index'][] = 7;
            }
        }

        //telephone
        $telephone = str_replace($this->strReplaceTel, '', $rowData[8]);
        $telephone = preg_replace('/\D/', '', $telephone);
        if ($telephone != '' && $telephone != '0032') {
            if (ctype_digit($telephone) == false) {
                $rowData['messages'][] = 'Telephone incorrect';
                $rowData['errors'][] = 'Telephone';
                $rowData['errors']['index'][] = 8;
                if (strlen($telephone) == 12) {
                    if (substr($telephone, 0, 4) != '0032') {
                        $rowData['messages'][] = 'Telephone incorrect';
                        $rowData['errors'][] = 'Telephone';
                        $rowData['errors']['index'][] = 8;
                    }
                } else if ((strlen($telephone) != 8) || (!(strlen($telephone) == 9 && substr($telephone, 0, 1) == '0'))) {
                    $rowData['messages'][] = 'Telephone incorrect';
                    $rowData['errors'][] = 'Telephone';
                    $rowData['errors']['index'][] = 8;
                }
            }
        }

        //GSM
        for ($i = 9; $i <= 11; $i++) {
            $gsm = str_replace($this->strReplaceTel, '', $rowData[$i]);
            $gsm = preg_replace('/\D/', '', $gsm);
            if ($gsm != '' && $gsm != '0032') {
                if (ctype_digit($gsm) == false) {
                    $rowData['messages'][] = 'GSM incorrect';
                    $rowData['errors'][] = 'GSM';
                    $rowData['errors']['index'][] = $i;
                    if (strlen($gsm) == 13) {
                        if (substr($gsm, 0, 4) != '0032') {
                            $rowData['messages'][] = 'GSM incorrect';
                            $rowData['errors'][] = 'GSM';
                            $rowData['errors']['index'][] = $i;
                        }
                    } else if (strlen($gsm) == 9) {
                        $rowData[$i] = '0032' . $rowData[$i];
                    } else if (strlen($gsm) == 10 && substr($gsm, 0, 1) == '0') {
                        $rowData[$i] = '0032' . substr($rowData[$i], 1);
                    } else {
                        $rowData['messages'][] = 'GSM incorrect';
                        $rowData['errors'][] = 'GSM';
                        $rowData['errors']['index'][] = $i;
                    }
                }
            }
        }

        //E-MAILS
        $emails = trim(strtolower($rowData[12]));
        if ($emails != '') {
            $arrayEmails = \app\components\Functions::multiexplode([' et ', '&', ',', ' ou ', ';'], $emails);
            foreach ($arrayEmails as $singleEmail) {
                if ($singleEmail != '' && !filter_var(trim($singleEmail), FILTER_VALIDATE_EMAIL)) {
                    $rowData['messages'][] = 'Email Incorrect';
                    $rowData['errors'][] = 'Email';
                    $rowData['errors']['index'][] = 12;
                    break;
                }
            }
        }


        return $rowData;
    }

    public function CheckConcordance($rowData) {

        $civ = strtolower($rowData[1]);
        if ($civ != '') {
            $arrayCiv = \app\components\Functions::multiexplode(['et', '&', ',', '-'], $civ);
            $nbCiv = count($arrayCiv);
            foreach ($arrayCiv as $singleCiv) {
                if ($singleCiv == 'messieurs' || $singleCiv == 'mesdames') {
                    $nbCiv = 9; // impossible de savoir si cela concerne 2, 3 ou + de personnes
                    break;
                }
            }
        } else {
            $nbCiv = 0;
        }

        $rowData = $this->GetNumberOfPersons($rowData);

        if ($nbCiv != 0) {
            if ($rowData['nbNom'] != 0 && $rowData['nbPrenom'] != 0 && ($nbCiv < $rowData['nbNom'] || $nbCiv < $rowData['nbPrenom'] || $rowData['nbNom'] != $rowData['nbPrenom'])) {
                if ($nbCiv < max([$rowData['nbNom'], $rowData['nbPrenom']])) {
                    $rowData['messages'][] = 'Le nombre de Civilité ne correspond pas';
                    $rowData['concordance'][] = 'Civ';
                    $rowData['concordance']['index'] = [1, 2, 3];
                }
                if ($rowData['nbNom'] > $rowData['nbPrenom']) {
                    $rowData['messages'][] = 'Le nombre de Nom ne correspond pas';
                    $rowData['concordance'][] = 'Nom';
                    $rowData['concordance']['index'] = [2, 3];
                }
            }
        } else {
            if ($rowData['nbNom'] != 0 && $rowData['nbPrenom'] != 0 && ($rowData['nbNom'] > $rowData['nbPrenom'])) {
                $rowData['messages'][] = 'Le nombre de Nom ne correspond pas';
                $rowData['concordance'][] = 'Nom';
                $rowData['concordance']['index'] = [2, 3];
            }
        }

        if ($rowData['nbNom'] != $rowData['nbPrenom']) {
            $arrayPrenoms = \app\components\Functions::multiexplode([' Et ', '&', ','], trim(ucwords($rowData[3])));
            for ($i = 0; $i < count($arrayPrenoms); $i++) {
                $rowData['toAdd'][$i]['nom'] = $rowData[2];
                $rowData['toAdd'][$i]['prenom'] = $arrayPrenoms[$i];
            }
        } else {
            $arrayPrenoms = \app\components\Functions::multiexplode([' Et ', ' et ', ' ET ', '&', ','], trim($rowData[3]));
            $arrayNoms = \app\components\Functions::multiexplode([' Et ', ' et ', ' ET ', '&', ',', '-'], trim($rowData[2]));
            for ($i = 0; $i < count($arrayPrenoms); $i++) {
                $rowData['toAdd'][$i]['nom'] = $arrayNoms[$i];
                $rowData['toAdd'][$i]['prenom'] = $arrayPrenoms[$i];
            }
        }


        return $rowData;
    }

    public function CheckExists($rowData) {


        $exploitation = $rowData[0];
        if ($exploitation != '') {
            $prod = Producteur::find()->where(['prod_nom_exploitation' => $exploitation])->one();
            if ($prod != null) {
                return $prod;
            }
        }

        if ($rowData['toAdd'][0]['nom'] != '' && $rowData['toAdd'][0]['prenom'] != '') {
            $prod = Producteur::find()->joinWith(['contact'])->where(['cont_nom' => $rowData['toAdd'][0]['nom'], 'cont_prenom' => $rowData['toAdd'][0]['prenom']])->one();
            if ($prod != null) {
                return $prod;
            }
        }

        return false;
    }

    public function GetNumberOfPersons($rowData) {
        $nom = strtolower(($rowData[2]));
        if ($nom != '') {
            $arrayNom = \app\components\Functions::multiexplode([' et ', '&', ',', '-'], $nom);
            $rowData['nbNom'] = count($arrayNom);
        } else {
            $rowData['nbNom'] = 0;
        }

        $prenom = trim(strtolower(($rowData[3])));
        if ($prenom != '') {
            $arrayPrenom = \app\components\Functions::multiexplode([' et ', '&', ','], $prenom);
            $rowData['nbPrenom'] = count($arrayPrenom);
        } else {
            $rowData['nbPrenom'] = 0;
        }

        if (trim($rowData[11]) != '') {
            $rowData['nbGSM'] = 3;
        } else if (trim($rowData[10]) != '') {
            $rowData['nbGSM'] = 2;
        } else {
            $rowData['nbGSM'] = 1;
        }

        $mails = trim(strtolower(($rowData[12])));
        if ($mails != '') {
            $arrayMails = \app\components\Functions::multiexplode([' et ', ' ou ', '&', ',', ';'], $mails);
            $rowData['nbMails'] = count($arrayMails);
        } else {
            $rowData['nbMails'] = 0;
        }

        $rowData['nbPerson'] = max([$rowData['nbPrenom'], $rowData['nbGSM'], $rowData['nbMails']]);

        return $rowData;
    }

    public function TryImportRow($rowData) {


        if (Yii::$app->request->post('secteur') == 3) {
            if (strpos(strtolower($rowData[13]), 'ovins') !== false || strpos(strtolower($rowData[13]), 'ovin') !== false) {
                $secteurs[] = \app\models\Secteur::find()->where(['sect_nom' => 'Ovins'])->one()->sect_id;
            }
            if (strpos(strtolower($rowData[13]), 'caprins') !== false || strpos(strtolower($rowData[13]), 'caprin') !== false) {
                $secteurs[] = \app\models\Secteur::find()->where(['sect_nom' => 'Caprins'])->one()->sect_id;
            }
        } else {
            $secteurs[] = Yii::$app->request->post('secteur');
        }

        if (Yii::$app->request->post('secteur') == 2 || Yii::$app->request->post('secteur') == 3 || Yii::$app->request->post('secteur') == 9) {
            if (strpos(strtolower($rowData[14]), 'bio') !== false || strpos(strtolower($rowData[13]), 'bio') !== false) {
                $secteurs[] = \app\models\Secteur::find()->where(['sect_nom' => 'Agriculture Bio'])->one()->sect_id;
            }
        }

        if (!isset($secteurs) || empty($secteurs)) {
            $rowData['error']['index'] = [13, 14];
            return $rowData;
        }

        $existingProd = $this->CheckExists($rowData);
        if ($existingProd != false) {
            $existingContact = \app\models\Contact::find()->where(['cont_id' => $existingProd->cont_id])->one();
            $existingContact->setScenario('typeProducteur');
            //add secteurs if needed
            $existingProdSecteur = array();
            foreach ($existingProd->secteurs as $v) {
                $existingProdSecteur[] = $v->sect_id;
            }
            foreach ($secteurs as $secteur) {
                if (!in_array($secteur, $existingProdSecteur, false)) {
                    $travailler = new \app\models\Travailler();
                    $travailler->cont_id = $existingProd->cont_id;
                    $travailler->sect_id = $secteur;
                    $travailler->save();
                }
            }

            if ($existingProd->prod_nom_exploitation == null && $rowData[0] != null) {
                $existingProd->prod_nom_exploitation = $rowData[0];
            }
            if ($existingProd->contact->cont_nom == null && $rowData['toAdd'][0]['nom'] != null) {
                $existingContact->cont_nom = $rowData['toAdd'][0]['nom'];
            }
            if ($existingProd->contact->cont_prenom == null && $rowData['toAdd'][0]['prenom'] != null) {
                $existingContact->cont_prenom = $rowData['toAdd'][0]['prenom'];
            }
            if (($existingProd->contact->adresse == null || $existingProd->contact->adresse->adr_rue == null) && ($rowData[4] != null || $rowData[5] != null || $rowData[6] != null || $rowData[7] != null)) {
                if ($existingProd->contact->adresse == null) {
                    $adresse = new \app\models\Adresse();
                    $adresse->adr_rue = $rowData[4];
                    $adresse->adr_code_postal = (string) $rowData[5];
                    if (strlen($rowData[6]) < 26)
                        $adresse->adr_ville = $rowData[6];
                    $adresse->adr_province = $rowData[7];
                    if (!$adresse->save()) {
                        $rowData['error']['index'][] = [4, 5, 6, 7];
                        $rowData['error']['message'][] = $adresse->getErrors();
                    } else {
                        $existingContact->adr_id = $adresse->adr_id;
                    }
                } else {
                    $adresse = \app\models\Adresse::find()->where(['adr_id' => $existingProd->contact->adr_id])->one();
                    $adresse->adr_rue = $rowData[4];
                    $adresse->adr_code_postal = (string) $rowData[5];
                    if (strlen($rowData[6]) < 26)
                        $adresse->adr_ville = $rowData[6];
                    $adresse->adr_province = $rowData[7];
                    $adresse->save();
                }
            }

            if ($existingProd->contact->cont_telephone_principal == null && $rowData[8] != null) {
                $telephone = str_replace($this->strReplaceTel, '', $rowData[8]);
                $telephone = preg_replace('/\D/', '', $telephone);
                if (strlen($telephone) == 12 && substr($telephone, 0, 4) == '0032') {
                    $existingContact->cont_telephone_principal = $telephone;
                } else if (strlen($telephone) == 8) {
                    $existingContact->cont_telephone_principal = '0032' . $telephone;
                } else if (strlen($telephone) == 9 && substr($telephone, 0, 1) == '0') {
                    $existingContact->cont_telephone_principal = '0032' . substr($telephone, 1);
                }
            }
            if ($existingProd->contact->cont_gsm_principal == null && $rowData[9] != null) {
                $gsm = str_replace($this->strReplaceTel, '', $rowData[9]);
                $gsm = preg_replace('/\D/', '', $gsm);
                if (strlen($gsm) == 13 && substr($gsm, 0, 4) == '0032') {
                    $existingContact->cont_gsm_principal = $gsm;
                } else if (strlen($gsm) == 9) {
                    $existingContact->cont_gsm_principal = '0032' . $gsm;
                } else if (strlen($gsm) == 10 && substr($gsm, 0, 1) == '0') {
                    $existingContact->cont_gsm_principal = '0032' . substr($gsm, 1);
                }
            }

            $mails = trim(strtolower(($rowData[12])));
            if ($existingProd->contact->cont_email_principal == null && $mails != null) {
                $arrayMails = \app\components\Functions::multiexplode([' et ', ' ou ', '&', ',', ';'], $mails);
                if (filter_var($arrayMails[0], FILTER_VALIDATE_EMAIL))
                    $existingContact->cont_email_principal = $arrayMails[0];
            }


            try {
                $existingContact->save();
                $existingProd->cont_id = $existingContact->cont_id;
                $existingProd->save();
            } catch (Exception $e) {
                $rowData['error']['index'][] = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                return $rowData;
            }
        } else {
            $prod = new Producteur();
            $contact = new \app\models\Contact();
            $contact->setScenario('typeProducteur');

            if ($rowData[0] != null) {
                $prod->prod_nom_exploitation = $rowData[0];
            }
            if ($rowData['toAdd'][0]['nom'] != null) {
                $contact->cont_nom = $rowData['toAdd'][0]['nom'];
            }
            if ($rowData['toAdd'][0]['prenom'] != null) {
                $contact->cont_prenom = $rowData['toAdd'][0]['prenom'];
            }
            if (($rowData[4] != null || $rowData[5] != null || $rowData[6] != null || $rowData[7] != null)) {
                $adresse = new \app\models\Adresse();
                $adresse->adr_rue = $rowData[4];
                $adresse->adr_code_postal = (string) $rowData[5];
                if (strlen($rowData[6]) < 26)
                    $adresse->adr_ville = $rowData[6];
                $adresse->adr_province = $rowData[7];
                if (!$adresse->save()) {
                    $rowData['error']['index'][] = [4, 5, 6, 7];
                } else {
                    $contact->adr_id = $adresse->adr_id;
                }
            }

            if ($rowData[8] != null) {
                $telephone = str_replace($this->strReplaceTel, '', $rowData[8]);
                $telephone = preg_replace('/\D/', '', $telephone);
                if (strlen($telephone) == 12 && substr($telephone, 0, 4) == '0032') {
                    $contact->cont_telephone_principal = $telephone;
                } else if (strlen($telephone) == 8) {
                    $contact->cont_telephone_principal = '0032' . $telephone;
                } else if (strlen($telephone) == 9 && substr($telephone, 0, 1) == '0') {
                    $contact->cont_telephone_principal = '0032' . substr($telephone, 1);
                } else {
                    $rowData['error']['index'][] = 8;
                    $rowData['error']['message'][] = 'telephone not ok';
                }
            }
            if ($rowData[9] != null) {
                $gsm = str_replace($this->strReplaceTel, '', $rowData[9]);
                $gsm = preg_replace('/\D/', '', $gsm);
                if (strlen($gsm) == 13 && substr($gsm, 0, 4) == '0032') {
                    $contact->cont_gsm_principal = $gsm;
                } else if (strlen($gsm) == 9) {
                    $contact->cont_gsm_principal = '0032' . $gsm;
                } else if (strlen($gsm) == 10 && substr($gsm, 0, 1) == '0') {
                    $contact->cont_gsm_principal = '0032' . substr($gsm, 1);
                } else {
                    $rowData['error']['index'][] = 9;
                    $rowData['error']['message'][] = 'gsm not ok';
                }
            }

            $mails = trim(strtolower(($rowData[12])));
            if ($mails != null) {
                $arrayMails = \app\components\Functions::multiexplode([' et ', ' ou ', '&', ',', ';'], $mails);
                if (filter_var($arrayMails[0], FILTER_VALIDATE_EMAIL))
                    $contact->cont_email_principal = $arrayMails[0];
            }

            try {
                if (!$contact->save()) {
                    $rowData['error']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                    return $rowData;
                } else {
                    $prod->cont_id = $contact->cont_id;
                    if (!$prod->save()) {
                        $rowData['error']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                        return $rowData;
                    } else {
                        foreach ($secteurs as $secteur) {
                            $travailler = new \app\models\Travailler();
                            $travailler->cont_id = $prod->cont_id;
                            $travailler->sect_id = $secteur;
                            $travailler->save();
                        }
                    }
                }
            } catch (\yii\db\Exception $e) {
                $rowData['error']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                $rowData['error']['message'] = $e->getMessage()[0];
                return $rowData;
            }
        }

        if ($rowData['nbPerson'] > 1) {
            if (isset($existingProd) && is_object($existingProd)) {
                $cont_id = $existingProd->cont_id;
            } else if (isset($prod) && is_object($prod)) {
                $cont_id = $prod->cont_id;
            }
            if ($cont_id == null) {
                $rowData['error']['index'] = [10, 11, 12];
            } else {
                $this->addAssocies($rowData, $cont_id);
            }
        }
        return $rowData;
    }

    public function addAssocies($rowData, $cont_id) {
        $mails = trim(strtolower(($rowData[12])));
        if ($mails != '') {
            $arrayMails = \app\components\Functions::multiexplode([' et ', ' ou ', '&', ',', ';'], $mails);
        }
        for ($i = 1; $i < $rowData['nbPerson']; $i++) {
            $params = array();

            //check already exists
            if (isset($rowData['toAdd'][$i]['nom']) && isset($rowData['toAdd'][$i]['prenom'])) {
                if (\app\models\Associe::find()->joinWith('contact')->where(['cont_nom' => $rowData['toAdd'][$i]['nom'], 'cont_prenom' => $rowData['toAdd'][$i]['prenom']])->one() == null) {
                    $associe = new \app\models\Associe();
                    $contact = new \app\models\Contact();
                } else {
                    $contact = \app\models\Contact::find()->where(['cont_nom' => $rowData['toAdd'][$i]['nom'], 'cont_prenom' => $rowData['toAdd'][$i]['prenom']])->one();
                    $associe = \app\models\Associe::find()->where(['cont_id' => $contact->cont_id])->one();
                }
            } else {
                $associe = new \app\models\Associe();
                $contact = new \app\models\Contact();
            }

            $contact->setScenario('typeAssocie');
            if (array_key_exists($i, $rowData['toAdd'])) {
                if (isset($rowData['toAdd'][$i]['nom']) && $rowData['toAdd'][$i]['nom'] != '') {
                    $contact->cont_nom = $rowData['toAdd'][$i]['nom'];
                    $params['cont_nom'] = $contact->cont_nom;
                }
                if (isset($rowData['toAdd'][$i]['prenom']) && $rowData['toAdd'][$i]['prenom'] != '') {
                    $contact->cont_prenom = $rowData['toAdd'][$i]['prenom'];
                    $params['cont_prenom'] = $contact->cont_prenom;
                }
            }

            if ($rowData['nbMails'] > $i && array_key_exists($i, $arrayMails)) {
                $contact->cont_email_principal = $arrayMails[$i];
                $params['cont_email_principal'] = $contact->cont_email_principal;
            }

            if ($rowData['nbGSM'] > $i && $rowData[$i + 9]) {
                $gsm = str_replace($this->strReplaceTel, '', $rowData[$i + 9]);
                $gsm = preg_replace('/\D/', '', $gsm);
                if (strlen($gsm) == 13 && substr($gsm, 0, 4) == '0032') {
                    $contact->cont_gsm_principal = $gsm;
                } else if (strlen($gsm) == 9) {
                    $contact->cont_gsm_principal = '0032' . $gsm;
                } else if (strlen($gsm) == 10 && substr($gsm, 0, 1) == '0') {
                    $contact->cont_gsm_principal = '0032' . substr($gsm, 1);
                } else {
                    $rowData['error']['index'][] = $i + 9;
                    $rowData['error']['message'][] = 'gsm not ok';
                }
                if ($contact->cont_gsm_principal) {
                    $params['cont_gsm_principal'] = $contact->cont_gsm_principal;
                }
            }

            //if there is something to import AND if object with $params doesn't already exist
            if ((!empty($params)) && \app\models\Associe::find()->joinWith('contact')->where($params)->one() == null) {
                try {
                    if (!$contact->save()) {
                        $rowData['error']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                        $rowData['error']['message'] = $contact->getErrors();
                        return $rowData;
                    } else {
                        try {
                            $associe->cont_id_prod = $cont_id;
                            $associe->cont_id = $contact->cont_id;
                            $associe->assoc_type = 3;
                            if (!$associe->save()) {
                                $contact->delete();
                            }
                        } catch (\Exception $ex) {
                            $rowData['error']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                            $rowData['error']['message'] = $ex->getMessage();
                            return $rowData;
                        }
                    }
                } catch (Exception $e) {
                    $rowData['error']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                    $rowData['error']['message'] = $e->getMessage();
                    return $rowData;
                }
            }
        }
    }

    public function importJourn($sheet) {
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $dataImport = array();

        for ($row = 1; $row <= $highestRow; $row++) {
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
            if ($row == 1 || $row == 2)
                continue;
            if ($rowData[0][3] == null && $rowData[0][5] == null && $rowData[0][6] == null && $rowData[0][8] == null && $rowData[0][9] == null) {
                $rowData[0]['error']['index'][] = [3, 5, 6, 8, 9];
                $rowData[0]['error']['message'][] = 'Aucune information principale';
                $dataImport[] = $rowData[0];
                continue;
            }
            
            $contact = new \app\models\Contact();
            $contact->setScenario('typeJournaliste');
            $contact->cont_langue = trim($rowData[0][1]);
            
            $contact->cont_email_principal = trim($rowData[0][3]);
            
            $contact->cont_civilite = trim($rowData[0][4]);
            if(trim($rowData[0][5]) != '')
                $contact->cont_prenom = trim($rowData[0][5]);
            if(trim($rowData[0][6]) != '')
                $contact->cont_nom = mb_strtoupper(trim($rowData[0][6]));

            $contact->cont_gsm_principal = trim(str_replace($this->strReplaceTel, '', $rowData[0][8]));
            $contact->cont_telephone_principal = trim(str_replace($this->strReplaceTel, '', $rowData[0][9]));
            
            $journaliste = new \app\models\Journaliste();
            $journaliste->journaliste_fonction = trim($rowData[0][7]);
            if ($rowData[0][12] && trim(strtolower($rowData[0][12]) != 'freelance')) {
                $media = \app\models\Media::find()->where(['media_titre' => $rowData[0][12]])->one();
                if ($media == null) {
                    $media = new \app\models\Media();
                    $media->media_titre = trim($rowData[0][12]);
                    $media->media_societe = trim($rowData[0][11]);
                    $media->media_audience = trim($rowData[0][13]);
                    $media->media_site_internet = trim($rowData[0][17]);
                    $media->media_type_presse = trim($rowData[0][18]);
                    $media->media_type_support = trim($rowData[0][19]);
                    $media->media_frequence = trim($rowData[0][20]);
                    if ($media->save()) {
                        if($rowData[0][14] || $rowData[0][15] || $rowData[0][16] ) {
                            $adresse = new \app\models\Adresse();
                            $adresse->adr_rue = $rowData[0][14];
                            $adresse->adr_code_postal = $rowData[0][15];
                            $adresse->adr_ville = mb_strtoupper($rowData[0][16]);
                            $adresse->save();
                        }
                    }
                }
            }
            if ($media != null)
                $journaliste->media_id = $media->media_id;
            
            if (!$contact->validate()) {
                $rowData[0]['errors']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                foreach ($contact->getErrors() as $champ => $errors) {
                    foreach ($errors as $error) {
                        $rowData[0]['messages'][] = $error;
                    }
                    $contact->$champ = null;
                }
                $dataImport[] = $rowData[0];
            }
            
             if ($rowData[0][3] == null && $rowData[0][5] == null && $rowData[0][6] == null && $rowData[0][8] == null && $rowData[0][9] == null) {
                 continue;
             }
            // doublon check //
            if ($contact->cont_email_principal && $contact->cont_nom && $contact->cont_prenom) {
                if (\app\models\Journaliste::find()->joinWith('contact')->where(['cont_nom' => $contact->cont_nom, 'cont_prenom' => $contact->cont_prenom,
                            'cont_email_principal' => $contact->cont_email_principal])->one()) {
                    continue;
                }
            } else if ($contact->cont_email_principal) {
                if (\app\models\Journaliste::find()->joinWith('contact')->where(['cont_email_principal' => $contact->cont_email_principal])->one()) {
                    continue;
                }
            } else if ($contact->cont_nom && $contact->cont_prenom) {
                if (\app\models\Journaliste::find()->joinWith('contact')->where(['cont_nom' => $contact->cont_nom, 'cont_prenom' => $contact->cont_prenom])->one()) {
                    continue;
                }
            }
            // doublon check //
            
            if (!$contact->save()) {
                continue;
            } else {
                $journaliste->cont_id = $contact->cont_id;
                if (!$journaliste->validate()) {
                    $rowData[0]['errors']['index'] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
                    foreach ($journaliste->getErrors() as $champ => $errors) {
                        foreach ($errors as $error) {
                            $rowData[0]['messages'][] = $error;
                        }
                        $journaliste->$champ = null;
                    }
                    $dataImport[] = $rowData[0];
                }
                
                if (!$journaliste->save()) {
                    $contact->delete();
                    $dataImport[] = $rowData[0];
                    continue;
                }
            }
        }
        return $dataImport;
    }

    public function importConso($sheet) {
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();
        $rowData = $sheet->rangeToArray('A1:' . $highestColumn.$highestRow , NULL, TRUE, FALSE);
        $errorArray = [];
        
        for($i = 2; $i< $highestRow  ; $i++) { // line 3 to highest row
            $line = $rowData[$i];
            
            $contact = new \app\models\Contact();
            
            
            $nom = trim($line[0]);
            if($nom != '' || $nom != null) {
                $contact->cont_nom = $nom;
            }
            
            $prenom = trim($line[1]);
            if($prenom != '' || $prenom != null) {
                $contact->cont_prenom = $prenom;
            }
            
            $existingConso = \app\models\Consommateur::find()->joinWith('contact')->where(['contact.cont_nom' => $nom, 'contact.cont_prenom' => $prenom])->all();
            if(!empty($existingConso)) {
                if(count($existingConso) == 1) {
                    $contact = $existingConso[0]->contact;
                    $conso = $existingConso[0];
                } else {
                    $line['errors'][] = 'Plusieurs consommateurs existent avec ce nom et prénom';
                    $errorArray[] = $line;
                    continue;
                }
            } else {
                $conso = new \app\models\Consommateur();
            }
            
            if($line[5] != '' || $line[5] != null) {
                $gsm = trim(str_replace($this->strReplaceTel, '', $line[5]));
                if(!is_numeric($gsm)) {
                    $line['errors'][] = 'Erreur GSM';
                } else {
                    $contact->cont_gsm_principal = $gsm;
                }
            }
            
            if($line[6] != '' || $line[6] != null) {
                $telephone = trim(str_replace($this->strReplaceTel, '', $line[6]));
                if(!is_numeric($telephone)) {
                    $line['errors'][] = 'Erreur telephone';
                } else {
                    $contact->cont_telephone_principal = $telephone;
                }
            }
            
            if($line[7] != '' || $line[7] != null) {
                $email = trim($line[7]);
                if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $line['errors'][] = 'Erreur Email';
                } else {
                    $contact->cont_email_principal = $email;
                }
            }
            if (!$contact->validate()) {
                foreach ($contact->getErrors() as $champ => $errors) {
                    foreach ($errors as $error) {
                        $line['errors'][] = $error;
                    }
                    $contact->$champ = null;
                }
            }
            
            if(!$contact->save()) {
                $line['errors'][] = 'Le consommateur n\'a pas été enregistré';
                $errorArray[] = $line;
                continue;
            } 
            
            //keep on only if $contact has been saved
            
            if($line[8] != '' || $line[8] != null) {
                $conso->conso_fonction = $line[8];
            }
            
            if($line[10] != '' || $line[10] != null) {
                $lieuContact = trim($line[10]);
                $lieuID = \app\models\Lieuprisecontact::find()->where(['like', 'lieu', $lieuContact])->one();
                if($lieuID) {
                    $conso->conso_lieu_contact = $lieuID->lieu_prise_contact_id;
                } else {
                    $newLieu = new \app\models\Lieuprisecontact();
                    $newLieu->lieu = $lieuContact;
                    if(!$newLieu->save()) {
                        $line['errors'][] = 'Erreur concernant la source du contact';
                    } else {
                        $lieuID = $newLieu->lieu_prise_contact_id;
                        $conso->conso_lieu_contact = $lieuID;
                    }
                }
            }
            
            if (!$conso->validate()) {
                foreach ($conso->getErrors() as $champ => $errors) {
                    foreach ($errors as $error) {
                        $line['errors'][] = $error;
                    }
                    $conso->$champ = null;
                }
            }
            $conso->cont_id = $contact->cont_id;
            if(!$conso->save()) {
                $line['errors'][] = 'Le consommateur n\'a pas été enregistré';
                $contact->delete();
                $errorArray[] = $line;
                continue;
            }
            
            //keep on only if $contact has been saved
            
            if( ($line[2] != '' || $line[2] != null) && ($line[3] != '' || $line[3] != null) && ($line[4] != '' || $line[4] != null) ) {
                if(count($existingConso) == 1 && $existingConso[0]->contact->adresse != null) {
                    $adresse = $existingConso[0]->contact->adresse;
                } else {
                    $adresse = new \app\models\Adresse();
                }
                if($line[2] != '' || $line[2] != null) {
                    $adresse->adr_rue = trim($line[2]);
                }
                if($line[3] != '' || $line[3] != null) {
                    $adresse->adr_code_postal = trim($line[3]);
                }
                if($line[4] != '' || $line[4] != null) {
                    $adresse->adr_ville = trim($line[4]);
                }
                
                if (!$adresse->validate()) {
                    foreach ($adresse->getErrors() as $champ => $errors) {
                        foreach ($errors as $error) {
                            $line['errors'][] = $error;
                        }
                        $adresse->$champ = null;
                    }
                }
                if (!$adresse->save()) {
                    $line['errors'][] = 'L\'adresse n\'a pas été enregistré';
                    $errorArray[] = $line;
                } else {
                    $contact->adr_id = $adresse->adr_id;
                    $contact->save();
                }
            }
            
            if(array_key_exists('errors', $line)) {
                $errorArray[] = $line;
            }
        }
        return $errorArray;
    }
}
