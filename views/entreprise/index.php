<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\EntrepriseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Entreprises-Assoc.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="entreprise-index">
    <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createEntreprise')) : ?>
            <?= Html::a('Ajouter une Entreprise', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        <?php endif; ?>
    </div>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    
    <?php $gridColumns =
            [
//                ['class' => 'yii\grid\SerialColumn'],
                [
                    'attribute' => 'entr_societe',
                    'label' => 'Société',
                ],
                [
                    'attribute' => 'entr_telephone',
                    'label' => 'Téléphone',
                ],
                [
                    'attribute' => 'entr_email',
                    'label' => 'E-mail',
                    'format' => 'email'
                ],
                [
                    'attribute' => 'entr_domaine_activite',
                    'label' => 'Domaine Activité',
                ],
                // 'adr_id',
                ['class' => 'kartik\grid\ActionColumn','template' => '{view}{update}{delete}','hiddenFromExport' => true ,
                    'buttons' => [
                        'delete' => function($url, $model) {
                            if (!\Yii::$app->user->can('deleteEntreprise')) {
                                return null;
                            }
                            $url = \Yii::$app->urlManager->createUrl(['/entreprise/delete', 'id' => $model->entr_id]);
                            return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['title' => Yii::t('yii', 'Delete'), 'data-confirm' => 'Êtes-vous sûr de vouloir supprimer cette entreprise?' . (!empty($model->contacts)? ' Attention : des contacts font partie de cette entreprise, ils seront détachés de celle ci':''), 'data-method' => 'post']);
                        }
                    ],
                ],
            ];?>
    <div class="x_panel">
    <div class="pull-right">
                <label>Exportation :</label>
                <?=
                ExportMenu::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'target' => ExportMenu::TARGET_POPUP,
//                    'stream' => false,
                    'exportConfig' => [
                        ExportMenu::FORMAT_HTML => false,
                        ExportMenu::FORMAT_TEXT => false,
                        ExportMenu::FORMAT_PDF => false,
                        ExportMenu::FORMAT_EXCEL => false,
//                        'encoding' => 'utf-8'
                    ],
                    'noExportColumns' => [count($gridColumns) - 1]
                ]);
                ?>
    </div>
    </div>
    <div class="x_panel">
        <?=
        GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'entrGrid',
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'filterModel' => $searchModel,
            'columns' => $gridColumns,
            'responsiveWrap' => false
        ]);
        ?>
    </div>
</div>
<?php
$responsiveCSS = \app\components\WhResponsive::writeResponsiveCss($gridColumns, 'entrGrid');
$this->registerCss($responsiveCSS);
$this->registerJsFile(\yii\helpers\Url::home() . 'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);

