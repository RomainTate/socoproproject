<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ContactdiversSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$search = Yii::$app->request->pathInfo == 'contactdivers/search'? true : false;
if(Yii::$app->request->post('export_type')) {
    $exporting = true;
} else {
    $exporting = false;
}
$this->title = 'Contacts Divers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactdivers-index">

    <div id="gridHeader">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <?php if (Yii::$app->user->can('createContactDivers')) : ?>
                <?= Html::a('Ajouter un Contact Divers', ['create'], ['class' => 'btn btn-success pull-right']) ?>
        <?php endif; ?>
    </div>
    
    <?php if($search) : ?>
        <div class="x_panel">
            <?php echo $this->render('_search', ['model' => $searchModel]); ?>
        </div>
    <?php endif;?>
    
    <?php if (!$search || Yii::$app->request->get('ContactdiversSearch') != null) : 
    $gridColumns = [
            [
                'attribute' => 'contact.cont_civilite',
                'label' => 'Civ',
                'format' => 'text',
            ],
//            'cont_id',
        [
                'attribute' => 'contactNom',
                'label' => 'Nom',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactPrenom',
                'label' => 'Prénom',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactTelephonePrincipal',
                'label' => 'Téléphone',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactGSMPrincipal',
                'label' => 'GSM',
                'format' => 'text',
            ],
         [
                'attribute' => 'contactEmailPrincipal',
                'label' => 'E-mail',
                'format' => 'html',
                 'value' => function ($model) use($exporting) {
                    if($exporting) return $model->contactEmailPrincipal; 
                    return Html::a(yii\helpers\BaseStringHelper::truncate($model->contactEmailPrincipal, 40, '...'), 'mailto:'.$model->contactEmailPrincipal);
                },
            ],
        [
                'attribute' => 'rue',
                'label' => 'Rue',
                'format' => 'text',
            ],
            [
                'attribute' => 'codepostal',
                'label' => 'Code postal',
                'header' => 'C.P.',
                'format' => 'text',
            ],
            [
                'attribute' => 'localite',
                'label' => 'Localité',
                'format' => 'text',
            ],
            [
                'attribute' => 'province',
                'label' => 'Province',
                'format' => 'text',
            ],
//            'divers_fonction',
            [
            'label' => 'AS / AG',    
            'attribute' => 'divers_AS_AG',
                'headerOptions' => ['style' => 'min=width:30px'],
            'filter' => ['1' => 'AS' , '2' => 'AG', '3' => 'AS/AG'],
            'value' => function($model) {
                switch ($model->divers_AS_AG) :
                                case 1: return 'AS';
                                    break;
                                case 2: return 'AG';
                                    break;
                                case 3: return 'AS / AG';
                                    break;
                                default : return 'non';
                                    break;
                            endswitch;
            }
            ],
            [
                'label' => 'Intérêts',
                'attribute' => 'secteursID',
                'format' => 'html',
                'filter' => yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->asArray()->all(), 'sect_id', 'sect_nom'),
                'value' => function($model) {
                    $secteurNames = array();
                    $secteursArray = \yii\helpers\ArrayHelper::map($model->secteurs, 'sect_id', 'sect_nom');
                    asort($secteursArray);
                    foreach ($secteursArray as $secteur) {
                        $secteurNames[] = '<span style="display:block;">' . $secteur . '</span>';
                    }
                    return implode(", ", $secteurNames);
                }
            ],
            [
                'attribute' => 'membrecollege',
                'label' => 'Membre College',
                'filter' => ['1' => 'Oui' , '0' => 'Non'],
//                'hidden' => true,
                'value' => function ($model) {
                if ($model->contact->membrecollege) {
                    return 'Oui';
                }
                return 'Non';
                }
            ],
            [
                'attribute' => 'membrefiliere',
                'label' => 'Membre Filière',
                'filter' => yii\helpers\ArrayHelper::map(\app\models\Secteur::find()->orderBy('sect_nom')->asArray()->all(), 'sect_id', 'sect_nom'),
                'format' => 'html',
                'value' => function ($model) {
                if  (!$model->contact->secteursMembrecommission) 
                    return 'Non';
                foreach ($model->contact->secteursMembrecommission as $filiere) {
                        $filieresNames[] = '<span style="display:block;"> -' . $filiere->sect_nom .'</span>';
                    }
                    return implode(", ", $filieresNames);
                }
            ],
            [
                'attribute' => 'nomentr',
                'label' => 'Entreprises',
                'format' => 'html',
                'value' => function($model) {
                    if(!$model->contact->entreprises) {
                        return null;
                    }
                    foreach ($model->contact->entreprises as $entreprise) {
                        
//                        $fonction = app\models\Appartenir::find()->where(['entr_id' => $entreprise->entr_id, 'cont_id' => $model->cont_id])->one()->fonction;
//                        if (!$fonction) $fonction = 'inconnue';
//                        $entreprisesNames[] = '<span style="display:block;"> -' . $entreprise->entr_societe . ' ('. $fonction .')</span>';
                        $entreprisesNames[] = '<span style="display:block;"> -' . $entreprise->entr_societe .'</span>';
                    }
                    return implode("<hr />", $entreprisesNames);
                }
            ],

            ['class' => 'kartik\grid\ActionColumn', 'hiddenFromExport' => true ,
                    'buttons' => [
                        'view' => function($url, $model) {
                            $url = \Yii::$app->urlManager->createUrl(['contactdivers/view', 'id' => $model->cont_id]);
                            return Html::a('<span class="glyphicon glyphicon-eye-open text-info"></span>', $url, ['class' => 'action','title' => Yii::t('yii', 'View')]);
                        },
                        'update' => function($url, $model) {
                            if(!\Yii::$app->user->can('createContactDivers')) {
                                return null;
                            }
                            $url = \Yii::$app->urlManager->createUrl(['contactdivers/update', 'id' => $model->cont_id]);
                            return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, ['class' => 'action','title' => Yii::t('yii', 'Update')]);
                        },
                        'delete' => function($url, $model) {
                            if(!\Yii::$app->user->can('deleteContactDivers')) {
                                return null;
                            }
                            $url = \Yii::$app->urlManager->createUrl(['contactdivers/delete', 'id' => $model->cont_id]);
                            return Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', $url, ['class' => 'action','title' => Yii::t('yii', 'Delete'), 'data-confirm' =>  'Etes vous sûr de vouloir supprimer ce contact?', 'data-method' => 'post']);
                        }
                    ],
            ],
        ] ?>
    
    <hr />
    <div class="x_panel">
        <div class="pull-right">
            Exportation :
        <?= ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'exportConfig' => [
                ExportMenu::FORMAT_HTML => false,
                ExportMenu::FORMAT_TEXT => false,
                ExportMenu::FORMAT_PDF => false,
                ExportMenu::FORMAT_EXCEL => false
            ],
            'noExportColumns' => [ count($gridColumns) - 1]
        ]);?>
        </div>
    </div>
    
    <div class="x_panel">
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'diversGrid',
            'filterModel' => $searchModel,
            'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '-'],
            'columns' => $gridColumns,
            'responsiveWrap' => false
        ]); ?>
    </div>
        <?php 
        $responsiveCSS = \app\components\WhResponsive::writeResponsiveCss($gridColumns, 'diversGrid');
        $this->registerCss($responsiveCSS);
        $this->registerJsFile(\yii\helpers\Url::home().'js/adaptGridJS.js', ['depends' => [yii\web\JqueryAsset::className()]]);
     endif; ?>
</div>

