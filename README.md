Socopro Project
============================

Socopro Project is a project created with Yii 2 framework [Yii 2](http://www.yiiframework.com/) 


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            -not commited- contains files generated during runtime
      tests/              -not commited- contains various tests for the basic application
      vendor/             -not commited- contains dependent 3rd-party packages 
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



INSTALLATION
------------

add depedency via Composer


~~~
main function located @ : 
http://localhost/basic/web/index.php
~~~


Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

TRANSLATION
-------------
French translations for some modules are saved in the vendor folder
add files after 'composer update' or 'composer install'